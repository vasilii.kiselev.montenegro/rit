export const CalendarIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8 2.67554V5.67554"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16 2.67554V5.67554"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.5 9.76562H20.5"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M21 9.17554V17.6755C21 20.6755 19.5 22.6755 16 22.6755H8C4.5 22.6755 3 20.6755 3 17.6755V9.17554C3 6.17554 4.5 4.17554 8 4.17554H16C19.5 4.17554 21 6.17554 21 9.17554Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.6947 14.3755H15.7037"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.6947 17.3755H15.7037"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.9955 14.3755H12.0045"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.9955 17.3755H12.0045"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.29431 14.3755H8.30329"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.29395 17.3755H8.30293"
        stroke="#2DAD96"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
