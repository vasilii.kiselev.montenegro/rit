export const ProgressIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.5 10.2617L12.3 14.4617L10.7 12.0617L7.5 15.2617"
        stroke="#14C8DF"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.5 10.2617H16.5V12.2617"
        stroke="#14C8DF"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9 22.7617H15C20 22.7617 22 20.7617 22 15.7617V9.76172C22 4.76172 20 2.76172 15 2.76172H9C4 2.76172 2 4.76172 2 9.76172V15.7617C2 20.7617 4 22.7617 9 22.7617Z"
        stroke="#14C8DF"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
