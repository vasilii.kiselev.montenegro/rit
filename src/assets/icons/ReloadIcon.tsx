export const ReloadIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M2.59297 13.7013V17.868V13.7013Z" fill="#7F879E" />
      <path
        d="M18.3346 10.0013C18.3346 14.6013 14.6013 18.3346 10.0013 18.3346C5.4013 18.3346 2.59297 13.7013 2.59297 13.7013M2.59297 13.7013H6.35964M2.59297 13.7013V17.868M1.66797 10.0013C1.66797 5.4013 5.36797 1.66797 10.0013 1.66797C15.5596 1.66797 18.3346 6.3013 18.3346 6.3013M18.3346 6.3013V2.13464M18.3346 6.3013H14.6346"
        stroke="#7F879E"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
