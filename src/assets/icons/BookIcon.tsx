export const BookIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.5 18.6755V7.67554C3.5 3.67554 4.5 2.67554 8.5 2.67554H15.5C19.5 2.67554 20.5 3.67554 20.5 7.67554V17.6755C20.5 17.8155 20.5 17.9555 20.49 18.0955"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.35 15.6755H20.5V19.1755C20.5 21.1055 18.93 22.6755 17 22.6755H7C5.07 22.6755 3.5 21.1055 3.5 19.1755V18.5255C3.5 16.9555 4.78 15.6755 6.35 15.6755Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8 7.67554H16"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8 11.1755H13"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
