export const EditIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="17"
      height="16"
      viewBox="0 0 17 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.33958 2.4L3.86624 8.19333C3.65958 8.41333 3.45958 8.84667 3.41958 9.14667L3.17291 11.3067C3.08624 12.0867 3.64624 12.62 4.41958 12.4867L6.56624 12.12C6.86624 12.0667 7.28624 11.8467 7.49291 11.62L12.9662 5.82667C13.9129 4.82667 14.3396 3.68667 12.8662 2.29334C11.3996 0.913336 10.2862 1.4 9.33958 2.4Z"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.42676 3.36667C8.71342 5.20667 10.2068 6.61333 12.0601 6.8"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.5 14.6667H14.5"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
