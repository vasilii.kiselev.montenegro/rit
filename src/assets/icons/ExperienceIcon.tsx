export const ExperienceIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="18"
      height="19"
      viewBox="0 0 18 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9 9.02734C11.0711 9.02734 12.75 7.34841 12.75 5.27734C12.75 3.20628 11.0711 1.52734 9 1.52734C6.92893 1.52734 5.25 3.20628 5.25 5.27734C5.25 7.34841 6.92893 9.02734 9 9.02734Z"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.55664 16.5273C2.55664 13.6248 5.44414 11.2773 8.99914 11.2773C9.71914 11.2773 10.4166 11.3748 11.0691 11.5548"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.5 13.5273C16.5 14.0898 16.3425 14.6223 16.065 15.0723C15.9075 15.3423 15.705 15.5823 15.4725 15.7773C14.9475 16.2498 14.2575 16.5273 13.5 16.5273C12.405 16.5273 11.4525 15.9423 10.935 15.0723C10.6575 14.6223 10.5 14.0898 10.5 13.5273C10.5 12.5823 10.935 11.7348 11.625 11.1873C12.1425 10.7748 12.795 10.5273 13.5 10.5273C15.1575 10.5273 16.5 11.8698 16.5 13.5273Z"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.3301 13.527L13.0726 14.2695L14.6701 12.792"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
