export const EnterProfileIcon = ({ style = {} }) => {
  return (
    <svg style={style} className="icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M8.89844 7.55828C9.20844 3.95828 11.0584 2.48828 15.1084 2.48828H15.2384C19.7084 2.48828 21.4984 4.27828 21.4984 8.74828V15.2683C21.4984 19.7383 19.7084 21.5283 15.2384 21.5283H15.1084C11.0884 21.5283 9.23844 20.0783 8.90844 16.5383"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <g opacity="0.4">
        <path d="M2 12H14.88" stroke="#2DAD96" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        <path
          d="M12.6484 8.64844L15.9984 11.9984L12.6484 15.3484"
          stroke="#2DAD96"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  )
}
