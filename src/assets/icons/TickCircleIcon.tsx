export const TickCircleIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12 22.0106C17.5 22.0106 22 17.5106 22 12.0106C22 6.51062 17.5 2.01062 12 2.01062C6.5 2.01062 2 6.51062 2 12.0106C2 17.5106 6.5 22.0106 12 22.0106Z"
        stroke="#14C8DF"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.75 12.0106L10.58 14.8406L16.25 9.18063"
        stroke="#14C8DF"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
