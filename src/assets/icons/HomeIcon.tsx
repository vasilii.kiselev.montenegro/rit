export const HomeIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12 18.6757V15.6757"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.0693 3.49563L3.13929 9.04563C2.35929 9.66563 1.85929 10.9756 2.02929 11.9556L3.35929 19.9156C3.59929 21.3356 4.95929 22.4856 6.39929 22.4856H17.5993C19.0293 22.4856 20.3993 21.3256 20.6393 19.9156L21.9693 11.9556C22.1293 10.9756 21.6293 9.66563 20.8593 9.04563L13.9293 3.50563C12.8593 2.64563 11.1293 2.64563 10.0693 3.49563Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
