export const BriefcaseIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="19"
      height="19"
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M6.5006 16.5273H12.5006C15.5156 16.5273 16.0556 15.3198 16.2131 13.8498L16.7756 7.84984C16.9781 6.01984 16.4531 4.52734 13.2506 4.52734H5.7506C2.5481 4.52734 2.0231 6.01984 2.2256 7.84984L2.7881 13.8498C2.9456 15.3198 3.4856 16.5273 6.5006 16.5273Z"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.5 4.52734V3.92734C6.5 2.59984 6.5 1.52734 8.9 1.52734H10.1C12.5 1.52734 12.5 2.59984 12.5 3.92734V4.52734"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11 9.77734V10.5273C11 10.5348 11 10.5348 11 10.5423C11 11.3598 10.9925 12.0273 9.5 12.0273C8.015 12.0273 8 11.3673 8 10.5498V9.77734C8 9.02734 8 9.02734 8.75 9.02734H10.25C11 9.02734 11 9.02734 11 9.77734Z"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.7375 8.27734C15.005 9.53734 13.025 10.2873 11 10.5423"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.46484 8.47949C4.15234 9.63449 6.05734 10.332 7.99984 10.5495"
        stroke="#7F879E"
        strokeWidth="1.2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
