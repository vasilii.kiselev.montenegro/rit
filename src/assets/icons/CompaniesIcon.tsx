export const CompaniesIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.99023 12.6756V22.6756"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinejoin="round"
      />
      <path
        d="M11.9902 12.6756V22.6756"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinejoin="round"
      />
      <path
        d="M15.9902 12.6756V22.6756"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinejoin="round"
      />
      <path
        d="M2 22.6756H22"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 2.67563C13.6 3.31563 15.4 3.31563 17 2.67563V5.67563C15.4 6.31563 13.6 6.31563 12 5.67563V2.67563Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 5.67563V8.67563"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17 8.67563H7C5 8.67563 4 9.67563 4 11.6756V22.6756H20V11.6756C20 9.67563 19 8.67563 17 8.67563Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M4.58008 12.6756H19.4201"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
