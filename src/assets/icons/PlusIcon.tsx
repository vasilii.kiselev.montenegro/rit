export const PlusIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="20"
      height="21"
      viewBox="0 0 20 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.99984 18.8333C14.5832 18.8333 18.3332 15.0833 18.3332 10.5C18.3332 5.91666 14.5832 2.16666 9.99984 2.16666C5.4165 2.16666 1.6665 5.91666 1.6665 10.5C1.6665 15.0833 5.4165 18.8333 9.99984 18.8333Z"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.6665 10.5H13.3332"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10 13.8333V7.16666"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
