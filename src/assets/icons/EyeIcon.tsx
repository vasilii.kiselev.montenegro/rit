export const EyeIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="17"
      height="16"
      viewBox="0 0 17 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.8866 7.99999C10.8866 9.31999 9.81995 10.3867 8.49995 10.3867C7.17995 10.3867 6.11328 9.31999 6.11328 7.99999C6.11328 6.67999 7.17995 5.61333 8.49995 5.61333C9.81995 5.61333 10.8866 6.67999 10.8866 7.99999Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.4999 13.5133C10.8532 13.5133 13.0466 12.1267 14.5732 9.72666C15.1732 8.78666 15.1732 7.20666 14.5732 6.26666C13.0466 3.86666 10.8532 2.48 8.4999 2.48C6.14656 2.48 3.95323 3.86666 2.42656 6.26666C1.82656 7.20666 1.82656 8.78666 2.42656 9.72666C3.95323 12.1267 6.14656 13.5133 8.4999 13.5133Z"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
