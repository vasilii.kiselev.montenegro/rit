export const SidebarIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M18.3073 12.5003V7.50033C18.3073 3.33366 16.6406 1.66699 12.474 1.66699H7.47396C3.30729 1.66699 1.64062 3.33366 1.64062 7.50033V12.5003C1.64062 16.667 3.30729 18.3337 7.47396 18.3337H12.474C16.6406 18.3337 18.3073 16.667 18.3073 12.5003Z"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.64062 1.66699V18.3337"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.4732 7.86621L10.3398 9.99954L12.4732 12.1329"
        stroke="#2DAD96"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
