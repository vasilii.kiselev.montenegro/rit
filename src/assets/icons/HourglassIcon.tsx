export const HourglassIcon = ({ style = {} }) => {
  return (
    <svg
      style={style}
      className="icon"
      width="16"
      height="22"
      viewBox="0 0 16 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M11.241 1H4.76101C1.00101 1 0.711014 4.38 2.74101 6.22L13.261 15.78C15.291 17.62 15.001 21 11.241 21H4.76101C1.00101 21 0.711014 17.62 2.74101 15.78L13.261 6.22C15.291 4.38 15.001 1 11.241 1Z"
        stroke="#151B33"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
