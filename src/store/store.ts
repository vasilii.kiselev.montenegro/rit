import { configureStore } from '@reduxjs/toolkit'
import vacanciesSlice from './slices/vacanciesSlice/vacanciesSlice'
import companiesSlice from './slices/companiesSlice/companiesSlice'
import userSlice from './slices/userSlice/userSlice'
import timeSlotsSlice from './slices/timeSlotsSlice/timeSlotsSlice'
import createVacancySlice from './slices/createVacancySlice/createVacancySlice'
import calendarSlice from './slices/calendarSlice/calendarSlice'

const store = configureStore({
  reducer: {
    user: userSlice,
    vacancies: vacanciesSlice,
    companies: companiesSlice,
    timeSlots: timeSlotsSlice,
    createVacancy: createVacancySlice,
    calendar: calendarSlice,
  },
})
export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
