import { getCompanies } from '@/api/companies'
import { FullVacancy } from '@/types/vacancy'

export const changeApiValues = async (ApiValues): Promise<FullVacancy[]> => {
  const companies = await getCompanies()
  const newArr = Array.isArray(ApiValues) ? ApiValues : [ApiValues]
  const getCompanyValues = (
    id: number
  ): { name: string; logo: string; id: number } => {
    for (const obj of companies) {
      if (obj.id === id) {
        return { name: obj.name, logo: obj.logo, id: obj.id }
      }
    }
  }
  return newArr.map((el): FullVacancy => {
    const company = getCompanyValues(el.company)
    return {
      id: el.id,
      imgCompany: company.logo,
      company: company.name,
      companyId: company.id,
      name: el.name,
      views: el.views,
      passedTest: el.test_passed,
      registration: el.registered,
      status: el.status ? el.status : 'active',
      currency: el.currency,
      link: el.link,
      description: el.description,
      jobRequirements: el.requirements,
      workSchedule: el.schedule,
      type: el.type,
      wages: el.salary,
      workExperience: el.experience,
      placeOfWork: el.place_of_work,
      testingMethod: el.testing_method,
    }
  })
}
