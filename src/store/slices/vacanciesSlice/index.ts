export {
  sortingWords,
  sortingNumbers,
  reverseState,
  openVacancies,
  closedVacancies,
  resetState,
} from './vacanciesSlice'
export { getVacanciesThunk } from './asyncThunks/getVacanciesThunk'
