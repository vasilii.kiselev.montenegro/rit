import { getVacancies } from '@/api/vacancies'
import { Vacancy } from '@/types/vacancy'
import { createAsyncThunk } from '@reduxjs/toolkit'
import { changeApiValues } from '../utils/changeApiValues'

export const getVacanciesThunk = createAsyncThunk<Vacancy[]>(
  'vacancies/getVacancies',
  async () => {
    const response = await getVacancies()
    const changeValues = await changeApiValues(response)
    return changeValues
  }
)
