import { Vacancy } from '@/types/vacancy'
import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { getVacanciesThunk } from './asyncThunks/getVacanciesThunk'

type VacancyThunk = {
  zeroState: Vacancy[]
  workState: Vacancy[]
  status: string
}
type InitialState = VacancyThunk | null[]
const initialState: InitialState = {
  zeroState: [],
  workState: [],
  status: 'empty',
}

const vacanciesSlice = createSlice({
  name: 'vacancies',
  initialState: initialState,
  reducers: {
    sortingWords: (state, { payload }: PayloadAction<string>) => {
      const sortState = state.workState.sort(function (a, b) {
        //@ts-expect-error
        return ('' + a[payload]).localeCompare(b[payload])
      })
      state.workState = [...sortState]
    },
    sortingNumbers: (state, { payload }: PayloadAction<string>) => {
      const sortState = state.workState.sort(function (a, b) {
        //@ts-expect-error
        return b[payload] - a[payload]
      })

      state.workState = [...sortState]
    },
    reverseState: (state) => {
      const sortState = state.workState.reverse()

      state.workState = [...sortState]
    },
    openVacancies: (state) => {
      const sortState = state.zeroState.filter((el) => el.status !== 'closed')
      state.workState = [...sortState]
    },
    closedVacancies: (state) => {
      const sortState = state.zeroState.filter((el) => el.status === 'closed')
      state.workState = [...sortState]
    },
    resetState: (state) => {
      state.workState = [...state.zeroState]
    },
  },
  extraReducers: (builder) => {
    // builder.addCase(getVacanciesThunk.pending, () => {
    //   return {
    //     zeroState: [],
    //     workState: [],
    //     status: 'loading',
    //   }
    // })
    builder.addCase(getVacanciesThunk.fulfilled, (_, action) => {
      return {
        zeroState: action.payload,
        workState: action.payload,
        status: 'succes',
      }
    })
    builder.addCase(getVacanciesThunk.rejected, () => {
      return {
        zeroState: [],
        workState: [],
        status: 'err',
      }
    })
  },
})
export const { sortingWords, sortingNumbers, reverseState, openVacancies, closedVacancies, resetState } = vacanciesSlice.actions
export default vacanciesSlice.reducer
