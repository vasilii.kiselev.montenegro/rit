import { ApiCompany, FullCompany } from '@/types/company'

export const changeApiValues = (ApiValues: ApiCompany[]): FullCompany[] => {
  return ApiValues.map((el): FullCompany => {
    return {
      id: el.id,
      name: el.name,
      inn: el.inn,
      logo: el.logo,
      description: el.description,
      domainAddress: el.site,
      location: el.place,
      employeeCount: el.employee_count,
      approvalStatus: el.verificated,
      foundedAt: el.founded_at,
      numberOfVacancies: el.vacansies_count,
      interviewsAreScheduled: el.meetings_count,
    }
  })
}
export const changeApiValuesForOneItem = (
  ApiValues: ApiCompany
): FullCompany => {
  return {
    id: ApiValues.id,
    name: ApiValues.name,
    inn: ApiValues.inn,
    logo: ApiValues.logo,
    description: ApiValues.description,
    domainAddress: ApiValues.site,
    location: ApiValues.place,
    employeeCount: ApiValues.employee_count,
    approvalStatus: ApiValues.verificated,
    foundedAt: ApiValues.founded_at,
    numberOfVacancies: ApiValues.vacansies_count,
    interviewsAreScheduled: ApiValues.meetings_count,
  }
}
