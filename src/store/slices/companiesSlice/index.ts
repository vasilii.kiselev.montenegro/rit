export { getCompaniesThunk } from './asyncThunks/getCompaniesThunk'
export {
  companySortingWords,
  companyReverseState,
  companySortingNumbers,
} from './companiesSlice'
