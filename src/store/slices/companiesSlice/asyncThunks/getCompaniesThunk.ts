import { getCompanies } from '@/api/companies/companies'
import { FullCompany } from '@/types/company'
import { createAsyncThunk } from '@reduxjs/toolkit'
import { changeApiValues } from '../utils/changeApiValues'

export const getCompaniesThunk = createAsyncThunk<FullCompany[]>('companies/getCompanies', async () => {
  const response = await getCompanies()
  const changeValues = await changeApiValues(response)
  return changeValues
})
