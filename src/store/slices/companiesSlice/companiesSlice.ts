import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { getCompaniesThunk } from './asyncThunks/getCompaniesThunk'
import { FullCompany } from '@/types/company'

type CompanyThunk = {
  zeroState: FullCompany[]
  workState: FullCompany[]
  status: string
}
type InitialState = CompanyThunk | null[]
const initialState: InitialState = {
  zeroState: [],
  workState: [],
  status: 'empty',
}

const companiesSlice = createSlice({
  name: 'companies',
  initialState: initialState,
  reducers: {
    companySortingWords: (state) => {
      const sortState = state.workState.sort(function (a: FullCompany, b: FullCompany) {
        return ('' + a.name).localeCompare(b.name)
      })
      state.workState = [...sortState]
    },
    companySortingNumbers: (state, { payload }: PayloadAction<'interviewsAreScheduled' | 'numberOfVacancies'>) => {
      console.log(payload)
      const sortState = state.workState.sort(function (a, b) {
        return b[payload] - a[payload]
      })

      state.workState = [...sortState]
    },
    companyReverseState: (state) => {
      const sortState = state.workState.reverse()

      state.workState = [...sortState]
    },
    addCompanyToSlice: (state, { payload }) => {
      const newState = [payload, ...state.workState]

      state.workState = [...newState]
    },
  },
  extraReducers: (builder) => {
    // builder.addCase(getCompaniesThunk.pending, () => {
    //   return {
    //     zeroState: [],
    //     workState: [],
    //     status: 'loading',
    //   }
    // })
    builder.addCase(getCompaniesThunk.fulfilled, (sta, action) => {
      return {
        zeroState: action.payload,
        workState: action.payload,
        status: 'succes',
      }
    })
    builder.addCase(getCompaniesThunk.rejected, () => {
      return {
        zeroState: [],
        workState: [],
        status: 'err',
      }
    })
  },
})
export const { companySortingWords, companyReverseState, companySortingNumbers, addCompanyToSlice } = companiesSlice.actions
export default companiesSlice.reducer
