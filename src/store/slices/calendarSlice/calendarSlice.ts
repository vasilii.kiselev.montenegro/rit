import { CALENDAR_VIEW } from '@/pages/NewCalendarPage/consts'
import { CalendarViewItem } from '@/types/calendar'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import dayjs from 'dayjs'

interface Calendar {
  currentDate: string
  sidebarIsOpen: boolean
  showSlots: boolean
  viewType: CalendarViewItem
  sidebarEvents: {
    slots: boolean
    slotsEvents: boolean
    slotsLock: boolean
  }
}
const initState: Calendar = {
  viewType: CALENDAR_VIEW[1],
  currentDate: dayjs().format('YYYY-MM-DD'),
  showSlots: true,
  sidebarIsOpen: false,
  sidebarEvents: {
    slots: true,
    slotsEvents: true,
    slotsLock: true,
  },
}

const calendarSlice = createSlice({
  name: 'calendar',
  initialState: initState,
  reducers: {
    setCurrentDate: (state, { payload }: PayloadAction<string>) => {
      state.currentDate = payload
    },
    setViewSlots: (state) => {
      state.showSlots = !state.showSlots
    },
    setViewType: (state, { payload }: PayloadAction<CalendarViewItem>) => {
      state.viewType = payload
    },
    setSidebarState: (state) => {
      state.sidebarIsOpen = !state.sidebarIsOpen
    },
    setSidebarEventsSlotsState: (state) => {
      state.sidebarEvents.slots = !state.sidebarEvents.slots
    },
    setSidebarEventsSlotsEventsState: (state) => {
      state.sidebarEvents.slotsEvents = !state.sidebarEvents.slotsEvents
    },
    setSidebarEventsSlotsLockState: (state) => {
      state.sidebarEvents.slotsLock = !state.sidebarEvents.slotsLock
    },
  },
})
export const {
  setCurrentDate,
  setViewSlots,
  setViewType,
  setSidebarState,
  setSidebarEventsSlotsState,
  setSidebarEventsSlotsEventsState,
  setSidebarEventsSlotsLockState,
} = calendarSlice.actions
export default calendarSlice.reducer
