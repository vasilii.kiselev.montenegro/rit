import { getSlots } from '@/api/calendar'
import { Slot } from '@/types/calendar'
import { createAsyncThunk } from '@reduxjs/toolkit'

export const getSlotsEventsThunk = createAsyncThunk<Slot[]>(
  'calendar/getSlotsEvents',
  async () => {
    const response = await getSlots()
    return response
  }
)
