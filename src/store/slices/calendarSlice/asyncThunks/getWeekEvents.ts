import { getWeekEvents } from '@/api/calendar'
import { Slot } from '@/types/calendar'
import { createAsyncThunk } from '@reduxjs/toolkit'

export const getWeekEventsThunk = createAsyncThunk<Slot[]>(
  'calendar/getWeekSEvents',
  async () => {
    const response = await getWeekEvents()

    return response
  }
)
