import { getUser } from '@/api/profile/profile'
import { Company } from '@/types/company'
import { createAsyncThunk } from '@reduxjs/toolkit'

export const getUserThunk = createAsyncThunk<Company[]>(
  'user/getUser',
  async () => {
    const response = await getUser()
    return response
  }
)
