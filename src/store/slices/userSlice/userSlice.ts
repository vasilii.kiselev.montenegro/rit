import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { User, UserForm, UserStatus } from '@/types/user'
import { removeTockens, setTockens, TokensType } from './utils'

type UserThunk = {
  user: User
  status: UserStatus
}
type InitialState = UserThunk | null[]
const initialState: InitialState = {
  user: {
    email: '',
    username: '',
    company: '',
    img: '',
    firstName: '',
    lastName: '',
    companyVerificated: false,
    vacanciesCount: null,
  },
  status: UserStatus.LOAD,
}

const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    logIn: (state, { payload }) => {
      if (payload) {
        state.user = payload
        state.status = UserStatus.LOGIN
      }
    },
    setNewFormValues: (state, { payload }: PayloadAction<UserForm>) => {
      const data = Object.entries(payload)
      data.forEach(([key, value]: [string, number]) => {
        //@ts-expect-error
        state.user[key] = value
      })
    },
    setStatusLogIn: (state) => {
      state.status = UserStatus.LOGIN
      localStorage.setItem('user', state.status)
    },
    setTockensAndStatus: (state, { payload }: PayloadAction<TokensType>) => {
      setTockens(payload)
      state.status = UserStatus.LOGIN
      localStorage.setItem('user', state.status)
    },
    logOut: (state) => {
      state.status = UserStatus.UNSET
      localStorage.setItem('user', state.status)
      removeTockens()
    },
  },
})
export const { logIn, logOut, setStatusLogIn, setNewFormValues, setTockensAndStatus } = userSlice.actions
export default userSlice.reducer
