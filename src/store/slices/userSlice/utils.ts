import { Tokens } from '@/types/user'

export type TokensType = { refresh: string; access: string }

export const setTockens = (tokens: TokensType) => {
  localStorage.setItem(Tokens.REFRESH_TOKEN, tokens.refresh)
  localStorage.setItem(Tokens.ACCESS_TOKEN, tokens.access)
}
export const removeTockens = () => {
  localStorage.removeItem(Tokens.REFRESH_TOKEN)
  localStorage.removeItem(Tokens.ACCESS_TOKEN)
}
