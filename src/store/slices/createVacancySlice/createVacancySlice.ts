import { FullVacancy, VacancyCreate } from '@/types/vacancy'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { setFullCompanyThunk } from './asyncThunks/setFullCompanyThunk'

const initialState: FullVacancy = {
  company: null,
  companyId: null,
  currency: null,
  description: null,
  id: null,
  imgCompany: null,
  jobRequirements: null,
  link: null,
  name: null,
  passedTest: null,
  placeOfWork: null,
  registration: null,
  status: null,
  testingMethod: null,
  type: null,
  views: null,
  wages: null,
  workExperience: null,
  workSchedule: null,
  selectionChoices: null,
  timeForRepeat: null,
  interviewType: null,
  interviewDuration: null,
  descriptionInvitation: null,
}

const createVacancySlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    cleanState: () => {
      return initialState
    },
    setId: (state, { payload }: PayloadAction<number>) => {
      state.id = payload
    },
    setCompanyId: (state, { payload }: PayloadAction<number>) => {
      state.companyId = payload
    },
    setValuesForSecondStep: (state, { payload }: PayloadAction<VacancyCreate>) => {
      return {
        ...state,
        ...payload,
      }
    },
    setTestingSettings: (state, { payload }: PayloadAction<{ timeForRepeat: string; selectionChoices: string }>) => {
      state.timeForRepeat = payload.timeForRepeat
      state.selectionChoices = payload.selectionChoices
    },
    setTestingMethod: (state, { payload }: PayloadAction<string>) => {
      state.testingMethod = payload
    },
    setSlotValuesForVacancy: (
      state,
      {
        payload,
      }: PayloadAction<{
        interviewType: string
        descriptionInvitation: string
        interviewDuration: number
      }>
    ) => {
      state.interviewType = payload.interviewType
      state.descriptionInvitation = payload.descriptionInvitation
      state.interviewDuration = payload.interviewDuration
    },
  },
  extraReducers: (builder) => {
    builder.addCase(setFullCompanyThunk.pending, () => {})
    builder.addCase(setFullCompanyThunk.fulfilled, (_, { payload }: PayloadAction<FullVacancy>) => {
      return payload
    })
    builder.addCase(setFullCompanyThunk.rejected, () => {})
  },
})
export const { cleanState, setId, setCompanyId, setValuesForSecondStep, setTestingSettings, setTestingMethod, setSlotValuesForVacancy } =
  createVacancySlice.actions
export default createVacancySlice.reducer
