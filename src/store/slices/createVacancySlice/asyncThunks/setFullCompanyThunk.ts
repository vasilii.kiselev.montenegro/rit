import { getVacany } from '@/api/vacancies/vacancies'
import { FullVacancy } from '@/types/vacancy'

import { createAsyncThunk } from '@reduxjs/toolkit'

export const setFullCompanyThunk = createAsyncThunk<FullVacancy, number>(
  'vacancyCreate/setFullCompany',
  async (id) => {
    const response = await getVacany(id)

    return response
  }
)
