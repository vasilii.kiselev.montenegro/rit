import { getWeekEvents } from '@/api/calendar'
import { TimeEvent } from '@/types/calendar'
import { createAsyncThunk } from '@reduxjs/toolkit'
interface FetchSlotsArgs {
  startDate: string
  endDate: string
}
export const getWeekEventsThunk = createAsyncThunk<TimeEvent[], FetchSlotsArgs>(
  'calendar/getWeekSEvents',
  async ({ startDate, endDate }) => {
    const response = await getWeekEvents(startDate, endDate)
    return response
  }
)
