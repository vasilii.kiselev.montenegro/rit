import { getSlots } from '@/api/calendar'
import { GetSlotsAndFreeSlots } from '@/types/calendar'
import { createAsyncThunk } from '@reduxjs/toolkit'

export const getSlotsEventsThunk = createAsyncThunk<GetSlotsAndFreeSlots>('calendar/getSlotsEvents', async () => {
  const response = await getSlots()
  return response
})
