import { getSlotsForToolbar } from '@/api/calendar/calendar'
import { TimeSlotWithEvents } from '@/types/calendar'
import { createAsyncThunk } from '@reduxjs/toolkit'
interface FetchSlotsArgs {
  startDate: string
  endDate: string
}
export const getSlotsEventsForToolbarThunk = createAsyncThunk<TimeSlotWithEvents[], FetchSlotsArgs>(
  'calendar/getSlotsEventsForToolbar',
  async ({ startDate, endDate }) => {
    const response = await getSlotsForToolbar(startDate, endDate)
    return response
  }
)
