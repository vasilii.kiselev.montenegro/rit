import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { getSlotsEventsThunk } from './asyncThunks/getSlotsEventsThunk'
import { getWeekEventsThunk } from './asyncThunks/getWeekEvents'
import { GetFreeTimeSlot, GetSlotsAndFreeSlots, TimeEvent, TimeSlotWithEvents } from '@/types/calendar'
import { getSlotsEventsForToolbarThunk } from './asyncThunks/getSlotsEventsForToolbarThunk'

export enum Status {
  PENDING = 'pending',
  FULFILLED = 'fulfilled',
}

type InitialState = {
  slotsEvents: TimeSlotWithEvents[]
  toolbarSlots: TimeSlotWithEvents[]
  weekEvents: TimeEvent[]
  freeTimeslots: GetFreeTimeSlot[]
  status: Status
}

const initialState: InitialState = {
  slotsEvents: [],
  weekEvents: [],
  toolbarSlots: [],
  freeTimeslots: [],
  status: Status.PENDING,
}

const timeSlotsSlice = createSlice({
  name: 'timeSlots',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getSlotsEventsThunk.fulfilled, (state, { payload }: PayloadAction<GetSlotsAndFreeSlots>) => {
      state.slotsEvents = payload.timeslots
      state.freeTimeslots = payload.freeTimeslots
      state.status = Status.FULFILLED
    }),
      builder.addCase(getWeekEventsThunk.fulfilled, (state, { payload }: PayloadAction<TimeEvent[]>) => {
        state.weekEvents = [...payload]
      }),
      builder.addCase(getSlotsEventsForToolbarThunk.fulfilled, (state, { payload }: PayloadAction<TimeSlotWithEvents[]>) => {
        state.toolbarSlots = [...payload]
      })
  },
})
// export const {

// } = userSlice.actions
export default timeSlotsSlice.reducer
