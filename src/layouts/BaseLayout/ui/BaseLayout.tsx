import { Header } from '@layouts/Header'
import { Sidebar } from '@layouts/Sidebar'
import cls from './BaseLayout.module.scss'
import { useLocation } from 'react-router-dom'
import { Url } from '@/routes/url'
import { FreeLayout } from '@/layouts/FreeLayout'
import { FirstVisitSlider } from '@/components/FirstVisitSlider/ui/FirstVisitSlider'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { useEffect } from 'react'
import { getVacanciesThunk } from '@/store/slices/vacanciesSlice'
import { getCompaniesThunk } from '@/store/slices/companiesSlice'
import { getSlotsEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsThunk'

export const BaseLayout = ({ content }) => {
  const location = useLocation()
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(getVacanciesThunk())
    dispatch(getCompaniesThunk())
    dispatch(getSlotsEventsThunk())
  })
  if (location.pathname.split('/')[1] === Url.VIEW_VACANCY.split('/')[1]) {
    return <FreeLayout content={content} />
  }
  return (
    <div className={cls.BaseLayout}>
      <FirstVisitSlider />
      <Sidebar />
      <div className={cls.Content}>
        <Header />
        <div className={cls.ContentPage}>{content}</div>
      </div>
      <div className={cls.version}> {import.meta.env.VITE_VERSION} </div>
    </div>
  )
}
