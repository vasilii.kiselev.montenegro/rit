import { Logo } from '@/assets/images/Logo'
import cls from './Sidebar.module.scss'
import { NavLinks } from '@/components/NavLinks/NavLinks'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'

import { useAppSelector } from '@/hooks/useAppSelector'
import { UserDefaultImg } from '@/assets/images/UserDefaultImg'
import { PM3 } from '@/UI/TextComponents'

export const Sidebar = () => {
  const navigate = useNavigate()
  const user = useAppSelector((state) => state.user.user)
  return (
    <div className={cls.Sidebar}>
      <div>
        <Logo style={{ marginBottom: '50px' }} />
        <NavLinks />
      </div>
      <div className={cls.userView} onClick={() => navigate(Url.USER)}>
        <div className={cls.userLogo}>{user?.img ? <img src={user?.img} alt="" /> : <UserDefaultImg />}</div>
        <div className={cls.username}>
          <PM3>{user?.firstName}</PM3>
          <PM3>{user?.lastName}</PM3>
        </div>
      </div>
    </div>
  )
}
