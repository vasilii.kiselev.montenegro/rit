import { ReactElement } from 'react'
import cls from './FreeLayout.module.scss'
import { ViewVacancyImage } from '@/assets/images/ViewVacancyImage'

import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { CrossIcon } from '@/assets/icons/CrossIcon'

interface FreeLayoutProps {
  content: ReactElement
}

export const FreeLayout = ({ content }: FreeLayoutProps) => {
  const navigate = useNavigate()
  return (
    <div className={cls.FreeLayout}>
      <div className={cls.contentAndBackbutton}>
        {/* <div className={cls.content}> */}
        {content}

        {/* </div> */}
        {/* <div onClick={() => navigate(Url.MAIN)} style={{ cursor: 'pointer' }}>
          <CrossIcon />
        </div> */}
      </div>
      <div className={cls.FreeLayoutimageBG}>
        <ViewVacancyImage />
      </div>
    </div>
  )
}
