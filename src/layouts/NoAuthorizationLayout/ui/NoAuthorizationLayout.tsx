import { ReactElement } from 'react'
import { Logo } from '@/assets/images/Logo'
import cls from './NoAuthorizationLayout.module.scss'
import { useLocation } from 'react-router-dom'
import { Url } from '@/routes/url'
import { FreeLayout } from '@/layouts/FreeLayout'

interface NoAuthorizationLayoutProps {
  content: ReactElement
}

export const NoAuthorizationLayout = ({
  content,
}: NoAuthorizationLayoutProps) => {
  const location = useLocation()
  if (location.pathname.split('/')[1] === Url.VIEW_VACANCY.split('/')[1]) {
    return <FreeLayout content={content} />
  }
  return (
    <div className={cls.NoAuthorization}>
      <div className={cls.NoAuthorizationContent}>{content}</div>
      <div className={cls.NoAuthorizationLogo}>
        <Logo />
      </div>
    </div>
  )
}
