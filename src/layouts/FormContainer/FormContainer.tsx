import { useFormik } from 'formik'
import { FC } from 'react'

interface FormContainerProps {
  Form: FC
  getQuery: string
  postQury?: string
  deleteQuery?: string
  initValue: { any }
}

export const FormContainer = ({
  Form,
  initValue,
  getQuery,
}: FormContainerProps) => {
  const formik = useFormik({
    initialValues: initValue,
  })
  return (
    <div className={cls.FormContainer}>
      <Form />
    </div>
  )
}
