import { TextInput } from '@/UI/TextInput/TextInput'
import cls from './Header.module.scss'
import { SearchIcon } from '@/assets/icons/SearchIcon'
import { ThemeIcon } from '@/assets/icons/ThemeIcon'
import { MenuIcon } from '@/assets/icons/MenuIcon'
import { BreadCrumbs } from '../components/BreadCrumbs/BreadCrumbs'

export const Header = () => {
  return (
    <div className={cls.Header}>
      {/* <div>bread crumbs</div> */}
      <BreadCrumbs />
      <div className={cls.headerSearchTheme}>
        <TextInput placeholder="Найди меня я тут" img={<SearchIcon />} />{' '}
        {/*нет функционала*/}
        <div className={cls.btnsCont}>
          <ThemeIcon />
          <MenuIcon />
        </div>
      </div>
    </div>
  )
}
