import { useLocation, useNavigate } from 'react-router-dom'
import cls from './BreadCrumbs.module.scss'
import { Url, UrlNames } from '@/routes/url'
import React from 'react'
import { ArrowBackIcon } from '@/assets/icons/ArrowBackIcon'

export const BreadCrumbs = () => {
  const location = useLocation()
  const navigate = useNavigate()

  const getPaths = (paths: string[]): string[] => {
    let counter = 0
    const max = paths.length
    const newArr: string[] = []
    const getNextPath = () => {
      if (counter < max) {
        const prev = newArr[counter - 1]
        if (!counter) {
          newArr.push('/')
        } else if (counter === 1) {
          newArr.push(`${prev}${paths[counter]}`)
        } else {
          newArr.push(`${prev}/${paths[counter]}`)
        }
        counter++
        getNextPath()
      }
      return
    }
    getNextPath()
    return newArr
  }

  const urlElements = location.pathname === '/' ? [''] : location.pathname.split('/')
  const urls = getPaths(urlElements)

  return (
    <div className={cls.BreadCrumbs}>
      <div onClick={() => navigate(urls[urls.length - 2])} className={cls.breadCrumbsArrowBAck}>
        {urls.length > 1 && <ArrowBackIcon />}
      </div>
      {urls.map((el, id) => (
        <React.Fragment key={`${el}${id}`}>
          <div
            className={`${cls.breadCrumbsItem} ${urls.length - 1 === id || UrlNames[el] == UrlNames[Url.NEW_EDIT] ? cls.breadCrumbsItemLast : ''}`}
            onClick={() => !(urls.length - 1 === id) && navigate(el)}
          >
            {UrlNames[el]}
          </div>

          {urls.length > 1 && id < urls.length - 1 && UrlNames[el] !== UrlNames[Url.NEW_EDIT] ? (
            <span className={cls.breadCrumbsSeparator}>|</span>
          ) : (
            ''
          )}
        </React.Fragment>
      ))}
    </div>
  )
}
