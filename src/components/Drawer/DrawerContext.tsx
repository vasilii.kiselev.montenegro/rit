import React, { ReactNode } from 'react'

import { Drawer } from './Drawer'
import { useDrawer } from '@/hooks/useDrawer/useDrawer'

type Context = {
  drawer: boolean
  handleDrawer: (content?: ReactNode | string, header?: ReactNode | string) => void
  drawerContent: ReactNode | string
  drawerHeader: ReactNode | string
}
//@ts-ignore
const DrawerContext = React.createContext<Context>({})

type DrawerProviderProps = {
  children: ReactNode
}

const DrawerProvider = ({ children }: DrawerProviderProps) => {
  const { drawer, handleDrawer, drawerContent, drawerHeader } = useDrawer()

  return (
    <DrawerContext.Provider value={{ drawer, handleDrawer, drawerContent, drawerHeader }}>
      <Drawer />
      {children}
    </DrawerContext.Provider>
  )
}

export { DrawerContext, DrawerProvider }
