import { ArrowBackIcon } from '@/assets/icons/ArrowBackIcon'
import cls from './Drawer.module.scss'
import { useCallback, useContext, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { DrawerContext } from './DrawerContext'

export const Drawer = () => {
  const { drawer, handleDrawer, drawerContent, drawerHeader } = useContext(DrawerContext)

  const handleCloseToBackdrop = useCallback(() => {
    handleDrawer()
  }, [handleDrawer])

  const escFunction = useCallback(
    (event: any) => {
      if (event.key === 'Escape') {
        handleCloseToBackdrop()
      }
    },
    [handleCloseToBackdrop]
  )
  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)

    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  if (drawer) {
    return ReactDOM.createPortal(
      <>
        <div
          onClick={handleCloseToBackdrop}
          className={`${cls.drawerBackdrop} ${drawer ? cls.drawerBackdropOpen : cls.drawerBackdropClose}`}
        >
          <div onClick={(e) => e.stopPropagation()} className={`${cls.Drawer} ${drawer ? cls.drawerOpen : cls.drawerClose}`}>
            <div className={cls.drawerHeader}>
              <div className={cls.arrowBack} onClick={handleCloseToBackdrop}>
                <ArrowBackIcon />
              </div>
              {drawerHeader}
            </div>
            <div className={cls.drawerBody}>{drawerContent}</div>

            {/* {footer && <div className={cls.drawerFooter}>{footer}</div>} */}
          </div>
        </div>
      </>,
      document.body
    )
  } else {
    return null
  }
}
