import { PR2 } from '@/UI/TextComponents'
import cls from './TechSupportModal.module.scss'

export const TechSupportModal = () => {
  return (
    <div className={cls.TechSupportModal}>
      <PR2>В данный момент функция восстановления пароля недоступна.</PR2>
      <PR2>
        Если у вас возникли проблемы, свяжитесь с нами по адресу:{' '}
        <a href="#" target="_blanck" style={{ color: '#2dad96' }}>
          example@mail.ru
        </a>
        .
      </PR2>
    </div>
  )
}
