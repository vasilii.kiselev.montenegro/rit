import { PR1 } from '@/UI/TextComponents'
import cls from './ConfirmModal.module.scss'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import React from 'react'
import { ModalContext } from '../Modal/modalCotext'
import { deleteChildrenSlot, deleteScheduleSlot } from '@/api/calendar/calendar'
import { updateTimeslots } from '@/pages/NewCalendarPage/utils'

interface ConfirmModalProps {
  slotId: number
  operation: 'schedule' | 'children'
}

export const ConfirmModal = ({ slotId, operation }: ConfirmModalProps) => {
  const { handleModal } = React.useContext(ModalContext)
  const deleteSlot = () => {
    if (operation === 'children') {
      deleteChildrenSlot(slotId).then((data) => {
        if (data) {
          updateTimeslots()
          handleModal()
        }
      })
    } else if (operation === 'schedule') {
      deleteScheduleSlot(slotId).then((data) => {
        if (data) {
          updateTimeslots()
          handleModal()
        }
      })
    }
  }
  return (
    <div className={cls.ConfirmModal}>
      <PR1>Вы уверены, что хотите удалить слот?</PR1>
      <div className={cls.buttonsCont}>
        <Button color={ButtonColors.GREEN} onClick={deleteSlot}>
          Подтвердить
        </Button>
        <Button color={ButtonColors.RED} onClick={() => handleModal()}>
          Отменить
        </Button>
      </div>
    </div>
  )
}
