import { PR2 } from '@/UI/TextComponents'
import cls from './TimeslotsErorrsModal.module.scss'

interface TimeslotsErorrsModalProps {
  text: string
}

export const TimeslotsErorrsModal = ({ text }: TimeslotsErorrsModalProps) => {
  return (
    <div className={cls.TimeslotsErorrsModal}>
      <PR2>{text}</PR2>
    </div>
  )
}
