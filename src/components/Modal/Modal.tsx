import cls from './Modal.module.scss'
import ReactDOM from 'react-dom'
import { ModalContext } from './modalCotext'
import { useContext } from 'react'
import { CrossIcon } from '@/assets/icons/CrossIcon'
export const CustomModal = () => {
  const { modalContent, handleModal, modal } = useContext(ModalContext)

  const onClose = () => {
    handleModal()
  }
  if (modal) {
    return ReactDOM.createPortal(
      <div onClick={() => onClose()} className={cls.modalContainer}>
        <div onClick={(e) => e.stopPropagation()} className={cls.modalWindow}>
          <div className={cls.headerWithCloseBtn}>
            <div onClick={() => onClose()} className={cls.crossIconCont}>
              <CrossIcon />
            </div>
          </div>
          <div className={cls.modalContent}>{modalContent}</div>
        </div>
      </div>,
      document.body
    )
  } else return null
}
