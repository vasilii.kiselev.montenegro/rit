import React, { ReactNode } from 'react'
import { useModal } from '@/hooks/useModal/useModal'
import { CustomModal } from './Modal'

type Context = {
  modal: boolean
  handleModal: (content?: ReactNode | string) => void
  modalContent: ReactNode | string
}
//@ts-ignore
const ModalContext = React.createContext<Context>({})

type ModalProviderProps = {
  children: ReactNode
}

const ModalProvider = ({ children }: ModalProviderProps) => {
  const { modal, handleModal, modalContent } = useModal()

  return (
    <ModalContext.Provider value={{ modal, handleModal, modalContent }}>
      <CustomModal />
      {children}
    </ModalContext.Provider>
  )
}

export { ModalContext, ModalProvider }
