import { SendCompany } from '@/types/company'
import cls from './CompanyModalView.module.scss'
import { Comment, H1, PM2, PM3, PR3 } from '@/UI/TextComponents'
import { TickCircleIcon } from '@/assets/icons/TickCircleIcon'
import { InfoCircleIcon } from '@/assets/icons/InfoCircleIcon'
import { GlobalIcon } from '@/assets/icons/GlobalIcon'
import { LocationIcon } from '@/assets/icons/LocationIcon'
import { SendIcon } from '@/assets/icons/SendIcon'
import { useAppSelector } from '@/hooks/useAppSelector'
import { UserStatus } from '@/types/user'

interface CompanyModalViewProps {
  company: SendCompany
}

export const CompanyModalView = ({ company }: CompanyModalViewProps) => {
  const status = useAppSelector((state) => state.user.status)

  return (
    <div className={cls.CompanyModalView}>
      <div className={cls.CompanyModalHeader}>
        <div className={cls.CompanyModalHeaderUp}>
          <H1>{company.name}</H1>
          {company.approvalStatus ? (
            <TickCircleIcon />
          ) : (
            <div className={cls.CompanyModalHeaderUpVerification}>
              <InfoCircleIcon />{' '}
              {status === UserStatus.LOGIN && (
                <a href="mailto:admin@rity.app?subject=стать проверенной компанией">
                  Верифицировать
                </a>
              )}
            </div>
          )}
        </div>
        <div className={cls.CompanyModalHeaderDown}>
          <div
            className={`${cls.CompanyModalHeaderDownitem} ${cls.CompanyModalHeaderDownName}`}
          >
            {company.name}{' '}
          </div>
          <div className={cls.CompanyModalHeaderDownSeparator}> </div>
          <div className={`${cls.CompanyModalHeaderDownitem} `}>
            <GlobalIcon /> {company.domainAddress}
          </div>
          <div className={cls.CompanyModalHeaderDownSeparator}> </div>
          <div className={`${cls.CompanyModalHeaderDownitem} `}>
            <LocationIcon /> {company.location}
          </div>
          <div className={cls.CompanyModalHeaderDownSeparator}> </div>
          <div
            className={`${cls.CompanyModalHeaderDownitem} ${cls.CompanyModalHeaderDownDesctiption}`}
          >
            {company.name} -{' '}
            {company.description.split(' ').splice(0, 2).join(' ')}...
            <SendIcon />
          </div>
        </div>
      </div>
      <div className={cls.companyInfo}>
        <div className={cls.companyInfoItem}>
          <Comment>Основана в...</Comment>
          <div className={cls.companyInfoItemValue}>
            <PM3>{company.foundedAt}</PM3>
          </div>
        </div>
        <div className={cls.companyInfoItemSeparator}></div>
        <div className={cls.companyInfoItem}>
          <Comment>Кол-во сотрудников</Comment>
          <div className={cls.companyInfoItemValue}>
            <PM3>{'Не указано'}</PM3>
          </div>
        </div>
        <div className={cls.companyInfoItemSeparator}></div>
        <div className={cls.companyInfoItem}>
          <Comment>Средняя з/п</Comment>
          <div className={cls.companyInfoItemValue}>
            <PM3>{'Не указано'} /Месяц</PM3>
          </div>
        </div>
      </div>
      <div className={cls.companyDescription}>
        <PM2>Описание компании</PM2>
        <Comment>
          <PR3>{company.description}</PR3>
        </Comment>
      </div>
    </div>
  )
}
