import { AppLink } from '@/UI/NavLink/AppLink'
import cls from './NavLinks.module.scss'
import { Url } from '@/routes/url'
import { BookIcon } from '@/assets/icons/BookIcon'
import { CalendarIcon } from '@/assets/icons/CalendarIcon'
import { Comment } from '@/UI/TextComponents/TextComponents'
import { CompaniesIcon } from '@/assets/icons/CompaniesIcon'
import { HomeIcon } from '@/assets/icons/HomeIcon'

export const NavLinks = () => {
  return (
    <div className={cls.NavLinks}>
      {/* <Comment className={cls.title}>Главные</Comment> */}
      <div className={cls.links}>
        {/* <AppLink to={Url.MAIN} Img={HomeIcon} label="Главная" /> */}
        <AppLink to={Url.CALENDAR} Img={CalendarIcon} label="Календарь" />
        <AppLink to={Url.MAIN} Img={BookIcon} label="Вакансии" />
        {/* <AppLink to={Url.VACANSY} Img={BookIcon} label="Вакансии" /> */}
        <AppLink to={Url.COMPANIES} Img={CompaniesIcon} label="Компании" />
      </div>
    </div>
  )
}
