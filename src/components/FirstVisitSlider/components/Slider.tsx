import { useEffect, useRef, useState } from 'react'
import cls from './Slider.module.scss'
import { FirstViewSlider_first } from '@/assets/firstViewSliderImages/FirstViewSlider_first'
import { FirstViewSlider_second } from '@/assets/firstViewSliderImages/FirstViewSlider_second'
import { FirstViewSlider_third } from '@/assets/firstViewSliderImages/FirstViewSlider_third'
import { FirstViewSlider_fourth } from '@/assets/firstViewSliderImages/FirstViewSlider_fourth'
import { FirstViewSlider_fifth } from '@/assets/firstViewSliderImages/FirstViewSlider_fifth'
import { FirstViewSlider_sixth } from '@/assets/firstViewSliderImages/FirstViewSlider_sixth'

interface SliderProps {
  slide: number
}

export const Slider = ({ slide }: SliderProps) => {
  const [left, setLeft] = useState()
  const slideWidth = useRef()
  console.log(left)

  useEffect(() => {
    setLeft(-(slideWidth?.current?.clientWidth * slide))
  }, [slide])
  return (
    <div ref={slideWidth} className={cls.Slider}>
      <div style={{ left: left }} className={cls.sliderLine}>
        <div className={`${cls.sliderItem} ${cls.sliderItem1}`}>
          <FirstViewSlider_first />
        </div>
        <div className={`${cls.sliderItem} ${cls.sliderItem2}`}>
          <FirstViewSlider_second />
        </div>
        <div className={`${cls.sliderItem} ${cls.sliderItem3}`}>
          <FirstViewSlider_third />
        </div>
        <div className={`${cls.sliderItem} ${cls.sliderItem4}`}>
          <FirstViewSlider_fourth />
        </div>
        <div className={`${cls.sliderItem} ${cls.sliderItem5}`}>
          <FirstViewSlider_fifth />
        </div>
        <div className={`${cls.sliderItem} ${cls.sliderItem6}`}>
          <FirstViewSlider_sixth />
        </div>
      </div>
    </div>
  )
}
