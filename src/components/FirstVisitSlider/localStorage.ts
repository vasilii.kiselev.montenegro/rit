const sliderFirstVisit = 'FIRST_VISIT_SLIDER'

// localStorage.setItem(sliderFirstVisit, 'true')

export const getFirstVisitValue = () => {
  const item = localStorage.getItem(sliderFirstVisit)
  return item === 'false' ? false : true
}

export const setFirstVisitValueFalse = () =>
  localStorage.setItem(sliderFirstVisit, 'false')
export const setFirstVisitValueTrue = () =>
  localStorage.setItem(sliderFirstVisit, 'true')
