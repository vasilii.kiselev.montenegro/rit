import ReactDOM from 'react-dom'
import cls from './FirstVisitSlider.module.scss'
import { Slider } from '../components/Slider'
import { PRWithTags } from '@/UI/TextComponents/TextComponents'
import { firstVisitSliderTexts } from '../consts'
import { useEffect, useState } from 'react'
import { Button } from '@/UI/Button/Button'
import { getFirstVisitValue, setFirstVisitValueFalse } from '../localStorage'

export const FirstVisitSlider = () => {
  const [isModal, setIsModal] = useState<boolean>(getFirstVisitValue())

  const lastSlide = 5
  const [slide, setSlide] = useState<number>(0)

  useEffect(() => {
    if (!isModal) {
      setFirstVisitValueFalse()
    }
  }, [isModal])

  const handleNext = () => {
    if (slide === lastSlide) return
    setSlide((prev) => prev + 1)
  }
  const handlePrev = () => {
    if (slide === 0) return
    setSlide((prev) => prev - 1)
  }

  if (isModal) {
    return ReactDOM.createPortal(
      <div className={cls.FirstVisitSlider}>
        <div className={cls.sliderModal}>
          <div className={cls.sliderContainer}>
            <Slider slide={slide} />
          </div>
          <div className={cls.sliderText}>
            <PRWithTags fontSize="18px" isBold>
              {firstVisitSliderTexts[slide]}
            </PRWithTags>
          </div>
          <div className={cls.radioButtons}>
            {firstVisitSliderTexts.map((_, id) => {
              return (
                <div
                  onClick={() => setSlide(id)}
                  key={id}
                  style={{ background: id === slide ? '#2dad96' : '#A2E5D9' }}
                  className={cls.radioButtonsItem}
                ></div>
              )
            })}
          </div>
          <div className={cls.navigateBlock}>
            {slide === lastSlide ? (
              ' '
            ) : (
              <div className={cls.skip} onClick={() => setIsModal(false)}>
                {' '}
                Пропустить всё
              </div>
            )}
            <div className={`${cls.Buttons} ${slide === lastSlide ? cls.lastButtons : ''}`}>
              {slide ? (
                <Button color="btnIconWhite" onClick={handlePrev}>
                  Назад
                </Button>
              ) : (
                ''
              )}

              {lastSlide === slide ? (
                <Button
                  color="btnGreen"
                  onClick={() => {
                    console.log(setIsModal)

                    setIsModal(false)
                  }}
                >
                  Начать
                </Button>
              ) : (
                <Button color="btnGreen" onClick={handleNext}>
                  Дальше
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>,
      document.body
    )
  } else return null
}
