import { FullCompany, SendCompany } from '@/types/company'
import cls from './CompanyForm.module.scss'
import { Comment, PM2 } from '@/UI/TextComponents'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { Button as ButtonMui } from '@mui/material'
import { useFormik } from 'formik'
import { TextInput } from '@/UI/TextInput/TextInput'
import { TextArea } from '@/UI/TextArea/TextArea'
import { Tooltip } from '@/UI/Tooltip/Tooltip'
import { Button } from '@/UI/Button/Button'
import { addCompany, changeCompany } from '@/api/companies'
import * as Yup from 'yup'
import BasicDatePicker from '@/UI/DatePicker/DatePicker'
// import { addCompanyToSlice } from '@/store/slices/companiesSlice/companiesSlice'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import moment from 'moment'
import { useCallback, useEffect, useLayoutEffect, useState } from 'react'
import { getCompaniesThunk } from '@/store/slices/companiesSlice'
import { ButtonColors } from '@/UI/Button/consts'
import { uploadImg } from '@/api/companies/companies'
interface CompanyFormProps {
  company?: SendCompany
  operation: 'new' | 'change'
  setClose: () => void
}
type InitValues = Omit<SendCompany, 'logo'> & {
  logo: File
}
const initState: InitValues = {
  id: null,
  name: '',
  logo: null,
  location: '',
  domainAddress: '',
  description: '',
  approvalStatus: false,
  inn: '',
  employeeCount: '',
  foundedAt: '',
}
enum Operation {
  NEW = 'new',
  CHANGE = 'change',
}
export const CompanyForm = ({ company, operation, setClose }: CompanyFormProps) => {
  const [imgUrl, setImageUrl] = useState<URL>()
  const dispatch = useAppDispatch()
  const valiadtionSchema = Yup.object<SendCompany>().shape({
    name: Yup.string().required(),
    location: Yup.string().required(),
    domainAddress: Yup.string().required(),
    inn: Yup.string().required(),
    employeeCount: Yup.string().required(),
    foundedAt: Yup.string().required(),
    description: Yup.string().required(),
  })
  console.log(operation === Operation.NEW)

  const formik = useFormik({
    initialValues: operation === Operation.NEW ? initState : company ? { ...company } : initState,
    validationSchema: valiadtionSchema,
    validateOnMount: false,
    validateOnChange: false,
    validateOnBlur: true,
    onSubmit(values: FullCompany) {
      console.log('submit', operation)

      if (operation === Operation.NEW) {
        addCompany(values).then((data) => {
          const { errors, res } = data
          if (errors) {
            Object.entries(res).map((el) => {
              if (el[1][0] === 'Enter a valid URL.') {
                formik.setFieldError('domainAddress', 'Введите корректный адрес сайта')
              }
            })
            // dispatch(addCompanyToSlice(data))
          } else {
            dispatch(getCompaniesThunk())
            setClose()
          }
        })
      }
      if (operation === Operation.CHANGE) {
        console.log(values)

        changeCompany(values).then((data) => {
          const { errors, res } = data
          if (errors) {
            Object.entries(res).map((el) => {
              if (el[1][0] === 'Enter a valid URL.') {
                formik.setFieldError('domainAddress', 'Введите корректный адрес сайта')
              }
            })
            // dispatch(addCompanyToSlice(data))
          } else {
            dispatch(getCompaniesThunk())
            setClose()
          }
        })
      }
    },
  })
  useEffect(() => {
    if (typeof formik.values.logo !== 'string' && formik.values.logo) {
      //@ts-expect-error
      setImageUrl(URL.createObjectURL(formik.values?.logo))
    }
  }, [company?.logo, formik.values?.logo])

  useLayoutEffect(() => {
    if (operation === 'change') {
      uploadImg(company.logo).then((data) => {
        formik.setFieldValue('logo', data)
      })
    }
  }, [operation, company?.logo])
  const escFunction = useCallback((event: KeyboardEvent) => {
    if (event.key === 'Escape') {
      formik.handleSubmit()
    }
  }, [])
  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)

    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  useEffect(() => {
    const thisDate = moment().format('YYYY-MM-DD')
    if (!formik.values.foundedAt) formik.setFieldValue('foundedAt', thisDate)
  }, [])
  console.log(imgUrl, formik.values.logo)

  return (
    <>
      <form onSubmit={formik.handleSubmit} className={cls.CompanyForm}>
        <div className={cls.CompanyFormBody}>
          <div className={cls.companyInputLabel}>
            <PM2>
              Наименование компании<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextInput name="name" text={formik.values.name} onChange={formik.handleChange} placeholder="Введите текст" />
            <span style={{ color: '#E60019' }}>{formik.errors.name}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>Логотип</PM2>
            <div className={cls.companyLoadFileAndImg}>
              {formik.values.logo && (
                <img
                  //@ts-ignore
                  src={imgUrl ? imgUrl : company?.logo}
                  className={cls.logo}
                />
              )}

              <ButtonMui
                component="label"
                role={undefined}
                variant="contained"
                tabIndex={-1}
                startIcon={<PlusIcon />}
                className={cls.greyButton}
              >
                <input
                  type="file"
                  className={cls.companyInputHidden}
                  name="logo"
                  onChange={(e) => formik.setFieldValue('logo', e.currentTarget.files[0])}
                />
              </ButtonMui>
            </div>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              Описание<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextArea
              rows={3}
              placeholder="Введите текст"
              name="description"
              text={formik.values.description}
              onChange={formik.handleChange}
            />
            <span style={{ color: '#E60019' }}>{formik.errors.description}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              Адрес сайта<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextInput
              placeholder="https://rity.com"
              name="domainAddress"
              text={formik.values.domainAddress}
              onChange={formik.handleChange}
              // onBlur={formik.handleBlur}
            />
            <span style={{ color: '#E60019' }}>{formik.errors.domainAddress}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              ИНН<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextInput placeholder="Введите текст" name="inn" text={formik.values.inn} onChange={formik.handleChange} />
            <span style={{ color: '#E60019' }}>{formik.errors.inn}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              Количество сотрудников<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextInput placeholder="Введите текст" name="employeeCount" text={formik.values.employeeCount} onChange={formik.handleChange} />
            <span style={{ color: '#E60019' }}>{formik.errors.employeeCount}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              Дата основания<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            {/* <TextInput
              placeholder="Введите текст"
              name="foundedAt"
              text={formik.values.foundedAt}
              onChange={formik.handleChange}
            /> */}
            <BasicDatePicker
              name="foundedAt"
              value={formik.values.foundedAt}
              onChange={(data) => formik.setFieldValue('foundedAt', data)}
            />
            <span style={{ color: '#E60019' }}>{formik.errors.foundedAt}</span>
          </div>
          <div className={cls.companyInputLabel}>
            <PM2>
              Местоположение<span style={{ color: '#E60019' }}>*</span>
            </PM2>
            <TextInput placeholder="Введите текст" name="location" text={formik.values.location} onChange={formik.handleChange} />
            <span style={{ color: '#E60019' }}>{formik.errors.location}</span>
          </div>

          <a href="mailto:admin@rity.app?subject=стать проверенной компанией" className={cls.companyAprove}>
            <span className={cls.linkToAprove}>Стать проверенной компанией</span>
            <Tooltip>
              <Comment>
                Вы получите статус
                <br /> "Компания проверена"
              </Comment>
            </Tooltip>
          </a>
          {/* <div className={cls.btnViewVompany}>
            <Button color="btnblack ">Просмотр компании</Button>
          </div> */}
        </div>
        <div className={cls.footerForm}>
          {operation === 'change' ? (
            <Button onClick={() => formik.handleSubmit()} type={'submit'} color={ButtonColors.ICON_WHITE}>
              Изменить
            </Button>
          ) : (
            <Button onClick={formik.handleSubmit} type={'submit'} color={ButtonColors.GREEN}>
              Добавить компанию
            </Button>
          )}
        </div>
      </form>
    </>
  )
}
