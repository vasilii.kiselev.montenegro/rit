import { useCallback } from 'react'
import { Route, Routes } from 'react-router-dom'

import { LoadRoute, MainRoutes, NoAuthRoutes } from './routes'
import { useAppSelector } from '@/hooks/useAppSelector'
import { UserStatus } from '@/types/user'

export function AppRouter() {
  const login = useAppSelector((state) => state.user.status)

  const renderWithWrapper = useCallback((route: { path: string; element: JSX.Element }) => {
    return <Route key={route.path} path={route.path} element={route.element} />
  }, [])

  return (
    <Routes>
      {Object.values(login === UserStatus.LOGIN ? MainRoutes : login === UserStatus.UNSET ? NoAuthRoutes : LoadRoute).map(
        renderWithWrapper
      )}
    </Routes>
  )
}
