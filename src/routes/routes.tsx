import { Url } from './url'
import { CompaniesPage } from '@/pages/CompaniesPage'
import { LogSignPage } from '@/pages/LogSignPage'
import { VacanciesPage } from '@/pages/VacanciesPage'
import { NonePage } from '@/pages/NonePage'
import { DataRecoveryPage } from '@/pages/DataRecoveryPage'
import { ViewVacancyPage } from '@/pages/ViewVacancy'
import { FreeUpTimePage } from '@/pages/FreeUpTimePage'
import { UserPage } from '@/pages/UserPage'
import { LoadPage } from '@/pages/LoadPage/LoadPage'
import { NewCreateVacancy } from '@/pages/NewCreateVacancy/ui/NewCreateVacancy'
import { NewEditVacancy } from '@/pages/NewEditVacancy/ui/NewEditVacancy'
import { NewCalendarPage } from '@/pages/NewCalendarPage/ui/NewCalendarPage'

export const MainRoutes = [
  // {
  //   path: Url.MAIN,
  //   element: <VacanciesPage />,
  // },
  {
    path: Url.MAIN,
    element: <VacanciesPage />,
  },
  {
    path: Url.CALENDAR,
    element: <NewCalendarPage />,
  },
  {
    path: Url.COMPANIES,
    element: <CompaniesPage />,
  },
  {
    path: Url.CREATE_VACANCY,
    element: <NewCreateVacancy />,
  },
  {
    path: `${Url.NEW_EDIT}/:vacancyId`,
    element: <NewEditVacancy />,
  },
  {
    path: `${Url.VIEW_VACANCY}/:vacancyId`,
    element: <ViewVacancyPage />,
  },
  {
    path: Url.FREE_UP_TIME,
    element: <FreeUpTimePage />,
  },
  {
    path: Url.USER,
    element: <UserPage />,
  },
  {
    path: Url.NEW_CALENDAR,
    element: <NewCalendarPage />,
  },

  {
    path: '*',
    element: <NonePage />,
  },
]

export const NoAuthRoutes = [
  {
    path: Url.MAIN,
    element: <LogSignPage />,
  },
  {
    path: Url.DATA_RECOVERY,
    element: <DataRecoveryPage />,
  },
  {
    path: '*',
    element: <LogSignPage />,
  },
  {
    path: `${Url.VIEW_VACANCY}/:vacancyId`,
    element: <ViewVacancyPage />,
  },
]
export const LoadRoute = [
  {
    path: Url.MAIN,
    element: <LoadPage />,
  },
]
