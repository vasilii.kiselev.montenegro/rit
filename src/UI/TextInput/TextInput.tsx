import { FocusEventHandler, ReactElement, useState } from 'react'
import { PM2 } from '../TextComponents'
import cls from './TextInput.module.scss'
import { EyeBlackIcon } from '@/assets/icons/EyeBlackIcon'
import { EyeBlackLockIcon } from '@/assets/icons/EyeBlackLockIcon'

export enum InputType {
  TEXT = 'text',
  PASSWORD = 'password',
}

interface TextInputProps {
  onChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => void

  onBlur?: FocusEventHandler<HTMLInputElement>
  text: string
  label?: string
  placeholder: string
  img?: ReactElement
  name?: string
  type?: string
  disabled?: boolean
}

export const TextInput = ({
  onChange,
  text = '',
  label,
  placeholder = '',
  name = '',
  img,
  onBlur,
  type = InputType.TEXT,
  disabled = false,
  ...restProps
}: TextInputProps) => {
  const [viewPass, setView] = useState<boolean>(false)
  return (
    <div className={cls.TextInputContainer}>
      {!!label && <PM2>{label}</PM2>}
      <div className={cls.inputImg}>
        {img && img}
        <input
          className={`${cls.textInput} ${img ? cls.textInputIcon : ''} ${disabled ? cls.disabled : ''}`}
          type={viewPass ? InputType.TEXT : type}
          placeholder={placeholder}
          name={name}
          value={text}
          onBlur={onBlur !== undefined ? onBlur : null}
          onChange={onChange}
          disabled={disabled}
          {...restProps}
        />

        {type === InputType.PASSWORD && (
          <div className={cls.passwordCheck} onClick={() => setView((prev) => !prev)}>
            {viewPass === true ? <EyeBlackIcon /> : <EyeBlackLockIcon />}
          </div>
        )}
      </div>
    </div>
  )
}
