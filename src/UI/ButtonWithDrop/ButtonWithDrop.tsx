import { DropMenu } from '@/pages/VacanciesPage/components/DropMenu/DropMenu'
import cls from './ButtonWithDrop.module.scss'
import { ReactNode, useState } from 'react'

// export enum ButtonColor {
//   green,
// }

interface ButtonProps {
  children: ReactNode
  disabled?: boolean
  color: string

  id: number
}

export const ButtonWithDrop = ({
  children,
  disabled = false,
  color,
  id,
}: ButtonProps) => {
  const [dropMenu, setDropMenu] = useState(false)

  return (
    <button
      disabled={disabled}
      className={`${cls.Button}  ${color}`}
      onClick={() => setDropMenu((prev) => !prev)}
    >
      {children}
      {dropMenu && <DropMenu id={id} />}
    </button>
  )
}
