import { PM2 } from '../TextComponents'
import cls from './TextArea.module.scss'
import { TextareaAutosize } from '@mui/material'

interface TextAreaProps {
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void

  onBlur?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void
  text: string
  label?: string
  placeholder?: string
  rows?: number
  name?: string
}

export const TextArea = ({
  onChange,
  text,
  label,
  rows = 0,
  placeholder = '',
  name = '',
  onBlur = (e) => e,
}: TextAreaProps) => {
  return (
    <div className={cls.TextAreaContainer}>
      {!!label && <PM2>{label}</PM2>}
      <div className={cls.inputImg}>
        <TextareaAutosize
          className={cls.textArea}
          placeholder={placeholder}
          value={text}
          minRows={rows}
          name={name}
          onChange={onChange}
          onBlur={onBlur}
        />
      </div>
    </div>
  )
}
