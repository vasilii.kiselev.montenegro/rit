import { FocusEventHandler, ReactElement } from 'react'
import { PM2 } from '../TextComponents'
import cls from './TextInput.module.scss'
interface NumberInputProps {
  onChange: (value: number) => void
  //   onChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => void

  onBlur?: FocusEventHandler<HTMLInputElement>
  text: number
  label?: string
  placeholder: string
  img?: ReactElement
  name: string
  type?: string
  disabled?: boolean
  min: number
  max: number
}

export const NumberInput = ({
  onChange,
  text = null,
  label,
  placeholder = '',
  name = '',
  onBlur,
  disabled = false,
  min,
  max,
  ...restProps
}: NumberInputProps) => {
  const setValue = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
    const data = Number(e.target.value)

    if (data > max) {
      onChange(max)
    } else if (data < min) {
      onChange(min)
    } else {
      onChange(data)
    }
  }
  return (
    <div className={cls.TextInputContainer}>
      {!!label && <PM2>{label}</PM2>}
      <div className={cls.inputImg}>
        <input
          className={`${cls.textInput}   ${disabled ? cls.disabled : ''}`}
          type={'number'}
          placeholder={placeholder}
          name={name}
          value={text}
          onBlur={onBlur !== undefined ? onBlur : null}
          onChange={setValue}
          disabled={disabled}
          min={min}
          max={max}
          {...restProps}
        />
      </div>
    </div>
  )
}
