import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import cls from './InputSelect.module.scss'
import { useState } from 'react'

interface InputSelectProps {
  values: string[] | number[] | { id: number; value: string }[]
  placeholder?: string
  name?: string
  value: string | number
  setValue: (e: React.ChangeEvent<string | number>) => void
}

export const InputSelect = ({
  values,
  value,
  setValue,
  placeholder = '',
  name = '',
}: InputSelectProps) => {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }
  // console.log(values)

  return (
    <FormControl className={cls.InputSelectForm}>
      <InputLabel
        className={cls.InputSelectFormLabel}
        id="demo-controlled-open-select-label"
      >
        {placeholder}
      </InputLabel>
      <Select
        labelId="demo-controlled-open-select-label"
        id="demo-controlled-open-select"
        className={cls.InputSelect}
        sx={
          open && {
            '&.MuiInputBase-root': { border: '2px solid #2dad96' },
          }
        }
        // placeholder={placeholder}
        name={name}
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        value={value ? value : ''}
        onChange={setValue}
        displayEmpty
      >
        {typeof values[0] === 'string' || typeof values[0] === 'number'
          ? values.map((el) => (
              <MenuItem key={el} value={el}>
                {el}
              </MenuItem>
            ))
          : values.map((el) => (
              <MenuItem key={el.id} value={el.id}>
                {el.value}
              </MenuItem>
            ))}
      </Select>
    </FormControl>
  )
}
