import cls from './ButtonIcon.module.scss'
import { ReactNode } from 'react'

// export enum ButtonColor {
//   green,
// }

interface ButtonProps {
  children: ReactNode
  disabled?: boolean
  color: string
  onClick?: (e: React.MouseEvent<HTMLElement>) => void
  style?: any
}

export const ButtonIcon = ({ children, disabled = false, color, onClick, style = {} }: ButtonProps) => {
  return (
    <button disabled={disabled} className={`${cls.Button}  ${color}`} onClick={onClick} style={style}>
      {children}
    </button>
  )
}
