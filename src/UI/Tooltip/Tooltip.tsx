import { ReactNode } from 'react'
import cls from './Tooltip.module.scss'

interface TooltipProps {
  children: ReactNode
}

export const Tooltip = ({ children }: TooltipProps) => {
  return (
    <div className={cls.Tooltip}>
      i<span>{children}</span>
    </div>
  )
}
