import cls from './MultiSelectInput.module.scss'
import { Menu, MenuItem } from '@mui/material'
import { useEffect, useRef, useState } from 'react'
import { Comment } from '../TextComponents'
type Item = { id: number; name: string }
interface MultiSelectInputProps {
  value: Item[]
  values: Item[]
  setValues: React.Dispatch<React.SetStateAction<Item[]>>
}

export const VacanciesMultiSelectInput = ({ setValues, value, values }: MultiSelectInputProps) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const fakeInput = useRef(null)
  const [thisValues, setThisValues] = useState<string[]>([])
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  useEffect(() => {
    if (value !== null || value?.length) {
      const getValues = value?.map((el) => {
        return values.find((elem) => elem?.id === el.id)?.name
      })

      setThisValues(getValues)
    }
  }, [value])
  const addRemoveItem = (item: Item) => {
    const addItem = (prev: Item[], item: Item) => {
      if (value) {
        setValues([...value, { id: item?.id, name: item?.name }])
      } else {
        setValues([{ id: item?.id, name: item?.name }])
      }
      if (prev) {
        return [...prev, { id: item?.id, name: item?.name }]
      } else {
        return [{ id: item?.id, name: item?.name }]
      }
    }
    const removeItem = (prev: Item[], item: Item) => {
      setValues(value?.filter((el) => el.id !== item.id))
      return prev.filter((el) => el.name !== item.name)
    }
    //@ts-ignore
    setThisValues((prev) => (prev?.includes(item.name) ? removeItem(prev, item) : addItem(prev, item)))
  }

  return (
    <div className={cls.NewInputSelect}>
      <div
        key="asdasdasdas"
        className={cls.InputSelect}
        color="btnIconWhite"
        style={{ height: thisValues?.length ? 'auto' : '56px' }}
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        ref={fakeInput}
      >
        {!thisValues?.length ? 'Выберите вакансии' : value.map((el) => <div className={cls.InputSelectItem}>{el.name}</div>)}
      </div>
      <Menu
        className={cls.menu}
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',

          horizontal: 'left',
        }}
        sx={{
          maxHeight: '300px',
          '& .MuiPaper-root': {
            width: fakeInput.current?.offsetWidth,
          },
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        {values.map((el) => {
          return (
            <MenuItem
              sx={{
                background: thisValues?.includes(el.name) ? '#e0e0e0' : '#fff',
              }}
              key={el.id}
              className={cls.menuItem}
              onClick={() => addRemoveItem({ id: el.id, name: el.name })}
            >
              <Comment>{el.name}</Comment>
            </MenuItem>
          )
        })}
      </Menu>
    </div>
  )
}
