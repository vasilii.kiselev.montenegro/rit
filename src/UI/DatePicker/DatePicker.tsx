import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { TextField, styled } from '@mui/material'
import dayjs, { Dayjs } from 'dayjs'
import 'dayjs/locale/ru' // Импортируйте локализацию для dayjs
import { useState } from 'react'

interface Props {
  onChange: (date: string) => void
  name: string
  value: string
}

export default function BasicDatePicker({ onChange, name, value }: Props) {
  const [date, setDate] = useState<Dayjs | null>(
    dayjs(value).isValid() ? dayjs(value) : dayjs()
  )
  // console.log(value, date)

  const CustomDatePicker = styled(DatePicker)({
    '& .MuiInputBase-input': {
      padding: '8px 16px',
      color: '#3d425a',
      borderRadius: '5px',
      width: '100%',
      background: '#fff',
      outline: 'none',
      fontSize: '14px',
      fontFamily: 'inherit',
      '&.textInputIcon': {
        padding: '8px 16px 8px 50px',
      },
    },
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: '#d4d7df',
    },
    '& .Mui-focused .MuiOutlinedInput-notchedOutline': {
      border: '2px solid #0cc9a6',
    },
    '&::placeholder': {
      color: '#7f879e',
    },
  })

  const handleDateChange = (data: Dayjs | null) => {
    setDate(data)

    if (data && data.isValid()) {
      onChange(data.format('YYYY-MM-DD'))
    } else {
      onChange('')
    }
  }

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="ru">
      <CustomDatePicker
        value={date}
        onChange={handleDateChange}
        // formatDensity="spacious"
        views={['year', 'month', 'day']}
        format="YYYY-MM-DD"
      />
    </LocalizationProvider>
  )
}
