import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import cls from './InputSelectMini.module.scss'
import { useState } from 'react'

type Value = string | number
interface InputSelectMiniProps {
  values: Value[] | { label: string; value: string | number }[]
  placeholder?: string
  setValue: React.Dispatch<React.SetStateAction<SelectChangeEvent<Value>>>
  name?: string
  value: Value
}

export const InputSelectMini = ({ values, placeholder = '', value, setValue, name = '' }: InputSelectMiniProps) => {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }
  return (
    <FormControl className={cls.InputSelectForm}>
      <InputLabel className={cls.InputSelectFormLabel} id="demo-controlled-open-select-label">
        {placeholder}
      </InputLabel>
      <Select
        labelId="demo-controlled-open-select-label"
        id="demo-controlled-open-select"
        className={cls.InputSelect}
        // placeholder={placeholder}
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        value={value ? value : ''}
        name={name}
        onChange={setValue}
        displayEmpty
      >
        {/* <MenuItem value={''}>
          <Comment>Выберите</Comment>
        </MenuItem> */}
        {typeof values[0] === 'string' || typeof values[0] === 'number'
          ? values.map((el) => (
              <MenuItem key={el} value={el}>
                {el}
              </MenuItem>
            ))
          : values.map((el) => (
              <MenuItem key={el.value} value={el.value}>
                {el.label}
              </MenuItem>
            ))}
      </Select>
    </FormControl>
  )
}
