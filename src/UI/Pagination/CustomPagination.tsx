import { Pagination, PaginationItem } from '@mui/material'
import styled from '@emotion/styled'
import cls from './CustomPagination.module.scss'

interface PaginationProps {
  setActivePage: React.Dispatch<React.SetStateAction<number>>
  paginationCount: number
  activePage: number
}

const CustomPaginationItem = styled(PaginationItem)`
  color: #2dad96;
  &:hover {
    background: #7ecabc;
    color: #fff;
  }
  &.Mui-selected {
    color: #fff;
    background: #2dad96;
  }
  &.Mui-selected:hover {
    color: #fff;
    background: #2dad96;
  }
`

export const CustomPagination = ({
  setActivePage,
  paginationCount,
  activePage,
}: PaginationProps) => {
  return (
    <>
      {paginationCount ? (
        <div className={cls.paginationCont}>
          <Pagination
            count={paginationCount}
            siblingCount={0}
            page={activePage}
            shape="rounded"
            onChange={(_, page) => {
              setActivePage(page)
            }}
            renderItem={(item) => <CustomPaginationItem {...item} />}
          />
        </div>
      ) : (
        ''
      )}
    </>
  )
}
