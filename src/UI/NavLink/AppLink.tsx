import { FC } from 'react'
import cls from './AppLink.module.scss'
import { LinkProps, NavLink } from 'react-router-dom'
import { PM2 } from '@/UI/TextComponents'

interface navLinkProps extends LinkProps {
  Img: FC
  label: string
}

export const AppLink = ({ Img, label, to }: navLinkProps) => {
  return (
    <NavLink
      to={to}
      className={({ isActive }) => `${cls.AppLink} ${isActive && cls.active}`}
    >
      <Img />
      <PM2>{label}</PM2>
    </NavLink>
  )
}
