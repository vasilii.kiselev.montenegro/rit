import { LocalizationProvider, TimeField } from '@mui/x-date-pickers'
// import cls from './TimeInput.module.scss'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { Dayjs } from 'dayjs'

interface TimeInputProps {
  value: Dayjs
  onChange: (e: Dayjs) => void
  name: string
  format?: string
}

export const TimeInput = ({ value, onChange, name, format = 'HH:mm' }: TimeInputProps) => {
  return (
    <div>
      <LocalizationProvider adapterLocale="Ru-ru" dateAdapter={AdapterDayjs}>
        <TimeField
          format={format}
          value={value}
          name={name}
          onChange={(e) => onChange(e)}
          sx={{
            '&': { background: '#fff' },
            '.Mui-focused .MuiOutlinedInput-notchedOutline': { border: '2px solid #2dad96 !important' },
            // '.Mui-error .MuiOutlinedInput-notchedOutline': { borderColor: '#000003b' },
          }}
        />
      </LocalizationProvider>
    </div>
  )
}
