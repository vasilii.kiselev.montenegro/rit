import toast, { Toaster } from 'react-hot-toast'

export const notify = (text) => toast(text)

export const Toast = () => {
  return (
    <Toaster
      position="bottom-center"
      toastOptions={{
        className: '',
        duration: 5000,
        style: {
          background: '#5E5E5F',
          color: '#fff',
          borderRadius: '5px',
        },
      }}
    />
  )
}
