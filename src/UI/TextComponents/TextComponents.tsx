import styled from '@emotion/styled'

export const PR1 = styled.div({
  fontSize: ' 18px',
  lineHeight: '150%',
  fontWeight: '500',
  fontFamily: 'Onest',
})
export const PR2 = styled.div({
  fontSize: '16px',
  lineHeight: '150%',
  fontWeight: '500',
  fontFamily: 'Onest',
})
export const PR3 = styled.div({
  fontSize: '14px',
  lineHeight: '150%',
  fontWeight: '500',
  fontFamily: 'Onest',
})
export const PR4 = styled.div({
  fontSize: '12px',
  lineHeight: '150%',
  fontWeight: '500',
  fontFamily: 'Onest',
})

export const PM1 = styled.div({
  fontSize: ' 18px',
  lineHeight: '150%',
  fontWeight: '600',
  fontFamily: 'Onest',
})
export const PM2 = styled.div({
  fontSize: '16px',
  lineHeight: '150%',
  fontWeight: '600',
  fontFamily: 'Onest',
})
export const PM3 = styled.div({
  fontSize: '14px',
  lineHeight: '150%',
  fontWeight: '600',
  fontFamily: 'Onest',
})
export const PM4 = styled.div({
  fontSize: '12px',
  lineHeight: '150%',
  fontWeight: '600',
  fontFamily: 'Onest',
})

export const Comment = styled.div({
  fontSize: '14px',
  lineHeight: '150%',
  color: '#8d9091',
  fontWeight: '500',
  fontFamily: 'Onest',
})

export const H1 = styled.div({
  fontSize: '24px',
  lineHeight: '150%',
  fontWeight: '700',
  fontFamily: 'Onest',
})

enum textColor {
  BLACK = '#1B2124',
  GREY = '#8d9091',
}
export const PRWithTags = ({
  fontSize = '16px',
  isBold = false,
  isComment = false,
  children = '',
}: {
  fontSize?: string
  isBold?: boolean
  children: string
  isComment?: boolean
}) => {
  const Pr = styled.div({
    fontSize: window.innerWidth > 1400 ? fontSize : '14px',
    lineHeight: '150%',
    fontWeight: isBold ? '700' : '500',
    fontFamily: 'Onest',
    color: isComment ? textColor.GREY : textColor.BLACK,
  })
  return (
    <Pr
      dangerouslySetInnerHTML={{
        __html: children,
      }}
    />
  )
}
