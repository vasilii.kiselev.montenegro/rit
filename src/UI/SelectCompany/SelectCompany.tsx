import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import cls from './SelectCompany.module.scss'
import { useState } from 'react'
import { Company } from '@/types/company'
import { LocationIcon } from '@/assets/icons/LocationIcon'

interface SelectCompanyProps {
  values: Company[]
  placeholder?: string
  name?: string
  value: string | number
  setValue: (e: React.ChangeEvent<string | number>) => void
}

export const SelectCompany = ({
  values,
  value,
  setValue,
  placeholder = '',
  name = '',
}: SelectCompanyProps) => {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <FormControl className={cls.InputSelectForm}>
      <InputLabel
        className={`${cls.InputSelectFormLabel} ${cls.menuItem}`}
        id="demo-controlled-open-select-label"
      >
        {placeholder}
      </InputLabel>
      <Select
        labelId="demo-controlled-open-select-label"
        id="demo-controlled-open-select"
        className={cls.InputSelect}
        // placeholder={placeholder}
        name={name}
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        value={value ? value : ''}
        onChange={setValue}
        displayEmpty
      >
        {/* <MenuItem value={''}>Выберите</MenuItem> */}
        {values.map((el) => (
          <MenuItem key={el.id} value={el.id}>
            <div className={cls.menuItem}>
              <div className={cls.logo}>
                <img src={el.logo} />{' '}
              </div>
              <div className={cls.name}>{el.name} </div>
              <div className={cls.namelocation}>
                <LocationIcon /> {el.location}
              </div>
              <div className={cls.description}> {el.description}</div>
            </div>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
