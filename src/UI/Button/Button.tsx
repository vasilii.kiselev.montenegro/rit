import cls from './Button.module.scss'
import { ReactNode } from 'react'
import { ButtonColors } from './consts'

interface ButtonProps {
  children: ReactNode
  color: ButtonColors
  disabled?: boolean
  onClick?: () => void
  className?: string
  type?: 'submit' | 'reset' | 'button'
  ref?: React.MutableRefObject<undefined>
}

export const Button = ({
  children,
  color,
  onClick,
  className = '',
  disabled = false,
  type = 'button',
  ref = null,
}: ButtonProps) => {
  return (
    <button
      ref={ref}
      disabled={disabled}
      onClick={onClick}
      type={type}
      className={`${cls.Button} ${
        disabled && 'disabled'
      }  ${color} ${className}`}
    >
      {children}
    </button>
  )
}
