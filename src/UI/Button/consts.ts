export enum ButtonColors {
  ICON_GREEN = 'btnIconGreen',
  ICON_WHITE = 'btnIconWhite',
  GREEN = 'btnGreen',
  YELLOW = 'btnYellow',
  BLACK = 'btnblack',
  GREY = 'btnGrey',
  RED = 'btnRed',
  GREY_GREEN = 'btnGreyGreen',
  WHITE = 'btnWhite',
}
