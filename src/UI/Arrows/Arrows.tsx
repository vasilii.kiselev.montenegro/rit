export const ArrowBack = () => {
  return <div style={{ color: '#2DAD96' }}>{'<'}</div>
}
export const ArrowNext = () => {
  return <div style={{ color: '#2DAD96' }}>{'>'}</div>
}
