import '@styles/index.scss'
import { AppRouter } from '@/routes/AppRouter'
import { BaseLayout } from '@/layouts/BaseLayout'
import { useEffect, useLayoutEffect } from 'react'
import { useAppDispatch } from './hooks/useAppDispatch'
import { Toast } from '@/UI/Toast'
import { useAppSelector } from './hooks/useAppSelector'
import { Tokens, UserStatus } from './types/user'
import { NoAuthorizationLayout } from './layouts/NoAuthorizationLayout'
import { ModalProvider } from './components/Modal/modalCotext'
import { getUser } from './api/profile/profile'
import { logIn, logOut } from './store/slices/userSlice/userSlice'
import { FreeLayout } from './layouts/FreeLayout'
import { getVacanciesThunk } from './store/slices/vacanciesSlice'
import { getCompaniesThunk } from './store/slices/companiesSlice'
import { getSlotsEventsThunk } from './store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsThunk'
import { DrawerProvider } from './components/Drawer/DrawerContext'
import { useLocation } from 'react-router-dom'
import { cleanState } from './store/slices/createVacancySlice/createVacancySlice'

function App() {
  const login = useAppSelector((state) => state.user.status)
  const location = useLocation()
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(getVacanciesThunk())
    dispatch(getCompaniesThunk())
    dispatch(getSlotsEventsThunk())
  })

  useEffect(() => {
    dispatch(cleanState())
  }, [dispatch, location.pathname])
  const access = localStorage.getItem(Tokens.ACCESS_TOKEN)
  const refresh = localStorage.getItem(Tokens.REFRESH_TOKEN)

  useLayoutEffect(() => {
    if (!refresh && !access) {
      dispatch(logOut())
    }
    getUser().then((data) => {
      if (data) {
        dispatch(logIn(data))
      } else {
        dispatch(logOut())
      }
    })
  }, [refresh, access])

  return (
    <ModalProvider>
      <DrawerProvider>
        <div className="app">
          {login === UserStatus.LOGIN && <BaseLayout content={<AppRouter />} />}
          {login === UserStatus.UNSET && <NoAuthorizationLayout content={<AppRouter />} />}
          {login === UserStatus.LOAD && <FreeLayout content={<AppRouter />} />}

          <Toast />
        </div>
      </DrawerProvider>
    </ModalProvider>
  )
}

export default App
