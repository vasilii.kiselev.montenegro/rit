import * as React from 'react'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import { MoreIcon } from '@/assets/icons/MoreIcon'
import cls from './Menu.module.scss'
import { Comment } from '@/UI/TextComponents'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
interface Props {
  id: number
}
export default function MenuDrop({ id }: Props) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const navigate = useNavigate()
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const navigateEditPage = () => {
    navigate(`${Url.NEW_EDIT}/${id}`)
  }
  return (
    <div>
      <ButtonIcon
        color="btnIconWhite"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <MoreIcon />
      </ButtonIcon>

      <Menu
        className={cls.menu}
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem className={cls.menuItem} onClick={navigateEditPage}>
          <Comment>Редактировать</Comment>
        </MenuItem>
        {/* <MenuItem className={cls.menuItem} onClick={handleClose}>
          <Comment>Открыть слот</Comment>
        </MenuItem>
        <MenuItem className={cls.menuItem} onClick={handleClose}>
          <Comment>Закрыть вакансию</Comment>
        </MenuItem> */}
      </Menu>
    </div>
  )
}
