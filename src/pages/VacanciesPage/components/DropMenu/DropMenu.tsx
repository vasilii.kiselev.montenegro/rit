import { Comment } from '@/UI/TextComponents'
import cls from './DropMenu.module.scss'

type Item = { label: string; handleClick: (id: number) => void }

interface DropMenuProps {
  id: number
}

export const DropMenu = ({ id }: DropMenuProps) => {
  const dropMenuItems: Item[] = [
    {
      label: 'Редактировать',
      handleClick: (id) => {
        console.log(id)
      },
    },
    {
      label: 'Открыть слот',
      handleClick: (id) => {
        console.log(id)
      },
    },
    {
      label: 'Закрыть вакансию',
      handleClick: (id) => {
        console.log(id)
      },
    },
  ]
  return (
    <div className={cls.DropMenu}>
      {dropMenuItems.map((el) => {
        return (
          <div
            className={cls.dropMenuItem}
            key={el.label}
            onClick={() => el.handleClick(id)}
          >
            <Comment>{el.label}</Comment>
          </div>
        )
      })}
    </div>
  )
}
