import { Vacancy } from '@/types/vacancy'
import cls from './VacanciesTableItem.module.scss'
import { PM3, PR3, PR4 } from '@/UI/TextComponents'
import { CopyIcon } from '@/assets/icons/CopyIcon'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import { useCopyToClipboard } from '@/hooks/useClipBoard'
import MenuDrop from '../Menu/Menu'
import { Url } from '@/routes/url'

interface VacanciesTableItemProps {
  vacancy: Vacancy
}

const vacancyStatus = {
  OPEN: { status: 'Открытая', color: '#151B33' },
  CLOSED: { status: 'Закрытая', color: '#F6AE20' },
  WITHOUT: { status: 'Без слота', color: '#EB4335' },
  ACTIVE: { status: 'Активная', color: '#2DAD96' },
}

export const VacanciesTableItem = ({ vacancy }: VacanciesTableItemProps) => {
  const { copy } = useCopyToClipboard()

  return (
    <div className={`${cls.VacanciesTableItem} ${vacancy?.status === 'WITHOUT' && cls.withoutSlot}`}>
      <div className={cls.vacancyItemName}>
        <img src={vacancy?.imgCompany} className={cls.vacanciesItemImg} alt={vacancy.company} />
        <PR4>{vacancy?.company}</PR4>
      </div>
      <div>
        <PR4>{vacancy?.name}</PR4>
      </div>
      <div>
        <PM3 style={{ textDecoration: 'underline' }}>{vacancy?.views}</PM3>
      </div>
      <div>
        <PM3 style={{ textDecoration: 'underline' }}>{vacancy?.passedTest}</PM3>
      </div>
      <div>
        <PM3 style={{ textDecoration: 'underline' }}>{vacancy.registration}</PM3>
      </div>
      <div>
        <PR3 style={{ color: vacancyStatus[vacancy?.status]?.color }}>{vacancyStatus[vacancy.status]?.status}</PR3>
      </div>
      <div className={cls.vacancyItemButtons}>
        <ButtonIcon onClick={() => copy(`${import.meta.env.VITE_RITY_LINK}${Url.VIEW_VACANCY}/${vacancy.id}`)} color="btnIconGreen">
          <CopyIcon />
        </ButtonIcon>
        {/* <ButtonIcon color="btnIconWhite">
          <CalendarIcon />
        </ButtonIcon> */}
        {/* <ButtonWithDrop id={vacancy.id} color="btnIconWhite">
          <MoreIcon />
        </ButtonWithDrop> */}
        <MenuDrop id={vacancy.id} />
      </div>
    </div>
  )
}
