import { Comment } from '@/UI/TextComponents'
import cls from './VacanciesTable.module.scss'
import { Vacancy } from '@/types/vacancy'
import { VacanciesTableItem } from '../VacanciesTableItem/VacanciesTableItem'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { reverseState, sortingNumbers, sortingWords } from '@/store/slices/vacanciesSlice'
import { useState } from 'react'
import { titles } from './utils'
import { Button } from '@/UI/Button/Button'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { FullArrowIcon } from '@/assets/icons/FullArrowIcon'
import { Url } from '@/routes/url'
import { useNavigate } from 'react-router-dom'
import { ButtonColors } from '@/UI/Button/consts'

interface VacanciesTableProps {
  vacanciesArr: Vacancy[]
}

export const VacanciesTable = ({ vacanciesArr }: VacanciesTableProps) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [targetSort, setTargetSort] = useState({
    name: '',
    targetCounter: 0,
  })

  const handleChangeSort = (text: string, sortType: string) => {
    if (text === targetSort.name && targetSort.targetCounter === 1) {
      setTargetSort((prev) => ({ ...prev, targetCounter: 0 }))
      dispatch(reverseState())
      return
    }
    setTargetSort((prev) => ({ ...prev, name: text, targetCounter: 1 }))
    if (sortType === 'word') {
      dispatch(sortingWords(text))
    } else {
      dispatch(sortingNumbers(text))
    }
  }

  return (
    <div className={cls.VacanciesTable}>
      <div className={`${cls.headVacanciesTable} ${cls.VacanciesTableGrid}`}>
        {titles.map((el) => {
          {
            return (
              <div key={el.name} className={cls.filterTitle}>
                <Comment key={el.name} style={{ cursor: 'pointer' }} onClick={() => handleChangeSort(el.name, el.sortType)}>
                  {el.label}
                </Comment>
                <div className={cls.filterTitleIcon}>
                  {el.name === targetSort.name && targetSort.targetCounter === 1 && <FullArrowIcon style={{ rotate: '180deg' }} />}
                  {el.name === targetSort.name && targetSort.targetCounter === 0 && <FullArrowIcon />}
                </div>
              </div>
            )
          }
        })}

        <Comment>Статус</Comment>
        <Comment>Ссылка</Comment>
      </div>
      <div className={cls.bodyVacanciesTable}>
        {vacanciesArr.length !== 0 ? (
          vacanciesArr.map((el) => <VacanciesTableItem key={el.id} vacancy={el} />)
        ) : (
          <div className={cls.emptyVacancies}>
            <Comment>Тут пока что нет вакансий</Comment>
            <Button color={ButtonColors.BLACK} onClick={() => navigate(Url.CREATE_VACANCY)}>
              добавить вакансию <PlusIcon />
            </Button>
          </div>
        )}
      </div>
    </div>
  )
}
