export const titles = [
  { name: 'company', label: 'Компания', sortType: 'word' },
  { name: 'name', label: 'Вакансия', sortType: 'word' },
  { name: 'views', label: 'Посмотрели вакансию', sortType: 'number' },
  { name: 'passedTest', label: 'Прошли тестирование', sortType: 'number' },
  {
    name: 'registration',
    label: 'Записались на собеседование',
    sortType: 'number',
  },
]
