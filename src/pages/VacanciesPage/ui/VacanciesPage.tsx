import { Comment, H1 } from '@/UI/TextComponents'
import cls from './VacanciesPage.module.scss'
import { Button } from '@/UI/Button/Button'
import { VacanciesTable } from '../components/VacanciesTable/VacanciesTable'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useEffect, useState } from 'react'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { openVacancies, closedVacancies, resetState } from '@/store/slices/vacanciesSlice'
import { PlusIcon } from '@/assets/icons/PlusIcon'

import { Url } from '@/routes/url'
import { useNavigate } from 'react-router-dom'
import { CustomPagination } from '@/UI/Pagination'
import { ButtonColors } from '@/UI/Button/consts'

export const VacanciesPage = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [openClosedSort, setOpenClosedSort] = useState({
    open: false,
    closed: false,
  })
  const [activePage, setActivePage] = useState(1)

  const vacanciesArr = useAppSelector((store) => store.vacancies.workState)

  useEffect(() => {
    dispatch(resetState())
    if (openClosedSort.closed) {
      dispatch(closedVacancies())
    }
    if (openClosedSort.open) {
      dispatch(openVacancies())
    }
  }, [openClosedSort])
  console.log(vacanciesArr)

  const paginationCount = Math.ceil(vacanciesArr?.length / 5)

  // const start = activePage === 1 ? 0 : activePage * 10 - 10
  // const end = activePage === 1 ? 10 : activePage * 10
  const start = activePage === 1 ? 0 : activePage * 5 - 5
  const end = activePage === 1 ? 5 : activePage * 5

  const openVacanciesCounter = typeof vacanciesArr === 'object' && (vacanciesArr?.filter((el) => el.status !== 'closed').length ?? 0)
  return (
    <div className={cls.VacanciesPage}>
      <div className={cls.pageHeading}>
        <div className={cls.pageHeadingLeftSide}>
          <H1>Список вакансий</H1>

          <Comment>{openVacanciesCounter} вакансий открытых</Comment>
        </div>
        <div className={cls.pageHeadingRightSide}>
          <div className={cls.pageHeadingRightSideFilterButtons}>
            <Button
              color={ButtonColors.YELLOW}
              className={openClosedSort.closed && 'active'}
              onClick={() =>
                setOpenClosedSort((prev) => ({
                  ...prev,
                  closed: !prev.closed,
                  open: false,
                }))
              }
            >
              Закрытые
            </Button>
            <Button
              color={ButtonColors.YELLOW}
              className={openClosedSort.closed && 'active'}
              onClick={() =>
                setOpenClosedSort((prev) => ({
                  ...prev,
                  open: !prev.open,
                  closed: false,
                }))
              }
            >
              Открытые
            </Button>
          </div>
          <Button color={ButtonColors.BLACK} onClick={() => navigate(Url.CREATE_VACANCY)}>
            Добавить вакансию <PlusIcon />
          </Button>
        </div>
      </div>
      <VacanciesTable vacanciesArr={vacanciesArr.slice(start, end)} />

      <CustomPagination setActivePage={setActivePage} paginationCount={paginationCount} activePage={activePage} />
    </div>
  )
}
