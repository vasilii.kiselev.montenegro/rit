import cls from './CompaniesPage.module.scss'
import { Comment, H1 } from '@/UI/TextComponents'
import { Button } from '@/UI/Button/Button'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { useAppSelector } from '@/hooks/useAppSelector'
import { CompaniesTable } from '../components/CompanyTable/CompaniesTable'
import { useContext, useState } from 'react'
import { CustomPagination } from '@/UI/Pagination'
import { CompanyForm } from '@components/CompanyForm'
import { SendCompany } from '@/types/company'
import { DrawerContext } from '@/components/Drawer/DrawerContext'
import { ButtonColors } from '@/UI/Button/consts'

export const CompaniesPage = () => {
  const companiesArr = useAppSelector((state) => state.companies.workState)
  const [activePage, setActivePage] = useState(1)
  const { handleDrawer } = useContext(DrawerContext)

  const handleOpenDrawerWithCompany = (companyInfo: SendCompany, companyOperation: 'new' | 'change') => {
    handleDrawer(
      <CompanyForm operation={companyOperation} company={companyInfo} setClose={handleDrawer} />,
      <H1>{companyOperation ? 'Редактирование компании ' : 'Добавление новой компании'}</H1>
    )
  }

  const paginationCount = Math.ceil(companiesArr.length / 5)

  const start = activePage === 1 ? 0 : activePage * 5 - 5
  const end = activePage === 1 ? 5 : activePage * 5

  return (
    <>
      <div className={cls.CompaniesPage}>
        <div className={cls.pageHeading}>
          <div className={cls.pageHeadingLeftSide}>
            <H1>Список компаний</H1>
            <Comment>{companiesArr.length} компаний</Comment>
          </div>
          <div className={cls.pageHeadingRightSide}>
            <Button
              color={ButtonColors.BLACK}
              onClick={() => {
                handleOpenDrawerWithCompany(null, 'new')
              }}
            >
              Добавить компанию <PlusIcon />
            </Button>
          </div>
        </div>
        <CompaniesTable companiesArr={companiesArr.slice(start, end)} setOpenDrawer={handleOpenDrawerWithCompany} />
        <CustomPagination setActivePage={setActivePage} paginationCount={paginationCount} activePage={activePage} />
      </div>
    </>
  )
}
