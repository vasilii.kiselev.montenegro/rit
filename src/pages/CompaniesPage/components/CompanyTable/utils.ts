export const titles = [
  { name: 'companyLogo', label: 'Логотип', sortType: '' },
  { name: 'name', label: 'Наименование компании', sortType: 'word' },
  { name: 'description', label: 'Описание', sortType: '' },
  {
    name: 'numberOfVacancies',
    label: 'Количество вакансий',
    sortType: 'number',
  },
  {
    name: 'interviewsAreScheduled',
    label: 'Назначено собеседований',
    sortType: 'number',
  },
]
