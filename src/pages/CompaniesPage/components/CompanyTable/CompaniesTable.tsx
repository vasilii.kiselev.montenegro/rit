import { Comment } from '@/UI/TextComponents'
import cls from './CompaniesTable.module.scss'
import { Button } from '@/UI/Button/Button'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { FullCompany, SendCompany } from '@/types/company'
import { CompaniesTableItem } from '../CompaniesTableItem/CompaniesTableItem'
import React, { useState } from 'react'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { titles } from './utils'
import { companyReverseState, companySortingNumbers, companySortingWords } from '@/store/slices/companiesSlice'
import { FullArrowIcon } from '@/assets/icons/FullArrowIcon'
import { ButtonColors } from '@/UI/Button/consts'

interface CompaniesTableProps {
  companiesArr: FullCompany[]
  setOpenDrawer: (companyInfo: SendCompany, companyOperation: 'new' | 'change') => void
}

export const CompaniesTable = ({ companiesArr, setOpenDrawer }: CompaniesTableProps) => {
  const dispatch = useAppDispatch()
  const [targetSort, setTargetSort] = useState({
    name: '',
    targetCounter: 0,
  })

  const handleChangeSort = (text: string, sortType: string) => {
    if (text === targetSort.name && targetSort.targetCounter === 1) {
      setTargetSort((prev) => ({ ...prev, targetCounter: 0 }))
      dispatch(companyReverseState())
      return
    }
    setTargetSort((prev) => ({ ...prev, name: text, targetCounter: 1 }))
    if (sortType === 'word') {
      dispatch(companySortingWords())
    } else {
      {/*@ts-expect-error */}
      dispatch(companySortingNumbers(text))
    }
  }

  return (
    <div className={cls.CompaniesTable}>
      <div className={`${cls.headCompaniesTable} ${cls.CompaniesTableGrid}`}>
        {titles.map((el, id) => {
          {
            return (
              <React.Fragment key={`${el.label} ${id}`}>
                {el.sortType.length ? (
                  <div className={cls.filterTitle}>
                    <Comment key={el.name} style={{ cursor: 'pointer' }} onClick={() => handleChangeSort(el.name, el.sortType)}>
                      {el.label}
                    </Comment>
                    <div className={cls.filterTitleIcon}>
                      {el.name === targetSort.name && targetSort.targetCounter === 1 && <FullArrowIcon style={{ rotate: '180deg' }} />}
                      {el.name === targetSort.name && targetSort.targetCounter === 0 && <FullArrowIcon />}
                    </div>
                  </div>
                ) : (
                  <Comment key={el.name}>{el.label}</Comment>
                )}
              </React.Fragment>
            )
          }
        })}
      </div>
      <div className={cls.bodyCompaniesTable}>
        {companiesArr.length !== 0 ? (
          <>
            {companiesArr.map((el) => (
              <CompaniesTableItem setOpenDrawer={setOpenDrawer} key={el.id} company={el} />
            ))}
          </>
        ) : (
          <div className={cls.emptyCompanies}>
            <Comment>Тут пока что нет компаний</Comment>
            <Button
              color={ButtonColors.BLACK}
              onClick={() => {
                setOpenDrawer(null, 'new')
              }}
            >
              добавить компанию <PlusIcon />
            </Button>
          </div>
        )}
      </div>
    </div>
  )
}
