import { FullCompany, SendCompany } from '@/types/company'
import cls from './CompaniesTableItem.module.scss'
import { PM3, PR4 } from '@/UI/TextComponents'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import { EyeIcon } from '@/assets/icons/EyeIcon'
import { EditIcon } from '@/assets/icons/EditIcon'
import { TickCircleIcon } from '@/assets/icons/TickCircleIcon'
import { InfoCircleIcon } from '@/assets/icons/InfoCircleIcon'
import { useContext } from 'react'
import { ModalContext } from '@/components/Modal/modalCotext'
import { CompanyModalView } from '@/components/CompanyModalView/CompanyModalView'

interface CompaniesTableItemProps {
  company: FullCompany
  setOpenDrawer: (companyInfo: SendCompany, companyOperation: 'new' | 'change') => void
}

export const CompaniesTableItem = ({ setOpenDrawer, company }: CompaniesTableItemProps) => {
  const { handleModal } = useContext(ModalContext)

  const handleOpenModalWithCompany = (company: FullCompany) => {
    handleModal(<CompanyModalView company={company} />)
  }
  return (
    <div className={`${cls.CompaniesTableItem} `}>
      <div className={cls.companyItemName}>
        <img src={company.logo} className={cls.companyItemImg} alt={company.name} />
      </div>
      <div>
        <PR4>{company.name}</PR4>
      </div>
      <div className={cls.companyItemDescriprion}>
        <PR4>{company.description}</PR4>
      </div>
      <div>
        <PM3 style={{ textDecoration: 'underline' }}>{company.numberOfVacancies}</PM3>
      </div>
      <div>
        <PM3 style={{ textDecoration: 'underline' }}>{company.interviewsAreScheduled}</PM3>
      </div>
      <div className={cls.companyItemButtons}>
        <ButtonIcon color="btnIconGreen" onClick={() => handleOpenModalWithCompany(company)}>
          <EyeIcon />
        </ButtonIcon>
        <ButtonIcon
          color="btnIconWhite"
          onClick={() => {
            setOpenDrawer(company, 'change')
          }}
        >
          <EditIcon />
        </ButtonIcon>
        {company.approvalStatus ? (
          <TickCircleIcon />
        ) : (
          <div className={cls.verificationHover}>
            <div>
              <span>
                <InfoCircleIcon /> Компания не верифицированая
              </span>{' '}
              <a href="mailto:admin@rity.app?subject=стать проверенной компанией">Верифицировать</a>{' '}
            </div>
            <InfoCircleIcon />
          </div>
        )}
      </div>
    </div>
  )
}
