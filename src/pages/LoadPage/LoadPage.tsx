import { Logo } from '@/assets/images/Logo'
import cls from './LoadPage.module.scss'

export const LoadPage = () => {
  return (
    <div className={cls.LoadPage}>
      <Logo />
    </div>
  )
}
