import { useAppSelector } from '@/hooks/useAppSelector'
import cls from './UserPage.module.scss'
import { UserInfo } from '../components/UserInfio/UserInfo'
import { useState } from 'react'
import { H1, PM1 } from '@/UI/TextComponents'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { EditIcon } from '@/assets/icons/EditIcon'
import { TextInput } from '@/UI/TextInput/TextInput'
import { useFormik } from 'formik'
import { InfoCircleIcon } from '@/assets/icons/InfoCircleIcon'
import { TickCircleIcon } from '@/assets/icons/TickCircleIcon'
import { Drawer } from '@/components/Drawer'
import { ChangePasswordForm } from '../components/ChangePasswordForm/ChangePasswordForm'
import { UserForm } from '@/types/user'
import * as Yup from 'yup'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { changeUserValues } from '@/api/profile/profile'
import { setNewFormValues } from '@/store/slices/userSlice/userSlice'

export const UserPage = () => {
  const user = useAppSelector((state) => state.user.user)
  const dispatch = useAppDispatch()
  const [chageUserData, setChangeUserData] = useState<boolean>(false)
  const [isOpenDrawer, setIsOpenDrower] = useState<boolean>(false)
  console.log(user)

  const valiadtionSchema = Yup.object<UserForm>().shape({
    company: Yup.string().required(),
    email: Yup.string().required(),
    firstName: Yup.string().required(),
    lastName: Yup.string().required(),
  })

  const closeDrawer = () => {
    setIsOpenDrower(false)
  }
  const formik = useFormik<UserForm>({
    initialValues: user,
    validationSchema: valiadtionSchema,
    onSubmit(values, formikHelpers) {
      console.log(values, formikHelpers)
      changeUserValues(values).then((data) => {
        if (data) {
          dispatch(setNewFormValues(values))
        }
      })
    },
  })
  const reverseChangeUserMode = () => {
    setChangeUserData((prev) => !prev)
  }
  console.log(formik.errors)

  return (
    <div className={`${cls.UserPage} ${chageUserData ? cls.active : ''}`}>
      <div className={cls.UserPageContent}>
        <UserInfo user={user} />
        <div className={cls.userData}>
          <div className={cls.userDataHeader}>
            <H1>Данные профиля</H1>
            <div className={cls.changeButtons}>
              {!chageUserData && (
                <Button
                  onClick={() => setIsOpenDrower(true)}
                  className={cls.EditIcon}
                  color={ButtonColors.ICON_GREEN}
                >
                  Сменить пароль
                </Button>
              )}
              {!chageUserData && (
                <Button
                  onClick={reverseChangeUserMode}
                  className={cls.EditIcon}
                  color={ButtonColors.ICON_GREEN}
                >
                  <EditIcon /> Редактировать
                </Button>
              )}
            </div>
          </div>
          <div className={cls.userDataItems}>
            <div className={cls.userDataItem}>
              <PM1>Имя</PM1>
              <TextInput
                disabled={!chageUserData}
                text={formik.values.firstName}
                onChange={formik.handleChange}
                name="firstName"
                placeholder="Введите имя"
              />
            </div>
            <div className={cls.userDataItem}>
              <PM1>Фамилия</PM1>
              <TextInput
                disabled={!chageUserData}
                text={formik.values.lastName}
                onChange={formik.handleChange}
                name="lastName"
                placeholder="Введите имя"
              />
            </div>
            {/* <div className={cls.userDataItem}>
            <PM1>Фамилия</PM1>
            <TextInput
            disabled={!chageUserData}
              text={formik.values.}
              onChange={formik.handleChange}
              name=""
              placeholder=""
            />
          </div> */}

            <div className={cls.userDataItem}>
              <PM1>E-mail</PM1>
              <TextInput
                disabled={!chageUserData}
                text={formik.values.email}
                onChange={formik.handleChange}
                name="email"
                placeholder="Введите логин"
              />
            </div>

            <div className={cls.userDataItem}>
              <PM1>Компания</PM1>
              <div className={cls.userDataSpecialItem}>
                <div className={cls.userDataSpecialItemInputCont}>
                  <TextInput
                    disabled={!chageUserData}
                    text={formik.values.company}
                    onChange={formik.handleChange}
                    name="company"
                    placeholder="Введите название компании"
                  />
                </div>

                {user?.companyVerificated ? (
                  <div className={cls.userDataSpecialItemCompanyTrue}>
                    <TickCircleIcon />
                    Верифицированно
                  </div>
                ) : (
                  <div className={cls.userDataSpecialItemCompanyFalse}>
                    <InfoCircleIcon />
                    Не верифицированно
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {chageUserData && (
        <div className={cls.footer}>
          <Button
            onClick={reverseChangeUserMode}
            color={ButtonColors.ICON_WHITE}
          >
            Отменить
          </Button>

          <Button
            color={ButtonColors.GREEN}
            disabled={!!Object.entries(formik.errors).length}
            onClick={formik.handleSubmit}
          >
            Сохранить
          </Button>
        </div>
      )}
      <Drawer
        isOpen={isOpenDrawer}
        closeDrawer={setIsOpenDrower}
        head={<H1>Смена пароля</H1>}
        body={
          <ChangePasswordForm
            closeDrawer={closeDrawer}
            resetPasswordUrl={user?.buttonChangePassword}
          />
        }
      />
    </div>
  )
}
