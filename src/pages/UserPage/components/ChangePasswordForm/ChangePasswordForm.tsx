import { useFormik } from 'formik'
import cls from './ChangePasswordForm.module.scss'
import { PM1 } from '@/UI/TextComponents'
import { TextInput } from '@/UI/TextInput/TextInput'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { resetPassword } from '@/api/profile/profile'
import * as Yup from 'yup'
interface ChangePasswordFormProps {
  closeDrawer: () => void
  resetPasswordUrl: string
}

export const ChangePasswordForm = ({
  closeDrawer,
  resetPasswordUrl,
}: ChangePasswordFormProps) => {
  const initValues = {
    password: '',
    secondPassword: '',
  }
  const validationSchema = Yup.object().shape({
    password: Yup.string()
      .required('Пароль обязателен')
      .min(8, 'Пароль должен содержать минимум 8 символов'),
    secondPassword: Yup.string()
      .oneOf([Yup.ref('password'), null], 'Пароли должны совпадать')
      .required('Подтверждение пароля обязательно'),
  })
  const formik = useFormik({
    initialValues: initValues,
    validationSchema: validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit(values, formikHelpers) {
      console.log(values, formikHelpers)
      resetPassword(resetPasswordUrl, values).then((data) => {
        if (!data.errors) {
          closeDrawer()
        }
      })

      // formik.setErrors()
    },
  })
  console.log(formik.errors)

  return (
    <div className={cls.ChangePasswordForm}>
      <div className={cls.userDataItems}>
        <div className={cls.userDataItem}>
          <PM1>
            Введите логин <span className={cls.redStar}>*</span>
          </PM1>
          <TextInput
            text={formik.values.password}
            onChange={formik.handleChange}
            name="password"
            placeholder="Введите логин"
            type="password"
          />
        </div>
        <div className={cls.userDataItem}>
          <PM1>
            Подтвердите новый пароль <span className={cls.redStar}>*</span>
          </PM1>
          <TextInput
            text={formik.values.secondPassword}
            onChange={formik.handleChange}
            name="secondPassword"
            placeholder="Введите логин"
            type="password"
          />
        </div>
        <div className={cls.errors}>
          {Object.values(formik.errors).map((el) => {
            return <div className={cls.error}>{el} </div>
          })}{' '}
        </div>
      </div>
      <div className={cls.footer}>
        <Button color={ButtonColors.ICON_WHITE} onClick={closeDrawer}>
          Отменить
        </Button>

        <Button color={ButtonColors.GREEN} onClick={formik.handleSubmit}>
          Сохранить
        </Button>
      </div>
    </div>
  )
}
