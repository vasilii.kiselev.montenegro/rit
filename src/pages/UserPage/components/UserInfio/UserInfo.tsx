import { User } from '@/types/user'
import cls from './UserInfo.module.scss'
import { UserDefaultImg } from '@/assets/images/UserDefaultImg'
import { Comment, PM2, PM3, PR3 } from '@/UI/TextComponents'
import { ProgressIcon } from '@/assets/icons/ProgressIcon'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { logOut } from '@/store/slices/userSlice/userSlice'
interface UserInfoProps {
  user: User
}

export const UserInfo = ({ user }: UserInfoProps) => {
  const dispatch = useAppDispatch()

  return (
    <div className={cls.userInfo}>
      <div className={cls.userLogo}>
        {user?.img ? <img src={user?.img} alt="" /> : <UserDefaultImg className={cls.userLogoImg} />}
        <span className={cls.changeAvatar}>Изменить аватар</span>
      </div>
      <div className={cls.userInfoSeparator}> </div>
      <div className={cls.userNameLogin}>
        <PM2>
          {user?.firstName} {user?.lastName}
        </PM2>
        <PM3>@{user?.username} </PM3>
      </div>
      <div className={cls.userInfoSeparator}> </div>

      <div className={cls.userExperience}>
        <div className={cls.userExperienceItem}>
          <Comment>Дата регистрации</Comment>
          <PR3>{user?.dateJoined}</PR3>
        </div>
        <div className={cls.userExperienceItem}>
          <Comment>Последнее обновление</Comment>
          <PR3>00/00/00</PR3>
        </div>
        <div className={cls.userExperienceItem}>
          <Comment>Последний логин</Comment>
          <PR3>{user?.lastLogin}</PR3>
        </div>
        <div className={cls.userVacanciesCounter}>
          <ProgressIcon /> Вакансий: {user?.vacanciesCount}
        </div>
        <Button onClick={() => dispatch(logOut())} className={cls.quitBtn} color={ButtonColors.RED}>
          Выйти
        </Button>
      </div>
    </div>
  )
}
