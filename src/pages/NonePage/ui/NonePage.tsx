import { H1 } from '@/UI/TextComponents'
import cls from './NonePage.module.scss'

export const NonePage = () => {
  return <H1 className={cls.NonePage}>404</H1>
}
