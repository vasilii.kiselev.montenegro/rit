import { PR2 } from '@/UI/TextComponents'

export const HomePage = () => {
  return (
    <div>
      <PR2>
        AI инструментарий упакованный в вёб приложение, обогощающее
        автоматизированными средствами рабочий процесс hr служб компаний,
        самозанятых рекрутёров и специалистов смежных областей. Виртуальное
        рабочее пространство.
      </PR2>
      <div style={{ height: '20px' }}></div>
      <div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
        <PR2>
          {' '}
          backend deploymend admin panel and api{' '}
          <a
            target="_blank"
            style={{ color: '#2dad96' }}
            href="https://rity.app/admin"
          >
            {' '}
            https://rity.api
          </a>
        </PR2>
        <PR2>
          {' '}
          self-hosted git{' '}
          <a
            target="_blank"
            style={{ color: '#2dad96' }}
            href="https://rity.fun"
          >
            https://rity.fun
          </a>{' '}
        </PR2>
        <PR2>
          self-hosted сервер видео конверенций{' '}
          <a
            target="_blank"
            style={{ color: '#2dad96' }}
            href="https://RIsmarTY.ru/"
          >
            https://RIsmarTY.ru/
          </a>
          <div></div>
          <a
            target="_blank"
            style={{ color: '#2dad96' }}
            href="https://riSMARTy.ru/"
          >
            {' '}
            https://riSMARTy.ru/
          </a>
        </PR2>
      </div>
      <div style={{ height: '20px' }}></div>

      <PR2>
        Инновационная, высокотехнологичная платформа, предоставляющая
        комплексные решения для в области управления человеческими ресурсами,
        ориентированная как на кадровые отделых крупных компаний , так и на
        самозанятых рекрутеров, включая специалистов смежных областей.
      </PR2>
      <div style={{ height: '20px' }}></div>

      <PR2>
        Наша платформа предлагает виртуальное рабочее пространство с интеграцией
        передовых автоматизированных инструментов для манипулированием воронкой
        соискателей.
      </PR2>
      <div style={{ height: '20px' }}></div>

      <PR2>Умный помощник составит описание вакансии</PR2>
      <PR2>
        AI оператор сэкономит вам время проведёт собеседование Самостоятельно -
        Оценит кандидата и выдаст короткое summary
      </PR2>
      <div style={{ height: '20px' }}></div>

      <PR2>
        Гибкий календарь составит комфортный график Самостоятельно назначит
        встречи, и подстроиться под ваше рассписание
      </PR2>
      <div style={{ height: '20px' }}></div>

      <PR2>
        {' '}
        Self-hosted Видео Конференции с методами и алгоритмами глубокого
        обучения - для анализа видео в режиме реального времени - функция
        помогает рекрутёру понять эмоциональные реакции соискателя -
        способствует более успешному отбору кандидатов
      </PR2>
      <div style={{ height: '20px' }}></div>

      <a
        target="_blank"
        style={{ color: '#2dad96' }}
        href="https://rismarty.ru/"
      >
        {' '}
        https://rismarty.ru/
      </a>
    </div>
  )
}
