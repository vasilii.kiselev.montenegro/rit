import { useEffect, useState } from 'react'
import { Stepper } from '../components/stepper/Stepper'
import cls from './EditVacancy.module.scss'
import { FullVacancy } from '@/types/vacancy'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { Button } from '@/UI/Button/Button'
import { FirstStep } from '../steps/FirstStep/FirstStep'
import { useNavigate, useParams } from 'react-router-dom'
import { getVacany } from '@/api/vacancies/vacancies'
import { Url } from '@/routes/url'
import { SecondStep } from '../steps/SecondStep/SecondStep'
import { sendMiniSecondStep, sendSecondStep } from '@/api/steps/steps'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { getVacanciesThunk } from '@/store/slices/vacanciesSlice'
import { ButtonColors } from '@/UI/Button/consts'

export interface StepEditVacancy {
  target: number
  steps: {
    first: boolean
    second: boolean
  }
}

type InitialValues = FullVacancy
const initialValues: InitialValues = {
  companyId: null,
  description: '',
  jobRequirements: '',
  name: '',
  wages: '',
  currency: '',
  placeOfWork: '',
  workExperience: '',
  workSchedule: '',
  testingMethod: '',
}
export const EditVacancy = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { vacancyId } = useParams()
  const valiadtionSchema = Yup.object<FullVacancy>().shape({
    company: Yup.string().required(),
    description: Yup.string().required(),
    name: Yup.string().required(),
    jobRequirements: Yup.string().required(),
  })

  const [step, setStep] = useState<StepEditVacancy>({
    target: 0,
    steps: {
      first: false,
      second: false,
    },
  })
  const [validateWages, setValidateWages] = useState<boolean>(true)
  const [question, setQuestion] = useState<boolean | string>('')
  const handleSubmit = async (values) => {
    console.log('submit', values)
    await sendSecondStep(values, { id: values.id }, values.companyId, {
      question: question,
      testingMethod: values.testingMethod,
    })
    navigate(Url.MAIN)
    await dispatch(getVacanciesThunk())
  }
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: valiadtionSchema,
    // validateOnBlur: false,
    validateOnChange: false,
    onSubmit: handleSubmit,
  })
  const generateQuestion = async () => {
    console.log(
      formik.getFieldMeta('name').touched,
      formik.getFieldMeta('description').touched,
      formik.getFieldMeta('jobRequirements').touched
    )
    if (
      formik.getFieldMeta('name').touched &&
      formik.getFieldMeta('description').touched &&
      formik.getFieldMeta('jobRequirements').touched
    ) {
      await sendMiniSecondStep(
        formik.values,
        { id: formik.values.id },
        formik.values.companyId
      )
      // await generateQuestionWhithoutResponse(formik.values.id)
    }
  }
  useEffect(() => {
    console.log('first')

    generateQuestion()
  }, [
    formik.values.name,
    formik.values.description,
    formik.values.jobRequirements,
  ])
  useEffect(() => {
    if (+formik.values.wages && formik.values.wages?.length) {
      setValidateWages(true)
    } else if (!+formik.values.wages && formik.values.wages?.length) {
      setValidateWages(false)
    } else if (!formik.values.wages?.length) {
      setValidateWages(true)
    }
  }, [formik.values.wages])

  useEffect(() => {
    getVacany(vacancyId).then((data) => {
      formik.setValues(data)
    })
  }, [])

  useEffect(() => {
    if (
      formik.values.name &&
      formik.values.description &&
      formik.values.jobRequirements &&
      formik.values.company &&
      validateWages
    ) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, first: true } }))
    } else {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, first: false } }))
    }
  }, [
    formik.values.name,
    formik.values.description,
    formik.values.jobRequirements,
    formik.values.company,
    validateWages,
  ])
  const prevStep = () => {
    setStep((prev) => ({ ...prev, target: step.target - 1 }))
  }
  const nextStep = () => {
    setStep((prev) => ({ ...prev, target: step.target + 1 }))
  }

  useEffect(() => {
    if (question) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: true } }))
    } else {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: false } }))
    }
  }, [question])
  const sendFirstStep = () => {
    formik.handleSubmit()
    nextStep()
  }
  return (
    <div className={cls.EditVacancy}>
      <div>
        <Stepper step={step} />
        <div className={cls.formContainer}>
          {step.target === 0 && (
            <FirstStep formik={formik} validateWages={validateWages} />
          )}
          {step.target === 1 && (
            <SecondStep
              setQuestion={setQuestion}
              setTestingMethod={(data) =>
                formik.setFieldValue('testingMethod', data)
              }
              vacancyId={formik.values.id}
            />
          )}
        </div>
      </div>
      <div className={cls.footer}>
        {step.target === 0 ? (
          <Button
            onClick={() => navigate(Url.VACANSY)}
            color={ButtonColors.ICON_GREEN}
          >
            Отменить
          </Button>
        ) : (
          <Button onClick={prevStep} color={ButtonColors.ICON_GREEN}>
            Назад
          </Button>
        )}
        {step.target === 0 ? (
          <Button
            disabled={!step.steps.first}
            onClick={() => {
              // sendFirstStep()
              nextStep()
            }}
            color={ButtonColors.ICON_GREEN}
          >
            Далее
          </Button>
        ) : (
          <Button
            disabled={!step.steps.second}
            onClick={formik.handleSubmit}
            color={ButtonColors.ICON_GREEN}
          >
            Отправить
          </Button>
        )}
      </div>
    </div>
  )
}
