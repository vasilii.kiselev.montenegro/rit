import { TextInput } from '@/UI/TextInput/TextInput'
import cls from './FirstStep.module.scss'
import { PM2, PR4 } from '@/UI/TextComponents'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { TextArea } from '@/UI/TextArea/TextArea'
import { SelectCompany } from '../../components/SelectCompany/SelectCompany'
import { getCurrencies, getShedules } from '@/api/steps/steps'
import { useEffect, useState } from 'react'
import { InputSelect } from '@/UI/InputSelect/InputSelect'
import { useAppSelector } from '@/hooks/useAppSelector'

interface FirstStepProps {}

export const FirstStep = ({ formik, validateWages }: FirstStepProps) => {
  const companies = useAppSelector((state) => state.companies.workState)
  const [currencies, setCurrencies] = useState<string[]>([])
  const [shedules, setShedules] = useState<string[]>([])
  const getCurr = async () => {
    const data = await getCurrencies()
    setCurrencies(data)
  }
  const getShed = async () => {
    const data = await getShedules()
    setShedules(data)
  }
  useEffect(() => {
    getCurr()
    getShed()
  }, [])
  console.log(formik.values)

  return (
    <>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Компания<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <div className={cls.inputAndError}>
          <SelectCompany
            values={companies}
            name="companyId"
            value={formik.values.companyId}
            setValue={formik.handleChange}
            // setValue={(data) => formik.setFieldValue('company', data)}
          />
          {formik.errors.company && (
            <span style={{ color: '#E60019' }}>
              Поле "Компания" обязательное
            </span>
          )}
        </div>
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Название<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <div className={cls.secondStepInputLabel}>
          <TextInput
            name="name"
            text={formik.values.name}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            placeholder="Введите текст"
          />
          {formik.errors.name && formik.touched.name ? (
            <span style={{ color: '#E60019' }}>
              Поле "Название" обязательное
            </span>
          ) : null}
        </div>
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Описание вакансии<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <div className={cls.secondStepInputLabel}>
          <TextArea
            rows={3}
            text={formik.values.description}
            name="description"
            placeholder="Введите текст"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.description && formik.touched.description ? (
            <span style={{ color: '#E60019' }}>
              Поле "Описание вакансии" обязательное
            </span>
          ) : null}
        </div>
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Требования к вакансии<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <div className={cls.secondStepInputLabel}>
          <TextArea
            rows={3}
            placeholder="Введите текст"
            name="jobRequirements"
            text={formik.values.jobRequirements}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.errors.jobRequirements && formik.touched.jobRequirements ? (
            <span style={{ color: '#E60019' }}>
              Поле "Требования к вакансии" обязательное
            </span>
          ) : null}
        </div>
      </div>
      <div className={`${cls.secondStepInputLabel} `}>
        <PM2>Заработная плата за ваш труд / валюта</PM2>
        <div className={`${cls.secondStepInputLabelSelect}`}>
          <div
            style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}
          >
            <TextInput
              placeholder="Введите текст"
              name="wages"
              text={formik.values.wages}
              onChange={formik.handleChange}
            />
            {!validateWages && (
              <PR4 style={{ color: '#E60019' }}>
                Это поле принимат числовое значение
              </PR4>
            )}
          </div>
          <InputSelectMini
            values={currencies}
            setValue={formik.handleChange}
            name="currency"
            value={formik.values.currency}
          />
        </div>
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>График работы</PM2>
        <InputSelect
          values={shedules}
          name="workSchedule"
          value={formik.values.workSchedule}
          setValue={formik.handleChange}
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>Опыт работы</PM2>
        <TextInput
          placeholder="Введите текст"
          name="workExperience"
          text={formik.values.workExperience}
          onChange={formik.handleChange}
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>Место работы</PM2>
        <TextInput
          placeholder="Введите текст"
          name="placeOfWork"
          text={formik.values.placeOfWork}
          onChange={formik.handleChange}
        />
      </div>
    </>
  )
}
