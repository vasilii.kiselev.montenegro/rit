import { Comment, PR2 } from '@/UI/TextComponents'
import { DropDownSection } from '../../DropDownSection/DropDownSection'
import cls from './QuestionSettings.module.scss'
import { useState } from 'react'
import { Button } from '@/UI/Button/Button'
import { TextInput } from '@/UI/TextInput/TextInput'
import { Tooltip } from '@/UI/Tooltip/Tooltip'

enum Mode {
  'Do not apply',
  average,
  high,
}

export const QuestionSettings = () => {
  const [mode, setMode] = useState<Mode>(1)
  const [timeActive, setTimeActive] = useState<boolean>(false)
  const [time, setTime] = useState<string>('24')

  const handleSetTime = (e) => {
    const text = e.target.value

    const numberText = text
      ?.split('')
      .filter((el) => !isNaN(el))
      .join('')
    const maxText = +numberText > 48 ? 48 : numberText
    setTime(maxText)
  }

  const form = (
    <>
      <PR2 className={cls.qustionSettingsLabel}>
        Режим отбора{' '}
        <Tooltip>
          <Comment>
            <strong>Не применять</strong> - собеседования назначаются всегда при
            наличии ответа, при этом вы увидите результат оценки в карточке
            кандидата.
          </Comment>
          <Comment>
            <strong>Средний</strong> - собеседования назначаются со
            специалистами оценёнными на 5 из 10, кандидаты с меньшей оценкой до
            собеседования не допускаются, но вы сможете их назначить на
            собеседование вручную.
          </Comment>
          <Comment>
            <strong>Высокий</strong> - собеседования назначаются со
            специалистами оценёнными на 7 из 10, кандидаты с меньшей оценкой до
            собеседования не допускаются, но вы сможете их назначить на
            собеседование вручную.
          </Comment>
        </Tooltip>
      </PR2>
      <div className={cls.qustionSettingsBtnsCont}>
        <Button
          onClick={() => setMode(0)}
          color="btnGreyGreen"
          className={mode === 0 && 'active'}
        >
          Не применять
        </Button>
        <Button
          onClick={() => setMode(1)}
          color="btnGreyGreen"
          className={mode === 1 && 'active'}
        >
          Средний
        </Button>
        <Button
          onClick={() => setMode(2)}
          color="btnGreyGreen"
          className={mode === 2 && 'active'}
        >
          Высокий
        </Button>
      </div>
      <div className={cls.qustionSettingsTimeCheckBox}>
        <span
          className={`${cls.qustionSettingsTimeCheckBoxIcon} ${
            timeActive && cls.active
          }`}
          onClick={() => setTimeActive((prev) => !prev)}
        >
          &#10003;
        </span>
        <PR2 className={cls.qustionSettingsTimeCheckBoxTime}>
          Дать возможность пройти переоценку через{' '}
          <TextInput text={time} onChange={handleSetTime} />
          часа
        </PR2>
      </div>
      <Comment>*Ограничение 48 часов</Comment>
    </>
  )

  return <DropDownSection label="Настройки тестирования" form={form} />
}
