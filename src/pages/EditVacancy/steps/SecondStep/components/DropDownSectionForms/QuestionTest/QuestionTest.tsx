import { PM3, PR2 } from '@/UI/TextComponents'
import { DropDownSection } from '../../DropDownSection/DropDownSection'
import cls from './QuestionTest.module.scss'
import { TextArea } from '@/UI/TextArea/TextArea'
import { useState } from 'react'
import { Button } from '@/UI/Button/Button'
import { Skeleton } from '@mui/material'
import { getAnswer } from '@/api/question'

interface QuestionTestProps {
  questionId?: number
}

type Answer = {
  estimation: string
  answer: string
}

export const QuestionTest = ({ questionId = 0 }: QuestionTestProps) => {
  const [text, setText] = useState<string>()
  const [answer, setAnswer] = useState<Answer>()
  const [answerLoading, setAnswerLoading] = useState<boolean>(false)
  console.log(questionId, text)

  const sendAnswer = async () => {
    setAnswerLoading(true)
    setAnswer(null)

    const data = await getAnswer(questionId, text)
    setAnswer(data)
    setAnswerLoading(false)
  }
  const clearForm = () => {
    setAnswer(null)
    setText('')
  }

  const form = (
    <div className={cls.QuestionTestCont}>
      <PM3>Ваш ответ</PM3>
      <TextArea
        placeholder="Протестируйте оценку роботом, введите свой ответ"
        text={text}
        onChange={(e) => setText(e.target.value)}
        rows={3}
      />
      <div className={cls.questionTestBtnsCont}>
        <Button color="btnGreen" onClick={sendAnswer}>
          Получить оценку
        </Button>
        {answer?.estimation ? (
          <Button color="btnGrey" onClick={clearForm}>
            Новый ответ
          </Button>
        ) : (
          <Button color="btnGrey" onClick={() => setText('')}>
            Отменить
          </Button>
        )}
      </div>
      {answer?.estimation && (
        <div className={cls.questionTestAnswer}>
          <PR2>Оценка: {answer?.estimation} из 10</PR2>
          <PR2>{answer?.answer}</PR2>
        </div>
      )}
      {answerLoading && (
        <div className={cls.QuestionTestSkeleton}>
          <Skeleton width="100%" />
          <Skeleton width="100%" />
          <Skeleton width="100%" />
        </div>
      )}
    </div>
  )

  return <DropDownSection label="Протестировать вопрос" form={form} />
}
