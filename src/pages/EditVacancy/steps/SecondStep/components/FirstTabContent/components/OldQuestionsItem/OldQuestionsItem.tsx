import { PR2 } from '@/UI/TextComponents'
import cls from './OldQuestionsItem.module.scss'
import { useState } from 'react'

interface OldQuestionsItemProps {
  oldQuestionsItem: number
  id: number
  el: string
  setOldQuestionsItem: React.Dispatch<React.SetStateAction<number>>
}

export const OldQuestionsItem = ({
  oldQuestionsItem,
  id,
  setOldQuestionsItem,
  el,
}: OldQuestionsItemProps) => {
  const [open, setOpen] = useState<boolean>(false)
  return (
    <label
      htmlFor={`oldQuestionsItem${id}`}
      className={`${cls.oldQuestionsItem} `}
      onClick={() => setOldQuestionsItem(id)}
      key={id}
    >
      <div
        className={`${cls.oldQuestionsItemRadio} ${
          oldQuestionsItem === id && cls.active
        }`}
      ></div>
      <PR2
        onClick={() => setOpen((prev) => !prev)}
        className={`${cls.oldQuestionsItemTextTruncate} ${
          open ? cls.open : ''
        }`}
      >
        {el}
      </PR2>
      <input type="radio" id={`oldQuestionsItem${id}`} />
    </label>
  )
}
