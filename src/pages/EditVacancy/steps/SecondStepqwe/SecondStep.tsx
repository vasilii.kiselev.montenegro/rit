import { useEffect, useState } from 'react'
import cls from './SecondStep.module.scss'
import { ThirdStepTabs } from '@/pages/CreateVacancy/steps/ThirdStep/components/Tabs/ThirdStepTabs'
import { FirstTabContent } from '@/pages/CreateVacancy/steps/ThirdStep/components/FirstTabContent/FirstTabContent'
import { SecondTabContent } from '@/pages/CreateVacancy/steps/ThirdStep/components/SecondTabContent/SecondTabContent'
import { ThirdTabContent } from '@/pages/CreateVacancy/steps/ThirdStep/components/ThirdTabContent/ThirdTabContent'

interface SecondStepProps {
  vacancyId: number
  setTestingMethod: (data: number) => void
  setQuestion: React.Dispatch<React.SetStateAction<string | boolean>>
}

export const SecondStep = ({
  setTestingMethod,
  vacancyId,
  setQuestion,
}: SecondStepProps) => {
  const [tab, setTab] = useState(0)
  useEffect(() => {
    if (tab === 2) {
      setQuestion(true)
    } else {
      setQuestion(null)
    }
    setTestingMethod(tab)
  }, [tab])
  console.log(vacancyId)

  return (
    <div className={cls.ThirdStep}>
      <ThirdStepTabs tab={tab} setTab={setTab} />
      {tab === 0 && (
        <FirstTabContent
          vacancyId={{ id: vacancyId }}
          setQuestionUp={setQuestion}
        />
      )}
      {tab === 1 && <SecondTabContent setQuestionUp={setQuestion} />}
      {tab === 2 && <ThirdTabContent />}
    </div>
  )
}
