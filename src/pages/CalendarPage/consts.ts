export const months = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
]
export const weekDays = [
  'Понедельник',
  'Вторник',
  'Среда',
  'Четверг',
  'Пятница',
  'Суббота',
  'Воскресение',
]
export const shortWeekDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']

export const calendarView = [
  // { value: 'Month', label: 'Месяц' },
  { value: 'Week', label: 'Неделя' },
  { value: 'Day', label: 'День' },
]
export enum CalendarType {
  MONTH = 'Month',
  WEEK = 'Week',
  DAY = 'Day',
}

export const schedulerData = (date: string) => [
  {
    id: 1,
    startDate: new Date(2024, 7, 3),
    endDate: new Date(2024, 7, 4),
    title: 'Meeting',
    // color: '#ff0000',
    // text: 'asd',
    // location: 1,
  },
  // {
  //   id: 2,
  //   startDate: `${date}T12:00`,
  //   endDate: `${date}T13:30`,
  //   title: 'Go to a gym',
  //   color: '#ffff00',
  //   text: 'qwe',
  //   location: 2,
  // },
  // {
  //   id: 3,
  //   startDate: `${date}T12:00`,
  //   endDate: `${date}T13:30`,
  //   title: 'Go to a gym',
  //   color: '#0000ff',
  //   text: 'qwe',
  //   location: 3,
  // },
  // {
  //   id: 4,
  //   startDate: `${date}T12:00`,
  //   endDate: `${date}T13:30`,
  //   title: 'Go to a gym qrw qwr qwr qwr',
  //   color: '#008000',
  //   text: 'qwe',
  //   location: 4,
  // },
]

export const resources = [
  {
    fieldName: 'location',
    title: 'Location',
    instances: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }],
  },
]
export const sidebarCheckSlotsObj = {
  myMeetings: {
    name: 'Мои встречи',
    status: true,
  },
  holidays: {
    name: 'Праздники',
    status: true,
  },
  slots: {
    name: 'Слоты',
    status: true,
  },
}

export const AwaitSchedulerData = [
  {
    timeSlotId: null,
    startDate: ``,
    endDate: ``,
    color: ' ',
    vacancies: [],

    events: [
      {
        eventId: null,
        startDate: ``,
        endDate: ``,
        title: ' ',
        color: ' ',
      },
    ],
  },
]
