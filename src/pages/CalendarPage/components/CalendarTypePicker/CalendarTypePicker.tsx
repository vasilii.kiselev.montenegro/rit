import { Comment } from '@/UI/TextComponents/TextComponents'
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import cls from './CalendarTypePicker.module.scss'
import { useState } from 'react'

type Value = {
  label: string
  value: string | number
}

interface CalendarTypePickerProps {
  values: Value[]
  placeholder?: string
  setValue: React.Dispatch<React.SetStateAction<Value>>

  name?: string
  value: Value
}

export const CalendarTypePicker = ({
  values,
  placeholder = '',
  value,
  setValue,
  name = '',
}: CalendarTypePickerProps) => {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <FormControl className={cls.InputSelectForm}>
      <InputLabel
        className={cls.InputSelectFormLabel}
        id="demo-controlled-open-select-label"
      >
        {placeholder}
      </InputLabel>
      <Select
        labelId="demo-controlled-open-select-label"
        id="demo-controlled-open-select"
        className={cls.InputSelect}
        // placeholder={placeholder}
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        value={value.value ? value.value : ''}
        name={name}
        onChange={(e) =>
          setValue(...values.filter((el) => el.value === e.target.value))
        }
        displayEmpty
      >
        {values.map((el) => (
          <MenuItem key={`key-${el.label}`} value={el.value}>
            <Comment>{el.label}</Comment>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
