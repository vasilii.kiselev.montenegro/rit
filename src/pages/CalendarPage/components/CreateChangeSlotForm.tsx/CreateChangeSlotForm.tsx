import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import cls from './CreateChangeSlotForm.module.scss'
import { LocalizationProvider, MobileTimePicker } from '@mui/x-date-pickers'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'

import { Button } from '@/UI/Button/Button'
import { useFormik } from 'formik'
import dayjs from 'dayjs'
import { addSlot, editSlot } from '@/api/calendar/calendar'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useEffect, useState } from 'react'
import {
  generateWeek,
  repeatSlot,
} from '@/pages/CreateVacancy/steps/FourthStep/consts'
import * as Yup from 'yup'
import { VacanciesMultiSelectInput } from '@/UI/VacanciesMultiSelectInput/VacanciesMultiSelectInput'
import { CreateSlot } from '@/types/vacancy'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { getSlotsEventsThunk } from '@/store/slices/calendarSlice/asyncThunks/getSlotsEventsThunk'
import { ButtonColors } from '@/UI/Button/consts'
import { CalendarFormSlot } from '@/types/calendar'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { BlackCalendarIcon } from '@/assets/icons/BlackCalendarIcon'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { ClockIcon } from '@/assets/icons/ClockIcon'

interface CreateChangeSlotFormProps {
  slot?: CalendarFormSlot
  closeDrawer: (isOpen: boolean) => void
}

export const CreateChangeSlotForm = ({
  slot,
  closeDrawer,
}: CreateChangeSlotFormProps) => {
  const [timeDifferenceBool, setTimeDifferenceBool] = useState<boolean>(false)
  const vacancies = useAppSelector((state) => state.vacancies.workState)
  const dispatch = useAppDispatch()
  const weekValues = generateWeek(slot?.date)
  const initState: CalendarFormSlot = {
    startDate: slot?.startDate ? slot.startDate : dayjs('2000-01-01T08:00'),
    endDate: slot?.endDate ? slot.endDate : dayjs('2000-01-01T20:00'),
    date: weekValues[0],
    // date: dayjs().format('YYYY-MM-DD'),
    vacancies: slot?.vacancies,

    repeatSlot: repeatSlot[0],
  }
  const valiadtionSchema = Yup.object<CreateSlot>().shape({
    repeatSlot: Yup.string().required(),
    startDate: Yup.mixed().required(),
    endDate: Yup.mixed().required(),
    date: Yup.string().required(),
    vacancies: Yup.array().of(Yup.number()).required(),
  })
  console.log(slot)

  const formik = useFormik({
    initialValues: initState,
    validationSchema: valiadtionSchema,
    onSubmit(values) {
      slot
        ? editSlot(values, vacancies, slot.id).then((data) => {
            if (!data.errors) {
              closeDrawer(false)
              dispatch(getSlotsEventsThunk())
            }
          })
        : addSlot(values, vacancies).then((data) => {
            if (!data.errors) {
              closeDrawer(false)
              dispatch(getSlotsEventsThunk())
            }
          })

      // addSlot(values, vacancies)
      //   setClose(false)
    },
  })

  useEffect(() => {
    if (formik.values?.startDate && formik.values?.endDate) {
      const start = formik.values?.startDate
        ?.format('HH:mm')
        .split(':')
        .map(Number)
      const end = formik.values?.endDate?.format('HH:mm').split(':').map(Number)
      const difference = (end[0] - start[0]) * 60 + (end[1] - start[1])
      if (difference < 15) {
        setTimeDifferenceBool(true)
      } else {
        setTimeDifferenceBool(false)
      }
    }
  }, [formik.values?.startDate, formik.values?.endDate])

  const haveErrors = !!Object.values(formik.errors).length
  return (
    <div className={cls.CreateChangeSlotForm}>
      <div className={cls.CreateChangeSlotFormCont}>
        <div className={cls.defaultField}>
          <PM2>Выбор вакансии</PM2>

          <VacanciesMultiSelectInput
            value={formik.values?.vacancies}
            setValues={(data) => formik.setFieldValue('vacancies', data)}
            values={vacancies}
          />
        </div>

        <div className={cls.FormImputLabel}>
          <PM2>Выбор времени</PM2>
          <div className={cls.FormImputLabelDoubleSelectTime}>
            <div className={cls.FormImputLabelSelectIcon}>
              <ClockIcon />
              <div className={cls.FormImputLabelSelectTime}>
                <LocalizationProvider
                  adapterLocale={'ru'}
                  dateAdapter={AdapterDayjs}
                >
                  <MobileTimePicker
                    sx={{
                      background: '#fff',
                      '& .MuiOutlinedInput-notchedOutline': {
                        border: '1px solid #d4d7df',
                      },
                    }}
                    name="startDate"
                    value={formik.values.startDate}
                    onChange={(data) => formik.setFieldValue('startDate', data)}
                  />
                </LocalizationProvider>
                <Comment>-</Comment>

                <LocalizationProvider
                  adapterLocale={'ru'}
                  dateAdapter={AdapterDayjs}
                >
                  <MobileTimePicker
                    sx={{
                      background: '#fff',
                      '& .MuiOutlinedInput-notchedOutline': {
                        border: '1px solid #d4d7df',
                      },
                    }}
                    name="endDate"
                    value={formik.values.endDate}
                    onChange={(data) => formik.setFieldValue('endDate', data)}
                  />
                </LocalizationProvider>
              </div>
            </div>
          </div>
          {timeDifferenceBool && (
            <div className={cls.startEndSlotError}>
              Минимальная длительность слота - 15мин{' '}
            </div>
          )}
        </div>
        <div>
          {formik.values.repeatSlot !== repeatSlot[0] && <PR2>Начать с:</PR2>}
          <div className={cls.FormImputLabelSelectIcon}>
            <BlackCalendarIcon />
            <InputSelectMini
              values={weekValues}
              placeholder=" "
              name="date"
              value={formik.values.date}
              setValue={formik.handleChange}
            />
          </div>
        </div>
        <div className={cls.FormImputLabel}>
          <PM2>Повтор слота</PM2>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="repeatSlot"
            value={formik.values.repeatSlot}
            onChange={formik.handleChange}
            className={cls.FormImputLabelRadio}
          >
            {repeatSlot.map((el) => (
              <FormControlLabel
                style={{ marginLeft: '0' }}
                value={el}
                control={<Radio style={{ display: 'none' }} />}
                label={
                  <div className={cls.FormImputLabelRadioItem}>
                    <div
                      className={`${cls.FormImputLabelRadioItemIcon} ${
                        el === formik.values.repeatSlot ? cls.active : ''
                      }`}
                    >
                      &#10003;
                    </div>
                    <Comment> {el}</Comment>
                  </div>
                }
              />
            ))}
          </RadioGroup>
        </div>
      </div>
      <div className={cls.footerForm}>
        <Button
          onClick={() => formik.handleSubmit()}
          type={'submit'}
          color={ButtonColors.ICON_WHITE}
          disabled={
            timeDifferenceBool || haveErrors || !formik.values.vacancies?.length
          }
        >
          {slot ? 'Изменить' : 'Отправить'}
        </Button>
      </div>
    </div>
  )
}
