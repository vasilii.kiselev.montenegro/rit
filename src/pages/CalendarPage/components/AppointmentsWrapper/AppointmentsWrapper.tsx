import { ArrowDownIcon } from '@/assets/icons/ArrowDownIcon'
import {
  Appointments,
  DateNavigator,
  DayView,
  Toolbar,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui'
import {
  Grid,
  IconButton,
  Popover,
  styled,
  TableCell,
  Typography,
} from '@mui/material'
import moment from 'moment'
import { FC, useCallback } from 'react'
import { CalendarTypePicker } from '../CalendarTypePicker/CalendarTypePicker'
import { Comment } from '@/UI/TextComponents'
import { SidebarIcon } from '@/assets/icons/SidebarIcon'
import { Button } from '@/UI/Button/Button'
import cls from './WrapperStyles.module.scss'

const PREFIX = 'WeekendCell'

const classes = {
  weekendCell: `${PREFIX}-weekendCell`,
  weekendDayScaleCell: `${PREFIX}-weekendDayScaleCell`,
}
