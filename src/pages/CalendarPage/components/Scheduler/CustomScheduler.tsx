import {
  Appointments,
  DateNavigator,
  DayView,
  MonthView,
  Scheduler,
  Toolbar,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui'
import {
  AwaitSchedulerData,
  CalendarType,
  calendarView,
  sidebarCheckSlotsObj,
} from '../../consts'
import { CustomDayScaleCell } from '../ChildComponentsMainCalendarComponents/Day/Day'
import { ToolbarRoot } from '../ChildComponentsMainCalendarComponents/Toolbar/Toolbar'
import {
  AppointmentContentDay,
  AppointmentContentMonth,
  AppointmentContentWeek,
  MonthContentAppointment,
  WeekDayContentAppointment,
} from '../ChildComponentsMainCalendarComponents/Appointments/Appointments'
import {
  DayScaleCell,
  TimeTableCell,
} from '../ChildComponentsMainCalendarComponents/Month/Month'
import { CustomWeekDayScaleCell } from '../ChildComponentsMainCalendarComponents/Week/Week'
import {
  DateNavigatorNavigationButton,
  DateNavigatorOpenButton,
  DateNavigatorOverlay,
  DateNavigatorRoot,
} from '../ChildComponentsMainCalendarComponents/DateNavigator/DateNavigator'
import { ViewState } from '@devexpress/dx-react-scheduler'
import {
  CustomDefaultRootComponent,
  CustomRootComponent,
} from '../ChildComponentsMainCalendarComponents/Scheduler/Scheduler'
import dayjs from 'dayjs'
import { useState } from 'react'

interface SchedulerProps {
  data: typeof AwaitSchedulerData
  calendarType: (typeof calendarView)[0]
  setCalendarType: React.Dispatch<
    React.SetStateAction<{
      value: string
      label: string
    }>
  >
  currentDate: string
  setCurrentDate: React.Dispatch<React.SetStateAction<string>>
  viewSlots: boolean
  setSidebarIsOpen: React.Dispatch<React.SetStateAction<boolean>>
  setOpenDrawer: React.Dispatch<React.SetStateAction<boolean>>
  setFilterSlots: React.Dispatch<React.SetStateAction<number>>
  setSlotForm: React.Dispatch<React.SetStateAction<typeof AwaitSchedulerData>>
  sidebarIsOpen: boolean
  filterSlots: number | 'unfilter'
}

export const CustomScheduler = ({
  data,
  calendarType,
  setCalendarType,
  setCurrentDate,
  currentDate,
  viewSlots,
  setSidebarIsOpen,
  sidebarIsOpen,
  setSlotForm,
  setOpenDrawer,
  setFilterSlots,
  filterSlots,
}: SchedulerProps) => {
  const [radioEvents, setRadioEvents] =
    useState<typeof sidebarCheckSlotsObj>(sidebarCheckSlotsObj)

  const ToolbarHighOrder = ({ children }) => (
    <ToolbarRoot
      children={children}
      value={calendarType}
      setValue={setCalendarType}
      values={calendarView}
      slots={data}
      viewSlots={viewSlots}
      currentDate={currentDate}
      setSidebarIsOpen={setSidebarIsOpen}
      setSlotForm={setSlotForm}
      setOpenDrawer={setOpenDrawer}
      setFilterSlots={setFilterSlots}
      filterSlots={filterSlots}
    />
  )

  const CustomDayScaleCellHighOrder = (props) => {
    return (
      <CustomDayScaleCell
        setDate={setCurrentDate}
        currentDate={dayjs(currentDate)}
        {...props}
      ></CustomDayScaleCell>
    )
  }

  const thisWeekDayContentAppointment = ({ children, data, ...props }) => {
    return (
      <WeekDayContentAppointment children={children} data={data} {...props} />
    )
  }

  return (
    <Scheduler
      rootComponent={({ children, props }) =>
        calendarType.value === CalendarType.MONTH ? (
          <CustomRootComponent
            isSidebarActive={sidebarIsOpen}
            children={children}
            setRadioEvents={setRadioEvents}
            currentDate={currentDate}
            radioEvents={radioEvents}
            setCurrentDate={setCurrentDate}
            setCalendarType={setCalendarType}
            {...props}
          />
        ) : (
          <CustomDefaultRootComponent
            isSidebarActive={sidebarIsOpen}
            children={children}
            setRadioEvents={setRadioEvents}
            radioEvents={radioEvents}
            currentDate={currentDate}
            setCurrentDate={setCurrentDate}
            setCalendarType={setCalendarType}
            {...props}
          />
        )
      }
      locale="ru-RU"
      firstDayOfWeek={1}
      data={data}
    >
      <ViewState
        // defaultCurrentDate={currentDate}
        currentDate={currentDate}
        onCurrentDateChange={(e) =>
          setCurrentDate(dayjs(e).format('YYYY-MM-DD'))
        }
        currentViewName={calendarType.value}
      />
      <Toolbar rootComponent={ToolbarHighOrder} />
      <DateNavigator
        rootComponent={DateNavigatorRoot}
        overlayComponent={DateNavigatorOverlay}
        openButtonComponent={DateNavigatorOpenButton}
        navigationButtonComponent={DateNavigatorNavigationButton}
      />

      <DayView
        // endDayHour={24}
        // startDayHour={8}
        cellDuration={60}
        dayScaleRowComponent={CustomDayScaleCellHighOrder}
      />
      <WeekView
        // endDayHour={24}
        // startDayHour={8}
        cellDuration={60}
        dayScaleCellComponent={CustomWeekDayScaleCell}
        // timeScaleLabelComponent={CustomTimeScaleCell}
        // timeScaleTickCellComponent={zxc}
      />

      <MonthView
        timeTableCellComponent={TimeTableCell}
        dayScaleCellComponent={DayScaleCell}
      />
      <Appointments
        appointmentContentComponent={
          calendarType.value === CalendarType.MONTH
            ? (props) => (
                <AppointmentContentMonth
                  {...props}
                  eventsSlotsView={radioEvents}
                />
              )
            : calendarType.value === CalendarType.WEEK
            ? (props) => (
                <AppointmentContentWeek
                  {...props}
                  eventsSlotsView={radioEvents}
                />
              )
            : (props) => (
                <AppointmentContentDay
                  {...props}
                  eventsSlotsView={radioEvents}
                />
              )
        }
        appointmentComponent={
          calendarType.value === CalendarType.MONTH
            ? MonthContentAppointment
            : thisWeekDayContentAppointment
        }
      />
    </Scheduler>
  )
}
