import cls from './Sidebar.module.scss'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'
import { memo, useEffect, useState } from 'react'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar'
import {
  calendarView,
  schedulerData,
  sidebarCheckSlotsObj,
  weekDays,
} from '../../consts'
import dayjs from 'dayjs'
import { useAppSelector } from '@/hooks/useAppSelector'

interface SidebarProps {
  isActive: boolean
  setRadioEvents: React.Dispatch<
    React.SetStateAction<typeof sidebarCheckSlotsObj>
  >
  radioEvents: typeof sidebarCheckSlotsObj
  currentDate: string
  setCurrentDate: React.Dispatch<React.SetStateAction<string>>
  setCalendarType: React.Dispatch<React.SetStateAction<typeof calendarView>>
}

export const Sidebar = memo(function Sidebar({
  setRadioEvents,
  radioEvents,
  isActive,
  currentDate,
  setCalendarType,
  setCurrentDate,
}: SidebarProps) {
  const weekSlots = useAppSelector((state) => state.calendar.weekEvents)
  // const [radioEvents, setRadioEvents] =
  //   useState<typeof sidebarCheckSlotsObj>(sidebarCheckSlotsObj)

  // const data = schedulerData(dayjs().format('YYYY-MM-DD'))
  // console.log(currentDate)
  const handleChangeDate = (data) => {
    setCurrentDate(data.format('YYYY-MM-DD'))
    setCalendarType(calendarView[2])
  }
  useEffect(() => {
    console.log(weekSlots)
  }, [weekSlots])
  return (
    <div
      className={`${cls.Sidebar} ${isActive ? cls.active : cls.unactive}`}
      style={{ width: isActive ? '350px' : '0px' }}
    >
      <div className={cls.sidebarCalendar}>
        <LocalizationProvider adapterLocale="ru" dateAdapter={AdapterDayjs}>
          <DateCalendar
            sx={{
              '& .Mui-selected': {
                bgcolor: '#2dad96 !important',
              },
            }}
            value={dayjs(currentDate)}
            onChange={(data) => handleChangeDate(data)}
          />
        </LocalizationProvider>
      </div>
      <div className={cls.sidebarEvents}>
        <PM2>События</PM2>
        <div className={cls.sidebarEventsContent}>
          <div className={cls.sidebarInputDaysRadioItems}>
            {Object.entries(radioEvents).map(([el, { name, status }]) => (
              <label
                className={cls.sidebarImputLabelRadioItem}
                onClick={() => {
                  setRadioEvents((prev) => {
                    return {
                      ...prev,
                      [el]: { ...prev[el], status: !prev[el].status },
                    }
                  })
                }}
              >
                <div
                  className={`${cls.sidebarImputLabelRadioItemIcon} ${
                    status ? cls.active : ''
                  }`}
                >
                  &#10003;
                </div>
                <PR2>{name}</PR2>
              </label>
            ))}
          </div>
        </div>
      </div>
      <div className={cls.meetengsOnWeek}>
        <PM2>Встречи на неделе</PM2>
        <div className={cls.meetengsOnWeekContent}>
          {weekSlots?.length &&
            weekSlots.map((el) => {
              return (
                <div
                  key={el.id}
                  className={`${cls.meetengsOnWeekItem}  `}
                  style={{ background: el.color }}
                >
                  <Comment className={cls.meetengsOnWeekItemContent}>
                    {dayjs(el.startDate).format('HH:mm')}-
                    {dayjs(el.endDate).format('HH:mm')}{' '}
                    {weekDays[dayjs(el.date).isoWeekday() - 1]}{' '}
                    <PM2> {el.name}</PM2>
                  </Comment>
                </div>
              )
            })}
        </div>
      </div>
    </div>
  )
})
