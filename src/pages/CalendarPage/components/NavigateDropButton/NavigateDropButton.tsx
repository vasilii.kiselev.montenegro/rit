import * as React from 'react'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import { MoreIcon } from '@/assets/icons/MoreIcon'
import cls from './Menu.module.scss'
import { Comment } from '@/UI/TextComponents'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { Button } from '@/UI/Button/Button'
import { PlusIcon } from '@/assets/icons/PlusIcon'

export function NavigateDropButton({ setOpenDrawer }) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const navigate = useNavigate()
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const navigateFreeUpTime = () => {
    navigate(Url.FREE_UP_TIME)
  }
  // navigate(`${Url.CREATE_SLOT}`)
  const navigateEditPage = () => {
    setOpenDrawer(true)
    setAnchorEl(null)
  }
  return (
    <div>
      <ButtonIcon
        color="btnblack"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        style={{
          width: 'fit-content',
          height: 'auto',
          display: 'flex',
          gap: '10px',
        }}
      >
        <span>добавить</span> <PlusIcon />
      </ButtonIcon>
      <Menu
        className={cls.menu}
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem className={cls.menuItem} onClick={navigateEditPage}>
          <Comment>Слот</Comment>
        </MenuItem>
        <MenuItem className={cls.menuItem} onClick={navigateFreeUpTime}>
          <Comment>Свободное время</Comment>
        </MenuItem>
      </Menu>
    </div>
  )
}
