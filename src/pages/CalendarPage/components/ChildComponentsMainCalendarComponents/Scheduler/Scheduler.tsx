import { styled } from '@mui/material'
import React from 'react'
import { Sidebar } from '../../Sidebar/Sidebar'

export const CustomDefaultRootComponent = ({
  children,
  isSidebarActive,
  currentDate,
  setCurrentDate,
  setCalendarType,
  ...restProps
}) => {
  // console.log(restProps)

  return (
    <div {...restProps} style={{ position: 'relative', display: 'flex' }}>
      <div>{children}</div>
      <Sidebar
        setRadioEvents={restProps.setRadioEvents}
        radioEvents={restProps.radioEvents}
        isActive={isSidebarActive}
        currentDate={currentDate}
        setCurrentDate={setCurrentDate}
        setCalendarType={setCalendarType}
      />
    </div>
  )
}
export const CustomRootComponent = ({
  children,
  isSidebarActive,
  currentDate,
  setCurrentDate,
  setCalendarType,
  ...restProps
}) => {
  // console.log(isSidebarActive)
  //   console.log(children, restProps)
  const Wrapper = styled('div')({
    '&': {
      position: 'relative',
      display: 'flex',
      // gap: '10px',
    },
    // '& .children': {
    //   flexShrink: isSidebarActive ? '0' : '1',
    //   width: '100%',
    //   transition: 'all 0.3s ease',
    // },
    '& tr': {
      display: 'grid',
      gridTemplateColumns: 'repeat(7, 1fr)',
    },
    '& td': {
      borderRight: 'none',
    },
    '& td:not(:last-child)': {
      borderRight: '1px solid #EBEBEB',
    },
    '& .MainLayout-container': {
      border: '1px solid #EBEBEB',
      borderRadius: '10px',
      borderCollapse: 'collapse',
    },
  })
  return (
    <Wrapper className="custom-root" {...restProps}>
      <div className="children">{children}</div>
      <Sidebar
        setRadioEvents={restProps.setRadioEvents}
        radioEvents={restProps.radioEvents}
        isActive={isSidebarActive}
        currentDate={currentDate}
        setCurrentDate={setCurrentDate}
        setCalendarType={setCalendarType}
      />
    </Wrapper>
  )
}
export const renderAppointment = (props) => {
  console.log('render', props)

  return (
    <React.Fragment>
      <i>qwe</i>
      <p>asd</p>
    </React.Fragment>
  )
}
