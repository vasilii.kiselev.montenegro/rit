import { WeekView } from '@devexpress/dx-react-scheduler-material-ui'
import { TableCell, styled, withStyles } from '@mui/material'
import dayjs from 'dayjs'
import React from 'react'
import cls from './Week.module.scss'
const PREFIX = 'WeekendCell'
const classes = {
  weekendCell: `${PREFIX}-weekendCell`,
  weekendDayScaleCell: `${PREFIX}-weekendDayScaleCell`,
}

const StyledWeekViewTimeTableCell = styled(WeekView.TimeTableCell, {
  shouldForwardProp: (prop) => prop !== 'isWeekend',
})(({ theme, isWeekend }) => ({
  [`&.${classes.weekendCell}`]: {
    backgroundColor: theme.palette.action.disabledBackground,
    '&:hover': {
      backgroundColor: theme.palette.action.disabled,
    },
    '&:focus': {
      backgroundColor: theme.palette.action.disabled,
    },
  },
  ...(isWeekend && {
    backgroundColor: theme.palette.action.disabledBackground,
  }),
}))
export const TimeWeekTableCell = (props) => {
  const { startDate, children, ...restProps } = props
  const isWeekend = startDate.getDay() === 0 || startDate.getDay() === 6
  console.log(props)

  return (
    <StyledWeekViewTimeTableCell
      {...restProps}
      startDate={startDate}
      className={isWeekend ? classes.weekendCell : undefined}
      isWeekend={isWeekend}
      children={children}
    />
  )
}
export const CustomWeekDayScaleCell = ({
  ...props
}: WeekView.DayScaleCellProps) => {
  const isToday = dayjs(props.startDate).isSame(dayjs(), 'day')

  return (
    // <WeekView.DayScaleCell
    //   {...props}
    //   startDate={props.startDate}
    //   today={props.today}
    //   style={{
    //     backgroundColor: isToday ? '#2dad96' : '#f5f5f5',
    //     color: isToday ? '#fff !important' : '#000',
    //     fontWeight: isToday ? 'bold' : 'normal',
    //     textAlign: 'center', // Центрируем текст по горизонтали
    //     padding: '10px', // Добавляем отступы
    //   }}
    // >
    <TableCell>
      <span
        className={`${cls.weekDayCell} ${isToday ? cls.weekDayCellToday : ''}`}
      >
        {Intl.DateTimeFormat('Ru-Ru', { weekday: 'short' }).format(
          props.startDate
        )}{' '}
        {dayjs(props.startDate).format('DD')}
      </span>
    </TableCell>
    // </WeekView.DayScaleCell>
  )
}
export const CustomTimeScaleCell = (props) => {
  const isToday = dayjs(props.startDate).isSame(dayjs(), 'day')
  console.log(props)

  return (
    <TableCell className={`${cls.weekTimeCell} `}>
      <span {...props}></span>
    </TableCell>
  )
}

// export const CustomDayScaleCell = ({ startDate, endDate, today }) => (
//   <TableCell>
//     <span>
//       {Intl.DateTimeFormat('en-US', { weekday: 'short' }).format(startDate)}
//     </span>
//   </TableCell>
// )

export const zxc = (restProps) => {
  console.log(restProps)
  const Custom = styled('div')(() => ({
    '& .MuiTableCell-root.MuiTableCell-body': {
      //   display: 'flex',
    },
  }))
  console.log('zxc', restProps)

  return (
    <Custom
      style={{ display: 'flex', flexDirection: 'column' }}
      {...restProps}
    />
  )
}
