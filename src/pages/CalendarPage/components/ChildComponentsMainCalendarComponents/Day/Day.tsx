import { WeekView } from '@devexpress/dx-react-scheduler-material-ui'
import { TableCell } from '@mui/material'
import dayjs from 'dayjs'
import cls from './Day.module.scss'

import isoWeek from 'dayjs/plugin/isoWeek'
import { useEffect, useState } from 'react'

dayjs.extend(isoWeek)

const getCurrentWeekDays = (currentDate) => {
  const today = dayjs(currentDate)
  const startOfWeek = today.startOf('isoWeek')
  const endOfWeek = today.endOf('isoWeek')

  const days = []
  for (let i = 0; i <= 6; i++) {
    days.push(startOfWeek.add(i, 'day'))
  }

  return days
}

export const CustomDayScaleCell = ({ currentDate, setDate, ...props }) => {
  const [week, setWeek] = useState(getCurrentWeekDays(currentDate))
  // const week = getCurrentWeekDays()
  // console.log(week, currentDate)
  useEffect(() => {
    const newWeek = getCurrentWeekDays(currentDate)

    setWeek(newWeek)
  }, [currentDate])

  return (
    // <WeekView.DayScaleCell
    //   {...props}
    //   startDate={props.startDate}
    //   today={props.today}
    //   style={{
    //     backgroundColor: isToday ? '#2dad96' : '#f5f5f5',
    //     color: isToday ? '#fff !important' : '#000',
    //     fontWeight: isToday ? 'bold' : 'normal',
    //     textAlign: 'center', // Центрируем текст по горизонтали
    //     padding: '10px', // Добавляем отступы
    //   }}
    // >
    <>
      {week.map((el) => {
        // console.log(currentDate.format('DD-MM'), el.format('DD-MM'))
        const today = currentDate.format('DD-MM') === el.format('DD-MM')

        return (
          <TableCell>
            <span
              className={`${cls.weekDayCell} ${
                today ? cls.weekDayCellToday : ''
              }`}
              onClick={() => setDate(el.format('YYYY-MM-DD'))}
            >
              {Intl.DateTimeFormat('Ru-Ru', { weekday: 'short' }).format(el)}{' '}
              {dayjs(el).format('DD')}
            </span>
          </TableCell>
        )
      })}
    </>
    // </WeekView.DayScaleCell>
  )
}
