import {
  AppointmentTooltip,
  Appointments,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui'
import { Button, Menu, Popover, Typography, styled } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import cls from './Appointments.module.scss'
import { Comment, PM2, PR3 } from '@/UI/TextComponents'
import dayjs from 'dayjs'
import { checkPosition } from './utils'
import { LockIcon } from '@/assets/icons/LockIcon'
const StyledDiv = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(0.5),
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  color: '#fff',
  //   background: 'red'
}))
export const AppointmentContent = (props) => {
  const { CustomComponent } = props
  console.log(props?.resources)

  console.log(props)
  return <CustomComponent {...props}>{props.data.title}</CustomComponent>
}

export const AppointmentsWrapper: React.FC<Appointments.AppointmentProps> = ({
  // style,
  ...restProps
}) => {
  console.log(restProps)

  const Wrapper = styled('div')({
    height: '100%',
    width: '100%',
    minWidth: '50px',
    // color: '#000',
    background: '#f9226782',
  })
  return <Wrapper {...restProps}>{restProps.children[1]}</Wrapper>
}

export const AppointmentContentMonth = ({ eventsSlotsView, ...props }) => {
  const StyledAppointmentContentMonth = styled('div')(() => ({
    display: 'flex',
    alignItems: 'center',
    gap: '5px',
    // justifyContent: 'center',
    padding: '5px',
    // background: '#fff',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    color: '#000',
    fontFamily: 'Onest',
    fontSize: '14px',
    position: 'relative',
    background: `${props.data.color}6e`,

    cursor: eventsSlotsView.slots.status ? 'pointer' : 'default',
    opacity: eventsSlotsView.slots.status ? '1' : '0',
    pointerEvents: eventsSlotsView.slots.status ? 'unset' : 'none',
  }))
  // console.log(props.data.color)
  const newObj = { ...props }

  return (
    <StyledAppointmentContentMonth {...newObj}>
      <div
        style={{
          gap: '5px',
          display: 'flex',
          alignItems: 'center',
          height: '15px',
        }}
      >
        {eventsSlotsView.myMeetings.status && props.data.events.length
          ? props.data.events.map((el) => {
              return (
                <div>
                  <div
                    style={{
                      background: el.color,

                      width: '10px',
                      height: '10px',
                      borderRadius: '50%',
                    }}
                  ></div>
                </div>
              )
            })
          : ''}
      </div>
      <Comment>{dayjs(props.data.startDate).format('HH:mm') + ' '}</Comment>
    </StyledAppointmentContentMonth>
  )
}
export const AppointmentContentWeek = ({ eventsSlotsView, ...props }) => {
  const [viewType, setViewType] = useState<boolean>(false)

  const newObj = { ...props }

  useEffect(() => {
    if (props.data?.events !== undefined) {
      setViewType(true)
    } else {
      setViewType(false)
    }
  }, [])
  // console.log(props.data)

  const StyledAppointmentContentWeek = styled('div')(() => ({
    display: 'flex',
    alignItems: 'top',
    gap: '5px',
    // justifyContent: 'center',
    paddingLeft: viewType ? '0' : '20px',
    // background: '#fff',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    color: '#000',
    fontFamily: 'Onest',
    fontSize: '14px',
    position: 'relative',
    height: '100%',
    background: props.data.lock ? '' : ` `,
    // background: `${props.data.color}26`,
    // background: props.data.lock ? 'red' : `${props.data.color}26`,

    cursor: eventsSlotsView.slots.status ? 'pointer' : 'default',
    opacity: eventsSlotsView.slots.status ? '1' : '0',
    pointerEvents: eventsSlotsView.slots.status ? 'unset' : 'none',
  }))
  // console.log(viewType)
  // console.log(props.data?.events)

  return (
    <StyledAppointmentContentWeek
      {...newObj}
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      {eventsSlotsView.myMeetings.status && props.data.events.length
        ? props.data.events.map((el) => {
            const [height, startPosition] = checkPosition(props, el)

            return (
              <div
                style={{
                  position: 'absolute',
                  paddingLeft: '20px',
                  left: '5px',
                  top: `${startPosition}%`,
                  height: `${height}%`,
                  background: '#ffffff23',
                  display: 'flex',
                  gap: '15px',
                  width: '100%',
                }}
              >
                <div
                  style={{
                    background: el.color,
                    position: 'absolute',
                    left: '0px',
                    top: '0px',
                    width: '5px',
                    height: '100%',
                    color: '#000 !important',
                  }}
                ></div>
                <Comment>
                  {dayjs(el.startDate).format('HH:mm') + ' '} -{' '}
                  {dayjs(el.endDate).format('HH:mm') + ' '}
                </Comment>{' '}
                {/* {el.title} */}
              </div>
            )
          })
        : ''}
    </StyledAppointmentContentWeek>
  )
}
export const AppointmentContentDay = ({ eventsSlotsView, ...props }) => {
  const StyledAppointmentContentMonth = styled('div')(() => ({
    display: 'flex',
    alignItems: 'center',
    gap: '5px',
    paddingLeft: '20px',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    color: '#000',
    fontFamily: 'Onest',
    fontSize: '14px',
    position: 'relative',
    // background: '#fff',
    // marginTop: '10px',
    padding: '15px 0 15px 20px',
    // background: `${props.data.color}26`,
    cursor: eventsSlotsView.slots.status ? 'pointer' : 'default',
    opacity: eventsSlotsView.slots.status ? '1' : '0',
    pointerEvents: eventsSlotsView.slots.status ? 'unset' : 'none',
    height: '100%',
  }))

  const newObj = { ...props }

  return (
    <StyledAppointmentContentMonth
      {...newObj}
      style={
        {
          // display: 'flex',
          // flexDirection: 'column',
          // alignItems: 'start',
        }
      }
    >
      {eventsSlotsView.myMeetings.status && props.data.events.length
        ? props.data.events.map((el) => {
            const [height, startPosition] = checkPosition(props, el)

            return (
              <div
                style={{
                  position: 'absolute',
                  paddingLeft: '20px',
                  left: '5px',
                  top: `${startPosition}%`,
                  height: `${height}%`,
                  background: '#ffffff23',
                  display: 'flex',
                  gap: '15px',
                  width: '100%',
                }}
              >
                <div
                  style={{
                    background: el.color,
                    position: 'absolute',
                    left: '0px',
                    top: '0px',
                    width: '5px',
                    height: '100%',
                    color: '#000 !important',
                  }}
                ></div>
                <Comment>
                  {dayjs(el.startDate).format('HH:mm') + ' '} -{' '}
                  {dayjs(el.endDate).format('HH:mm') + ' '}
                </Comment>{' '}
                {el.title}
              </div>
            )
          })
        : ''}
    </StyledAppointmentContentMonth>
  )
}

export const CustomAppoinmentTooltip = ({ anchorEl, ...props }) => {
  console.log(props)

  return (
    <>
      <AppointmentTooltip.Layout
        anchorEl={anchorEl}
        className={cls.CustomAppoinmentTooltip}
        {...props}
      ></AppointmentTooltip.Layout>
    </>
  )
}

export const MonthContentAppointment = ({
  children,
  data,

  onClick,
  // toggleVisibility,
  onAppointmentMetaChange,
  ...restProps
}) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null)

  const handleOpenClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  // console.log(data)

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined
  return (
    <>
      <div
        onClick={(e) => handleOpenClick(e)}
        style={{ cursor: data.lock ? 'not-allowed' : 'default' }}
      >
        <Appointments.Appointment
          {...restProps}
          style={{
            position: 'relative',
            background: `#fff `,
            height: 'fit-content',
          }}
        >
          {data.lock ? (
            <span
              style={{
                background: '#bdbdbd6e',
                padding: '2px 0',
                width: '100%',
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: '',
              }}
            >
              <LockIcon color="#8D9092" />
            </span>
          ) : (
            children
          )}
        </Appointments.Appointment>
      </div>
      {data.events.length && !data.lock ? (
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          role="presentation"
          disableEnforceFocus={true}
        >
          {' '}
          <div className={cls.popoverContainer}>
            {data.events.map((el) => {
              return (
                <div className={cls.popoverItem} key={el.id}>
                  <div
                    className={cls.popoverItemColor}
                    style={{ background: el.color }}
                  ></div>
                  <div className={cls.popoverItemContent}>
                    <PM2>{el.title}</PM2>

                    <Comment>
                      {dayjs(el.startDate).format('HH:mm')}-
                      {dayjs(el.endDate).format('HH:mm')}
                    </Comment>
                  </div>
                </div>
              )
            })}
          </div>
        </Popover>
      ) : (
        ''
      )}
    </>
  )
}
export const WeekDayContentAppointment = ({
  children,
  data,

  ...restProps
}) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null)

  const handleOpenClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined
  // console.log(data)

  return (
    <>
      <div
        style={{
          height: '100%',
          cursor: data.lock ? 'not-allowed' : 'default',
        }}
        onClick={(e) => handleOpenClick(e)}
      >
        <Appointments.Appointment
          {...restProps}
          style={{
            position: 'relative',
            // height: 'unset',
            // padding: '5px',
            // background: `${data.color}6e`,
            background: data.lock ? '#bdbdbd6e' : `${data.color}6e`,
          }}
          onClick={(e) => {}}
        >
          {data.lock ? (
            <span
              style={{
                width: '100%',
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: '',
              }}
            >
              <LockIcon color="#8D9092" />
            </span>
          ) : (
            children
          )}
        </Appointments.Appointment>
      </div>
      {data.events.length && !data.lock ? (
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          role="presentation"
          disableEnforceFocus={true}
        >
          {' '}
          <div className={cls.popoverContainer}>
            {data.events.map((el) => {
              return (
                <div className={cls.popoverItem} key={el.id}>
                  <div
                    className={cls.popoverItemColor}
                    style={{ background: el.color }}
                  ></div>
                  <div className={cls.popoverItemContent}>
                    <PM2>{el.title}</PM2>

                    <Comment>
                      {dayjs(el.startDate).format('HH:mm')}-
                      {dayjs(el.endDate).format('HH:mm')}
                    </Comment>
                  </div>
                </div>
              )
            })}
          </div>
        </Popover>
      ) : (
        ''
      )}
    </>
  )
}
