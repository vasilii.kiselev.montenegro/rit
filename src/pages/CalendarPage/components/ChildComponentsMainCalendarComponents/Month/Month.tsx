import { DayView, WeekView } from '@devexpress/dx-react-scheduler-material-ui'
import { TableCell, styled } from '@mui/material'
import moment from 'moment'
import { FC } from 'react'
import { AppointmentContent } from '../Appointments/Appointments'

const TimeCell = styled(TableCell)(() => ({
  height: '48px',
  padding: '0',
  position: 'relative',
  boxSizing: 'border-box',
  //   borderRight: '1px solid rgba(224, 224, 224, 1)',
  '&:hover': {
    backgroundColor: 'rgba(0, 0, 0, 0.04)',
  },
  '&:focus': {
    outline: '0',
    backgroundColor: 'rgba(53, 58, 126, 0.15)',
  },
}))
export const TimeTableCell: FC<DayView.TimeTableCellProps> = (props) => {
  //   console.log(props)
  const isWeekend =
    props.startDate.getDay() === 0 || props.startDate.getDay() === 6
  const dateColor =
    props.today === true
      ? '#2dad96'
      : props.otherMonth === true
      ? '#EBEBEB'
      : isWeekend
      ? '#7F879E'
      : '#1B2124'
  return (
    <TimeCell
      //   className="qwe"
      style={{
        position: 'relative',
        // zIndex: '100',
        display: 'inline-flex',
        justifyContent: 'end',
        alignItems: 'top',
        aspectRatio: '1/1',
        // width: '14%',
        height: 'auto',
        padding: '6px',

        background: isWeekend ? '#F9F9F9' : 'fff',
      }}
      tabIndex={0}
    >
      <span
        style={{
          height: 'fit-content',
          color: dateColor,
          fontSize: '16px',
          borderBottom: props.today === true ? '1px solid #2dad96' : 'none',
        }}
      >
        {moment(props.startDate).format('DD')}
      </span>
    </TimeCell>
  )
}
const StyledMonthViewDayScaleCell = styled(WeekView.DayScaleCell)(() => ({
  '&': {
    border: 'none',
  },
  '& .Cell-dayOfMonth': {
    display: 'none !important ',
  },
}))
export const DayScaleCell = (props) => {
  const { startDate, ...restProps } = props
  const isWeekend = startDate.getDay() === 0 || startDate.getDay() === 6

  return (
    <StyledMonthViewDayScaleCell
      {...restProps}
      startDate={startDate}
      isWeekend={isWeekend}
    />
  )
}
