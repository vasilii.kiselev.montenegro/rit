import { AwaitSchedulerData } from '@/pages/CalendarPage/consts'
import dayjs from 'dayjs'

interface ReturnSlots {
  currentSlots: (typeof AwaitSchedulerData)[]
  otherSlots: (typeof AwaitSchedulerData)[]
}

const formatString = 'YYYY-MM-DD'

const getCurrentWeekDays = (currentDate) => {
  const today = dayjs(currentDate)
  const startOfWeek = today.startOf('isoWeek')

  const days = []
  for (let i = 0; i <= 6; i++) {
    days.push(startOfWeek.add(i, 'day').format(formatString))
  }

  return days
}
const getCurrentMonthDays = (currentDate) => {
  const today = dayjs(currentDate)
  const startOfMonth = today.startOf('month')
  const endOfMonth = Number(today.endOf('month').format('DD'))

  const days = []
  for (let i = 0; i <= endOfMonth - 1; i++) {
    days.push(startOfMonth.add(i, 'day').format(formatString))
  }

  return days
}

const getDaySlots = (date, slots): ReturnSlots => {
  const newSlots = slots.reduce(
    (acc: ReturnSlots, el) => {
      if (dayjs(el.startDate).format(formatString) === date) {
        acc.currentSlots.push(el)
        return acc
      } else {
        acc.otherSlots.push(el)
        return acc
      }
    },
    { currentSlots: [], otherSlots: [] }
  )
  return newSlots
}
const getWeekSlots = (date, slots): ReturnSlots => {
  const weekDays = getCurrentWeekDays(date)

  const newSlots = slots.reduce(
    (acc: ReturnSlots, el) => {
      let isUsed = false
      weekDays.forEach((date) => {
        if (dayjs(el.startDate).format(formatString) === date) {
          acc.currentSlots.push(el)
          isUsed = true
        }
      })
      if (!isUsed) {
        acc.otherSlots.push(el)
      }
      return acc
    },
    { currentSlots: [], otherSlots: [] }
  )
  console.log(newSlots)

  return newSlots
}
const getMonthSlots = (date, slots): ReturnSlots => {
  const monthDays = getCurrentMonthDays(date)

  const newSlots = slots.reduce(
    (acc: ReturnSlots, el) => {
      let isUsed = false
      monthDays.forEach((date) => {
        if (dayjs(el.startDate).format(formatString) === date) {
          acc.currentSlots.push(el)
          isUsed = true
        }
      })

      if (!isUsed) {
        acc.otherSlots.push(el)
      }
      return acc
    },
    { currentSlots: [], otherSlots: [] }
  )

  return newSlots
}

export const sortSlots = (calendarType, date, slots): ReturnSlots => {
  switch (calendarType.value) {
    case 'Month':
      return getMonthSlots(date, slots)
    case 'Week':
      return getWeekSlots(date, slots)
    case 'Day':
      return getDaySlots(date, slots)
  }
}
