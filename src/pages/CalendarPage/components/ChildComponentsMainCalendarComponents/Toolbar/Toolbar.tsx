import { Toolbar } from '@devexpress/dx-react-scheduler-material-ui'
import { FC, useCallback, useEffect, useState } from 'react'
import { CalendarTypePicker } from '../../CalendarTypePicker/CalendarTypePicker'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'
import { SidebarIcon } from '@/assets/icons/SidebarIcon'
import cls from './Toolbar.module.scss'
import { Button } from '@/UI/Button/Button'
import MenuDrop from '@/pages/VacanciesPage/components/Menu/Menu'
import dayjs from 'dayjs'
import { ArrowDropDownIcon } from '@mui/x-date-pickers/icons'
import { useAppSelector } from '@/hooks/useAppSelector'
import { sortSlots } from './utils'
import { Slot } from './Slot/Slot'
import { CalendarType } from '@/pages/CalendarPage/consts'
import { CloseFilterIcon } from '@/assets/icons/CloseFilterIcon'
import { ButtonColors } from '@/UI/Button/consts'
import { ReloadIcon } from '@/assets/icons/ReloadIcon'
import { getSlotsEventsThunk } from '@/store/slices/calendarSlice/asyncThunks/getSlotsEventsThunk'
import { getWeekEventsThunk } from '@/store/slices/calendarSlice/asyncThunks/getWeekEvents'
import { useAppDispatch } from '@/hooks/useAppDispatch'

export const ToolbarRoot: FC<Toolbar.RootProps> = ({
  value,
  setValue,
  values,
  children,
  slots,
  viewSlots,
  setSidebarIsOpen,
  currentDate,
  setSlotForm,
  setOpenDrawer,
  setFilterSlots,
  filterSlots,
}) => {
  const dispatch = useAppDispatch()
  // const slots = slots.reduce((acc, nac) => [...acc, ...nac.events], [])
  const [viewFullSlots, setViewFullSlots] = useState(false)
  const [thisSlots, setThisSlots] = useState()
  const slotsCeil = Math.ceil(slots.length / 3)
  const slotsHeight = slotsCeil * 70 + (slotsCeil - 1) * 15
  // console.log(events)
  console.log(slots)
  const vacancies = useAppSelector((state) => state.vacancies.workState)
  const getSlots = useCallback(() => {
    dispatch(getSlotsEventsThunk())
    dispatch(getWeekEventsThunk())
  }, [])
  useEffect(() => {
    const qwe = sortSlots(value, currentDate, slots)

    setThisSlots(qwe)
  }, [])
  const getVacancyName = (ids) => {
    if (!ids) return

    return ids.map((el) => {
      const vacancy = vacancies.find((elem) => {
        return elem.id === el
      })
      return { name: vacancy?.name, id: vacancy?.id }
    })
  }
  const reloadSlots = useCallback(() => {
    getSlots()
  }, [])
  const typeLabel =
    value.value === CalendarType.DAY
      ? 'день'
      : value.value === CalendarType.WEEK
        ? 'неделе'
        : 'месяц'

  return (
    <div className={cls.content}>
      <div className={cls.calendarControlPanelAndSidebarControl}>
        <div className={cls.controlPanel}>
          <div>{children}</div>
          <div>
            <Comment>Вид:</Comment>
            <CalendarTypePicker
              value={value}
              setValue={setValue}
              values={values}
            />
          </div>
        </div>
        <div className={cls.sidebarControl}>
          <Button
            color={ButtonColors.GREY}
            className={cls.reloadButton}
            // className='asfasfasfa'
            onClick={reloadSlots}
          >
            <div className={cls.reloadImage}>
              <ReloadIcon />
            </div>
          </Button>
          <Button color={ButtonColors.ICON_GREEN} onClick={setSidebarIsOpen}>
            <SidebarIcon />
          </Button>
        </div>
      </div>
      {viewSlots && (
        <div className={cls.slotsContainer}>
          <div
            className={cls.slotsHeader}
            onClick={() => setViewFullSlots((prev) => !prev)}
          >
            <PM2>Слоты</PM2>

            {slots.length > 3 && (
              <ArrowDropDownIcon
                style={{ rotate: viewFullSlots ? '0deg' : '180deg' }}
              />
            )}
            {typeof filterSlots === 'number' ? (
              <div
                style={{ display: 'flex', gap: ' 10px', paddingLeft: '15px' }}
                onClick={() => setFilterSlots('unfilter')}
              >
                <PM2>Скрыть фильтрацию</PM2> <CloseFilterIcon />
              </div>
            ) : (
              ''
            )}
          </div>
          <div
            style={{ height: viewFullSlots ? `100%` : '100%' }}
            // style={{ height: viewFullSlots ? `${slotsHeight}px` : '70px' }}
            className={cls.slots}
          >
            {!thisSlots?.currentSlots.length && (
              <Comment>
                {' '}
                На {value.value === CalendarType.WEEK
                  ? 'текущей'
                  : 'текущий'}{' '}
                {typeLabel} слотов нет{' '}
              </Comment>
            )}
            {thisSlots?.currentSlots.map((el, id) => (
              <Slot
                el={el}
                id={id}
                getVacancyName={getVacancyName}
                key={id}
                setSlotForm={setSlotForm}
                setOpenDrawer={setOpenDrawer}
                setFilterSlots={setFilterSlots}
              />
            ))}
            {viewFullSlots &&
              thisSlots?.otherSlots.map((el, id) => (
                <Slot
                  el={el}
                  id={id}
                  getVacancyName={getVacancyName}
                  key={id}
                  setSlotForm={setSlotForm}
                  setOpenDrawer={setOpenDrawer}
                  setFilterSlots={setFilterSlots}
                />
              ))}
          </div>
        </div>
      )}
    </div>
  )
}
