import MenuDrop from '@/pages/VacanciesPage/components/Menu/Menu'
import cls from './Slot.module.scss'
import dayjs from 'dayjs'
import { PR2 } from '@/UI/TextComponents'
import { SlotDropButton } from './components/SlotDropButton/SlotDropButton'

export const Slot = ({
  el,
  id,
  getVacancyName,
  setSlotForm,
  setOpenDrawer,
  setFilterSlots,
}) => {
  const names = getVacancyName(el?.vacancies)

  return (
    <div
      style={{
        background: `${el.color}6e`,
      }}
      className={cls.slot}
      key={id}
    >
      <div className={cls.nameAndDate}>
        <PR2>Слот {id + 1}</PR2>
        <div className={cls.slotDate}>
          {dayjs(el.startDate).format('HH:mm')}-
          {dayjs(el.endDate).format('HH:mm')}
        </div>
      </div>
      <div className={cls.vacanciesNames}>
        <div
          onClick={() => setFilterSlots(names[0].id)}
          className={cls.vacanciesName}
        >
          {names[0].name}
        </div>
        {names.length > 1 && (
          <div className={`${cls.vacanciesName} ${cls.hoverBtn}`}>
            ...
            <div>
              {names?.length &&
                names?.map((el, id) => {
                  if (id === 0) return
                  return (
                    <div
                      onClick={() => setFilterSlots(el.id)}
                      className={cls.vacanciesName}
                    >
                      {el.name}
                    </div>
                  )
                })}
            </div>
          </div>
        )}
      </div>
      <SlotDropButton
        slot={el}
        setOpenDrawer={setOpenDrawer}
        setSlotForm={setSlotForm}
      />
    </div>
  )
}
