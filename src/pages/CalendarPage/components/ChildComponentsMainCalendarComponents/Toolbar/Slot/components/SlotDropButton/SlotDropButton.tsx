import * as React from 'react'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import cls from './SlotDropButton.module.scss'
import { Comment } from '@/UI/TextComponents'
import { MoreIcon } from '@/assets/icons/MoreIcon'
import dayjs from 'dayjs'
import { useAppSelector } from '@/hooks/useAppSelector'

export function SlotDropButton({ setOpenDrawer, setSlotForm, slot }) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const vacancies = useAppSelector((state) => state.vacancies.workState)

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  // console.log(vacancies)
  console.log(slot)

  const navigateEditPage = () => {
    setSlotForm({
      startDate: dayjs(slot.startDate),
      endDate: dayjs(slot.endDate),
      id: slot.timeSlotId,
      date: dayjs(slot.startDate).format('YYYY-MM-DD'),
      repeat: '',
      vacancies: slot.vacancies,
    })
    setOpenDrawer(true)
    setAnchorEl(null)
  }
  return (
    <div>
      <ButtonIcon
        color="btnIconWhite"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <MoreIcon />
      </ButtonIcon>
      <Menu
        className={cls.menu}
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem className={cls.menuItem} onClick={navigateEditPage}>
          <Comment>Редактировать</Comment>
        </MenuItem>
        {/* <MenuItem className={cls.menuItem} onClick={navigateFreeUpTime}>
          <Comment>Свободное время</Comment>
        </MenuItem> */}
      </Menu>
    </div>
  )
}
