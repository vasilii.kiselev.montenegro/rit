import { Comment, H1, PR2 } from '@/UI/TextComponents'
import cls from './CalendarPage.module.scss'
import { AwaitSchedulerData, calendarView } from '../consts'
import { useEffect, useState } from 'react'
import moment from 'moment'
import { Switcher } from '@/UI/Switcher/Switcher'
import { CustomScheduler } from '../components/Scheduler/CustomScheduler'
import { useAppSelector } from '@/hooks/useAppSelector'
import { NavigateDropButton } from '../components/NavigateDropButton/NavigateDropButton'
import { Drawer } from '@/components/Drawer'
import { CreateChangeSlotForm } from '../components/CreateChangeSlotForm.tsx/CreateChangeSlotForm'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { getSlotsEventsThunk } from '@/store/slices/calendarSlice/asyncThunks/getSlotsEventsThunk'
import { getWeekEventsThunk } from '@/store/slices/calendarSlice/asyncThunks/getWeekEvents'

export const CalendarPage = () => {
  const dispatch = useAppDispatch()
  const [calendarType, setCalendarType] = useState<(typeof calendarView)[0]>(
    calendarView[1]
  )

  const [currentDate, setCurrentDate] = useState<string>(
    moment().format('YYYY-MM-DD')
  )
  const [viewSlots, setViewSlots] = useState<boolean>(true)
  const [sidebarIsOpen, setSidebarIsOpen] = useState<boolean>(false)
  const [filterSlots, setFilterSlots] = useState<number | 'unfilter'>(null)
  const [slotFrom, setSlotForm] = useState<typeof AwaitSchedulerData>(null)
  const [data, setData] =
    useState<typeof AwaitSchedulerData>(AwaitSchedulerData)

  const thunkData = useAppSelector((state) => state.calendar)
  useEffect(() => {
    setData(thunkData.slotsEvents)
  }, [thunkData.status])
  const [openDrawer, setOpenDrawer] = useState<boolean>(false)
  const closeDrawer = (isOpen: boolean) => {
    setSlotForm(null)
    setOpenDrawer(isOpen)
  }
  useEffect(() => {
    if (filterSlots === 'unfilter') {
      console.log(thunkData.slotsEvents)
      console.log(filterSlots)

      setData(thunkData.slotsEvents)
      return
    }
    if (typeof filterSlots === 'number') {
      console.log('gsdfsdfsdfhhsdfhsdf')

      setData((prev) =>
        prev.filter((el) => {
          for (let i = 0; i < el.vacancies.length; i++) {
            if (el.vacancies[i] === filterSlots) return true
          }
          return false
        })
      )
    }
  }, [filterSlots])

  useEffect(() => {
    dispatch(getSlotsEventsThunk())
    dispatch(getWeekEventsThunk())
  }, [])
  return (
    <>
      <div className={cls.CalendarPage}>
        <div className={cls.calendarHeader}>
          <div className={cls.leftHeader}>
            <H1>Календарь</H1>
            <Comment>{} событий</Comment>
          </div>
          <div className={cls.rightHeader}>
            <div className={cls.viewSlotsAndAddSlot}>
              <Switcher
                onchange={() => setViewSlots((prev) => !prev)}
                value={viewSlots}
              />{' '}
              <PR2>Показать слоты</PR2>{' '}
              <NavigateDropButton setOpenDrawer={setOpenDrawer} />
            </div>
          </div>
        </div>

        <div className={cls.calendar}>
          <CustomScheduler
            calendarType={calendarType}
            currentDate={currentDate}
            data={data}
            setCalendarType={setCalendarType}
            setCurrentDate={setCurrentDate}
            viewSlots={viewSlots}
            setSidebarIsOpen={() => setSidebarIsOpen((prev) => !prev)}
            sidebarIsOpen={sidebarIsOpen}
            setSlotForm={setSlotForm}
            setOpenDrawer={setOpenDrawer}
            setFilterSlots={setFilterSlots}
            filterSlots={filterSlots}
          />
          {/* <Sidebar isActive={sidebarIsOpen} /> */}
        </div>
      </div>
      <Drawer
        isOpen={openDrawer}
        closeDrawer={closeDrawer}
        head={<H1>Добавление слота на собеседование</H1>}
        body={
          <CreateChangeSlotForm slot={slotFrom} closeDrawer={closeDrawer} />
        }
      />
    </>
  )
}
