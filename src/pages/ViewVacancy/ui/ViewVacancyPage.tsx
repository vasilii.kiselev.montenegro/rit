import { useContext, useEffect, useState } from 'react'
import cls from './ViewVacancyPage.module.scss'
import { useParams } from 'react-router-dom'
import { getVacany } from '@/api/vacancies/vacancies'
import { FullVacancy } from '@/types/vacancy'
import { Comment, H1, PM1, PM3 } from '@/UI/TextComponents'
import { MiniLogoImage } from '@/assets/images/MiniLogo'
import { getCompany } from '@/api/companies/companies'
import { GlobalIcon } from '@/assets/icons/GlobalIcon'
import { LocationIcon } from '@/assets/icons/LocationIcon'
import { FullCompany, SendCompany } from '@/types/company'
import { ExperienceIcon } from '@/assets/icons/ExperienceIcon'
import { BriefcaseIcon } from '@/assets/icons/BriefcaseIcon'
import { EmptyWalletIcon } from '@/assets/icons/EmptyWalletIcon'
import { PRWithTags } from '@/UI/TextComponents/TextComponents'
import { ModalContext } from '@/components/Modal/modalCotext'
import { CompanyModalView } from '@/components/CompanyModalView/CompanyModalView'

export const ViewVacancyPage = () => {
  const { vacancyId } = useParams()
  const [vacancy, setVacancy] = useState<FullVacancy>()
  const [company, setCompany] = useState<FullCompany>()
  const handleSet = async () => {
    const resVacancy = await getVacany(vacancyId)
    const resCompany = await getCompany(resVacancy?.companyId)
    setVacancy(resVacancy)
    setCompany(resCompany)
  }
  useEffect(() => {
    handleSet()
  }, [])

  const { handleModal } = useContext(ModalContext)

  const handleOpenModalWithCompany = (company: SendCompany) => {
    handleModal(<CompanyModalView company={company} />)
  }
  console.log(vacancy)

  return (
    <div className={cls.ViewVacancy}>
      {!vacancy ? (
        ''
      ) : (
        <div className={cls.ViewVacancyContent}>
          <div className={cls.leftSide}>
            <div className={cls.logo}>
              <MiniLogoImage />
              <div className={cls.logoSeparator}> </div>
              <img src={company.logo} alt="" className={cls.companyLogo} />
            </div>
            <H1 style={{ marginBottom: '16px' }}>{vacancy?.name}</H1>

            {company?.name && (
              <>
                <div className={cls.companyInfo}>
                  <a href={company.domainAddress} className={cls.companyDomain}>
                    <GlobalIcon /> {company.domainAddress}
                  </a>
                  <span className={cls.dot}> </span>
                  <div className={cls.companyDomain}>
                    <LocationIcon /> {company.location}
                  </div>
                  <span className={cls.dot}> </span>
                </div>
                <div onClick={() => handleOpenModalWithCompany(company)} className={cls.companyDomainAddress}>
                  {company.name} - {company.description}
                </div>
              </>
            )}
            <div className={cls.vacancyInfo}>
              <div className={cls.vacancyInfoItem}>
                <Comment>Опыт</Comment>
                <div className={cls.vacancyInfoItemValue}>
                  <ExperienceIcon /> <PM3>{vacancy.workExperience}</PM3>
                </div>
              </div>
              <div className={cls.vacancyInfoItemSeparator}></div>
              <div className={cls.vacancyInfoItem}>
                <Comment>График</Comment>
                <div className={cls.vacancyInfoItemValue}>
                  <BriefcaseIcon />
                  <PM3>{vacancy.workSchedule ? vacancy.workSchedule : 'Не указано'}</PM3>
                </div>
              </div>
              <div className={cls.vacancyInfoItemSeparator}></div>
              <div className={cls.vacancyInfoItem}>
                <Comment>Зарплата</Comment>
                <div className={cls.vacancyInfoItemValue}>
                  <EmptyWalletIcon />{' '}
                  <PM3>
                    {' '}
                    {vacancy.wages ? vacancy.wages : 'Не указано'} {vacancy.currency}{' '}
                  </PM3>
                </div>
              </div>
            </div>
          </div>
          <div className={cls.vacancyPageSeparator}></div>

          <div className={cls.rightSide}>
            <div className={cls.vacancyDescription}>
              <PM1>Описание вакансии</PM1>
              <PRWithTags isComment={true} fontSize="18px" isBold={false}>
                {vacancy.description}
              </PRWithTags>
            </div>
            <div className={cls.vacancyJobRequirements}>
              <PM1>Описание вакансии</PM1>

              <PRWithTags isComment={true} fontSize="18px" isBold={false}>
                {vacancy.jobRequirements}
              </PRWithTags>
            </div>
            <div className={cls.otherVacancies}>
              <PM1>Другие вакансии</PM1>

              <div className={cls.otherVacaniesItem}>
                <div className={cls.otherVacaniesItemName}> qweqweqw</div>
                <div className={cls.otherVacaniesItemPrice}> 70000 RUB</div>
              </div>
            </div>

            <a className={cls.telegramLink} target="_blank" href={`https://t.me/rity_learn_aiogram_bot?start=${vacancy.id}`}>
              Записаться на собеседование
            </a>
          </div>
        </div>
      )}
    </div>
  )
}
