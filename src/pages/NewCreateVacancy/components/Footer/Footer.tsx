import { ReactNode } from 'react'
import cls from './Footer.module.scss'

interface FooterProps {
  rightSide: ReactNode
  leftSide: ReactNode
}

export const Footer = ({ rightSide, leftSide }: FooterProps) => {
  return (
    <div className={cls.footer}>
      <div className={cls.leftSide}>{leftSide} </div>
      <div className={cls.rightSide}>{rightSide} </div>
    </div>
  )
}
