import { StepMethods } from '@/pages/NewCreateVacancy/ui/NewCreateVacancy'
import cls from './SecondStep.module.scss'
import { PM2, PR4 } from '@/UI/TextComponents'
import { TextArea } from '@/UI/TextArea/TextArea'
import { TextInput } from '@/UI/TextInput/TextInput'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useFormik } from 'formik'
import { VacancyCreate } from '@/types/vacancy'
import { useEffect, useState } from 'react'

import { Footer } from '../../Footer/Footer'
import { ButtonColors } from '@/UI/Button/consts'
import { Button } from '@/UI/Button/Button'
import { InputSelect } from '@/UI/InputSelect/InputSelect'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import * as Yup from 'yup'
import { getCurrencies, getShedules, sendMiniSecondStep, sendSecondStep, ShedulesType } from '@/api/createVacancy/secondStep/secondStep'

import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setValuesForSecondStep } from '@/store/slices/createVacancySlice/createVacancySlice'

interface SecondStepProps {
  stepMethods: StepMethods
}
type InitialValues = VacancyCreate

export const SecondStep = ({ stepMethods }: SecondStepProps) => {
  const dispatch = useAppDispatch()
  const vacancy = useAppSelector((store) => store.createVacancy)
  const [currencies, setCurrencies] = useState<string[]>([])
  const [shedules, setShedules] = useState<ShedulesType[]>([])
  const [showAllInputs, setShowAllInputs] = useState<boolean>(false)

  const id = useAppSelector((store) => store.createVacancy.id)
  const companyId = useAppSelector((store) => store.createVacancy.companyId)

  const initialValues: InitialValues = vacancy.name
    ? vacancy
    : {
        description: '',
        jobRequirements: '',
        name: '',
        wages: '',
        currency: '',
        placeOfWork: '',
        workExperience: '',
        workSchedule: '',
        id: id,
        companyId: companyId,
      }

  const validationSchema = Yup.object<InitialValues>().shape({
    description: Yup.string().required(),
    jobRequirements: Yup.string().required(),
    name: Yup.string().required(),
    wages: Yup.number(),
  })
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: () => {},
  })

  useEffect(() => {
    getCurrencies().then((data) => {
      setCurrencies(data)
      formik.setFieldValue('currency', data[0])
    })
    getShedules().then((data) => {
      setShedules(data)
      formik.setFieldValue('workSchedule', data[0].id)
    })
  }, [])
  useEffect(() => {
    formik.setFieldValue('id', id)
    formik.setFieldValue('companyId', companyId)
  }, [id, companyId])
  const checkMainInputs = (): boolean => {
    return !formik.values.name.length || !formik.values.description.length || !formik.values.jobRequirements.length
  }
  const clickPrevBtn = () => {
    stepMethods.decrementStep()
  }
  const clickShowAllInputs = () => {
    setShowAllInputs(true)
    sendMiniSecondStep(formik.values)
  }
  const clickNextBtn = () => {
    dispatch(setValuesForSecondStep(formik.values))
    sendSecondStep(formik.values)
    stepMethods.incrementStep()
  }
  useEffect(() => {
    const bool = checkMainInputs()
    setShowAllInputs(!bool)
  }, [])
  return (
    <>
      <div className={cls.SecondStep}>
        <form className={cls.SecondStep} onSubmit={formik.handleSubmit}>
          <div className={cls.firstInputs}>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                название<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <TextInput
                name="name"
                text={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Введите текст"
              />
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Описание вакансии<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <TextArea
                rows={3}
                text={formik.values.description}
                name="description"
                placeholder="Введите текст"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Требования к вакансии<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <TextArea
                rows={3}
                placeholder="Введите текст"
                name="jobRequirements"
                text={formik.values.jobRequirements}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </div>
          </div>
          <div className={`${cls.allInputs} ${showAllInputs ? cls.active : false}`}>
            <div className={cls.allInputsCont}>
              <div className={`${cls.secondStepInputLabel} `}>
                <PM2>Заработная плата за ваш труд / валюта</PM2>
                <div className={`${cls.secondStepInputLabelSelect}`}>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '10px',
                    }}
                  >
                    <TextInput placeholder="Введите текст" name="wages" text={formik.values.wages} onChange={formik.handleChange} />
                    {formik.errors.wages && <PR4 style={{ color: '#E60019' }}>Это поле принимает числовое значение</PR4>}
                  </div>
                  <InputSelectMini values={currencies} setValue={formik.handleChange} name="currency" value={formik.values.currency} />
                </div>
              </div>
              <div className={cls.secondStepInputLabel}>
                <PM2>График работы</PM2>
                <InputSelect values={shedules} name="workSchedule" value={formik.values.workSchedule} setValue={formik.handleChange} />
              </div>
              <div className={cls.secondStepInputLabel}>
                <PM2>Опыт работы</PM2>
                <TextInput
                  placeholder="Введите текст"
                  name="workExperience"
                  text={formik.values.workExperience}
                  onChange={formik.handleChange}
                />
              </div>
              <div className={cls.secondStepInputLabel}>
                <PM2>Место работы</PM2>
                <TextInput placeholder="Введите текст" name="placeOfWork" text={formik.values.placeOfWork} onChange={formik.handleChange} />
              </div>
            </div>
          </div>
        </form>
      </div>
      <Footer
        leftSide={
          <Button onClick={clickPrevBtn} color={ButtonColors.ICON_GREEN}>
            Назад
          </Button>
        }
        rightSide={
          <Button
            onClick={() => (!showAllInputs ? clickShowAllInputs() : clickNextBtn())}
            disabled={!showAllInputs ? checkMainInputs() : !!Object.values(formik.errors).length}
            color={ButtonColors.ICON_GREEN}
          >
            Далее
          </Button>
        }
      />
    </>
  )
}
