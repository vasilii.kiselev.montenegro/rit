import { Button } from '@/UI/Button/Button'
import cls from './FirstTab.module.scss'
import { ButtonColors } from '@/UI/Button/consts'
import { PM2, PM3, PR2 } from '@/UI/TextComponents'
import { OldQuestionsItem } from './components/OldQuestionsItem/OldQuestionsItem'
import { TextArea } from '@/UI/TextArea/TextArea'
import { MessageEditIcon } from '@/assets/icons/MessageEditIcon'
import { useState } from 'react'
import { RobotLoaderImg } from '@/assets/images/RobotLoaderImg'
import { Question } from '@/types/question'
import {
  actualizeQuestion,
  createQuestionWithPrevious,
  deleteQuestion,
  generateQuestion,
  getAllQuestions,
} from '@/api/createVacancy/thirdStep/thirdStep'
import { Skeleton } from '@mui/material'

interface FirstTabProps {
  question: Question
  setQuestion: React.Dispatch<React.SetStateAction<Question>>
}

export const FirstTab = ({ question, setQuestion }: FirstTabProps) => {
  const [oldQuestions, setOldQuestions] = useState<Question[]>()
  const [oldQuestionsView, setOldQuestionsView] = useState<boolean>(false)
  const [changeQuestion, setChangeQuestion] = useState<string>()
  const [changeQuestionIsOpen, setChangeQuestionIsOpen] = useState<boolean>()
  const [oldQuestionsItem, setOldQuestionsItem] = useState<Question>()
  const [loadAllQuestions, setLoadAllQouestions] = useState<boolean>(false)

  const openChangeQuestion = () => {
    setChangeQuestion(question.text)
    setChangeQuestionIsOpen(true)
  }
  const closeOldQuestions = () => {
    setOldQuestionsItem(null)
    setOldQuestionsView(false)
  }
  const saveChangeQuestion = () => {
    createQuestionWithPrevious(question, changeQuestion).then((data) => {
      if (data) {
        setQuestion(data)
        setChangeQuestionIsOpen(false)
      }
    })
  }
  const undoChanges = () => {
    setChangeQuestion('')
    setChangeQuestionIsOpen(false)
  }
  const regenerateQuestion = () => {
    const vacancyId = question.vacancy
    setQuestion(null)
    closeOldQuestions()
    generateQuestion(vacancyId).then((data) => {
      setQuestion(data)
    })
  }
  const viewOldQuestions = () => {
    console.log(oldQuestionsView)

    if (!oldQuestionsView) {
      setOldQuestionsView(true)
      setLoadAllQouestions(true)
      getAllQuestions(question.vacancy, question.id).then((data) => {
        setOldQuestions(data)
        setLoadAllQouestions(false)
      })
    } else {
      closeOldQuestions()
    }
  }
  console.log(oldQuestionsItem)
  const actualizeOldQuestion = () => {
    closeOldQuestions()
    actualizeQuestion(oldQuestionsItem.id).then((data) => {
      console.log(data)

      setQuestion(data)
    })
  }
  const deleteOldQuestionItem = (id: number) => {
    setOldQuestions((state) => state.filter((el) => el.id !== id))
    deleteQuestion(id)
  }
  return (
    <div className={cls.FirstTab}>
      <PM2>Сгенерированный вопрос:</PM2>
      <div className={cls.generatedQuestion}>
        {question && (
          <div
            className={`${cls.generatedQuestionEditIcon} ${
              changeQuestionIsOpen || oldQuestionsView ? cls.disable : ''
            } ${oldQuestionsView ? cls.Tooltip : ''}  `}
            onClick={
              !changeQuestionIsOpen && !oldQuestionsView && openChangeQuestion
            }
          >
            {oldQuestionsView && <span>Сначла выберите вопрос</span>}
            <MessageEditIcon />
          </div>
        )}
        <PR2>
          {question ? (
            question.text
          ) : (
            <div className={cls.generatedQuestionSkeleton}>
              <div className={cls.dotsWithAnimation}>
                <div className={cls.dot}> </div>
                <div className={cls.dot}> </div>
                <div className={cls.dot}> </div>
              </div>
              <RobotLoaderImg />
              <div className={cls.loaderText}>Будим робота </div>
            </div>
          )}
        </PR2>
      </div>
      {changeQuestionIsOpen ? (
        <div className={cls.editGeneratedQuestion}>
          <PM2>Отредактированный вопрос:</PM2>
          <TextArea
            text={changeQuestion}
            onChange={(e) => setChangeQuestion(e.target.value)}
          />
          <div className={cls.editGeneratedQuestionBtns}>
            <Button color={ButtonColors.RED} onClick={undoChanges}>
              Отменить
            </Button>
            <Button
              disabled={
                question.text === changeQuestion || changeQuestion.length == 0
              }
              onClick={saveChangeQuestion}
              color={ButtonColors.GREEN}
            >
              Сохранить
            </Button>
          </div>
        </div>
      ) : (
        <div className={cls.generatedQuestionBtns}>
          <Button
            disabled={question === null}
            color={ButtonColors.GREEN}
            onClick={regenerateQuestion}
          >
            Сгенерировать вопрос
          </Button>
          {question && (
            <Button
              color={ButtonColors.GREY}
              className={oldQuestionsView && 'active'}
              onClick={viewOldQuestions}
            >
              Показать банк вопросов
            </Button>
          )}
        </div>
      )}

      {oldQuestionsView && (
        <div className={cls.oldQuestionsContainer}>
          <PM2>Банк вопросов:</PM2>
          <div className={cls.oldQuestionsList}>
            {!!oldQuestions?.length &&
              oldQuestions.map((el) => {
                return (
                  <OldQuestionsItem
                  deleteOldQuestionItem={deleteOldQuestionItem}
                  oldQuestionsItem={oldQuestionsItem}
                  el={el}
                  key={el.id}
                  setOldQuestionsItem={setOldQuestionsItem}
                  />
                )
              })}
              {loadAllQuestions ? <Skeleton /> :
            !oldQuestions?.length && <PM3>Пока что нет</PM3>}
          </div>
          <div className={cls.oldQuestionBtns}>
            {!!oldQuestions?.length && (
              <>
                <Button
                  onClick={actualizeOldQuestion}
                  disabled={!oldQuestionsItem}
                  color={ButtonColors.GREEN}
                >
                  Выбрать
                </Button>
                <Button color={ButtonColors.GREY} onClick={closeOldQuestions}>
                  Закрыть
                </Button>
              </>
            )}
          </div>
        </div>
      )}
    </div>
  )
}
