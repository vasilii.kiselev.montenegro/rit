import { Comment, PR1 } from '@/UI/TextComponents'
import cls from './DropDownSection.module.scss'
import { ReactNode } from 'react'
import { FullArrowIcon } from '@/assets/icons/FullArrowIcon'

interface DropDownSectionProps {
  label: string
  form: ReactNode
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const DropDownSection = ({ label, form, open, setOpen }: DropDownSectionProps) => {
  return (
    <div className={cls.DropDownSection}>
      <div className={cls.dropDownSectionHeading} onClick={() => setOpen((prev) => !prev)}>
        <Comment>
          <PR1>{label}</PR1>
        </Comment>
        <FullArrowIcon style={{ rotate: open && '180deg' }} />
      </div>
      {open && <div className={cls.dropDownSectionBody}>{form}</div>}
    </div>
  )
}
