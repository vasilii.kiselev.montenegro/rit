import { useAppSelector } from '@/hooks/useAppSelector'
import cls from './FirstStep.module.scss'
import { TextInput } from '@/UI/TextInput/TextInput'
import { SearchIcon } from '@/assets/icons/SearchIcon'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { useContext, useState } from 'react'
import { CompanyItem } from './components/CompanyItem'
import { H1 } from '@/UI/TextComponents'
import { CompanyForm } from '@/components/CompanyForm'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { StepMethods } from '@/pages/NewCreateVacancy/ui/NewCreateVacancy'
import { Footer } from '../../Footer/Footer'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { useDispatch } from 'react-redux'
import { cleanState, setCompanyId, setId } from '@/store/slices/createVacancySlice/createVacancySlice'
import { sendFirstStep } from '@/api/createVacancy/firstStep/firstStep'
import { DrawerContext } from '@/components/Drawer/DrawerContext'
import { SendCompany } from '@/types/company'

interface FirstStepProps {
  stepMethods: StepMethods
}

export const FirstStep = ({ stepMethods }: FirstStepProps) => {
  const companyArr = useAppSelector((store) => store.companies.workState)
  const companyState = useAppSelector((store) => store.createVacancy.companyId)
  const [company, setCompany] = useState<number>(companyState)
  const [search, setSearch] = useState<string>('')
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const clickPrev = () => {
    dispatch(cleanState())
    navigate(Url.MAIN)
  }
  const clickNext = () => {
    sendFirstStep(company).then((data) => {
      dispatch(setCompanyId(company))
      dispatch(setId(data))
    })
    stepMethods.incrementStep()
  }
  const { handleDrawer } = useContext(DrawerContext)

  const handleOpenDrawerWithCompany = (companyInfo: SendCompany, companyOperation: 'new' | 'change') => {
    handleDrawer(
      <CompanyForm operation={companyOperation} company={companyInfo} setClose={handleDrawer} />,
      <H1>{companyOperation ? 'Редактирование компании ' : 'Добавление новой компании'}</H1>
    )
  }

  return (
    <>
      {' '}
      <div className={cls.FirstStep}>
        <div className={cls.FirstStepHeader}>
          <TextInput
            placeholder="Введите название компании"
            img={<SearchIcon />}
            text={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <Button
            color={ButtonColors.BLACK}
            onClick={() => {
              handleOpenDrawerWithCompany(null, 'new')
            }}
          >
            добавить компанию <PlusIcon />
          </Button>
        </div>
        <div className={cls.test}>
          <div className={cls.FirstStepBody}>
            {companyArr &&
              companyArr
                ?.filter((el) => el.name.toLowerCase().includes(search.toLowerCase()))
                .map((el) => <CompanyItem key={el.name} company={el} active={company} setCompany={setCompany} />)}
          </div>
        </div>
      </div>
      <Footer
        leftSide={
          <Button onClick={clickPrev} color={ButtonColors.ICON_GREEN}>
            Отменить
          </Button>
        }
        rightSide={
          <Button onClick={clickNext} disabled={!company} color={ButtonColors.ICON_GREEN}>
            Далее
          </Button>
        }
      />
    </>
  )
}
