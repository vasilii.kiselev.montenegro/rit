import isoWeek from 'dayjs/plugin/isoWeek'
import dayjs from 'dayjs'
export const interiewType = ['Онлайн', 'Очное']
export const repeatSlot = ['Никогда', 'Будние', 'Каждый день']
export const duration = ['15 мин', '30 мин', '45 мин', '1 час', '1.5 часа', '2 часа']
const weekDays = ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
export const generateWeek = () => {
  dayjs.extend(isoWeek)
  dayjs.locale('ru')

  const week = []
  const returnWeek = []
  const currentDate = dayjs()

  for (let i = 0; i < 7; i++) {
    const currentWeekDay = dayjs().add(i, 'day').day()
    const date = currentDate.add(i, 'day').date()

    week.push({ dayWeek: currentWeekDay, day: date })
  }
  console.log(week)

  const month = dayjs().month()
  for (let i = 0; i < 7; i++) {
    returnWeek.push(`${weekDays[week[i]?.dayWeek]}, ${months[month]}, ${week[i].day}`)
  }

  return returnWeek
}

// generate week for slot form
export const newGenerateWeek = (date?: string) => {
  const thisDate = date ? dayjs(date) : dayjs()
  const week = []
  const returnWeek = []
  const valuesDays = []

  for (let i = 0; i < 7; i++) {
    const a = thisDate.add(i, 'day').format('d.DD.MM').split('.').map(Number)
    week.push({ dayWeek: a[0], day: a[1], month: a[2] })
    valuesDays.push(thisDate.add(i, 'day').format('YYYY-MM-DD'))
  }

  for (let i = 0; i < 7; i++) {
    returnWeek.push({ label: `${weekDays[week[i].dayWeek]}, ${months[week[i].month - 1]}, ${week[i].day}`, value: valuesDays[i] })
  }
  console.log(returnWeek)

  return returnWeek
}
// export const testingTypes = ['AUTO', ' MANUAL', 'NO_QUESTION']
