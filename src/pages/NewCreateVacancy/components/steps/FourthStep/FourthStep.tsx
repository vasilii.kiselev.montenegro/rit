import { StepMethods } from '@/pages/NewCreateVacancy/ui/NewCreateVacancy'
import cls from './FourthStep.module.scss'
import { Footer } from '../../Footer/Footer'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { BlackCalendarIcon } from '@/assets/icons/BlackCalendarIcon'
import { ClockIcon } from '@mui/x-date-pickers'
import { HourglassIcon } from '@/assets/icons/HourglassIcon'
import { TextArea } from '@/UI/TextArea/TextArea'
import { InputSelect } from '@/UI/InputSelect/InputSelect'
import { useFormik } from 'formik'
import { CreateSlotForm, FourthStepValues } from '@/types/vacancy'
import dayjs from 'dayjs'
import { generateWeek, interiewType } from './utils'
import * as Yup from 'yup'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { sendSlotInFourthStep, sendVacancyFourthStep } from '@/api/createVacancy/fourthStep/fourthStep'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { cleanState } from '@/store/slices/createVacancySlice/createVacancySlice'
import { TimeInput } from '@/UI/TimeInput/TimeInput'
import { NumberInput } from '@/UI/NumberInput/NumberInput'
import { repeatSlot } from '@/types/calendar'
import { useContext } from 'react'
import { ModalContext } from '@/components/Modal/modalCotext'
import { TimeslotsErorrsModal } from '@/components/TimeslotsErorrsModal/TimeslotsErorrsModal'

interface FourthStepProps {
  stepMethods: StepMethods
}

export const FourthStep = ({ stepMethods }: FourthStepProps) => {
  const weekValues = generateWeek()
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { handleModal } = useContext(ModalContext)
  const handleOpenModalInfoTimeslots = (text: string) => {
    handleModal(<TimeslotsErorrsModal text={text} />)
  }
  const vacancy = useAppSelector((store) => store.createVacancy)
  const initValues: FourthStepValues = {
    interviewType: interiewType[0],
    descriptionInvitation: '',
    interviewDuration: 20,
    startTime: dayjs().startOf('day').subtract(15, 'hour'),
    endTime: dayjs().startOf('day').subtract(13, 'hour'),
    day: weekValues[0],
    recurrenceType: repeatSlot[0].value,
    recurrenceDays: [],
  }
  const valiadtionSchema = Yup.object<CreateSlotForm>().shape({
    interviewType: Yup.string().required(),
    descriptionInvitation: Yup.string(),
    day: Yup.string().required(),
    recurrenceType: Yup.string(),
    interviewDuration: Yup.number().min(15),

    startTime: Yup.mixed().required('Обязательное поле'),
    endTime: Yup.mixed()
      .required('Обязательное поле')
      .test('is-valid-end', 'Длительность слота должна быть не меньше длительности собеседования.', function () {
        console.log(this.parent)
        const { startTime, endTime, interviewDuration } = this.parent
        const start = startTime.minute() + startTime.hour() * 60
        const end = endTime.minute() + endTime.hour() * 60

        return end - start >= interviewDuration
      }),
  })
  const formik = useFormik({
    initialValues: initValues,
    validationSchema: valiadtionSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      sendSlotInFourthStep(values, vacancy.id)
        .then(() => {
          const sendValues = {
            descriptionInvitation: values.descriptionInvitation,
            interviewType: values.interviewType,
            interviewDuration: values.interviewDuration,
          }

          const sendVacancy = { ...vacancy, ...sendValues }
          sendVacancyFourthStep(sendVacancy)
          navigate(Url.MAIN)
          dispatch(cleanState())
        })
        .catch((str: string) => {
          handleOpenModalInfoTimeslots(str)
        })
    },
  })

  const skipSlot = () => {
    const sendValues = {
      descriptionInvitation: formik.values.descriptionInvitation,
      interviewType: formik.values.interviewType,
      interviewDuration: formik.values.interviewDuration,
    }
    const sendVacancy = { ...vacancy, ...sendValues }
    sendVacancyFourthStep(sendVacancy)
    navigate(Url.MAIN)
    dispatch(cleanState())
  }

  return (
    <>
      <div className={cls.FourthStep}>
        <div className={cls.FormImputLabel}>
          <PM2>
            Тип собеседования <span className={cls.redStart}>*</span>
          </PM2>
          <InputSelect name="interviewType" value={formik.values.interviewType} setValue={formik.handleChange} values={interiewType} />
        </div>
        <div className={cls.FormImputLabel}>
          <PM2>Описание к приглашению на собеседование </PM2>
          <TextArea
            rows={2}
            placeholder="Введите текст"
            name="descriptionInvitation"
            text={formik.values.descriptionInvitation}
            onChange={formik.handleChange}
          />
        </div>
        <div className={cls.FormImputLabel}>
          <PM2>
            Длительность собеседования <span className={cls.redStart}>*</span>
          </PM2>
          <div className={cls.FormImputLabelSelectIcon}>
            <HourglassIcon style={{ width: '24px' }} />

            <NumberInput
              name="interviewDuration"
              text={formik.values.interviewDuration}
              placeholder="20 min"
              onChange={(value) => formik.setFieldValue('interviewDuration', value)}
              min={0}
              max={180}
            />
            <Comment>мин</Comment>
          </div>
        </div>
        <div className={cls.FormImputLabel}>
          <PM2>
            Выбор времени <span className={cls.redStart}>*</span>
          </PM2>
          <div className={cls.FormImputLabelDoubleSelectTime}>
            <div className={cls.FormImputLabelSelectTimeInputs}>
              <ClockIcon />
              <div className={cls.FormImputLabelSelectTime}>
                <TimeInput
                  name="startTime"
                  value={formik.values.startTime}
                  onChange={(value) => formik.setFieldValue('startTime', value)}
                />

                <Comment>-</Comment>

                <TimeInput name="endTime" value={formik.values.endTime} onChange={(value) => formik.setFieldValue('endTime', value)} />
              </div>
              {/*@ts-expect-error */}
              <div className={cls.error}>{formik.errors.endTime} </div>
            </div>
            <div>
              {formik.values.recurrenceType !== repeatSlot[0].value && <PR2>Начать с:</PR2>}
              <div className={cls.FormImputLabelSelectIcon}>
                <BlackCalendarIcon />
                <InputSelectMini values={weekValues} placeholder=" " name="day" value={formik.values.day} setValue={formik.handleChange} />
              </div>
            </div>
          </div>
        </div>
        <div className={cls.FormImputLabel}>
          <PM2>Повтор слота</PM2>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="recurrenceType"
            value={formik.values?.recurrenceType}
            onChange={formik.handleChange}
            className={cls.FormImputLabelRadio}
          >
            {repeatSlot.map((el) => {
              return (
                <FormControlLabel
                  style={{ marginLeft: '0' }}
                  key={el.value}
                  value={el.value}
                  control={<Radio style={{ display: 'none' }} />}
                  label={
                    <div className={cls.FormImputLabelRadioItem}>
                      <div className={`${cls.FormImputLabelRadioItemIcon} ${el.value === formik.values?.recurrenceType ? cls.active : ''}`}>
                        &#10003;
                      </div>
                      <Comment> {el.label}</Comment>
                    </div>
                  }
                />
              )
            })}
          </RadioGroup>
        </div>
      </div>
      <Footer
        leftSide={
          <Button onClick={stepMethods.decrementStep} color={ButtonColors.ICON_GREEN}>
            Назад
          </Button>
        }
        rightSide={
          <>
            <Button color={ButtonColors.ICON_GREEN} onClick={skipSlot}>
              Пропустить
            </Button>
            <Button color={ButtonColors.ICON_GREEN} onClick={formik.handleSubmit} disabled={!!Object.keys(formik.errors).length}>
              Отправить вакансию
            </Button>
          </>
        }
      />
    </>
  )
}
