import { useState } from 'react'
import { Stepper } from '../components/Stepper/Stepper'
import cls from './NewCreateVacancy.module.scss'
import { H1 } from '@/UI/TextComponents'
import { useAppSelector } from '@/hooks/useAppSelector'
import { FirstStep } from '../components/steps/FirstStep/FirstStep'
import { SecondStep } from '../components/steps/SecondStep/SecondStep'
import { ThirdStep } from '../components/steps/ThirdStep/ThirdStep'
import { FourthStep } from '../components/steps/FourthStep/FourthStep'

export interface Step {
  target: number
}

export interface StepMethods {
  incrementStep: () => void
  decrementStep: () => void
}
export const NewCreateVacancy = () => {
  const [step, setStep] = useState<Step>({
    target: 0,
  })
  const vacancyName = useAppSelector((store) => store.createVacancy.name)
  const stepMethods = {
    incrementStep: () =>
      setStep((state) => ({ ...state, target: state.target + 1 })),
    decrementStep: () =>
      setStep((state) => ({ ...state, target: state.target - 1 })),
  }
  return (
    <div className={cls.CreateVacancy} id="createVacancy">
      <div className={cls.CreateVacancyContent}>
        <div className={cls.stepperAndTitle}>
          <H1>Добваление вакансии {vacancyName && `: ${vacancyName}`}</H1>
          <Stepper step={step} />
        </div>
        <div className={cls.stepPage}>
          {step.target === 0 && <FirstStep stepMethods={stepMethods} />}
          {step.target === 1 && <SecondStep stepMethods={stepMethods} />}
          {step.target === 2 && <ThirdStep stepMethods={stepMethods} />}
          {step.target === 3 && <FourthStep stepMethods={stepMethods} />}
        </div>
      </div>
    </div>
  )
}
