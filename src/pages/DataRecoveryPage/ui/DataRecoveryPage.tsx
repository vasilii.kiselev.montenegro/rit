import { H1, PR2 } from '@/UI/TextComponents'
import cls from './DataRecoveryPage.module.scss'
import { TextInput } from '@/UI/TextInput/TextInput'
import { useFormik } from 'formik'
import { Button } from '@/UI/Button/Button'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { useState } from 'react'
import { sendRecoveryCode } from '@/api/profile/profile'
import { ButtonColors } from '@/UI/Button/consts'

type InitValues = {
  loginEmail: string
  code: string
}
const initValues: InitValues = {
  loginEmail: '',
  code: '',
}

export const DataRecoveryPage = () => {
  const navigate = useNavigate()
  const [codeInput, setCodeInput] = useState<boolean>(false)
  const formik = useFormik({
    initialValues: initValues,

    onSubmit(values) {
      console.log(values)
      sendRecoveryCode(values)
    },
  })
  const handleSubmit = () => {
    formik.submitForm()
    navigate(Url.MAIN)
  }
  return (
    <div className={cls.DataRecoveryPage}>
      <H1>Восстановление данных</H1>
      <form action="">
        <div className={cls.inputLabels}>
          <div className={cls.InputLabel}>
            <PR2>
              Email Или Логин <span className={cls.redStar}>*</span>
            </PR2>
            <TextInput
              name="loginEmail"
              text={formik.values.loginEmail}
              onChange={formik.handleChange}
              placeholder="Введите текст"
            />
          </div>
          {codeInput && (
            <div className={cls.InputLabel}>
              <PR2>
                код <span className={cls.redStar}>*</span>
              </PR2>
              <TextInput
                name="code"
                text={formik.values.code}
                onChange={formik.handleChange}
                placeholder="Введите текст"
              />
            </div>
          )}
        </div>
        <div className={cls.buttons}>
          <Button
            color={ButtonColors.GREEN}
            disabled={!formik.values.loginEmail}
            className={cls.submitButton}
            onClick={() => (!codeInput ? setCodeInput(true) : handleSubmit())}
          >
            {codeInput ? 'Восстановить' : 'Получить код'}
          </Button>
          <Button
            color={ButtonColors.GREY}
            className={cls.submitButton}
            onClick={() => navigate(Url.MAIN)}
          >
            Вернуться
          </Button>
        </div>
      </form>
    </div>
  )
}
