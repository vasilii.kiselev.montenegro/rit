import { useEffect, useState } from 'react'
import cls from './ThirdStep.module.scss'
import { ThirdStepTabs } from './components/Tabs/ThirdStepTabs'
import { FirstTabContent } from './components/FirstTabContent/FirstTabContent'
import { SecondTabContent } from './components/SecondTabContent/SecondTabContent'
import { ThirdTabContent } from './components/ThirdTabContent/ThirdTabContent'
import { QuestionSettingsInterface } from '../../ui/CreateVacancy'

interface ThirdStepProps {
  setQuestion: React.Dispatch<React.SetStateAction<boolean | string>>
  setTestingMethod: React.Dispatch<React.SetStateAction<number>>
  vacancyId: number
  setQuestionSettings: React.Dispatch<
    React.SetStateAction<QuestionSettingsInterface>
  >
}

export const ThirdStep = ({
  setQuestion,
  vacancyId,
  setTestingMethod,
  setQuestionSettings,
}: ThirdStepProps) => {
  const [tab, setTab] = useState(0)
  useEffect(() => {
    if (tab === 2) {
      setQuestion(true)
    } else {
      setQuestion(null)
    }
    setTestingMethod(tab)
  }, [tab])

  return (
    <div className={cls.ThirdStep}>
      <ThirdStepTabs tab={tab} setTab={setTab} />
      {tab === 0 && (
        <FirstTabContent
          vacancyId={vacancyId}
          setQuestionUp={setQuestion}
          setQuestionSettings={setQuestionSettings}
        />
      )}
      {tab === 1 && (
        <SecondTabContent vacancyId={vacancyId} setQuestionUp={setQuestion} />
      )}
      {tab === 2 && <ThirdTabContent />}
    </div>
  )
}
