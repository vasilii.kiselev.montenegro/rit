import { PM2, PM3, PR2 } from '@/UI/TextComponents'
import cls from './FirstTabContent.module.scss'
import { MessageEditIcon } from '@/assets/icons/MessageEditIcon'
import { useEffect, useLayoutEffect, useState } from 'react'
import { getQuestion } from '@/api/question'
import { Skeleton } from '@mui/material'
import { Button } from '@/UI/Button/Button'
import { TextArea } from '@/UI/TextArea/TextArea'
import { OldQuestionsItem } from './components/OldQuestionsItem/OldQuestionsItem'
import { QuestionTest } from '../DropDownSectionForms/QuestionTest/QuestionTest'
import { QuestionSettings } from '../DropDownSectionForms/QuestionSettings/QuestionSettings'
import {
  generateQuestion,
  getAllQuestions,
  createQuestion,
  actualizeQuestion,
} from '@/api/question/question'
import { ButtonColors } from '@/UI/Button/consts'
import { QuestionSettingsInterface } from '@/pages/CreateVacancy/ui/CreateVacancy'

interface FirstTabContentProps {
  setQuestionUp: React.Dispatch<React.SetStateAction<boolean | string>>
  vacancyId: { number: number }
  setQuestionSettings: React.Dispatch<
    React.SetStateAction<QuestionSettingsInterface>
  >
}
type Question = string

export const FirstTabContent = ({
  setQuestionUp,
  vacancyId,
  setQuestionSettings,
}: FirstTabContentProps) => {
  const [question, setQuestion] = useState<Question>()
  const [changeQuestion, setChangeQuestion] = useState<string>('')
  const [oldQuestionsView, setOldQuestionsView] = useState<boolean>(false)
  const [oldQuestionsItem, setOldQuestionsItem] = useState<number>()
  const [oldQuestions, setOldQuestions] = useState<string[]>()
  const [questionId, setQuestionId] = useState<number>()
  console.log(questionId, oldQuestionsItem)

  useLayoutEffect(() => {
    if (vacancyId) {
      console.log('vacancy', vacancyId)
      getQuestion(vacancyId).then(([text, id]) => {
        console.log(id, text)

        setQuestion(text)
        setQuestionId(id)
      })
      getAllQuestions(vacancyId).then((data) => {
        console.log('asfasfasffasf', data)

        const withoutLast = data.filter((el) => el.id !== questionId) // для удаления актуального вопроса из списка предыдущих.
        setOldQuestions(withoutLast)
      })
    }
  }, [])
  useEffect(() => {
    setQuestionUp(question)
  }, [question])

  const editQuestion = () => {
    console.log(vacancyId, question)
    createQuestion(vacancyId.id, changeQuestion).then((data) => {
      setQuestion(changeQuestion)
      setQuestionId(data)
      setChangeQuestion('')
    })
  }
  console.log('oldQuestions', oldQuestions)
  const regenerateQuestion = () => {
    setOldQuestions((prev) => {
      return [...prev, { body: question, id: questionId }]
    })
    setQuestion(null)
    generateQuestion(vacancyId).then(([text, id]) => {
      setQuestion(text)
      setQuestionId(id)
    })
  }
  const clearOldTargetItem = () => {
    setOldQuestionsView(false)
    setOldQuestionsItem(null)
  }
  const editQuestionWithOldItem = () => {
    setOldQuestionsView(false)

    setQuestion(
      oldQuestions.filter((el) => {
        console.log(el.id === oldQuestionsItem)
        console.log(el, oldQuestionsItem)

        return el.id === oldQuestionsItem
      })[0].body
    )
    getAllQuestions(vacancyId).then((data) => {
      console.log('asfasfasffasf', data)

      const withoutLast = data.filter((el) => el.id !== questionId) // для удаления актуального вопроса из списка предыдущих.
      setOldQuestions(withoutLast)
    })
    getQuestion(vacancyId).then(([text, id]) => {
      console.log(id, text)

      setQuestion(text)
      setQuestionId(id)
    })
    actualizeQuestion(oldQuestionsItem)
    setOldQuestionsItem(null)
  }

  useEffect(() => {
    if (!oldQuestionsView) {
      setOldQuestionsItem(null)
    }
  }, [oldQuestionsView])
  return (
    <div className={cls.FirstTabContent}>
      <PM2>Сгенерированный вопрос:</PM2>
      <div className={cls.generatedQuestion}>
        <div
          className={`${cls.generatedQuestionEditIcon} ${
            oldQuestionsView ? cls.disable : ''
          }`}
          onClick={() => !oldQuestionsView && setChangeQuestion(question)}
        >
          <MessageEditIcon />
        </div>
        <PR2>
          {question ? (
            question
          ) : (
            <div className={cls.generatedQuestionSkeleton}>
              Ваш вопрос генерируется
              <Skeleton width="100%" />
              <Skeleton width="100%" />
            </div>
          )}
        </PR2>
      </div>
      {changeQuestion && (
        <div className={cls.editGeneratedQuestion}>
          <PM2>Отредактированный вопрос:</PM2>
          <TextArea
            text={changeQuestion}
            onChange={(e) => setChangeQuestion(e.target.value)}
          />
          <div className={cls.editGeneratedQuestionBtns}>
            <Button
              color={ButtonColors.RED}
              onClick={() => setChangeQuestion('')}
            >
              Отменить
            </Button>
            <Button
              disabled={question === changeQuestion}
              color={ButtonColors.GREEN}
              onClick={editQuestion}
            >
              Сохранить
            </Button>
          </div>
        </div>
      )}
      {!changeQuestion?.length && (
        <div className={cls.generatedQuestionBtns}>
          <Button
            disabled={question === null}
            color={ButtonColors.GREEN}
            onClick={regenerateQuestion}
          >
            Сгенерировать вопрос
          </Button>
          <Button
            color={ButtonColors.GREY}
            className={oldQuestionsView && 'active'}
            onClick={() => setOldQuestionsView((prev) => !prev)}
          >
            Показать предыдущие
          </Button>
        </div>
      )}
      {oldQuestionsView && (
        <div className={cls.oldQuestionsContainer}>
          <PM2>Предыдущие вопросы:</PM2>
          <div className={cls.oldQuestionsList}>
            {oldQuestions.map((el, id) => {
              return (
                <OldQuestionsItem
                  oldQuestionsItem={oldQuestionsItem}
                  id={el.id}
                  key={id}
                  setOldQuestionsItem={setOldQuestionsItem}
                  el={el.body}
                />
              )
            })}
            {!oldQuestions.length && <PM3>Пока что нет</PM3>}
          </div>
          <div className={cls.oldQuestionBtns}>
            <Button
              disabled={question === changeQuestion}
              color={ButtonColors.GREEN}
              onClick={editQuestionWithOldItem}
            >
              Выбрать
            </Button>
            <Button color={ButtonColors.GREY} onClick={clearOldTargetItem}>
              Закрыть
            </Button>
          </div>
        </div>
      )}
      <div className={cls.questionDropdownForms}>
        <QuestionTest questionId={questionId} />
        <QuestionSettings setQuestionSettings={setQuestionSettings} />
      </div>
    </div>
  )
}
