import { PR3 } from '@/UI/TextComponents'
import cls from './ThirdStepTabs.module.scss'

interface ThirdStepTabsProps {
  tab: number
  setTab: React.Dispatch<React.SetStateAction<number>>
}

export const ThirdStepTabs = ({ tab, setTab }: ThirdStepTabsProps) => {
  return (
    <div className={cls.ThirdStepTabs}>
      <label className={cls.StepHeadingItem} onClick={() => setTab(0)}>
        <input name="tabRadio" type="radio" id="1-inputRadio" />
        <div
          className={`${cls.tabsRadioIcon} ${tab === 0 && cls.active}`}
        ></div>
        <PR3>Авто тестирование</PR3>
      </label>
      <label className={cls.StepHeadingItem} onClick={() => setTab(1)}>
        <input name="tabRadio" type="radio" id="2-inputRadio" />
        <div
          className={`${cls.tabsRadioIcon} ${tab === 1 && cls.active}`}
        ></div>
        <PR3>Ручное тестирование</PR3>
      </label>
      <label className={cls.StepHeadingItem} onClick={() => setTab(2)}>
        <input name="tabRadio" type="radio" id="3-inputRadio" />
        <div
          className={`${cls.tabsRadioIcon} ${tab === 2 && cls.active}`}
        ></div>
        <PR3>Не использовать вопрос</PR3>
      </label>
    </div>
  )
}
