import { PM2, PR2 } from '@/UI/TextComponents'
import cls from './SecondTabContent.module.scss'
import { MessageEditIcon } from '@/assets/icons/MessageEditIcon'

import { useEffect, useState } from 'react'
import { TextArea } from '@/UI/TextArea/TextArea'
import { Button } from '@/UI/Button/Button'
import { QuestionTest } from '../DropDownSectionForms/QuestionTest/QuestionTest'
import { QuestionSettings } from '../DropDownSectionForms/QuestionSettings/QuestionSettings'
import { createQuestion } from '@/api/question/question'

interface SecondTabContentProps {
  setQuestionUp: React.Dispatch<React.SetStateAction<boolean | string>>
  vacancyId: { id: number }
}

export const SecondTabContent = ({
  setQuestionUp,
  vacancyId,
}: SecondTabContentProps) => {
  const [question, setQuestion] = useState<string>('')
  const [save, setSave] = useState<boolean>(false)
  const addQuestion = () => {
    createQuestion(vacancyId.id, question).then((data) => {
      setQuestion(data)
      setSave(true)
    })
  }
  useEffect(() => {
    setQuestionUp(question ? question : null)
  }, [save])

  return (
    <div className={cls.SecondTabContent}>
      <PM2>Ваш впорос:</PM2>

      {!save && (
        <div className={cls.writingQuestionCont}>
          <TextArea
            text={question}
            onChange={(e) => setQuestion(e.target.value)}
            placeholder="Введите свой вопрос для тестирования"
          />
          <div
            className={`${cls.writingQuestionBtnsCont} ${
              question ? cls.right : ''
            }`}
          >
            {question && (
              <Button
                color="btnRed"
                onClick={() => {
                  setQuestion('')
                  setSave(true)
                }}
              >
                Отменить
              </Button>
            )}
            <Button
              disabled={question === ''}
              color="btnGreen"
              onClick={() => addQuestion()}
              // onClick={() => setSave(true)}
            >
              Сохранить
            </Button>
          </div>
        </div>
      )}

      {save && (
        <>
          <div className={cls.writedQuestion}>
            <div
              className={cls.writedQuestionEditIcon}
              onClick={() => setSave(false)}
            >
              <MessageEditIcon />
            </div>
            <PR2>{question}</PR2>
          </div>
        </>
      )}
      <div className={cls.questionDropdownForms}>
        <QuestionTest />
        {/* <QuestionSettings /> */}
      </div>
    </div>
  )
}
