import { PM2, PR4 } from '@/UI/TextComponents'
import cls from './SecondStep.module.scss'
import { InputSelect } from '@/UI/InputSelect/InputSelect'
import { useFormik } from 'formik'
import { FullVacancy, VacancyCreate } from '@/types/vacancy'
import { useEffect, useRef } from 'react'
import { TextArea } from '@/UI/TextArea/TextArea'
import { TextInput } from '@/UI/TextInput/TextInput'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'

interface SecondStepProps {
  setVacancy: React.Dispatch<React.SetStateAction<VacancyCreate>>
  setPreload: React.Dispatch<React.SetStateAction<boolean>>
  setValidateWages: React.Dispatch<React.SetStateAction<boolean>>
  vacancy: VacancyCreate
  currencies: string[]
  shedules: string[]
  validateWages: boolean
}
type InitialValues = VacancyCreate | FullVacancy
export const SecondStep = ({
  setVacancy,
  setPreload,
  vacancy,
  currencies,
  shedules,
  validateWages,
  setValidateWages,
}: SecondStepProps) => {
  const initialValues: InitialValues = vacancy
    ? vacancy
    : {
        description: '',
        jobRequirements: '',
        name: '',
        wages: '',
        currency: currencies[0],
        placeOfWork: '',
        workExperience: '',
        workSchedule:
          typeof shedules[0] === 'string' || typeof shedules[0] === 'number'
            ? shedules[0]
            : shedules[0]?.id,
      }
  const formik = useFormik({
    initialValues: { ...initialValues },
    onSubmit: () => {},
  })
  const closePreload = useRef(true)

  useEffect(() => {
    if (
      formik.values.name &&
      formik.values.description &&
      formik.values.jobRequirements
    ) {
      // console.log('first')

      if (
        formik.getFieldMeta('jobRequirements').touched &&
        formik.getFieldMeta('description').touched &&
        formik.getFieldMeta('name').touched
      ) {
        // console.log('second')
        setVacancy({ ...formik.values })
        if (closePreload.current) {
          // console.log('third')
          setPreload(true)
          closePreload.current = false
        } else if (!closePreload.current) {
          setPreload(false)
        }
      }
    }
  }, [
    formik.getFieldMeta('jobRequirements').touched,
    formik.getFieldMeta('description').touched,
    formik.getFieldMeta('name').touched,
    formik.values,
  ])

  useEffect(() => {
    console.log()

    if (formik.values.wages?.length > 0 && +formik.values.wages) {
      setValidateWages(true)
    }
    if (formik.values.wages?.length > 0 && !+formik.values.wages) {
      setValidateWages(false)
    }
  }, [formik.values.wages, setValidateWages])
  // console.log(
  //   formik.getFieldMeta('jobRequirements').touched,
  //   formik.getFieldMeta('description').touched,
  //   formik.getFieldMeta('name').touched
  // )

  // console.log('validateWages', validateWages)

  return (
    <form className={cls.SecondStep} onSubmit={formik.handleSubmit}>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          название<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <TextInput
          name="name"
          text={formik.values.name}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          placeholder="Введите текст"
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Описание вакансии<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <TextArea
          rows={3}
          text={formik.values.description}
          name="description"
          placeholder="Введите текст"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>
          Требования к вакансии<span style={{ color: '#E60019' }}>*</span>
        </PM2>
        <TextArea
          rows={3}
          placeholder="Введите текст"
          name="jobRequirements"
          text={formik.values.jobRequirements}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
      </div>
      <div className={`${cls.secondStepInputLabel} `}>
        <PM2>Заработная плата за ваш труд / валюта</PM2>
        <div className={`${cls.secondStepInputLabelSelect}`}>
          <div
            style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}
          >
            <TextInput
              placeholder="Введите текст"
              name="wages"
              text={formik.values.wages}
              onChange={formik.handleChange}
            />
            {!validateWages && (
              <PR4 style={{ color: '#E60019' }}>
                Это поле принимат числовое значение
              </PR4>
            )}
          </div>
          <InputSelectMini
            values={currencies}
            setValue={formik.handleChange}
            name="currency"
            value={formik.values.currency}
          />
        </div>
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>График работы</PM2>
        <InputSelect
          values={shedules}
          name="workSchedule"
          value={formik.values.workSchedule}
          setValue={formik.handleChange}
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>Опыт работы</PM2>
        <TextInput
          placeholder="Введите текст"
          name="workExperience"
          text={formik.values.workExperience}
          onChange={formik.handleChange}
        />
      </div>
      <div className={cls.secondStepInputLabel}>
        <PM2>Место работы</PM2>
        <TextInput
          placeholder="Введите текст"
          name="placeOfWork"
          text={formik.values.placeOfWork}
          onChange={formik.handleChange}
        />
      </div>
    </form>
  )
}
