import dayjs from 'dayjs'

export const interiewType = ['Онлайн', 'Очное']
export const duration = [
  '15 мин',
  '30 мин',
  '45 мин',
  '1 час',
  '1.5 часа',
  '2 часа',
]
export const hours = [
  '00:00',
  '01:00',
  '02:00',
  '03:00',
  '04:00',
  '05:00',
  '06:00',
  '07:00',
  '08:00',
  '09:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
  '14:00',
  '15:00',
  '16:00',
  '17:00',
  '18:00',
  '19:00',
  '20:00',
  '21:00',
  '22:00',
  '23:00',
  '24:00',
]
export const days = [
  'Понедельник, Июнь 21 2023',
  'Вторник, Июнь 21 2023',
  'Среда, Июнь 21 2023',
  'Четверг, Июнь 21 2023',
  'Пятница, Июнь 21 2023',
  'Суббота, Июнь 21 2023',
  'Воскресенье, Июнь 21 2023',
]
export const repeatSlot = ['Никогда', 'Будние', 'Каждый день']
const weekDays = [
  'Понедельник',
  'Вторник',
  'Среда',
  'Четверг',
  'Пятница',
  'Суббота',
  'Воскресение',
]
const months = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
]
export const generateWeek = (date?: string) => {
  const thisDate = date ? dayjs(date) : dayjs()
  const week = []
  const returnWeek = []
  console.log(date, thisDate)

  for (let i = 0; i < 7; i++) {
    const a = thisDate.add(i, 'day').format('d.DD.MM').split('.').map(Number)
    week.push({ dayWeek: a[0], day: a[1], month: a[2] })
  }

  for (let i = 0; i < 7; i++) {
    // const month = da.month()
    returnWeek.push(
      `${weekDays[week[i].dayWeek]}, ${months[week[i].month - 1]}, ${week[i].day}`
    )
  }
  return returnWeek
}
export const testingTypes = ['AUTO', ' MANUAL', 'NO_QUESTION']
