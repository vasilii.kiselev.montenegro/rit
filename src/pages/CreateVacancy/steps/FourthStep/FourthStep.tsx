import { InputSelect } from '@/UI/InputSelect/InputSelect'
import cls from './FourthStep.module.scss'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'
import { TextArea } from '@/UI/TextArea/TextArea'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { HourglassIcon } from '@/assets/icons/HourglassIcon'
import { ClockIcon } from '@/assets/icons/ClockIcon'
import { BlackCalendarIcon } from '@/assets/icons/BlackCalendarIcon'
import { CreateSlot } from '@/types/vacancy'
import { useFormik } from 'formik'
import { useEffect } from 'react'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { generateWeek, interiewType, repeatSlot } from './consts'
import dayjs from 'dayjs'
import { LocalizationProvider, MobileTimePicker } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import * as Yup from 'yup'

interface FourthStepProps {
  setSlot: React.Dispatch<React.SetStateAction<CreateSlot>>
  slot: CreateSlot
}

export const FourthStep = ({ setValidateSlot, setSlot }: FourthStepProps) => {
  const weekValues = generateWeek()
  const initValues: CreateSlot = {
    interviewType: interiewType[0],
    descriptionInvitation: '',
    interviewDuration: dayjs().startOf('day'),
    startDate: dayjs().startOf('day'),
    endDate: dayjs().startOf('day'),
    date: weekValues[0],

    repeatSlot: repeatSlot[0],
  }
  const valiadtionSchema = Yup.object<CreateSlot>().shape({
    interviewType: Yup.string().required(),
    // descriptionInvitation: Yup.string(),
    interviewDuration: Yup.mixed().required(),
    startDate: Yup.mixed().required(),
    endDate: Yup.mixed().required(),
    date: Yup.string().required(),
  })
  const formik = useFormik({
    initialValues: { ...initValues },
    validationSchema: valiadtionSchema,
    validateOnChange: true,
    validateOnBlur: true,
    onSubmit: () => {},
  })
  console.log(formik.values.repeatSlot)

  useEffect(() => {
    console.log('qwqwerqwrrqwqwrqwrqwrwr')
    if (
      !Object.keys(formik.errors).length &&
      formik.values.descriptionInvitation
    ) {
      console.log('asdasdasdasdasddasda')
      setValidateSlot(true)
      setSlot({ ...formik.values })
    } else {
      setValidateSlot(false)
    }
  }, [formik.values])

  console.log('errors', Object.keys(formik.errors).length)
  console.log(formik.values)

  return (
    <div className={cls.FourthStep}>
      <div className={cls.FormImputLabel}>
        <PM2>
          Тип собеседования <span className={cls.redStart}>*</span>
        </PM2>
        <InputSelect
          name="interviewType"
          value={formik.values.interviewType}
          setValue={formik.handleChange}
          values={interiewType}
        />
      </div>
      <div className={cls.FormImputLabel}>
        <PM2>Описание к приглашению на собеседование </PM2>
        <TextArea
          rows={2}
          placeholder="Введите текст"
          name="descriptionInvitation"
          text={formik.values.descriptionInvitation}
          onChange={formik.handleChange}
        />
      </div>
      <div className={cls.FormImputLabel}>
        <PM2>
          Длительность собеседования <span className={cls.redStart}>*</span>
        </PM2>
        <div className={cls.FormImputLabelSelectIcon}>
          <HourglassIcon style={{ width: '24px' }} />

          <LocalizationProvider adapterLocale={'ru'} dateAdapter={AdapterDayjs}>
            <MobileTimePicker
              sx={{
                background: '#fff',
                '& .MuiOutlinedInput-notchedOutline': {
                  border: '1px solid #d4d7df',
                },
              }}
              name="interviewDuration"
              value={formik.values.interviewDuration}
              onChange={(data) =>
                formik.setFieldValue('interviewDuration', data)
              }
            />
          </LocalizationProvider>
        </div>
      </div>
      <div className={cls.FormImputLabel}>
        <PM2>Выбор времени</PM2>
        <div className={cls.FormImputLabelDoubleSelectTime}>
          <div className={cls.FormImputLabelSelectIcon}>
            <ClockIcon />
            <div className={cls.FormImputLabelSelectTime}>
              <LocalizationProvider
                adapterLocale={'ru'}
                dateAdapter={AdapterDayjs}
              >
                <MobileTimePicker
                  sx={{
                    background: '#fff',
                    '& .MuiOutlinedInput-notchedOutline': {
                      border: '1px solid #d4d7df',
                    },
                  }}
                  name="startDate"
                  value={formik.values.startDate}
                  onChange={(data) => formik.setFieldValue('startDate', data)}
                />
              </LocalizationProvider>
              <Comment>-</Comment>

              <LocalizationProvider
                adapterLocale={'ru'}
                dateAdapter={AdapterDayjs}
              >
                <MobileTimePicker
                  sx={{
                    background: '#fff',
                    '& .MuiOutlinedInput-notchedOutline': {
                      border: '1px solid #d4d7df',
                    },
                  }}
                  name="endDate"
                  value={formik.values.endDate}
                  onChange={(data) => formik.setFieldValue('endDate', data)}
                />
              </LocalizationProvider>
            </div>
          </div>
          <div>
            {formik.values.repeatSlot !== repeatSlot[0] && <PR2>Начать с:</PR2>}
            <div className={cls.FormImputLabelSelectIcon}>
              <BlackCalendarIcon />
              <InputSelectMini
                values={weekValues}
                placeholder=" "
                name="date"
                value={formik.values.date}
                setValue={formik.handleChange}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={cls.FormImputLabel}>
        <PM2>Повтор слота</PM2>
        <RadioGroup
          aria-labelledby="demo-controlled-radio-buttons-group"
          name="repeatSlot"
          value={formik.values.repeatSlot}
          onChange={formik.handleChange}
          className={cls.FormImputLabelRadio}
        >
          {repeatSlot.map((el) => (
            <FormControlLabel
              style={{ marginLeft: '0' }}
              value={el}
              control={<Radio style={{ display: 'none' }} />}
              label={
                <div className={cls.FormImputLabelRadioItem}>
                  <div
                    className={`${cls.FormImputLabelRadioItemIcon} ${
                      el === formik.values.repeatSlot ? cls.active : ''
                    }`}
                  >
                    &#10003;
                  </div>
                  <Comment> {el}</Comment>
                </div>
              }
            />
          ))}
        </RadioGroup>
      </div>
    </div>
  )
}
