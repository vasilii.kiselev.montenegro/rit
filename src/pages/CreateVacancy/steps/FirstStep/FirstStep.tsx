import { Button } from '@/UI/Button/Button'
import cls from './FirstStep.module.scss'
import { PlusIcon } from '@/assets/icons/PlusIcon'
import { TextInput } from '@/UI/TextInput/TextInput'
import { SearchIcon } from '@/assets/icons/SearchIcon'
import { useAppSelector } from '@/hooks/useAppSelector'
import { CompanyItem } from './components/CompanyItem'
import { useState } from 'react'

interface FirstStepProps {
  setOpenDrawer: React.Dispatch<React.SetStateAction<boolean>>
  setCompany: React.Dispatch<React.SetStateAction<number>>
  company: number
}

export const FirstStep = ({
  setOpenDrawer,
  setCompany,
  company,
}: FirstStepProps) => {
  const companyArr = useAppSelector((store) => store.companies.workState)

  const [search, setSearch] = useState<string>('')

  return (
    <div className={cls.FirstStep}>
      <div className={cls.FirstStepHeader}>
        <TextInput
          img={<SearchIcon />}
          text={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <Button
          color="btnblack"
          onClick={() => {
            setOpenDrawer(true)
          }}
        >
          добавить компанию <PlusIcon />
        </Button>
      </div>
      <div className={cls.test}>
        <div className={cls.FirstStepBody}>
          {companyArr &&
            companyArr
              ?.filter((el) =>
                el.name.toLowerCase().includes(search.toLowerCase())
              )
              .map((el) => (
                <CompanyItem
                  key={el.id}
                  company={el}
                  active={company}
                  setCompany={setCompany}
                />
              ))}
        </div>
      </div>
    </div>
  )
}
