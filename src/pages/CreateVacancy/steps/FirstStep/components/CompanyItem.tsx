import { GlobalIcon } from '@/assets/icons/GlobalIcon'
import cls from './CompanyItem.module.scss'
import { Company } from '@/types/company'
import { PM2, PR4 } from '@/UI/TextComponents'
import { LocationIcon } from '@/assets/icons/LocationIcon'

interface CompanyItemProps {
  company: Company
  setCompany: React.Dispatch<React.SetStateAction<number>>
  active: number
}

export const CompanyItem = ({
  company,
  setCompany,
  active,
}: CompanyItemProps) => {
  return (
    <>
      <label
        className={cls.CompanyItem}
        htmlFor={`radio-${company.id}`}
        onClick={() => setCompany(company.id)}
      >
        <div className={cls.companyItemHeading}>
          <PR4 className={cls.companyItemLabel}>
            <img src={company.logo} />
            {company.name}
          </PR4>
          <input
            type="radio"
            value={company.id}
            name="companies"
            checked={company.id === active}
            id={`radio-${company.id}`}
          />
        </div>
        <div className={cls.companyItemInfo}>
          <a
            href={company.domainAddress}
            className={cls.companyItemDomainAddress}
          >
            {company.name}
          </a>
          <span>.</span>
          <div className={cls.companyItemDomain}>
            <GlobalIcon /> {company.domainAddress}
          </div>
          <span>.</span>
          <div className={cls.companyItemDomain}>
            <LocationIcon /> {company.location}
          </div>
        </div>
        <div className={cls.companyItemDescription}>
          <PM2>Описание</PM2>
          <div> {company.description}</div>
        </div>
      </label>
    </>
  )
}
