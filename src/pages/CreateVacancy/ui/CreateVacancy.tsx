import cls from './CreateVacansy.module.scss'
import { Button } from '@/UI/Button/Button'
import { useEffect, useRef, useState } from 'react'
import { Stepper } from '../components/stepper/Stepper'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { H1 } from '@/UI/TextComponents'
import { FirstStep } from '../steps/FirstStep/FirstStep'

import { CompanyForm } from '@/components/CompanyForm'
import { Drawer } from '@/components/Drawer'
import { SecondStep } from '../steps/SecondStep/SecondStep'
import { ThirdStep } from '../steps/ThirdStep/ThirdStep'
import { FourthStep } from '../steps/FourthStep/FourthStep'
import {
  getCurrencies,
  getShedules,
  // getCurrencies,
  sendEndOperation,
  sendFirstStep,
  sendFourthStep,
  sendMiniSecondStep,
  sendSecondStep,
} from '@/api/steps/steps'
import { CreateSlot, VacancyCreate } from '@/types/vacancy'

export interface Step {
  target: number
  steps: {
    first: boolean
    second: boolean
    third: boolean
    fourth: boolean
  }
}
export interface QuestionSettingsInterface {
  selection_choices: 'HIGH' | 'MEDIUM' | 'NONE'
  time_for_repeat: number
}
export const CreateVacancy = () => {
  const [step, setStep] = useState<Step>({
    target: 0,
    steps: {
      first: false,
      second: false,
      third: false,
      fourth: false,
    },
  })
  const [company, setCompany] = useState<number>()
  const [vacancy, setVacancy] = useState<VacancyCreate>()
  const [preload, setPreload] = useState<boolean>(false)
  const [question, setQuestion] = useState<string | true>()
  const [slot, setSlot] = useState<CreateSlot>()
  const [name, setName] = useState<string>()
  const [openDrawer, setOpenDrawer] = useState(false)
  const [vacancyId, setVacancyId] = useState<number>(null)
  const [testingMethod, setTestingMethod] = useState<number>(null)
  const [validateWages, setValidateWages] = useState<boolean>(true)
  const [validateSlot, setValidateSlot] = useState<boolean>(false)
  const [currencies, setCurrencies] = useState<string[]>([])
  const [shedules, setShedules] = useState<string[]>([])
  const [questionSettings, setQuestionSettings] =
    useState<QuestionSettingsInterface>()
  const getCurr = async () => {
    const data = await getCurrencies()
    setCurrencies(data)
  }
  const getShed = async () => {
    const data = await getShedules()
    setShedules(data)
  }
  useEffect(() => {
    getCurr()
  }, [])
  useEffect(() => {
    getShed()
  }, [])

  console.log(questionSettings)

  // useEffect(() => {
  //   if (!validateWages) {
  //     console.log('false')

  //     setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: false } }))
  //   }
  //   if (preload && validateWages) {
  //     console.log('true')
  //     setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: true } }))
  //   }
  //   console.log(step.steps, preload)
  // }, [validateWages])
  useEffect(() => {
    if (
      vacancy?.name &&
      vacancy?.jobRequirements &&
      vacancy?.description &&
      validateWages
    ) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: true } }))

      if (preload) {
        sendMiniSecondStep(vacancy, vacancyId, company)
      }
    }

    if (!validateWages) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: false } }))
    }
  }, [vacancy, preload, validateWages])
  const navigate = useNavigate()
  console.log('preload', shedules)

  useEffect(() => {
    if (company) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, first: true } }))
    }
  }, [company])
  useEffect(() => {
    if (vacancy) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, second: true } }))
      if (preload) {
        sendMiniSecondStep(vacancy, vacancyId, company)
      }
    }
  }, [vacancy, preload])
  useEffect(() => {
    setStep((prev) => ({
      ...prev,
      steps: { ...prev.steps, third: Boolean(question) },
    }))
  }, [question])
  useEffect(() => {
    if (slot && validateSlot) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, fourth: true } }))
    }
    if (!validateSlot) {
      setStep((prev) => ({ ...prev, steps: { ...prev.steps, fourth: false } }))
    }
  }, [slot, validateSlot])

  const chekCurrentStep = (num: number): boolean => {
    return Object.values(step.steps)[num]
  }

  const getVacancyId = async () => {
    const idVacancy = await sendFirstStep(company)
    setVacancyId(idVacancy)
  }

  const nextStep = async () => {
    switch (step.target) {
      case 0:
        getVacancyId()
        break
      case 1:
        sendSecondStep(vacancy, vacancyId, company, null, null)
        setName(vacancy.name)
        break
      case 2:
        sendSecondStep(vacancy, vacancyId, company, testingMethod, null)
        break
      case 3:
        sendSecondStep(vacancy, vacancyId, company, testingMethod, slot)
        sendFourthStep(slot, vacancyId)
        navigate(Url.MAIN)
        break
    }
    setStep((prev) => ({ ...prev, target: step.target + 1 }))
  }
  const prevStep = () => {
    setStep((prev) => ({ ...prev, target: step.target - 1 }))
  }

  const sendWithoutSlot = () => {
    // sendEndOperation()
    navigate(Url.MAIN)
  }

  return (
    <div className={cls.CreateVacansy}>
      <div className={cls.CreateVacancyContent}>
        <H1 style={{ marginBottom: '24px' }}>
          Добваление вакансии {name && `: ${name}`}
        </H1>
        <Stepper step={step} />

        <div className={cls.stepPage}>
          {step.target === 0 && (
            <FirstStep
              setCompany={setCompany}
              company={company}
              setOpenDrawer={setOpenDrawer}
            />
          )}
          {step.target === 1 && (
            <SecondStep
              currencies={currencies}
              shedules={shedules}
              setVacancy={setVacancy}
              setPreload={setPreload}
              vacancy={vacancy}
              validateWages={validateWages}
              setValidateWages={setValidateWages}
            />
          )}
          {step.target === 2 && (
            <ThirdStep
              vacancyId={vacancyId}
              setQuestionSettings={setQuestionSettings}
              setTestingMethod={setTestingMethod}
              setQuestion={setQuestion}
            />
          )}
          {step.target === 3 && (
            <FourthStep setValidateSlot={setValidateSlot} setSlot={setSlot} />
          )}
        </div>
      </div>

      <div className={cls.footer}>
        {step.target == 0 ? (
          <Button color="btnIconGreen" onClick={() => navigate(Url.VACANSY)}>
            Отменить
          </Button>
        ) : (
          <Button onClick={prevStep} color="btnIconGreen">
            Назад
          </Button>
        )}
        {step.target == 3 ? (
          <div>
            <Button color="btnIconGreen" onClick={sendWithoutSlot}>
              Пропустить
            </Button>
            <Button
              color="btnIconGreen"
              onClick={nextStep}
              disabled={!chekCurrentStep(step.target)}
            >
              Добавить вакансию
            </Button>
          </div>
        ) : (
          <Button
            onClick={nextStep}
            color="btnIconGreen"
            disabled={!chekCurrentStep(step.target)}
          >
            Далее
          </Button>
        )}
      </div>
      <Drawer
        isOpen={openDrawer}
        closeDrawer={setOpenDrawer}
        head={<H1>Добавление новой компании</H1>}
        body={
          <CompanyForm
            company={null}
            operation="new"
            setClose={setOpenDrawer}
          />
        }
      />
    </div>
  )
}
