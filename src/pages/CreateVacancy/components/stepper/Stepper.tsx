import { Step } from '../../ui/CreateVacancy'
import cls from './stepper.module.scss'

interface stepperProps {
  step: Step
}

export const Stepper = ({ step }: stepperProps) => {
  const checkStepTarget = (number: number): string => {
    return step.target === number ? cls.target : ''
  }
  const checkStepMoreEqualTarget = (number: number): boolean => {
    return step.target >= number
  }
  const checkStepMoreTarget = (number: number): string => {
    return step.target > number ? cls.complete : ''
  }
  return (
    <div className={cls.stepper}>
      <div
        className={`${cls.step} ${checkStepTarget(0)} ${checkStepMoreTarget(
          0
        )}`}
      >
        <div
          className={`${cls.circle} ${
            checkStepMoreEqualTarget(0) ? cls.target : ''
          } ${checkStepMoreTarget(0)}`}
        >
          1
        </div>
        Добавить компанию
      </div>
      <div
        className={`${cls.dash} ${
          checkStepMoreEqualTarget(1) ? cls.active : ''
        }`}
      ></div>
      <div
        className={`${cls.step} ${checkStepTarget(1)} ${checkStepMoreTarget(
          1
        )}`}
      >
        <div
          className={`${cls.circle} ${
            checkStepMoreEqualTarget(1) ? cls.target : ''
          } ${checkStepMoreTarget(1)}`}
        >
          2
        </div>
        Добавить вакансию{' '}
      </div>
      <div
        className={`${cls.dash} ${
          checkStepMoreEqualTarget(2) ? cls.active : ''
        }`}
      ></div>
      <div
        className={`${cls.step} ${checkStepTarget(2)} ${checkStepMoreTarget(
          2
        )}`}
      >
        <div
          className={`${cls.circle} ${
            checkStepMoreEqualTarget(2) ? cls.target : ''
          } ${checkStepMoreTarget(2)}`}
        >
          3
        </div>{' '}
        Тестирование
      </div>
      <div
        className={`${cls.dash} ${
          checkStepMoreEqualTarget(3) ? cls.active : ''
        }`}
      ></div>
      <div className={`${cls.step} ${checkStepTarget(3)}  `}>
        <div className={`${cls.circle} ${checkStepTarget(3)}`}>4</div>
        График собеседований{' '}
      </div>
    </div>
  )
}
