import { useState } from 'react'
import cls from './LogSignPage.module.scss'
import { H1, PR2 } from '@/UI/TextComponents'
// import { GoogleIcon } from '@/assets/icons/GoogleIcon'
// import { FacebookIcon } from '@/assets/icons/FacebookIcon'
// import { AppleIcon } from '@/assets/icons/AppleIcon'
import { LogInForm } from '../forms/LogInForm.tsx/LogInForm'
import { SignInForm } from '../forms/SignInForm/SignInForm'

enum Mode {
  SIGNIN = 'SignIn',
  LOGIN = 'LogIn',
}

const title = {
  [Mode.LOGIN]: 'Вход',
  [Mode.SIGNIN]: 'Создать аккаунт',
}

export const LogSignPage = () => {
  const [mode, setMode] = useState<Mode>(Mode.LOGIN)
  return (
    <div className={cls.LogSignPage}>
      <H1>{title[mode]}</H1>
      <div className={cls.setModeSwitcher}>
        <div className={`${cls.setModeSwitcherBG} ${Mode.LOGIN === mode ? cls.left : cls.right}`}></div>
        <div onClick={() => setMode(Mode.LOGIN)} className={`${cls.setModeSwitcherItem} ${mode === Mode.LOGIN ? cls.active : ''}`}>
          <PR2>Вход</PR2>
        </div>
        <div onClick={() => setMode(Mode.SIGNIN)} className={`${cls.setModeSwitcherItem} ${mode === Mode.SIGNIN ? cls.active : ''}`}>
          <PR2>Регистрация</PR2>
        </div>
      </div>
      <div className={cls.forms}>
        {mode === Mode.LOGIN && <LogInForm />}
        {mode === Mode.SIGNIN && <SignInForm />}
      </div>
      {/* <div className={cls.alternativeAuthorization}>
        <div className={cls.alternativeAuthorizationBorder}>
          <span>Или</span>
        </div>

        <div className={cls.alternativeAuthorizationItems}>
          <div className={cls.alternativeAuthorizationItem}>
            <GoogleIcon /> <PR2>Google</PR2>
          </div>
          <div className={cls.alternativeAuthorizationItem}>
            <FacebookIcon /> <PR2>Facebook</PR2>
          </div>
          <div className={cls.alternativeAuthorizationItem}>
            <AppleIcon /> <PR2>Apple</PR2>
          </div>
        </div>
      </div> */}
    </div>
  )
}
