import { PM1, PM2 } from '@/UI/TextComponents'
import cls from './ValidationErrorsModal.module.scss'

interface ValidationErrorsModalProps {
  errors?: string[]
  form: 'LogIn' | 'SignIn'
}

export const ValidationErrorsModal = ({ errors, form }: ValidationErrorsModalProps) => {
  if (form === 'LogIn') {
    return (
      <div className={cls.LoginErrors}>
        <PM1>Ошибка авторизации</PM1>
        <PM2>Не верные Логин или Пароль </PM2>
      </div>
    )
  }
  if (form === 'SignIn') {
    const errorsArr = Object.values(errors).flat()

    return (
      <div className={cls.LoginErrors}>
        <PM1>Ошибка регистрации</PM1>
        {errorsArr.map((el) => (
          <PM2>{el} </PM2>
        ))}
      </div>
    )
  }
}
