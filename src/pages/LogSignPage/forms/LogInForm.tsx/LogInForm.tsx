import { PR2 } from '@/UI/TextComponents'
import cls from './LogInForm.module.scss'
import { InputType, TextInput } from '@/UI/TextInput/TextInput'
import { LockIcon } from '@/assets/icons/LockIcon'
import { useFormik } from 'formik'
import { Button } from '@/UI/Button/Button'
import * as Yup from 'yup'
import { apiLogIn } from '@/api/authentification/authentification'
import { ButtonColors } from '@/UI/Button/consts'
import { ModalContext } from '@/components/Modal/modalCotext'
import { useContext } from 'react'
import { ValidationErrorsModal } from '../../components/ValidationErrorsModal'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setTockensAndStatus } from '@/store/slices/userSlice/userSlice'
import { TechSupportModal } from '@/components/TechSupportModal/TechSupportModal'

type InitValues = {
  loginEmail: string
  password: string
}
const initValues: InitValues = {
  loginEmail: '',
  password: '',
}

export const LogInForm = () => {
  const { handleModal } = useContext(ModalContext)
  const dispatch = useAppDispatch()
  const valiadtionSchema = Yup.object<InitValues>().shape({
    loginEmail: Yup.string().required(),
    password: Yup.string().required(),
  })
  const handleOpenErrorModal = () => {
    handleModal(<ValidationErrorsModal form="LogIn" />)
  }
  const handleOpenTechSupportModal = () => {
    handleModal(<TechSupportModal />)
  }

  const formik = useFormik({
    initialValues: initValues,
    validationSchema: valiadtionSchema,
    onSubmit(values) {
      console.log(values)
      apiLogIn(values)
        .then((tockens) => {
          console.log('dfghopjasdfgiophfgsdiophasdfghjlsdfghjlbngsdfjasdfgsdfghjkl;sdfghjkl;', tockens)

          if (tockens) {
            dispatch(setTockensAndStatus(tockens))
            // logInSession(tockens)
          }
        })
        .catch(handleOpenErrorModal)
    },
  })
  return (
    <form className={cls.LogInForm} onSubmit={formik.handleSubmit}>
      <div className={cls.InputLabel}>
        <PR2>
          Email Или Логин <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="loginEmail" onChange={formik.handleChange} text={formik.values.loginEmail} placeholder="Введите текст" />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Пароль <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput
          name="password"
          onChange={formik.handleChange}
          text={formik.values.password}
          type={InputType.PASSWORD}
          img={<LockIcon />}
          placeholder="Введите текст"
        />
      </div>
      {/* <Link to={Url.DATA_RECOVERY} className={cls.navLinkForgotPass}> */}
      <div className={cls.navLinkForgotPass} onClick={handleOpenTechSupportModal}>
        Забыли пароль?
      </div>

      <Button color={ButtonColors.GREEN} disabled={!formik.isValid} className={cls.submitButton} type="submit">
        Войти
      </Button>
    </form>
  )
}
