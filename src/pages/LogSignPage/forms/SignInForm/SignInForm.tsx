import { Button } from '@/UI/Button/Button'
import cls from './SignInForm.module.scss'
import { PR2 } from '@/UI/TextComponents'
import { InputType, TextInput } from '@/UI/TextInput/TextInput'
import { LockIcon } from '@/assets/icons/LockIcon'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { apiLogIn, apiSignIn } from '@/api/authentification/authentification'
import { ButtonColors } from '@/UI/Button/consts'
import { getUser } from '@/api/profile/profile'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { logIn, logOut, setTockensAndStatus } from '@/store/slices/userSlice/userSlice'
import { RegisterForm } from '@/types/user'
import { useContext } from 'react'
import { ModalContext } from '@/components/Modal/modalCotext'
import { ValidationErrorsModal } from '../../components/ValidationErrorsModal'

const initValues: RegisterForm = {
  login: '',
  email: '',
  company: '',
  password: '',
  job_title: '',
  consent: false,
  firstName: '',
  lastName: '',
}

export const SignInForm = () => {
  const { handleModal } = useContext(ModalContext)
  const dispatch = useAppDispatch()
  const valiadtionSchema = Yup.object<RegisterForm>().shape({
    login: Yup.string()
      .required()
      .test('is-only-spaces', 'Строка не должна состоять только из пробелов', (value) => value && value.trim().length > 0),
    email: Yup.string()
      .required()
      .test('is-only-spaces', 'Строка не должна состоять только из пробелов', (value) => value && value.trim().length > 0),
    job_title: Yup.string()
      .required()
      .test('is-only-spaces', 'Строка не должна состоять только из пробелов', (value) => value && value.trim().length > 0),
    password: Yup.string()
      .required()
      .test('is-only-spaces', 'Строка не должна состоять только из пробелов', (value) => value && value.trim().length > 0),
    firstName: Yup.string()
      .required()
      .test('is-only-spaces', 'Строка не должна состоять только из пробелов', (value) => value && value.trim().length > 0),
    consent: Yup.boolean().required(),
  })
  const handleOpenModalWithCompany = (textArr: string[]) => {
    handleModal(<ValidationErrorsModal form="SignIn" errors={textArr} />)
  }
  const formik = useFormik({
    initialValues: initValues,
    validationSchema: valiadtionSchema,
    onSubmit(values) {
      console.log(values)
      apiSignIn(values)
        .then((data) => {
          if (!data.error) {
            apiLogIn({
              loginEmail: values.login,
              password: values.password,
            }).then((tockens) => {
              if (tockens) {
                dispatch(setTockensAndStatus(tockens))
                getUser().then((data) => {
                  console.log('data', data)
                  if (data) {
                    dispatch(logIn(data))
                  } else {
                    dispatch(logOut())
                  }
                })
              }
            })
          }
        })
        .catch((err) => {
          handleOpenModalWithCompany(err)
        })
    },
  })

  return (
    <form className={cls.SignInForm} onSubmit={formik.handleSubmit}>
      <div className={cls.InputLabel}>
        <PR2>
          Логин <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="login" onChange={formik.handleChange} text={formik.values.login} placeholder="Введите имя" />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Имя <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="firstName" onChange={formik.handleChange} text={formik.values.firstName} placeholder="Введите имя" />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Фамилия <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="lastName" onChange={formik.handleChange} text={formik.values.lastName} placeholder="Введите имя" />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Специальность пользователя <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="job_title" onChange={formik.handleChange} text={formik.values.job_title} placeholder="Введите специальность" />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Email <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput name="email" onChange={formik.handleChange} text={formik.values.email} placeholder="Введите Email  " />
      </div>
      <div className={cls.InputLabel}>
        <PR2>
          Пароль <span className={cls.redStar}>*</span>
        </PR2>
        <TextInput
          name="password"
          onChange={formik.handleChange}
          text={formik.values.password}
          type={InputType.PASSWORD}
          img={<LockIcon />}
          placeholder="Введите текст"
        />
      </div>
      {/* <div className={cls.InputLabel}>
        <PR2>Специальность пользователя</PR2>
        <TextInput
          name="job_title"
          onChange={formik.handleChange}
          text={formik.values.job_title}
          placeholder="Введите специальность  "
        />
      </div> */}

      <label htmlFor="rememberMe" className={cls.consent}>
        <div className={`${cls.fakeRadio} ${formik.values.consent ? cls.active : ''}`}> &#10003;</div>
        {/*@ts-ignore */}
        <input type="checkbox" name="consent" id="rememberMe" onChange={formik.handleChange} value={formik.values.consent} />
        <PR2>Я согласен на обработку персональных данных</PR2>
      </label>
      <Button color={ButtonColors.GREEN} disabled={!formik.isValid || !formik.values.consent} className={cls.submitButton} type="submit">
        Зарегистрироваться
      </Button>
    </form>
  )
}
