import { PM2 } from '@/UI/TextComponents'
import cls from './FreeUpDay.module.scss'
import BasicDatePicker from '@/UI/DatePicker/DatePicker'
import { TextArea } from '@/UI/TextArea/TextArea'

export const FreeUpDay = ({ freeUpDayFormik }) => {
  return (
    <div className={cls.FreeUpDay}>
      <div className={cls.FreeUpDayFormCont}>
        <div className={cls.defaultField}>
          <PM2>Выберите дату</PM2>
          <BasicDatePicker
            name="date"
            value={freeUpDayFormik.values.date}
            onChange={(data) => freeUpDayFormik.setFieldValue('date', data)}
          />{' '}
        </div>
        <div className={cls.defaultField}>
          <PM2>Комментарий</PM2>
          <TextArea
            onChange={freeUpDayFormik.handleChange}
            name={'comment'}
            text={freeUpDayFormik.values.comment}
            rows={3}
            placeholder="Введите комментарий"
          />
        </div>
      </div>
    </div>
  )
}
