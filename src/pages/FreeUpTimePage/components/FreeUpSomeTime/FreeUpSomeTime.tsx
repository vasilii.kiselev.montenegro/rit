import { PM2 } from '@/UI/TextComponents'
import cls from './FreeUpSomeTime.module.scss'
import { LocalizationProvider } from '@mui/x-date-pickers'
import BasicDatePicker from '@/UI/DatePicker/DatePicker'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { TextArea } from '@/UI/TextArea/TextArea'
import { FormikProps } from 'formik'
import { FreeSLot } from '@/types/calendar'
import { TimeInput } from '@/UI/TimeInput/TimeInput'
interface FreeUpSomeTimeProps {
  freeUpSomeTimeFormik: FormikProps<FreeSLot>
  timeDifferenceBool: boolean
}

export const FreeUpSomeTime = ({ freeUpSomeTimeFormik, timeDifferenceBool }: FreeUpSomeTimeProps) => {
  return (
    <div className={cls.FreeUpSomeTime}>
      <div className={cls.FreeUpSomeTimeFormCont}>
        <div className={cls.startEndSlotTime}>
          <div className={cls.startEndSlotTimeItems}>
            <div className={cls.startEndSlotTimeItem}>
              <PM2>Освободить с</PM2>
              <LocalizationProvider adapterLocale="ru" dateAdapter={AdapterDayjs}>
                <TimeInput
                  value={freeUpSomeTimeFormik.values.startTime}
                  name="startTime"
                  onChange={(newValue) => freeUpSomeTimeFormik.setFieldValue('startTime', newValue)}
                />
              </LocalizationProvider>
            </div>
            <div className={cls.startEndSlotTimeItem}>
              <PM2>Освободить до</PM2>
              <LocalizationProvider adapterLocale="ru" dateAdapter={AdapterDayjs}>
                <TimeInput
                  value={freeUpSomeTimeFormik.values.endTime}
                  name="endTime"
                  onChange={(newValue) => freeUpSomeTimeFormik.setFieldValue('endTime', newValue)}
                />
              </LocalizationProvider>
            </div>
          </div>
          {timeDifferenceBool && <div className={cls.startEndSlotError}>Минимальная длительность слота - 15мин </div>}
        </div>
        <div className={cls.defaultField}>
          <PM2>Выберите дату</PM2>
          <BasicDatePicker
            name="date"
            value={freeUpSomeTimeFormik.values.date}
            onChange={(data) => freeUpSomeTimeFormik.setFieldValue('date', data)}
          />{' '}
        </div>

        <div className={cls.defaultField}>
          <PM2>Комментарий</PM2>
          <TextArea
            onChange={freeUpSomeTimeFormik.handleChange}
            name={'comment'}
            text={freeUpSomeTimeFormik.values.comment}
            rows={3}
            placeholder="Введите комментарий"
          />
        </div>
      </div>
    </div>
  )
}
