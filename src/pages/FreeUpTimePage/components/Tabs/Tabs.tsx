import { PR3 } from '@/UI/TextComponents'
import cls from './Tabs.module.scss'

interface TabsProps {
  tab: number
  setTab: React.Dispatch<React.SetStateAction<number>>
}

export const Tabs = ({ tab, setTab }: TabsProps) => {
  return (
    <div className={cls.Tabs}>
      <label className={cls.StepHeadingItem} onClick={() => setTab(0)}>
        <input name="tabRadio" type="radio" id="1-inputRadio" />
        <div
          className={`${cls.tabsRadioIcon} ${tab === 0 && cls.active}`}
        ></div>
        <PR3>Весь день</PR3>
      </label>
      <label className={cls.StepHeadingItem} onClick={() => setTab(1)}>
        <input name="tabRadio" type="radio" id="2-inputRadio" />
        <div
          className={`${cls.tabsRadioIcon} ${tab === 1 && cls.active}`}
        ></div>
        <PR3>Настроить слот</PR3>
      </label>
    </div>
  )
}
