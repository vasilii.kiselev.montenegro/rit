import { useEffect, useState } from 'react'
import { Tabs } from '../components/Tabs/Tabs'
import cls from './FreeUpTimePage.module.scss'
import { H1, PM1, PM2 } from '@/UI/TextComponents'
import { Button } from '@/UI/Button/Button'
import { FreeUpDay } from '../components/FreeUpDay/FreeUpDay'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'
import { FreeUpSomeTime } from '../components/FreeUpSomeTime/FreeUpSomeTime'
import { useFormik } from 'formik'

import dayjs from 'dayjs'
import { ButtonColors } from '@/UI/Button/consts'
import { checkFreeUpTime, createFreeTimeSlot, rescheduleEvents } from '@/api/calendar/calendar'
import { LongArrowIcon } from '@/assets/icons/LongArrowIcon'
import { EditIcon } from '@/assets/icons/EditIcon'
import { ErrorEventsApi, FreeSLot } from '@/types/calendar'

export const FreeUpTimePage = () => {
  const navigate = useNavigate()
  const [tab, setTab] = useState(0)
  const [timeDifferenceBool, setTimeDifferenceBool] = useState<boolean>(false)
  const [isDisabled, setIsDisabled] = useState<boolean>()
  const [errorEvents, setErrorEvents] = useState<ErrorEventsApi[]>()
  const [stageOfSending, setStageOfSending] = useState<'first' | 'second'>('first')

  const trySubmit = (values: FreeSLot) => {
    checkFreeUpTime(values).then((data) => {
      if (!data.length) {
        createFreeTimeSlot(values).then(() => {
          navigate(Url.CALENDAR)
        })
      } else {
        setStageOfSending('second')
        setErrorEvents(data)
      }
    })
  }

  const fullSubmit = (values: FreeSLot) => {
    rescheduleEvents(errorEvents).then(() => {
      checkFreeUpTime(values).then((data) => {
        if (!data.length) {
          createFreeTimeSlot(values).then(() => {
            navigate(Url.CALENDAR)
          })
        }
      })
    })
  }
  const freeUpDayFormik = useFormik<FreeSLot>({
    initialValues: {
      date: '',
      startTime: dayjs('2000-01-01T00:00'),
      endTime: dayjs('2000-01-01T23:59'),
      comment: '',
    },
    onSubmit: (values) => {
      stageOfSending === 'first' ? trySubmit(values) : fullSubmit(values)
      // checkFreeUpTime(values).then((data) => {
      //   setErrorEvents(data)
      // })
    },
  })
  const freeUpSomeTimeFormik = useFormik<FreeSLot>({
    initialValues: {
      startTime: dayjs('2000-01-01T08:00'),
      endTime: dayjs('2000-01-01T20:00'),
      date: '',
      comment: '',
    },
    onSubmit: (values) => {
      stageOfSending === 'first' ? trySubmit(values) : fullSubmit(values)
      // checkFreeUpTime(values).then((data) => {
      //   setErrorEvents(data)
      // })
    },
  })

  useEffect(() => {
    const thisDate = dayjs().format('YYYY-MM-DD')
    if (!freeUpDayFormik.values.date) freeUpDayFormik.setFieldValue('date', thisDate)
    if (!freeUpSomeTimeFormik.values.date) freeUpSomeTimeFormik.setFieldValue('date', thisDate)
  }, [])

  useEffect(() => {
    if (freeUpSomeTimeFormik.values.startTime && freeUpSomeTimeFormik.values.endTime) {
      const start = freeUpSomeTimeFormik.values.startTime?.format('HH:mm')?.split(':')?.map(Number)
      const end = freeUpSomeTimeFormik.values.endTime?.format('HH:mm')?.split(':')?.map(Number)
      const difference = (end[0] - start[0]) * 60 + (end[1] - start[1])
      console.log('adsasfasf', start, end, difference)

      if (difference < 20) {
        setTimeDifferenceBool(true)
      } else {
        setTimeDifferenceBool(false)
      }
    }
  }, [freeUpSomeTimeFormik.values?.startTime, freeUpSomeTimeFormik.values?.endTime, freeUpSomeTimeFormik])

  useEffect(() => {
    if (tab === 0) {
      setIsDisabled(!freeUpDayFormik.values.date)
    } else {
      setIsDisabled(
        timeDifferenceBool ||
          !freeUpSomeTimeFormik.values.date ||
          !freeUpSomeTimeFormik.values.startTime ||
          !freeUpSomeTimeFormik.values.endTime
      )
    }
  }, [
    tab,
    timeDifferenceBool,
    freeUpSomeTimeFormik.values.date,
    freeUpDayFormik.values.date,
    freeUpSomeTimeFormik.values.startTime,
    freeUpSomeTimeFormik.values.endTime,
  ])

  return (
    <div className={cls.FreeUpTimePage}>
      <div className={cls.formContainer}>
        <H1>Освободить время</H1>
        <Tabs tab={tab} setTab={setTab} />
        {tab === 0 ? (
          <FreeUpDay freeUpDayFormik={freeUpDayFormik} />
        ) : (
          <FreeUpSomeTime freeUpSomeTimeFormik={freeUpSomeTimeFormik} timeDifferenceBool={timeDifferenceBool} />
        )}
      </div>
      {errorEvents?.length && (
        <div className={cls.eventsErrors}>
          <PM1>Конфликты ( {errorEvents.length} )</PM1>
          {errorEvents.map((el) => {
            return (
              <div key={el.start_time} className={cls.eventsErrorsItem}>
                <PM2>
                  Name | Login Встреча {el.start_time} - {el.end_time}
                </PM2>
                <LongArrowIcon />
                <div className={cls.rightSide}>
                  <PM2>
                    Name | Login Встреча {el.new_start_time} - {el.new_end_time}
                  </PM2>
                  <div className={cls.editButton}>
                    <EditIcon />
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      )}
      <div className={cls.footer}>
        <Button color={ButtonColors.ICON_WHITE} onClick={() => navigate(Url.CALENDAR)}>
          Отменить
        </Button>
        {stageOfSending === 'first' ? (
          <Button
            color={ButtonColors.GREEN}
            disabled={isDisabled}
            onClick={() => (tab === 0 ? freeUpDayFormik.handleSubmit() : freeUpSomeTimeFormik.handleSubmit())}
          >
            Освободить
          </Button>
        ) : (
          <Button
            onClick={() => (tab === 0 ? freeUpDayFormik.handleSubmit() : freeUpSomeTimeFormik.handleSubmit())}
            color={ButtonColors.GREEN}
          >
            Принять
          </Button>
        )}
      </div>
    </div>
  )
}
