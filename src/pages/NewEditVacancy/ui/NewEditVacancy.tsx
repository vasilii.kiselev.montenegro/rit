import { useEffect, useState } from 'react'
import cls from './NewEditVacancy.module.scss'
import { useParams } from 'react-router-dom'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setFullCompanyThunk } from '@/store/slices/createVacancySlice/asyncThunks/setFullCompanyThunk'
import { EditFirstStep } from '../components/steps/EditFirstStep/EditFirstStep'
import { EditSecondStep } from '../components/steps/EditSecondStep/EditSecondStep'
import { Stepper } from '../components/stepper/Stepper'

export const NewEditVacancy = () => {
  const dispatch = useAppDispatch()
  const { vacancyId } = useParams()
  // dispatch(setFullCompanyThunk(Number(vacancyId)))
  useEffect(() => {
    if (vacancyId) {
      dispatch(setFullCompanyThunk(Number(vacancyId)))
    }
  }, [dispatch, vacancyId])
  const [step, setStep] = useState<number>(0)

  const incrementStep = () => setStep(1)
  const decrementStep = () => setStep(0)

  return (
    <div className={cls.NewEditVacancy}>
      <Stepper step={step} />

      {step === 0 && <EditFirstStep incrementStep={incrementStep} />}
      {step === 1 && <EditSecondStep decrementStep={decrementStep} />}
    </div>
  )
}
