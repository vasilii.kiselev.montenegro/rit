import { useAppSelector } from '@/hooks/useAppSelector'
import cls from './EditFirstStep.module.scss'
import * as Yup from 'yup'
import { FullVacancy } from '@/types/vacancy'
import { useFormik } from 'formik'
import { TextInput } from '@/UI/TextInput/TextInput'
import { PM2, PR4 } from '@/UI/TextComponents'
import { InputSelect } from '@/UI/InputSelect/InputSelect'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { TextArea } from '@/UI/TextArea/TextArea'
import { SelectCompany } from '@/UI/SelectCompany/SelectCompany'
import { useEffect, useState } from 'react'
import { Footer } from '../../Footer/Footer'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import {
  cleanState,
  setValuesForSecondStep,
} from '@/store/slices/createVacancySlice/createVacancySlice'
import {
  getCurrencies,
  getShedules,
  sendSecondStep,
  ShedulesType,
} from '@/api/createVacancy/secondStep/secondStep'
import { Url } from '@/routes/url'
import { useNavigate } from 'react-router-dom'

interface EditFirstStepProps {
  incrementStep: () => void
}

export const EditFirstStep = ({ incrementStep }: EditFirstStepProps) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const vacancy = useAppSelector((store) => store.createVacancy)
  const companies = useAppSelector((state) => state.companies.workState)
  const [currencies, setCurrencies] = useState<string[]>([])
  const [shedules, setShedules] = useState<ShedulesType[]>([])

  useEffect(() => {
    getCurrencies().then((data) => {
      setCurrencies(data)
    })
    getShedules().then((data) => {
      setShedules(data)
    })
  }, [])

  const valiadtionSchema = Yup.object<FullVacancy>().shape({
    company: Yup.string().required(),
    description: Yup.string().required(),
    name: Yup.string().required(),
    jobRequirements: Yup.string().required(),
    wages: Yup.number(),
  })

  const formik = useFormik<FullVacancy>({
    initialValues: vacancy,
    validationSchema: valiadtionSchema,
    onSubmit() {},
  })
  const clickNextBtn = () => {
    dispatch(setValuesForSecondStep(formik.values))
    sendSecondStep(formik.values)
    incrementStep()
  }
  const clickPrev = () => {
    dispatch(cleanState())
    navigate(Url.MAIN)
  }
  useEffect(() => {
    formik.setValues(vacancy)
  }, [vacancy.id])

  return (
    <>
      {!vacancy.id ? (
        <div className={cls.LoaderFirstStep}>
          <div className={cls.circle}> </div>{' '}
        </div>
      ) : (
        <>
          <div className={cls.EditFirstStep}>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Компания<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <div className={cls.inputAndError}>
                <SelectCompany
                  values={companies}
                  name="companyId"
                  value={formik.values.companyId}
                  setValue={formik.handleChange}
                  // setValue={(data) => formik.setFieldValue('company', data)}
                />
                {formik.errors.company && (
                  <span style={{ color: '#E60019' }}>
                    Поле "Компания" обязательное
                  </span>
                )}
              </div>
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Название<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <div className={cls.secondStepInputLabelq}>
                <TextInput
                  name="name"
                  text={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  placeholder="Введите текст"
                />
                {formik.errors.name && formik.touched.name ? (
                  <span style={{ color: '#E60019' }}>
                    Поле "Название" обязательное
                  </span>
                ) : null}
              </div>
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Описание вакансии<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <div className={cls.secondStepInputLabelq}>
                <TextArea
                  rows={3}
                  text={formik.values.description}
                  name="description"
                  placeholder="Введите текст"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.description && formik.touched.description ? (
                  <span style={{ color: '#E60019' }}>
                    Поле "Описание вакансии" обязательное
                  </span>
                ) : null}
              </div>
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>
                Требования к вакансии<span style={{ color: '#E60019' }}>*</span>
              </PM2>
              <div className={cls.secondStepInputLabelq}>
                <TextArea
                  rows={3}
                  placeholder="Введите текст"
                  name="jobRequirements"
                  text={formik.values.jobRequirements}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.jobRequirements &&
                formik.touched.jobRequirements ? (
                  <span style={{ color: '#E60019' }}>
                    Поле "Требования к вакансии" обязательное
                  </span>
                ) : null}
              </div>
            </div>
            <div className={`${cls.secondStepInputLabel} `}>
              <PM2>Заработная плата за ваш труд / валюта</PM2>
              <div className={`${cls.secondStepInputLabelSelect}`}>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '10px',
                  }}
                >
                  <TextInput
                    placeholder="Введите текст"
                    name="wages"
                    text={formik.values.wages}
                    onChange={formik.handleChange}
                  />
                  {formik.errors.wages && (
                    <PR4 style={{ color: '#E60019' }}>
                      Это поле принимат числовое значение
                    </PR4>
                  )}
                </div>
                <InputSelectMini
                  values={currencies}
                  setValue={formik.handleChange}
                  name="currency"
                  value={formik.values.currency}
                />
              </div>
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>График работы</PM2>
              <InputSelect
                values={shedules}
                name="workSchedule"
                value={formik.values.workSchedule}
                setValue={formik.handleChange}
              />
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>Опыт работы</PM2>
              <TextInput
                placeholder="Введите текст"
                name="workExperience"
                text={formik.values.workExperience}
                onChange={formik.handleChange}
              />
            </div>
            <div className={cls.secondStepInputLabel}>
              <PM2>Место работы</PM2>
              <TextInput
                placeholder="Введите текст"
                name="placeOfWork"
                text={formik.values.placeOfWork}
                onChange={formik.handleChange}
              />
            </div>
          </div>
          <Footer
            leftSide={
              <Button onClick={clickPrev} color={ButtonColors.ICON_GREEN}>
                Отменить
              </Button>
            }
            rightSide={
              <Button
                onClick={clickNextBtn}
                disabled={!!Object.values(formik.errors).length}
                color={ButtonColors.ICON_GREEN}
              >
                Далее
              </Button>
            }
          />
        </>
      )}
    </>
  )
}
