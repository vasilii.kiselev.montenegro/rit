import cls from './EditSecondStep.module.scss'
import { useEffect, useState } from 'react'
import { ThirdStepTabs } from './components/TabsMenu/ThirdStepTabs'
import { FirstTab } from './components/Tabs/FirstTab/FirstTab'
import { useAppSelector } from '@/hooks/useAppSelector'
import { Footer } from '../../Footer/Footer'

import { ButtonColors } from '@/UI/Button/consts'
import { Button } from '@/UI/Button/Button'
import {
  getQuestion,
  sendVacancyThirdStep,
} from '@/api/createVacancy/thirdStep/thirdStep'
import { Question } from '@/types/question'
import { SecondTab } from './components/Tabs/SecondTab/SecondTab'
import { QuestionTest } from './components/DropDownSectionForms/QuestionTest/QuestionTest'
import { QuestionSettings } from './components/DropDownSectionForms/QuestionSettings/QuestionSettings'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import {
  cleanState,
  setTestingMethod,
} from '@/store/slices/createVacancySlice/createVacancySlice'
import { useNavigate } from 'react-router-dom'
import { Url } from '@/routes/url'

interface EditSecondStepProps {
  decrementStep: () => void
}
const testingMethods = ['AUTO', 'MANUAL', 'NO_QUESTION']
export const EditSecondStep = ({ decrementStep }: EditSecondStepProps) => {
  const navigate = useNavigate()
  const [tab, setTab] = useState(0)
  const [question, setQuestion] = useState<Question>()
  const [questionForSecond, setQuestionForSecond] = useState<string>()

  const dispatch = useAppDispatch()

  const vacancy = useAppSelector((store) => store.createVacancy)

  useEffect(() => {
    getQuestion(vacancy.id).then((data) => setQuestion(data))
  }, [])
  console.log(question?.text, questionForSecond)

  useEffect(() => {
    if (question?.text !== questionForSecond) {
      setQuestionForSecond('')
    }
  }, [question?.text])

  const clickNextbtn = () => {
    dispatch(setTestingMethod(testingMethods[tab]))
    sendVacancyThirdStep(vacancy)
    dispatch(cleanState())
    navigate(Url.MAIN)
  }

  return (
    <>
      <div className={cls.ThirdStep}>
        <ThirdStepTabs tab={tab} setTab={setTab} />
        {tab === 0 && (
          <FirstTab question={question} setQuestion={setQuestion} />
        )}
        {tab === 1 && (
          <SecondTab
            globalQuestion={question}
            setGlobalQuestion={setQuestion}
            localQuestion={questionForSecond}
            setLocalQuestion={setQuestionForSecond}
          />
        )}
        {tab !== 2 && (
          <div className={cls.questionDropdownForms}>
            <QuestionTest questionId={question?.id} />
            <QuestionSettings />
          </div>
        )}
      </div>
      <Footer
        leftSide={
          <Button onClick={decrementStep} color={ButtonColors.ICON_GREEN}>
            Назад
          </Button>
        }
        rightSide={
          <Button
            disabled={
              tab !== 2
                ? !questionForSecond && tab === 1
                  ? true
                  : !question?.text
                : false
            }
            color={ButtonColors.ICON_GREEN}
            onClick={clickNextbtn}
          >
            Далее
          </Button>
        }
      />
    </>
  )
}
