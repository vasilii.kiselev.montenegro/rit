import { Comment, PR1 } from '@/UI/TextComponents'
import cls from './DropDownSection.module.scss'
import { ReactNode, useState } from 'react'
import { FullArrowIcon } from '@/assets/icons/FullArrowIcon'

interface DropDownSectionProps {
  label: string
  form: ReactNode
}

export const DropDownSection = ({ label, form }: DropDownSectionProps) => {
  const [open, setOpen] = useState<boolean>(false)

  return (
    <div className={cls.DropDownSection}>
      <div
        className={cls.dropDownSectionHeading}
        onClick={() => setOpen((prev) => !prev)}
      >
        <Comment>
          <PR1>{label}</PR1>
        </Comment>
        <FullArrowIcon style={{ rotate: open && '180deg' }} />
      </div>
      {open && <div className={cls.dropDownSectionBody}>{form}</div>}
    </div>
  )
}
