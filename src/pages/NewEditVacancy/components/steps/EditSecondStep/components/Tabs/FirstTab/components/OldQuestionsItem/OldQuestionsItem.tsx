import { PR2 } from '@/UI/TextComponents'
import cls from './OldQuestionsItem.module.scss'
import { Question } from '@/types/question'
import { CrossIcon } from '@/assets/icons/CrossIcon'

interface OldQuestionsItemProps {
  oldQuestionsItem: Question
  el: Question
  setOldQuestionsItem: React.Dispatch<React.SetStateAction<Question>>
  deleteOldQuestionItem: React.Dispatch<React.SetStateAction<number>>
}

export const OldQuestionsItem = ({
  oldQuestionsItem,
  el,
  setOldQuestionsItem,
  deleteOldQuestionItem,
}: OldQuestionsItemProps) => {
  return (
    <div className={cls.oldQuestionsItemAndButton}>
      <label
        htmlFor={`oldQuestionsItem${el.id}`}
        className={`${cls.oldQuestionsItem} `}
        onClick={() => setOldQuestionsItem(el)}
        key={el.id}
      >
        <div
          className={`${cls.oldQuestionsItemRadio} ${
            oldQuestionsItem?.id === el.id && cls.active
          }`}
        ></div>
        <PR2
          className={`${cls.oldQuestionsItemTextTruncate} ${
            oldQuestionsItem?.id === el.id ? cls.open : ''
          }`}
        >
          {el?.text}
        </PR2>
        <input type="radio" id={`oldQuestionsItem${el.id}`} />
      </label>
      <div
        className={cls.crossContainer}
        onClick={() => deleteOldQuestionItem(el.id)}
      >
        <CrossIcon />
      </div>
    </div>
  )
}
