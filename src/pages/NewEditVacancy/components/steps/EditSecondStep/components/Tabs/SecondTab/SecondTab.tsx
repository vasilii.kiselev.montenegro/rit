import { PM2, PR2 } from '@/UI/TextComponents'
import cls from './SecondTab.module.scss'
import { useState } from 'react'
import { Question } from '@/types/question'
import { TextArea } from '@/UI/TextArea/TextArea'
import { Button } from '@/UI/Button/Button'
import { ButtonColors } from '@/UI/Button/consts'
import { MessageEditIcon } from '@/assets/icons/MessageEditIcon'
import { createQuestionWithoutPrevious } from '@/api/createVacancy/thirdStep/thirdStep'

interface SecondTabProps {
  globalQuestion: Question
  setGlobalQuestion: React.Dispatch<React.SetStateAction<Question>>
  localQuestion: string
  setLocalQuestion: React.Dispatch<React.SetStateAction<string>>
}

export const SecondTab = ({
  globalQuestion,
  setGlobalQuestion,
  localQuestion,
  setLocalQuestion,
}: SecondTabProps) => {
  const [changeQuestion, setChangeQuestion] = useState<string>(localQuestion)
  const [save, setSave] = useState<boolean>(!!changeQuestion)

  const saveQuestion = () => {
    setLocalQuestion(changeQuestion)
    createQuestionWithoutPrevious(globalQuestion, changeQuestion).then(
      (data) => {
        setGlobalQuestion(data)
      }
    )
    setSave(true)
  }
  return (
    <div className={cls.SecondTabContent}>
      <PM2>Ваш впорос:</PM2>

      {!save && (
        <div className={cls.writingQuestionCont}>
          <TextArea
            text={changeQuestion}
            onChange={(e) => setChangeQuestion(e.target.value)}
            placeholder="Введите свой вопрос для тестирования"
          />
          <div
            className={`${cls.writingQuestionBtnsCont} ${
              changeQuestion ? cls.right : ''
            }`}
          >
            {changeQuestion && (
              <Button
                color={ButtonColors.RED}
                onClick={() => {
                  setChangeQuestion(null)
                  setSave(true)
                }}
              >
                Отменить
              </Button>
            )}
            <Button
              disabled={changeQuestion === ''}
              color={ButtonColors.GREEN}
              onClick={saveQuestion}
            >
              Сохранить
            </Button>
          </div>
        </div>
      )}

      {save && (
        <>
          <div className={cls.writedQuestion}>
            <div
              className={cls.writedQuestionEditIcon}
              onClick={() => setSave(false)}
            >
              <MessageEditIcon />
            </div>
            <PR2>{localQuestion}</PR2>
          </div>
        </>
      )}
    </div>
  )
}
