import cls from './stepper.module.scss'

interface stepperProps {
  step: number
}

export const Stepper = ({ step }: stepperProps) => {
  const checkStepTarget = (number: number): string => {
    return step === number ? cls.target : ''
  }
  const checkStepMoreEqualTarget = (number: number): boolean => {
    return step >= number
  }
  const checkStepMoreTarget = (number: number): string => {
    return step > number ? cls.complete : ''
  }
  return (
    <div className={cls.stepper}>
      <div
        className={`${cls.step} ${checkStepTarget(0)} ${checkStepMoreTarget(
          0
        )}`}
      >
        <div
          className={`${cls.circle} ${
            checkStepMoreEqualTarget(0) ? cls.target : ''
          } ${checkStepMoreTarget(0)}`}
        >
          1
        </div>
        Добавить компанию
      </div>
      <div
        className={`${cls.dash} ${
          checkStepMoreEqualTarget(1) ? cls.active : ''
        }`}
      ></div>
      <div
        className={`${cls.step} ${checkStepTarget(1)} ${checkStepMoreTarget(
          1
        )}`}
      >
        <div
          className={`${cls.circle} ${
            checkStepMoreEqualTarget(1) ? cls.target : ''
          } ${checkStepMoreTarget(1)}`}
        >
          2
        </div>
        Добавить вакансию{' '}
      </div>
    </div>
  )
}
