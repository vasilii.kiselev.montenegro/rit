import dayjs, { Dayjs } from 'dayjs'
import { Hour } from './consts'
import store from '@/store/store'
import { CalendarViewType, TimeSlotWithEvents } from '@/types/calendar'
import { getStartEndDates } from './components/Toolbar/utils'
import { getSlotsEventsForToolbarThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsForToolbarThunk'
import { getSlotsEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsThunk'
import { getWeekEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getWeekEvents'

export const getCurrentWeekDays = (currentDate: string): Dayjs[] => {
  const today = dayjs(currentDate)
  const startOfWeek = today.startOf('week').add(1, 'day')
  //   const endOfWeek = today.endOf('isoWeek')

  const days = []
  for (let i = 0; i <= 6; i++) {
    days.push(startOfWeek.add(i, 'day'))
  }

  return days
}
export const getArrHours = (): number[] => {
  const returnArr = []
  for (let i = Hour.Start + 1; i <= Hour.End; i++) {
    returnArr.push(i)
  }

  return returnArr
}

interface ToolbarSlots {
  currentSlots: TimeSlotWithEvents[]
  otherSlots: TimeSlotWithEvents[]
}
export const updateTimeslots = () => {
  const calendar = store.getState().calendar

  //@ts-ignore
  const slots: ToolbarSlots = {}
  const queryParamsForToolbar = getStartEndDates(calendar.viewType.value, calendar.currentDate)
  const queryParamsForWeekEvents = getStartEndDates(CalendarViewType.DAY, calendar.currentDate)
  store
    .dispatch(getSlotsEventsForToolbarThunk(queryParamsForToolbar))
    .unwrap()
    .then((data) => (slots.currentSlots = data))
  store
    .dispatch(getSlotsEventsThunk())
    .unwrap()
    .then((data) => (slots.otherSlots = data))
  store.dispatch(getWeekEventsThunk(queryParamsForWeekEvents))
  return slots
}
