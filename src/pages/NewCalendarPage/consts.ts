export enum Hour {
  Start = 0,
  End = 24,
}
export const OPACITY_VALUE = '99'

export const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

export const weekDays = ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']

export const shortWeekDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']

export const CALENDAR_VIEW = [
  { value: 'Day', label: 'День' },
  { value: 'Week', label: 'Неделя' },
]
