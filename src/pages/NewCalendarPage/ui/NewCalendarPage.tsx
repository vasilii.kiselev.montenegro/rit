import { Comment, H1, PR2 } from '@/UI/TextComponents'
import cls from './NewCalendarPage.module.scss'
import { Switcher } from '@/UI/Switcher/Switcher'
import { CalenadarBody } from '../components/CalenadarBody/CalenadarBody'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setViewSlots } from '@/store/slices/calendarSlice/calendarSlice'
import { useContext } from 'react'
import { DrawerContext } from '@/components/Drawer/DrawerContext'
import { NewCreateChangeSlotForm } from '../components/CreateChangeSlotForm/NewCreateChangeSlotForm'
import { SlotFormOperationTypeEnum } from '@/types/calendar'
import { NavigateDropButton } from '../components/NavigateDropButton/NavigateDropButton'

export const NewCalendarPage = () => {
  const dispatch = useAppDispatch()
  const slotsShown = useAppSelector((store) => store.calendar.showSlots)
  const { handleDrawer } = useContext(DrawerContext)

  const handleOpenDrawerWithCompany = () => {
    handleDrawer(<NewCreateChangeSlotForm operationType={SlotFormOperationTypeEnum.CREATE} />, <H1>Добавление слота на собеседование</H1>)
  }
  return (
    <div className={cls.NewCalendarPage}>
      <div className={cls.calendarHeader}>
        <div className={cls.leftHeader}>
          <H1>Календарь</H1>
          <Comment>{} событий</Comment>
        </div>
        <div className={cls.rightHeader}>
          <div className={cls.viewSlotsAndAddSlot}>
            <Switcher onchange={() => dispatch(setViewSlots())} value={slotsShown} /> <PR2>Показать слоты</PR2>{' '}
            <NavigateDropButton setOpenDrawer={handleOpenDrawerWithCompany} />
          </div>
        </div>
      </div>
      <CalenadarBody />
    </div>
  )
}
