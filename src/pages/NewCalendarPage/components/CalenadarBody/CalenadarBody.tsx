import { useAppSelector } from '@/hooks/useAppSelector'
import cls from './CalenadarBody.module.scss'
import { Scheduler } from '@devexpress/dx-react-scheduler-material-ui'
//@ts-expect-error
import { ViewState } from '@devexpress/dx-react-scheduler'
import { CustomDayView } from '../DayView/DayView'
import { CustomAppointment } from '../Appointment/Appointment'
import { CustomWeekView } from '../WeekView/WeekView'
import { CustomToolbar } from '../Toolbar/Toolbar'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import dayjs from 'dayjs'
import { setCurrentDate } from '@/store/slices/calendarSlice/calendarSlice'
import { CustomDateNavigator } from '../DateNavigator/DateNavigator'
import { CustomScheduler } from '../CustomSheduler/CustomScheduler'

export const CalenadarBody = () => {
  const dispatch = useAppDispatch()
  const currentDate = useAppSelector((state) => state.calendar.currentDate)
  const { freeTimeslots, slotsEvents } = useAppSelector((state) => state.timeSlots)
  const calendarType = useAppSelector((store) => store.calendar.viewType)
  const setDate = (e: Date) => {
    const date = dayjs(e).format('YYYY-MM-DD')
    dispatch(setCurrentDate(date))
  }
  return (
    <div className={cls.CalenadarBody}>
      <Scheduler locale="ru-RU" firstDayOfWeek={1} data={[...freeTimeslots, ...slotsEvents]} height="auto" rootComponent={CustomScheduler}>
        <ViewState currentDate={currentDate} currentViewName={calendarType.value} onCurrentDateChange={setDate} />
        <CustomToolbar />
        <CustomDateNavigator />
        <CustomWeekView />
        <CustomDayView />
        <CustomAppointment />
      </Scheduler>
    </div>
  )
}
