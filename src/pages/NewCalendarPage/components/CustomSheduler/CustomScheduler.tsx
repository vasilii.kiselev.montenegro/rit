import { Scheduler } from '@devexpress/dx-react-scheduler-material-ui'
import cls from './CustomScheduler.module.scss'
import { ReactElement } from 'react'
import { CalendarSidebar } from '../CalendarSidebar/CalendarSidebar'

export const CustomScheduler = (props: Scheduler.RootProps) => {
  return (
    <div className={cls.CustomSheduler}>
      <div className={cls.schedulerContent}>{props.children.map((el: ReactElement) => el)}</div>
      <CalendarSidebar />
    </div>
  )
}
