import { TimeEvent, TimeSlotWithEvents } from '@/types/calendar'
import dayjs from 'dayjs'

const getSlotTime = (data: TimeSlotWithEvents): [number[], number, number] => {
  const startTime: number[] = dayjs(data.startDate).format('HH.mm').split('.').map(Number)
  const endTime: number[] = dayjs(data.endDate).format('HH.mm').split('.').map(Number)
  const timeDifference = endTime[0] * 60 - startTime[0] * 60 + endTime[1] - startTime[1]
  return [startTime, timeDifference, timeDifference * 0.01]
}
const getEventTime = (event: TimeEvent, slotStartTime: number[]) => {
  const startTime: number[] = event.startDate.split(':').map(Number)
  const endTime: number[] = event.endDate.split(':').map(Number)
  const timeDifference = endTime[0] * 60 - startTime[0] * 60 + endTime[1] - startTime[1]
  const startTimeInMinutes = (startTime[0] - slotStartTime[0]) * 60 + startTime[1] - slotStartTime[1]
  return [startTimeInMinutes, timeDifference]
}
export const checkPosition = (slot: TimeSlotWithEvents, event: TimeEvent) => {
  const [slotStartTime, , percent] = getSlotTime(slot)

  const [startTime, eventTime] = getEventTime(event, slotStartTime)

  const height = eventTime / percent

  const startPosition = startTime / percent
  return [height, startPosition]
}

//для старого календаря

// const getSlotTime = (data) => {
//   const startTime: number[] = dayjs(data.data.startDate).format('HH.mm').split('.').map(Number)
//   const endTime: number[] = dayjs(data.data.endDate).format('HH.mm').split('.').map(Number)
//   const timeDifference = endTime[0] * 60 - startTime[0] * 60 + endTime[1] - startTime[1]
//   return [startTime, timeDifference, timeDifference * 0.01]
// }
// const getEventTime = (data, slotStartTime) => {
//   const startTime: number[] = dayjs(data.startDate).format('HH.mm').split('.').map(Number)
//   const endTime: number[] = dayjs(data.endDate).format('HH.mm').split('.').map(Number)
//   const timeDifference = endTime[0] * 60 - startTime[0] * 60 + endTime[1] - startTime[1]
//   const stertTimeInMinutes = (startTime[0] - slotStartTime[0]) * 60 + startTime[1] - slotStartTime[1]
//   return [stertTimeInMinutes, timeDifference]
// }
// export const checkPosition = (data, event) => {
//   const [slotStartTime, fullSize, percent] = getSlotTime(data)
//   const [startTime, eventTime] = getEventTime(event, slotStartTime)
//   const height = eventTime / percent
//   const startPosition = startTime / percent
//   return [height, startPosition]
// }
