import { Appointments } from '@devexpress/dx-react-scheduler-material-ui'
import cls from './Appointment.module.scss'
import { ComponentType } from 'react'
import { OPACITY_VALUE } from '../../consts'
import { CalendarViewType, TimeEvent, TimeSlotWithEvents } from '@/types/calendar'
import { checkPosition } from './utils'
import { useAppSelector } from '@/hooks/useAppSelector'

const AppointmentComponent = (props: ComponentType<Appointments.AppointmentProps>) => {
  //@ts-expect-error
  const timeSlot: TimeSlotWithEvents = props.data
  const calendarType = useAppSelector((store) => store.calendar.viewType)
  const { slots, slotsEvents, slotsLock } = useAppSelector((store) => store.calendar.sidebarEvents)
  console.log(timeSlot)

  if (!slots && !timeSlot.freeSlot) {
    return null
  }
  if (!slotsLock && timeSlot.freeSlot) {
    return null
  }
  return (
    <div
      {...props}
      style={{ background: `${timeSlot.color}${OPACITY_VALUE}  ` }}
      className={`${cls.contAppointment} ${slotsLock && timeSlot.freeSlot ? cls.lock : ''}`}
    >
      {(slotsLock ? !timeSlot.lock : true) &&
        slotsEvents &&
        timeSlot?.events?.map((el: TimeEvent) => {
          const [height, startPosition] = checkPosition(timeSlot, el)

          return (
            <div
              className={`${cls.appointmentEvent} ${calendarType.value === CalendarViewType.WEEK ? cls.contAppointmentWeek : cls.contAppointmentDay}`}
              style={{ top: `${startPosition}%`, height: `${height}%` }}
            >
              <div className={cls.EventColor} style={{ background: `${el?.color}${OPACITY_VALUE}` }} />
              <div className={cls.timeString}>
                <span>{el.startDate}</span>-<span>{el.endDate}</span>
              </div>
            </div>
          )
        })}
    </div>
  )
}

export const CustomAppointment = () => {
  return <Appointments appointmentComponent={AppointmentComponent} />
}
