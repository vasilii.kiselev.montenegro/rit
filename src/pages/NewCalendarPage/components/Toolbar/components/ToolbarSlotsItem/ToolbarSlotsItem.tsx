import { SlotVacancies, TIME_FORMAT_STRING, TimeSlotWithEvents } from '@/types/calendar'
import cls from './ToolbarSlotsItem.module.scss'
import { OPACITY_VALUE } from '@/pages/NewCalendarPage/consts'
import { PM2, PM4 } from '@/UI/TextComponents'
import dayjs from 'dayjs'
import { VacancyItemInSlot } from './components/VacancyItemInSlot/VacancyItemInSlot'
import { SlotDropButton } from './components/SlotDropButton/SlotDropButton'

interface ToolbarSlotsItemProps {
  slot: TimeSlotWithEvents
  changeFiltersArray: (filterItem: SlotVacancies) => void
}

export const ToolbarSlotsItem = ({ slot, changeFiltersArray }: ToolbarSlotsItemProps) => {
  return (
    <div
      style={{
        background: `${slot.color}${OPACITY_VALUE}`,
      }}
      className={cls.ToolbarSlotsItem}
    >
      <div className={cls.left}>
        <PM2>Слот {slot.timeSlotId}</PM2>
        <PM4>
          {dayjs(slot.startDate).format(TIME_FORMAT_STRING)}-{dayjs(slot.endDate).format(TIME_FORMAT_STRING)}
        </PM4>
      </div>
      {slot?.vacancies && (
        <div className={cls.middle}>
          {slot.vacancies.length < 3 ? (
            slot.vacancies.map((el) => <VacancyItemInSlot key={el.id} changeFiltersArray={changeFiltersArray} vacancy={el} />)
          ) : (
            <>
              <VacancyItemInSlot changeFiltersArray={changeFiltersArray} vacancy={slot.vacancies[0]} />
              <div className={cls.vacancyItemHover}>
                <div className={cls.vacancyItemHoverBtn}>...</div>
                <div className={cls.vacancyItemHoverContent} style={{ backgroundColor: `${slot.color}` }}>
                  {slot.vacancies.map((el) => (
                    <VacancyItemInSlot key={el.id} changeFiltersArray={changeFiltersArray} vacancy={el} />
                  ))}
                </div>
              </div>
            </>
          )}
        </div>
      )}
      <div className={cls.right}>
        <SlotDropButton slot={slot} />
      </div>
    </div>
  )
}
