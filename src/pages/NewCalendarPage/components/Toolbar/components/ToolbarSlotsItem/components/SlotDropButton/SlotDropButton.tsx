import * as React from 'react'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ButtonIcon } from '@/UI/ButtonIcon/ButtonIcon'
import cls from './SlotDropButton.module.scss'
import { Comment, H1 } from '@/UI/TextComponents'
import { MoreIcon } from '@/assets/icons/MoreIcon'
import { SlotFormOperationTypeEnum, TimeSlotWithEvents } from '@/types/calendar'
import { DrawerContext } from '@/components/Drawer/DrawerContext'
import { NewCreateChangeSlotForm } from '@/pages/NewCalendarPage/components/CreateChangeSlotForm/NewCreateChangeSlotForm'
import { ModalContext } from '@/components/Modal/modalCotext'
import { ConfirmModal } from '@/components/ConfirmModal/ConfirmModal'

interface SlotDropButtonProps {
  slot: TimeSlotWithEvents
}

export function SlotDropButton({ slot }: SlotDropButtonProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)

  const { handleDrawer } = React.useContext(DrawerContext)
  const { handleModal } = React.useContext(ModalContext)

  const handleOpenDrawerWithCompany = (slotId: number, operationType: SlotFormOperationTypeEnum) => {
    handleDrawer(<NewCreateChangeSlotForm slotId={slotId} operationType={operationType} />, <H1>Добавление слота на собеседование</H1>)
  }

  const handleOpenConfirmModal = (slotId: number, operationType: 'schedule' | 'children') => {
    handleModal(<ConfirmModal operation={operationType} slotId={slotId} />)
  }
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  const navigateDrawerPage = (slotId: number, operationType: SlotFormOperationTypeEnum) => {
    handleOpenDrawerWithCompany(slotId, operationType)
    setAnchorEl(null)
  }
  const navigateModalPage = (slotId: number, operationType: 'schedule' | 'children') => {
    handleOpenConfirmModal(slotId, operationType)
    setAnchorEl(null)
  }
  return (
    <div>
      <ButtonIcon
        color="btnIconWhite"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <MoreIcon />
      </ButtonIcon>
      <Menu
        className={cls.menu}
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem
          className={cls.menuItem}
          onClick={() => navigateDrawerPage(slot.timeslotSchedule, SlotFormOperationTypeEnum.CHANGE_PARENT)}
        >
          <Comment>Редактировать слот</Comment>
        </MenuItem>
        <MenuItem className={cls.menuItem} onClick={() => navigateDrawerPage(slot.timeSlotId, SlotFormOperationTypeEnum.CHANGE_CHILDREN)}>
          <Comment>Редактировать экземпляр слота</Comment>
        </MenuItem>
        <MenuItem className={cls.menuItem} onClick={() => navigateModalPage(slot.timeSlotId, 'children')}>
          <Comment>удалить экземпляр слота</Comment>
        </MenuItem>
        <MenuItem className={cls.menuItem} onClick={() => navigateModalPage(slot.timeslotSchedule, 'schedule')}>
          <Comment>удалить слот</Comment>
        </MenuItem>
        {/* <MenuItem className={cls.menuItem} onClick={navigateFreeUpTime}>
          <Comment>Свободное время</Comment>
        </MenuItem> */}
      </Menu>
    </div>
  )
}
