import { SlotVacancies } from '@/types/calendar'
import cls from './VacancyIteminSlot.module.scss'

interface VacancyItemInSlotProps {
  vacancy: SlotVacancies
  changeFiltersArray: (filterItem: SlotVacancies) => void
}

export const VacancyItemInSlot = ({ vacancy, changeFiltersArray }: VacancyItemInSlotProps) => {
  return (
    <div className={cls.vacancyItem} onClick={() => changeFiltersArray(vacancy)}>
      {vacancy.name}
    </div>
  )
}
