import { CalendarViewType, DATE_FORMAT_STRING, SlotVacancies, TimeSlotWithEvents } from '@/types/calendar'
import dayjs from 'dayjs'

interface ToolbarSlots {
  currentSlots: TimeSlotWithEvents[]
  otherSlots: TimeSlotWithEvents[]
}

const checkEqualsIds = (arr: SlotVacancies[], filterArr: SlotVacancies[]): boolean => {
  const arrIds = arr.map(({ id }) => id)
  const filterIds = filterArr.map(({ id }) => id)
  const returnValue = filterIds.every((filterId) => arrIds.includes(filterId))

  return returnValue
}

export const sortSlots = (filterIds: SlotVacancies[], slots: ToolbarSlots): ToolbarSlots => {
  const newCurrentSlots = slots.currentSlots.filter((el) => checkEqualsIds(el.vacancies, filterIds))
  const newOtherSlots = slots.otherSlots.filter((el) => checkEqualsIds(el.vacancies, filterIds))

  return {
    currentSlots: newCurrentSlots,
    otherSlots: newOtherSlots,
  }
}
interface StartEndDates {
  startDate: string
  endDate: string
}
const getWeekDays = (currentDate: string): StartEndDates => {
  const DateFormatCurrentDate = dayjs(currentDate)
  const startDate = DateFormatCurrentDate.startOf('week').add(1, 'day').format(DATE_FORMAT_STRING)
  const endDate = DateFormatCurrentDate.endOf('week').add(1, 'day').format(DATE_FORMAT_STRING)

  return { startDate, endDate }
}
export const getStartEndDates = (calendarType: string, currentDate: string): StartEndDates => {
  switch (calendarType) {
    case CalendarViewType.WEEK:
      return getWeekDays(currentDate)
    case CalendarViewType.DAY:
      return { startDate: currentDate, endDate: currentDate }
  }
}
