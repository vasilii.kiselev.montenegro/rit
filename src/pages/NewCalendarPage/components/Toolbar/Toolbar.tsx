import { Toolbar } from '@devexpress/dx-react-scheduler-material-ui'
import cls from './Toolbar.module.scss'
import { CalendarTypePicker } from '../CalendarTypePicker/CalendarTypePicker'
import { ButtonColors } from '@/UI/Button/consts'
import { Button } from '@/UI/Button/Button'
import { ReloadIcon } from '@/assets/icons/ReloadIcon'
import { SidebarIcon } from '@/assets/icons/SidebarIcon'
import { Comment, PM2 } from '@/UI/TextComponents'
import { ArrowDropDownIcon } from '@mui/x-date-pickers'
import { useAppSelector } from '@/hooks/useAppSelector'
import { useEffect, useState } from 'react'
import { CalendarViewType, SlotVacancies, TimeSlotWithEvents } from '@/types/calendar'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { sortSlots } from './utils'
import { ToolbarSlotsItem } from './components/ToolbarSlotsItem/ToolbarSlotsItem'
import { CrossIcon } from '@/assets/icons/CrossIcon'
import { setSidebarState } from '@/store/slices/calendarSlice/calendarSlice'
import { updateTimeslots } from '../../utils'

interface ToolbarSlots {
  currentSlots: TimeSlotWithEvents[]
  otherSlots: TimeSlotWithEvents[]
}

const ToolbarRoot = (props: Toolbar.RootProps) => {
  const dispatch = useAppDispatch()
  const calendar = useAppSelector((store) => store.calendar)
  const timeslotsState = useAppSelector((store) => store.timeSlots)

  const [viewFullSlots, setViewFullSlots] = useState<boolean>(false)
  const [slots, setSlots] = useState<ToolbarSlots>()
  const [filterIds, setFilterIds] = useState<SlotVacancies[]>([])

  const typeLabel = calendar.viewType.value === CalendarViewType.DAY ? 'день' : 'неделе'
  const fullString = `На ${calendar.viewType.value === CalendarViewType.WEEK ? 'текущей' : 'текущий'} ${typeLabel} слотов нет `

  const updateSlots = () => {
    console.log('asdas')
    console.log(slots)

    const updatedSlots = updateTimeslots()
    console.log(updatedSlots)

    setSlots(updatedSlots)
    console.log(slots)
  }
  // useEffect(() => {
  //   setSlots(updateTimeslots())
  // }, [])

  useEffect(() => {
    setSlots(updateTimeslots())
  }, [calendar.currentDate, calendar.viewType, dispatch])
  // const getSlotsEvents = useCallback(() => {
  //   const queryParamsForToolbar = getStartEndDates(calendar.viewType.value, calendar.currentDate)
  //   const queryParamsForWeekEvents = getStartEndDates(CalendarViewType.DAY, calendar.currentDate)
  //   dispatch(getSlotsEventsForToolbarThunk(queryParamsForToolbar))
  //     .unwrap()
  //     .then((data) => setSlots((state) => ({ ...state, currentSlots: data })))
  //   dispatch(getSlotsEventsThunk())
  //     .unwrap()
  //     .then((data) => setSlots((state) => ({ ...state, otherSlots: data })))
  //   dispatch(getWeekEventsThunk(queryParamsForWeekEvents))
  // }, [calendar.currentDate, calendar.viewType, dispatch])

  // useEffect(() => {
  //   getSlotsEvents()
  // }, [])

  // useEffect(() => {
  //   getSlotsEvents()
  // }, [getSlotsEvents, calendar.currentDate, calendar.viewType, dispatch])

  useEffect(() => {
    // if (slots?.currentSlots && slots?.otherSlots) {
    setSlots(
      sortSlots(filterIds, {
        currentSlots: timeslotsState.toolbarSlots,
        otherSlots: timeslotsState.slotsEvents,
      })
    )
    // }
  }, [filterIds, timeslotsState.slotsEvents, timeslotsState.toolbarSlots])

  const changeFiltersArray = (filterItem: SlotVacancies) => {
    if (filterIds.some((el) => el.id === filterItem.id)) {
      setFilterIds((state) => state.filter((el) => el.id !== filterItem.id))
    } else {
      setFilterIds((state) => [...state, filterItem])
    }
  }
  const deleteVacancyFilter = (filterItem: SlotVacancies) => {
    setFilterIds((state) => state.filter((el) => el.id !== filterItem.id))
  }

  return (
    <div className={cls.Toolbar}>
      <div className={cls.toolbarTop}>
        <div className={cls.lefSide}>
          {props.children}
          <CalendarTypePicker />
        </div>
        <div className={cls.rightSide}>
          <Button color={ButtonColors.GREY} onClick={updateSlots} className={cls.reloadButton}>
            {/* <Button color={ButtonColors.GREY} onClick={getSlotsEvents} className={cls.reloadButton}> */}
            <div className={cls.reloadImage}>
              <ReloadIcon />
            </div>
          </Button>
          <Button color={ButtonColors.ICON_GREEN} onClick={() => dispatch(setSidebarState())}>
            <SidebarIcon />
          </Button>
        </div>
      </div>
      {calendar.showSlots && (
        <div className={cls.toolbarDown}>
          <div className={cls.slotsHeader}>
            <div className={cls.slotsView} onClick={() => setViewFullSlots((prev) => !prev)}>
              <PM2>Слоты</PM2>
              <ArrowDropDownIcon style={{ rotate: viewFullSlots ? '0deg' : '180deg' }} />
            </div>
            {!!filterIds.length && (
              <div className={cls.filters}>
                <Comment>Фильтры ({filterIds.length})</Comment>
                {filterIds.map((el) => (
                  <div className={cls.filtersItem} onClick={() => deleteVacancyFilter(el)}>
                    {el.name}{' '}
                    <div className={cls.filtersItemCrossCont}>
                      {' '}
                      <CrossIcon />
                    </div>
                  </div>
                ))}
              </div>
            )}
            {/* {slots?.currentSlots?.length > 3 && <ArrowDropDownIcon style={{ rotate: viewFullSlots ? '0deg' : '180deg' }} />} */}
          </div>
          <div className={cls.slotsList}>
            {!slots?.currentSlots?.length && <Comment>{fullString}</Comment>}
            {!viewFullSlots && slots
              ? slots?.currentSlots?.map((el) => <ToolbarSlotsItem key={el.timeSlotId} slot={el} changeFiltersArray={changeFiltersArray} />)
              : slots?.otherSlots?.map((el) => <ToolbarSlotsItem key={el.timeSlotId} slot={el} changeFiltersArray={changeFiltersArray} />)}
          </div>
        </div>
      )}
    </div>
  )
}

export const CustomToolbar = () => {
  return <Toolbar rootComponent={ToolbarRoot}></Toolbar>
}
