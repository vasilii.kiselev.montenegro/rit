import cls from './CreateChangeSlotForm.module.scss'
import { useFormik } from 'formik'
import { useCallback, useContext, useEffect, useState } from 'react'
import { CalendarChildrenSlotForm, CalendarSlotFormSchedule, repeatSlot, SlotFormOperationTypeEnum } from '@/types/calendar'
import { changeChildrenSlot, changeScheduleSlot, createcheduleSlot, getChildrenSlot, getScheduleSlot } from '@/api/calendar/calendar'
import { Comment, PM2, PR2 } from '@/UI/TextComponents'
import { VacanciesMultiSelectInput } from '@/UI/VacanciesMultiSelectInput/VacanciesMultiSelectInput'
import { useAppSelector } from '@/hooks/useAppSelector'
import { ClockIcon } from '@mui/x-date-pickers'
import { TimeInput } from '@/UI/TimeInput/TimeInput'
import dayjs, { Dayjs } from 'dayjs'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { BlackCalendarIcon } from '@/assets/icons/BlackCalendarIcon'
import { InputSelectMini } from '@/UI/InputSelectMini/InputSelectMini'
import { ButtonColors } from '@/UI/Button/consts'
import { Button } from '@/UI/Button/Button'
import { newGenerateWeek } from '@/pages/NewCreateVacancy/components/steps/FourthStep/utils'
import * as Yup from 'yup'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { getSlotsEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsThunk'
import { ModalContext } from '@/components/Modal/modalCotext'
import { DrawerContext } from '@/components/Drawer/DrawerContext'
import { updateTimeslots } from '../../utils'
import { TimeslotsErorrsModal } from '@/components/TimeslotsErorrsModal/TimeslotsErorrsModal'

export type OperationType = SlotFormOperationTypeEnum

interface CreateChangeSlotFormProps {
  slotId?: number
  operationType: OperationType
}

export const NewCreateChangeSlotForm = ({ slotId, operationType }: CreateChangeSlotFormProps) => {
  const slotTypeGuard = (slot: CalendarSlotFormSchedule | CalendarChildrenSlotForm): slot is CalendarSlotFormSchedule => {
    return (slot as CalendarSlotFormSchedule)?.day !== undefined
  }
  const { handleModal } = useContext(ModalContext)
  const { handleDrawer } = useContext(DrawerContext)
  console.log(handleModal, handleDrawer)

  const vacancies = useAppSelector((state) => state.vacancies.workState)
  const dispatch = useAppDispatch()
  const endForm = () => {
    dispatch(getSlotsEventsThunk())

    handleDrawer()
  }
  const handleOpenModalInfoTimeslots = (text: string) => {
    console.log(handleModal)

    handleModal(<TimeslotsErorrsModal text={text} />)
  }

  const emptyScheduleSlot: CalendarSlotFormSchedule = {
    day: dayjs().format('YYYY-MM-DD'),
    endTime: dayjs('2000-01-01T20:00'),
    recurrenceDays: [],
    startTime: dayjs('2000-01-01T08:00'),
    recurrenceType: repeatSlot[0].value,
    vacancies: [],
  }

  const [slot, setSlot] = useState<CalendarSlotFormSchedule | CalendarChildrenSlotForm>()

  const submitForm = (values: CalendarSlotFormSchedule | CalendarChildrenSlotForm) => {
    if (operationType === SlotFormOperationTypeEnum.CHANGE_CHILDREN) {
      changeChildrenSlot(values, slotId)
        .then(() => {
          updateTimeslots()
          endForm()
        })
        .catch((err) => handleOpenModalInfoTimeslots(err))
    }
    if (operationType === SlotFormOperationTypeEnum.CHANGE_PARENT && slotTypeGuard(values)) {
      changeScheduleSlot(values, slotId)
        .then(() => {
          updateTimeslots()
          endForm()
        })
        .catch((err) => handleOpenModalInfoTimeslots(err))
    }
    if (operationType === SlotFormOperationTypeEnum.CREATE && slotTypeGuard(values)) {
      createcheduleSlot(values)
        .then(() => {
          updateTimeslots()
          endForm()
        })
        .catch((err) => {
          console.log(err)
          handleOpenModalInfoTimeslots(err)
        })
    }
  }

  const fullValiadtionSchema = Yup.object<CalendarSlotFormSchedule>().shape({
    recurrenceType: Yup.string().required(),
    startTime: Yup.mixed<Dayjs>().required(),
    endTime: Yup.mixed<Dayjs>()
      .required()
      .test('is-valid-end', 'Длительность слота должна быть не меньше длительности собеседования.', function () {
        console.log(this.parent)
        const { startTime, endTime } = this.parent
        const start = startTime.minute() + startTime.hour() * 60
        const end = endTime.minute() + endTime.hour() * 60

        return end - start >= 20
      }),
    day: Yup.string().required(),
    vacancies: Yup.array()
      .required()
      .test('is-empty', 'список вакансий не должен быть пустым', function () {
        return !!this.parent.vacancies.length
      }),
  })
  const childrenValiadtionSchema = Yup.object<CalendarChildrenSlotForm>().shape({
    startTime: Yup.mixed().required(),
    endTime: Yup.mixed()
      .required()
      .test('is-valid-end', 'Длительность слота должна быть не меньше длительности собеседования.', function () {
        console.log(this.parent)
        const { startTime, endTime } = this.parent
        const start = startTime.minute() + startTime.hour() * 60
        const end = endTime.minute() + endTime.hour() * 60

        return end - start >= 20
      }),
  })

  const loadState = useCallback(async () => {
    if (operationType === SlotFormOperationTypeEnum.CHANGE_PARENT) {
      const data = await getScheduleSlot(slotId)
      return data
    } else if (operationType === SlotFormOperationTypeEnum.CHANGE_CHILDREN) {
      const data = await getChildrenSlot(slotId)
      return data
    } else {
      return emptyScheduleSlot
    }
  }, [operationType, slotId])

  useEffect(() => {
    const fetchData = async () => {
      const data = await loadState()
      setSlot(data)
      formik.setValues(data)
    }
    fetchData()
  }, [loadState])
  const formik = useFormik({
    initialValues: slot || undefined,
    validationSchema: operationType === SlotFormOperationTypeEnum.CHANGE_CHILDREN ? childrenValiadtionSchema : fullValiadtionSchema,
    onSubmit: submitForm,
  })

  const weekValues = slotTypeGuard(formik.values) && newGenerateWeek(formik.values?.day)

  const isDisabled = !!Object.values(formik.errors).length

  console.log(formik.values)

  return formik.values !== undefined ? (
    <div className={cls.CreateChangeSlotForm}>
      <div className={cls.CreateChangeSlotFormCont}>
        {slotTypeGuard(formik.values) && (
          <div className={cls.defaultField}>
            <PM2>Выбор вакансии</PM2>

            <VacanciesMultiSelectInput
              //@ts-expect-error
              value={formik.values?.vacancies}
              setValues={(data) => formik.setFieldValue('vacancies', data)}
              values={vacancies}
            />
          </div>
        )}

        <div className={cls.FormImputLabel}>
          <PM2>Выбор времени</PM2>
          <div className={cls.FormImputLabelDoubleSelectTime}>
            <div className={cls.FormImputLabelSelectIcon}>
              <ClockIcon />
              <div className={cls.FormImputLabelSelectTime}>
                <TimeInput
                  name="startTime"
                  value={formik.values?.startTime}
                  onChange={(value) => formik.setFieldValue('startTime', value)}
                />
                <TimeInput name="endTime" value={formik.values?.endTime} onChange={(value) => formik.setFieldValue('endTime', value)} />
              </div>
            </div>
          </div>
          {formik.errors.endTime && <div className={cls.startEndSlotError}>Минимальная длительность слота - 15мин </div>}
        </div>
        {slotTypeGuard(formik.values) && (
          <>
            <div>
              {formik.values?.recurrenceType !== repeatSlot[0].value && <PR2>Начать с:</PR2>}
              <div className={cls.FormImputLabelSelectIcon}>
                <BlackCalendarIcon />
                <InputSelectMini values={weekValues} placeholder=" " name="day" value={formik.values?.day} setValue={formik.handleChange} />
              </div>
            </div>
            <div className={cls.FormImputLabel}>
              <PM2>Повтор слота</PM2>
              <RadioGroup
                aria-labelledby="demo-controlled-radio-buttons-group"
                name="recurrenceType"
                value={formik.values?.recurrenceType}
                onChange={formik.handleChange}
                className={cls.FormImputLabelRadio}
              >
                {repeatSlot.map((el) => {
                  return (
                    <FormControlLabel
                      style={{ marginLeft: '0' }}
                      key={el.value}
                      value={el.value}
                      control={<Radio style={{ display: 'none' }} />}
                      label={
                        <div className={cls.FormImputLabelRadioItem}>
                          <div
                            className={`${cls.FormImputLabelRadioItemIcon} ${slotTypeGuard(formik.values) && el.value === formik.values?.recurrenceType ? cls.active : ''}`}
                          >
                            &#10003;
                          </div>
                          <Comment> {el.label}</Comment>
                        </div>
                      }
                    />
                  )
                })}
              </RadioGroup>
            </div>
          </>
        )}
      </div>
      <div className={cls.footerForm}>
        <Button onClick={() => formik.handleSubmit()} type={'submit'} color={ButtonColors.ICON_WHITE} disabled={isDisabled}>
          {operationType === SlotFormOperationTypeEnum.CREATE ? 'Отправить' : 'Изменить'}
        </Button>
      </div>
    </div>
  ) : (
    <div className={cls.loadCircleCont}>
      <div className={cls.loadCircle}> </div>{' '}
    </div>
  )
}
