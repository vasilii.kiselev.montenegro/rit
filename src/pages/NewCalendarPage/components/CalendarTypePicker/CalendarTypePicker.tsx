import { Comment } from '@/UI/TextComponents/TextComponents'
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import cls from './CalendarTypePicker.module.scss'
import { useState } from 'react'
import { useAppSelector } from '@/hooks/useAppSelector'
import { CALENDAR_VIEW } from '../../consts'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setViewType } from '@/store/slices/calendarSlice/calendarSlice'
import { CalendarViewItem } from '@/types/calendar'

export const CalendarTypePicker = () => {
  const dispatch = useAppDispatch()
  const value = useAppSelector((store) => store.calendar.viewType)
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }
  const setType = (value: CalendarViewItem) => {
    dispatch(setViewType(value))
  }

  return (
    <div className={cls.pickerCont}>
      <Comment>Вид:</Comment>
      <FormControl className={cls.InputSelectForm}>
        <InputLabel className={cls.InputSelectFormLabel} id="demo-controlled-open-select-label"></InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          className={cls.InputSelect}
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={value}
          //@ts-expect-error
          onChange={(e) => setType(e.target.value)}
          displayEmpty
        >
          {CALENDAR_VIEW.map((el) => (
            //@ts-expect-error
            <MenuItem key={`key-${el.label}`} value={el}>
              <Comment>{el.label}</Comment>
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  )
}
