import { ArrowDownIcon } from '@/assets/icons/ArrowDownIcon'
import { DateNavigator } from '@devexpress/dx-react-scheduler-material-ui'
import { Grid, IconButton, Popover, Typography } from '@mui/material'
import { FC, useCallback } from 'react'

const DateNavigatorRoot: FC<DateNavigator.RootProps> = ({
  navigatorText,
  navigationButtonComponent: NavigationButton,

  rootRef,

  onNavigate,
}) => {
  const navigateBack = useCallback(() => {
    onNavigate('back')
  }, [onNavigate])
  const navigateForward = useCallback(() => {
    onNavigate('forward')
  }, [onNavigate])

  return (
    <Grid
      item
      container
      alignItems="center"
      justifyContent="center"
      component="div"
      ref={rootRef as unknown as React.RefObject<HTMLDivElement>}
    >
      <NavigationButton type="back" onClick={navigateBack} />
      {/* <OpenButton
          
          onVisibilityToggle={onVisibilityToggle}
          text={``}
        /> */}
      <NavigationButton type="forward" onClick={navigateForward} />
      {navigatorText?.replace(/\d{4}\W*/, '').trim()}
    </Grid>
  )
}

const DateNavigatorOverlay: FC<DateNavigator.OverlayProps> = ({ visible, onHide, children, target }) => {
  return (
    <Popover
      open={visible ? visible : false}
      anchorEl={target as Element}
      onClose={onHide}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
    >
      {children}
    </Popover>
  )
}

const DateNavigatorOpenButton: FC<DateNavigator.OpenButtonProps> = ({ onVisibilityToggle, text }) => {
  return (
    <Typography variant="body2" color="primary" onClick={onVisibilityToggle}>
      {text}
    </Typography>
  )
}

const DateNavigatorNavigationButton: FC<DateNavigator.NavigationButtonProps> = ({ type, onClick }) => {
  return (
    <IconButton onClick={onClick}>
      {type === 'back' ? <ArrowDownIcon style={{ rotate: '90deg' }} /> : <ArrowDownIcon style={{ rotate: '-90deg' }} />}
    </IconButton>
  )
}
export const CustomDateNavigator = () => (
  <DateNavigator
    rootComponent={DateNavigatorRoot}
    overlayComponent={DateNavigatorOverlay}
    openButtonComponent={DateNavigatorOpenButton}
    navigationButtonComponent={DateNavigatorNavigationButton}
  />
)
