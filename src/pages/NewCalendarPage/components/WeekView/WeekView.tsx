import { WeekView } from '@devexpress/dx-react-scheduler-material-ui'
import cls from './WeekView.module.scss'
import { Hour, shortWeekDays } from '../../consts'
import { useAppSelector } from '@/hooks/useAppSelector'
import { getCurrentWeekDays } from '../../utils'
import { useMemo } from 'react'
import dayjs from 'dayjs'
import { TableCell } from '@mui/material'

// interface WeekViewProps {
// }

const CustomWeekScaleRow = (currentDate: string) => {
  const week = useMemo(() => getCurrentWeekDays(currentDate), [currentDate])

  return (
    <>
      {week.map((el) => {
        const today = dayjs(currentDate).day() === el.day()
        const classes = `${cls.weekDayCell} ${today ? cls.weekDayCellToday : ''}`
        return (
          <TableCell>
            <span className={classes}>
              {shortWeekDays[el.day()]} {dayjs(el).format('DD')}
            </span>
          </TableCell>
        )
      })}
    </>
  )
}
const TimeScaleLayoutComponent = (props: WeekView.TimeScaleLayoutProps) => {
  return (
    <div className={cls.timeCellRow} style={{ height: props.height }}>
      {/*@ts-expect-error */}
      {props.cellsData.map(([{ startDate }]: Dayjs) => (
        <div className={cls.timeCell}>{dayjs(startDate).format('HH:mm')}</div>
      ))}
    </div>
  )
}

export const CustomWeekView = () => {
  const currentDate = useAppSelector((state) => state.calendar.currentDate)
  return (
    <WeekView
      name="Week"
      startDayHour={Hour.Start}
      endDayHour={Hour.End}
      cellDuration={60}
      dayScaleRowComponent={() => CustomWeekScaleRow(currentDate)}
      timeScaleLayoutComponent={TimeScaleLayoutComponent}
    />
  )
}
