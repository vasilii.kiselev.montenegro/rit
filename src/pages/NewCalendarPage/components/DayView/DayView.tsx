import { DayView } from '@devexpress/dx-react-scheduler-material-ui'
import cls from './DayView.module.scss'
import { useMemo } from 'react'
import { Hour, shortWeekDays } from '../../consts'
import dayjs, { Dayjs } from 'dayjs'
import { getCurrentWeekDays } from '../../utils'
import { useAppSelector } from '@/hooks/useAppSelector'
import { TableCell } from '@mui/material'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import { setCurrentDate } from '@/store/slices/calendarSlice/calendarSlice'

const CustomDayScaleRow = (currentDate: string) => {
  const dispatch = useAppDispatch()
  const week = useMemo(() => getCurrentWeekDays(currentDate), [currentDate])
  const changeCurrentDay = (el: Dayjs): void => {
    dispatch(setCurrentDate(el.format('YYYY-MM-DD')))
  }
  return (
    <>
      {week.map((el) => {
        const today = dayjs(currentDate).day() === el.day()
        const classes = `${cls.weekDayCell} ${today ? cls.weekDayCellToday : ''}`
        return (
          <TableCell>
            <span className={classes} onClick={() => changeCurrentDay(el)}>
              {shortWeekDays[el.day()]} {dayjs(el).format('DD')}
            </span>
          </TableCell>
        )
      })}
    </>
  )
}

const TimeScaleLayoutComponent = (props: DayView.TimeScaleLayoutProps) => {
  return (
    <div className={cls.timeCellRow} style={{ height: props.height }}>
      {/*@ts-expect-error */}
      {props.cellsData.map(([{ startDate }]: Dayjs) => (
        <div className={cls.timeCell}>{dayjs(startDate).format('HH:mm')}</div>
      ))}
    </div>
  )
}

export const CustomDayView = () => {
  const currentDate = useAppSelector((state) => state.calendar.currentDate)
  return (
    <DayView
      name="Day"
      startDayHour={Hour.Start}
      endDayHour={Hour.End}
      cellDuration={60}
      dayScaleRowComponent={() => CustomDayScaleRow(currentDate)}
      timeScaleLayoutComponent={TimeScaleLayoutComponent}
    />
  )
}
