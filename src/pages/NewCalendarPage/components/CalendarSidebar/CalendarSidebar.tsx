import { useAppSelector } from '@/hooks/useAppSelector'
import cls from './CalendarSidebar.module.scss'
import { DateCalendar, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import dayjs, { Dayjs } from 'dayjs'
import { useAppDispatch } from '@/hooks/useAppDispatch'
import {
  setCurrentDate,
  setSidebarEventsSlotsEventsState,
  setSidebarEventsSlotsLockState,
  setSidebarEventsSlotsState,
  setSidebarState,
  setViewType,
} from '@/store/slices/calendarSlice/calendarSlice'
import { DATE_FORMAT_STRING, TimeEvent } from '@/types/calendar'
import { PM2, PR2 } from '@/UI/TextComponents'
import { CALENDAR_VIEW, OPACITY_VALUE, weekDays } from '../../consts'
import { ButtonColors } from '@/UI/Button/consts'
import { SidebarIcon } from '@/assets/icons/SidebarIcon'
import { Button } from '@/UI/Button/Button'

export const CalendarSidebar = () => {
  const dispatch = useAppDispatch()
  const currentDate = useAppSelector((store) => store.calendar.currentDate)
  const open = useAppSelector((store) => store.calendar.sidebarIsOpen)
  const { slots, slotsEvents, slotsLock } = useAppSelector((store) => store.calendar.sidebarEvents)
  const weekEvents = useAppSelector((store) => store.timeSlots.weekEvents)

  const changeCurrentDate = (data: Dayjs) => {
    dispatch(setViewType(CALENDAR_VIEW[0]))
    dispatch(setCurrentDate(data.format(DATE_FORMAT_STRING)))
  }

  return (
    <div className={`${cls.CalendarSidebar} ${open ? cls.open : cls.close}`}>
      {document.body.offsetWidth < 1501 && (
        <Button color={ButtonColors.ICON_GREEN} onClick={() => dispatch(setSidebarState())}>
          <SidebarIcon />
        </Button>
      )}
      <LocalizationProvider adapterLocale="ru" dateAdapter={AdapterDayjs}>
        <DateCalendar
          sx={{
            '& .Mui-selected': {
              bgcolor: '#2dad96 !important',
            },
          }}
          value={dayjs(currentDate)}
          onChange={changeCurrentDate}
        />
      </LocalizationProvider>
      <div className={cls.events}>
        <PM2>События</PM2>
        <div className={cls.eventsItems}>
          <label className={cls.sidebarImputLabelRadioItem} onClick={() => dispatch(setSidebarEventsSlotsEventsState())}>
            <div className={`${cls.sidebarImputLabelRadioItemIcon} ${slotsEvents ? cls.active : ''}`}>&#10003;</div>
            <PR2>Мои встречи</PR2>
          </label>
          <label className={cls.sidebarImputLabelRadioItem} onClick={() => dispatch(setSidebarEventsSlotsLockState())}>
            <div className={`${cls.sidebarImputLabelRadioItemIcon} ${slotsLock ? cls.active : ''}`}>&#10003;</div>
            <PR2>Праздники</PR2>
          </label>
          <label className={cls.sidebarImputLabelRadioItem} onClick={() => dispatch(setSidebarEventsSlotsState())}>
            <div className={`${cls.sidebarImputLabelRadioItemIcon} ${slots ? cls.active : ''}`}>&#10003;</div>
            <PR2>Слоты</PR2>
          </label>
        </div>
      </div>
      <div className={cls.weekEvents}>
        <PM2>Встречи на неделе</PM2>
        <div className={cls.weekEventsItems}>
          {weekEvents?.length
            ? weekEvents?.map((el: TimeEvent) => {
                return (
                  <div key={el.startDate} className={`${cls.weekEventsItem}  `} style={{ background: `${el.color}20` }}>
                    <div className={cls.bgColorView} style={{ background: `${el.color}${OPACITY_VALUE}` }}></div>
                    <div className={cls.meetengsOnWeekItemContent}>
                      {dayjs(el.startDate).format('HH:mm')}-{dayjs(el.endDate).format('HH:mm')} {weekDays[dayjs(el.day).day() - 1]}{' '}
                      <PM2> {el.name}</PM2>
                    </div>
                  </div>
                )
              })
            : 'На этой неделе нет встреч'}
        </div>
      </div>
    </div>
  )
}
