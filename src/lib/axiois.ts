import { removeTockens, setTockens } from '@/store/slices/userSlice/utils'
import { Tokens } from '@/types/user'
import Axios from 'axios'

export const formDataAxios = Axios.create({
  headers: {
    'Content-Type': 'multipart/form-data',
  },
})

export const updateTokenApi = Axios.create({
  baseURL: import.meta.env.VITE_RITY_API,
  headers: {
    'Content-Type': 'application/json',
  },
})
export const api = Axios.create({
  baseURL: import.meta.env.VITE_RITY_API,
  headers: {
    'Content-Type': 'application/json',
  },
})

api.interceptors.request.use((request) => {
  const accessToken = localStorage.getItem(Tokens.ACCESS_TOKEN)

  if (accessToken) {
    request.headers.Authorization = `Bearer ${accessToken}`
  }

  return request
})
api.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config
    if (error.response?.status === 401 && !originalRequest._retry) {
      console.log('ERROR 401')

      originalRequest._retry = true

      try {
        const refreshToken = await localStorage.getItem(Tokens.REFRESH_TOKEN)
        console.log('REFRESH TOKEN', refreshToken)

        const response = await updateTokenApi.post('user-refresh/', {
          refresh: refreshToken,
        })
        console.log('RESPONSE', response)
        const accessToken = response.data.access
        const newRefreshToken = response.data.refresh
        console.log('TOKENS', accessToken, newRefreshToken)
        api.defaults.headers.Authorization = `Bearer ${accessToken}`
        originalRequest.headers.Authorization = `Bearer ${accessToken}`
        setTockens({ access: accessToken, refresh: newRefreshToken })
        return api(originalRequest)
      } catch (error) {
        console.log('ERRORS', error)
        removeTockens()
      }
    }

    return Promise.reject(error)
  }
)
