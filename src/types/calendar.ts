import { Dayjs } from 'dayjs'
//form types
export interface Slot {
  startDate: Dayjs
  endDate: Dayjs
  date: string
  repeat: string
  vacancies: { id: number; name: string }[]
}
export interface SendSlot {
  day: string
  end_time: string
  start_time: string
  vacancies: number[]
}
export interface CalendarFormSlot extends Omit<Slot, 'repeat'> {
  repeatSlot: string
  id?: number
}
export interface CreateSlot {
  interviewType: string

  startDate: Dayjs
  endDate: Dayjs
  date: string
  repeatSlot: string
}
export interface CreateSlotForm extends CreateSlot {
  descriptionInvitation: string
  interviewDuration: Dayjs
}

//calendar ui types
export enum CalendarViewType {
  WEEK = 'Week',
  DAY = 'Day',
}
export interface SlotVacancies {
  id: number
  name: string
}
export type CalendarViewItem = {
  label: string
  value: string
}

export const DATE_FORMAT_STRING = 'YYYY-MM-DD'
export const TIME_FORMAT_STRING = 'HH:mm'

export enum SlotFormOperationTypeEnum {
  CHANGE_CHILDREN = 'changeChildren',
  CHANGE_PARENT = 'changeParent',
  CREATE = 'create',
}
type Recurrence_Types = 'None' | 'daily' | 'weekly' | 'custom' | 'custom_weekly'
export const repeatSlot: { value: Recurrence_Types; label: string }[] = [
  { value: 'None', label: 'Не повторять' },
  { value: 'daily', label: 'Каждую неделю в этот день' },
  { value: 'weekly', label: 'Всю неделю' },
  // { value: 'custom', label: 'Настраиваемое повторение' }, убрали из ui, как вернут, раскомитить
  // { value: 'custom_weekly', label: 'Настраиваемое повторение на несколько недель' },
]

export interface CalendarSlotFormScheduleApi {
  day: string // YYYY-MM-DD
  start_time: string // HH:mm:ss
  end_time: string // HH:mm:ss
  recurrence_type: Recurrence_Types
  recurrence_days: number[] // 1 - 7
  vacancies: number[]
}
export interface CalendarSlotFormSchedule {
  day: string // YYYY-MM-DD
  startTime: Dayjs // HH:mm:ss
  endTime: Dayjs // HH:mm:ss
  recurrenceType: 'None' | 'daily' | 'weekly' | 'custom' | 'custom_weekly'
  recurrenceDays: number[] // 1 - 7
  vacancies: number[]
}
export interface CalendarChildrenSlotFormApi {
  start_time: string
  end_time: string
}
export interface CalendarChildrenSlotForm {
  startTime: Dayjs
  endTime: Dayjs
}

interface Color {
  name: string
  hex_code: string
  color: string
}

export interface ApiEvent {
  id: number
  day: string
  start_time: string
  end_time: string
  vacancy_id: number
  tg_user_id: number | null
  color: Color
  vacancy_name: string
}
export interface GetFreeTimeSlotApi {
  day: string
  start_time: string
  end_time: string
  id: number
}
export interface GetFreeTimeSlot {
  day: string
  startDate: string
  endDate: string
  id: number
  freeSlot: boolean
}
export interface ApiTimeSlotWithEvents {
  id: number
  day: string
  start_time: string
  end_time: string
  color: Color
  events: ApiEvent[]
  vacancies: SlotVacancies[]
  holidays: boolean
  timeslot_schedule: number
}

export interface TimeEvent {
  day: string
  startDate: string
  endDate: string
  color: string
  vacancyId: number
  name: string
}

export interface TimeSlotWithEvents {
  timeSlotId: number
  startDate: string
  endDate: string
  color: string
  lock: boolean
  vacancies: SlotVacancies[]
  events?: TimeEvent[]
  timeslotSchedule: number
  freeSlot: boolean
}
export interface GetSlotsAndFreeSlotsApi {
  free_timeslots: GetFreeTimeSlotApi[]
  timeslots: ApiTimeSlotWithEvents[]
}
export interface GetSlotsAndFreeSlots {
  freeTimeslots: GetFreeTimeSlot[]
  timeslots: TimeSlotWithEvents[]
}
export interface FreeSLot {
  startTime: Dayjs
  endTime: Dayjs
  date: string
  comment: string
}
export interface FreeSLotApi {
  start_time: string
  end_time: string
  day: string
}
export interface RescheduledEventApi {
  id: number
  day: string //YYYY-MM-DD
  start_time: string //HH:mm:ss
  end_time: string
  vacancy: number
}
export interface RescheduledEvent {
  id: number
  day: string //YYYY-MM-DD
  startTime: string //HH:mm:ss
  endTime: string
  vacancy: number
}

export interface RescheduledEventsApi {
  events: RescheduledEventApi[]
}
export interface RescheduledEvents {
  events: RescheduledEvent[]
}
export interface ErrorEventsApi {
  id: number
  day: string //YYYY-MM-DD
  start_time: string //HH:mm:ss
  end_time: string //HH:mm:ss
  vacancy_name: string
  tg_user: string
  new_day: string
  new_start_time: string
  new_end_time: string
  vacancy_id: number
}

export interface CreateFreeTimeSlot {
  date: string
  endTime: Dayjs
  startTime: Dayjs
  user?: string
}
export interface CreateFreeTimeSlotApi {
  day: string
  end_time: string
  start_time: string
  user?: number
}
