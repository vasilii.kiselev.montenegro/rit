export interface ApiQuestion {
  id: number
  body: string
  generated_by_ai: boolean
  previous_question: null | number
  vacancy: number
}
export interface Question {
  id: number
  text: string
  aIgenerated: boolean
  previousQuestion: null | number
  vacancy: number
}
