import { Dayjs } from 'dayjs'
import { CalendarSlotFormSchedule } from './calendar'
import { interiewType } from '@/pages/NewCreateVacancy/components/steps/FourthStep/utils'

export enum Status {
  open = 'OPEN',
  closed = 'CLOSED',
  without = 'WITHOUT',
  active = 'ACTIVE',
}
export interface MiniVacancy {
  name: string
  description: string
  jobRequirements: string
  id: number
  companyId: number
}
export interface Vacancy {
  id: number
  imgCompany: string
  company: string
  name: string
  views: number
  passedTest: number
  registration: number
  status: Status
  link: string
}
export interface VacancyCreate extends MiniVacancy {
  currency: string
  wages: string
  workExperience: string
  workSchedule: string
  placeOfWork: string
  testingMethod?: string
}
export interface CreateSlot {
  interviewType: string

  startDate: Dayjs
  endDate: Dayjs
  date: string
  repeatSlot: string
}
export interface CreateSlotForm extends CreateSlot {
  descriptionInvitation: string
  interviewDuration: number
  // interviewDuration: Dayjs
}
// enum Currency {
//   'RUB',
//   'EUR',
//   'USD',
// }

export interface FullVacancy extends Vacancy, VacancyCreate {
  companyId: number
  interviewType: string
  interviewDuration: number
  // interviewDuration: Dayjs
  descriptionInvitation: string
  // currency: Currency
  selectionChoices?: string
  timeForRepeat?: string
  testingMethod: string
  type: string
}

export interface FourthStepValues extends Omit<CalendarSlotFormSchedule, 'vacancies'> {
  interviewType: (typeof interiewType)[0]
  descriptionInvitation: string
  interviewDuration: number
}
