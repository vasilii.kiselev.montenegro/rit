export interface UserApiForm {
  job_title?: string
  email: string
  first_name: string
  last_name: string
}
export interface UserApi extends UserApiForm {
  username: string
  img?: string
  date_joined?: string
  button_change_password?: string
  last_login?: string
  company_verificated: boolean
  vacancies_count: number
}
export interface UserForm {
  company?: string
  email: string
  firstName: string
  lastName: string
}
export interface User extends UserForm {
  username: string
  img?: string
  dateJoined?: string
  buttonChangePassword?: string
  lastLogin?: string
  companyVerificated?: boolean
  vacanciesCount: number
}

export enum UserStatus {
  UNSET = 'unset',
  LOGIN = 'login',
  LOAD = 'load',
}
export enum Tokens {
  REFRESH_TOKEN = 'refresh',
  ACCESS_TOKEN = 'access',
}
export interface RegisterForm {
  login: string
  email: string
  company: string
  password: string
  consent: boolean
  job_title?: string
  firstName: string
  lastName: string
}
