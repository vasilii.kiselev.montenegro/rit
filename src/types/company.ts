export interface Company {
  id: number
  name: string
  logo: string
  location: string
  domainAddress: string
  description: string
  approvalStatus: boolean
  foundedAt: string
  inn: string
  employeeCount: string
}
export interface SendCompany extends Company {}
export interface FullCompany extends SendCompany {
  numberOfVacancies: number
  interviewsAreScheduled: number
}
export interface CompanyAdditionalFields {
  numberOfVacancies: number
  interviewsAreScheduled: number
}
export interface ApiCompany {
  id: number
  inn: string
  name: string
  logo: File
  description: string
  site: string
  place: string
  employee_count: string
  verificated: boolean
  founded_at: string
  vacansies_count: number
  meetings_count: number
  user: number | null
}
