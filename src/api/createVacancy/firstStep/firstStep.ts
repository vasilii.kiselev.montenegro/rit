import { api } from '@/lib/axiois'

export const sendFirstStep = async (data: number): Promise<number> => {
  try {
    const res = await api.post('vacancy_by_company/', {
      company: data,
    })

    return res.data.id
  } catch (err) {
    console.log(err)
  }
}
