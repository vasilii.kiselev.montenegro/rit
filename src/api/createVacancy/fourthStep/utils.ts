import { FullVacancy } from '@/types/vacancy'
// import { Dayjs } from 'dayjs'

export const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

// const checkDuration = (slot: Dayjs) => {
//   return Number(slot.format('mm'))
// }
export const changeVacancyValuesToApi = (data: FullVacancy) => {
  return {
    name: data.name,
    description: data.description,
    requirements: data.jobRequirements,
    salary: data.wages ? data.wages : null,
    currency: data.currency ? data.currency : null,
    experience: data.workExperience ? data.workExperience : null,
    place_of_work: data.placeOfWork ? data.placeOfWork : null,
    company: data.companyId,
    schedule: data.workSchedule ? data.workSchedule : null,
    time_for_repeat: data.timeForRepeat,
    estimation_type: data.selectionChoices,
    duration_of_interview: data.interviewDuration,
    // duration_of_interview: checkDuration(data.interviewDuration),
    type_of_interview: data.interviewType,
    description_of_the_invitation: data.descriptionInvitation,
  }
}
