import { FourthStepValues, FullVacancy } from '@/types/vacancy'
import { changeVacancyValuesToApi, months } from './utils'
import dayjs from 'dayjs'
import { api } from '@/lib/axiois'
import { CalendarSlotFormScheduleApi } from '@/types/calendar'

export const sendSlotInFourthStep = async (data: FourthStepValues, vacancyId: number) => {
  console.log(data, vacancyId)
  const day = data.day.split(' ')[2]
  const checkMonth = data.day.split(',')[1].trim()
  const month = Number(months.indexOf(checkMonth)) + 1
  const stringMonth = month < 10 ? `0${month}` : month
  const stringDay = day.length === 1 ? `0${day}` : day
  const year = dayjs().year()
  const date = `${year}-${stringMonth}-${stringDay}T00:00`

  const sendData: CalendarSlotFormScheduleApi = {
    // id: 2,
    recurrence_days: [],
    recurrence_type: data.recurrenceType,
    day: dayjs(date).format('YYYY-MM-DD'),
    start_time: data.startTime.format('HH:mm'),
    end_time: data.endTime.format('HH:mm'),
    vacancies: [vacancyId],
  }
  console.log(data, sendData)

  try {
    await api.post(`timeslot-schedule/`, sendData)

    return true
  } catch (err) {
    throw err.response.data.errors[0]
  }
}
export const sendVacancyFourthStep = async (data: FullVacancy) => {
  try {
    const sendData = changeVacancyValuesToApi(data)

    await api.put(`vacancy/${data.id}/`, sendData)
  } catch (err) {
    console.log(err)
  }
}
