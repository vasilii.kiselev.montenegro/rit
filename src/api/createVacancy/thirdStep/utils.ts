import { ApiQuestion, Question } from '@/types/question'
import { FullVacancy } from '@/types/vacancy'

export const changeApiQustionValues = (data: ApiQuestion): Question => {
  return {
    aIgenerated: data.generated_by_ai,
    id: data.id,
    previousQuestion: data.previous_question,
    text: data.body,
    vacancy: data.vacancy,
  }
}
export const changeApiQustionsValues = (data: ApiQuestion[]): Question[] => {
  return data.map((el) => ({
    aIgenerated: el.generated_by_ai,
    id: el.id,
    previousQuestion: el.previous_question,
    text: el.body,
    vacancy: el.vacancy,
  }))
}
export const changeVacancyValuesToApi = (data: FullVacancy) => {
  return {
    name: data.name,
    description: data.description,
    requirements: data.jobRequirements,
    salary: data.wages ? data.wages : null,
    currency: data.currency ? data.currency : null,
    experience: data.workExperience ? data.workExperience : null,
    place_of_work: data.placeOfWork ? data.placeOfWork : null,
    company: data.companyId,
    schedule: data.workSchedule ? data.workSchedule : null,
    time_for_repeat: data.timeForRepeat,
    estimation_type: data.selectionChoices,
  }
}
