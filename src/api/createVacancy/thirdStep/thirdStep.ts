import { api } from '@/lib/axiois'
import { changeApiQustionsValues, changeApiQustionValues, changeVacancyValuesToApi } from './utils'
import { Question } from '@/types/question'
import { FullVacancy } from '@/types/vacancy'

export const getQuestion = async (id: number): Promise<Question> => {
  let counter = 0
  while (true) {
    if (counter > 12) {
      return null
    }
    try {
      const res = await api.get(`questions/last/${id}`)
      if (res.data.body.length) {
        const data = changeApiQustionValues(res.data)
        return data
      }
    } catch (err) {
      console.log(err)
      console.error('The question is not loaded')
    }
    counter++
    await new Promise((resolve) => setTimeout(resolve, 5000))
  }
}
export const createQuestionWithPrevious = async (question: Question, text: string): Promise<Question> => {
  const data = {
    body: text,
    vacancy: question.vacancy,
    generated_by_ai: true,
    previous_question: question.id,
  }
  try {
    const res = await api.post(`questions/create/`, data)
    const responseData = changeApiQustionValues(res.data)
    return responseData
  } catch (err) {
    console.log(err)
    return null
  }
}
export const generateQuestion = async (id: number): Promise<Question> => {
  let counter = 0
  while (true) {
    if (counter > 12) {
      return null
    }
    try {
      await api.get(`generate_question?vacancy_id=${id}`)
      const res = await api.get(`questions/last/${id}`)
      if (res.data.body.length) {
        const data = changeApiQustionValues(res.data)
        return data
      }
    } catch (err) {
      console.log(err)
      console.error('The question is not loaded')
    }
    counter++
    await new Promise((resolve) => setTimeout(resolve, 5000))
  }
}
export const getAllQuestions = async (id: number, mainId: number): Promise<Question[]> => {
  try {
    const res = await api.get(`questions/all_actual/${id}`)

    const responseData = changeApiQustionsValues(res.data)
    const returnData = responseData.filter((el) => el.id !== mainId)
    console.log(returnData)

    return returnData
  } catch (err) {
    console.log(err)
    return
  }
}
export const actualizeQuestion = async (id: number) => {
  try {
    const res = await api.post(`questions/`, { id: id })
    const responseData = changeApiQustionValues(res.data)
    return responseData
  } catch (err) {
    console.log(err)
    // return null
  }
}
export const createQuestionWithoutPrevious = async (question: Question, text: string): Promise<Question> => {
  const data = {
    body: text,
    vacancy: question.vacancy,
    generated_by_ai: false,
    previous_question: null,
  }
  try {
    const res = await api.post(`questions/create/`, data)
    const responseData = changeApiQustionValues(res.data)
    return responseData
  } catch (err) {
    console.log(err)
    return null
  }
}
export const sendVacancyThirdStep = async (data: FullVacancy) => {
  try {
    const sendData = changeVacancyValuesToApi(data)

    await api.put(`vacancy/${data.id}/`, sendData)
  } catch (err) {
    console.log(err)
  }
}
export const deleteQuestion = async (id: number) => {
  try {
    const res = await api.delete(`questions/${id}`)
    const responseData = changeApiQustionValues(res.data)
    return responseData
  } catch (err) {
    console.log(err)
    // return null
  }
}
