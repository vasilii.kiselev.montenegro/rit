import { MiniVacancy, VacancyCreate } from '@/types/vacancy'

export const changeValuesToApi = (data: VacancyCreate) => {
  return {
    name: data.name,
    description: data.description,
    requirements: data.jobRequirements,
    salary: data.wages ? data.wages : null,
    currency: data.currency ? data.currency : null,
    experience: data.workExperience ? data.workExperience : null,
    place_of_work: data.placeOfWork ? data.placeOfWork : null,
    company: data.companyId,
    schedule: data.workSchedule ? data.workSchedule : null,
  }
}

export const changeValuesToMiniApi = (data: MiniVacancy) => {
  return {
    name: data.name,
    description: data.description,
    requirements: data.jobRequirements,
    company: data.companyId,
  }
}
