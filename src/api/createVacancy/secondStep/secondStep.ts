import { api } from '@/lib/axiois'
import { MiniVacancy, VacancyCreate } from '@/types/vacancy'
import { changeValuesToApi, changeValuesToMiniApi } from './utils'
interface Currency {
  code: string
  name: string
}
export interface ShedulesType {
  id: number
  value: string
  description: string
}
export const getCurrencies = async (): Promise<string[]> => {
  try {
    const res = await api.get('vacancy/currencies/')
    const data = await res.data.map((el: Currency) => el.code)
    return data
  } catch (err) {
    console.log(err)
  }
}

export const getShedules = async (): Promise<ShedulesType[]> => {
  try {
    const res = await api.get('schedules/')
    // const data = await res.data.map((el) => el.value)
    console.log('shedules', res.data)

    return res.data
  } catch (err) {
    console.log(err)
  }
}

export const sendSecondStep = async (data: VacancyCreate) => {
  try {
    const sendData = changeValuesToApi(data)

    await api.put(`vacancy/${data.id}/`, sendData)
  } catch (err) {
    console.log(err)
  }
}

export const sendMiniSecondStep = async (data: MiniVacancy) => {
  try {
    const sendData = changeValuesToMiniApi(data)

    await api.put(`vacancy/${data.id}/`, sendData)
  } catch (err) {
    console.log(err)
  }
}
