import { api } from '@/lib/axiois'
import { changeApiValues, changeValuesToApi } from './changeApiValues'
import { UserForm } from '@/types/user'

export const getUser = async (): Promise<UserForm> => {
  try {
    const res = await api.get('user/')

    const data = changeApiValues(res.data)
    return data
  } catch (err) {
    console.log(err)
    Promise.reject(err)
  }
}

export const changeUserValues = async (data: UserForm) => {
  const sendData = changeValuesToApi(data)
  try {
    const res = await api.put('user/', sendData)
    console.log('get user ', res.data)
    // const data = changeApiValues(res.data)
    return true
  } catch (err) {
    console.log(err)
    Promise.reject(err)
  }
}

export const sendRecoveryCode = async (data: {
  loginEmail: string
  code: string
}) => {
  try {
    const res = await api.post(`http://localhost:3001/${data}`)

    return res.data
  } catch (err) {
    console.log(err)
    return { profile: {}, status: 'unset' }
  }
}
type Passwords = {
  password: string
  secondPassword: string
}
interface ErrorObject {
  [key: string]: string[]
}

export const resetPassword = async (
  url: string,
  data: Passwords
): Promise<{ errors: boolean; res?: ErrorObject }> => {
  try {
    const res = await api.post(`${url}`, { new_password: data.password })
    console.log('get user ', res.data)

    return { errors: false }
  } catch (err) {
    console.log(err)
    return { errors: true, res: err.response.data }
  }
}
