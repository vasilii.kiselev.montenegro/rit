import { User, UserApi, UserApiForm, UserForm } from '@/types/user'
import dayjs from 'dayjs'

export const changeApiValues = (data: UserApi): User => {
  return {
    firstName: data.first_name,
    lastName: data.last_name,
    email: data.email,
    username: data.username,
    buttonChangePassword: data.button_change_password,
    company: data.job_title,
    companyVerificated: data.company_verificated,
    dateJoined: dayjs(data.date_joined).format('YYYY-MM-DD'),
    img: '',
    lastLogin: data.last_login,
    vacanciesCount: data.vacancies_count,
  }
}
export const changeValuesToApi = (data: UserForm): UserApiForm => {
  return {
    first_name: data.firstName,
    last_name: data.lastName,
    email: data.email,
    job_title: data.company,
  }
}
