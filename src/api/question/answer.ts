import { api } from '@/lib/axiois'

export const getAnswer = async (
  id: number,
  answer: string
): Promise<{
  estimation: string
  answer: string
}> => {
  try {
    const sendData = { asked: id, body: answer }
    const res = await api.post('answers/', sendData)

    return {
      estimation: res.data.answer_estimation,
      answer: res.data.estimation_description,
    }
  } catch (err) {
    console.log(err)
  }
}
