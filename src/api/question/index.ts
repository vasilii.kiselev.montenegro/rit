export { getAnswer } from './answer'

export { getQuestion, getAllQuestions } from './question'
