import { api } from '@/lib/axiois'

export const getQuestion = async (id: { number: number }): Promise<[string] | [string, number]> => {
  let counter = 0
  while (true) {
    if (counter > 12) {
      return null
    }
    try {
      const res = await api.get(`questions/last/${id.id}`)
      if (res.data.body.length) return [res.data.body, res.data.id]
    } catch (err) {
      console.log(err)
      console.error('The question is not loaded')
    }
    counter++
    await new Promise((resolve) => setTimeout(resolve, 5000))
  }
}
export const getAllQuestions = async (id: { number: number }) => {
  try {
    const res = await api.get(`questions/all/${id.id}`)
    console.log(res)

    return res.data
  } catch (err) {
    console.log(err)
    return 'The question is not loaded'
  }
}
export const generateQuestion = async (id: { number: number }) => {
  try {
    await api.get(`generate_question?vacancy_id=${id.id}`)
    const res = await api.get(`questions/last/${id.id}`)
    console.log(res.data)

    return [res.data.body, res.data.id]
  } catch (err) {
    console.log(err)
    return 'The question is not loaded'
  }
}
export const generateQuestionWhithoutResponse = async (id: { number: number }) => {
  try {
    await api.get(`generate_question?vacancy_id=${id.id}`)
  } catch (err) {
    console.log(err)
    return 'The question is not loaded'
  }
}
export const createQuestion = async (id, text) => {
  const data = {
    body: text,
    is_last: true,
    is_actual: true,
    vacancy: id,
  }
  try {
    const res = await api.post(`questions/create/`, data)
    return res.data.id
  } catch (err) {
    console.log(err)
    return null
  }
}
export const actualizeQuestion = async (id: number) => {
  try {
    await api.post(`questions/`, { id: id })
    // return res.data.id
  } catch (err) {
    console.log(err)
    // return null
  }
}
