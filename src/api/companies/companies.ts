import { changeCompanyToApiValues } from './utils/changeToApiValues'
import { api } from '@/lib/axiois'
import { changeApiValuesForOneItem } from '@/store/slices/companiesSlice/utils/changeApiValues'
import { FullCompany } from '@/types/company'
interface ErrorObject {
  [key: string]: string[]
}
interface ResponseObject {
  [key: string]: any
}

export const getCompanies = async () => {
  try {
    const res = await api.get('company/')
    // console.log(res)

    return res.data
  } catch (err) {
    // console.log(err)
    return []
  }
}
export const addCompany = async (data: FullCompany): Promise<{ errors: boolean; res: ResponseObject | ErrorObject }> => {
  try {
    const changeData = changeCompanyToApiValues(data)
    const logo = changeData.logo
    delete changeData.id
    delete changeData.logo

    const formData = new FormData()

    Object.entries(changeData).forEach((el) => {
      formData.append(el[0], String(el[1]))
    })
    formData.append('logo', logo)

    const res = await api.postForm(`${import.meta.env.VITE_RITY_API}company/`, formData, {
      headers: { 'Content-Type': `multipart/form-data` },
      // headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}` },
    })

    // const newData = await changeApiValues(res)

    console.log(res)

    return { errors: false, res }
  } catch (err) {
    console.log(err.response.data)
    return { errors: true, res: err.response.data }
  }
}
export const changeCompany = async (data: FullCompany): Promise<{ errors: boolean; res: ResponseObject | ErrorObject }> => {
  try {
    const changeData = changeCompanyToApiValues(data)
    const logo = changeData.logo

    delete changeData.logo

    const formData = new FormData()

    Object.entries(changeData).forEach((el) => {
      formData.append(el[0], String(el[1]))
    })
    formData.append('logo', logo)
    const res = await api.putForm(`${import.meta.env.VITE_RITY_API_V1}companies/${changeData.id}/`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })

    console.log(res)

    return { errors: false, res }
  } catch (err) {
    console.log(err)
    return { errors: true, res: err.response.data }
  }
}

export const getCompany = async (id: number): Promise<FullCompany> => {
  try {
    const res = await api.get(`company/${id}`)

    // const company = await res.data.find((el: FullCompany) => el.id === id)
    const changeCompany = await changeApiValuesForOneItem(res.data)
    // const changeCompany = await changeApiValuesForOneItem(company)

    return changeCompany
  } catch (err) {
    // console.log(err)
  }
}

export const uploadImg = async (url: string): Promise<File> => {
  try {
    const res = await api.get(url, {
      responseType: 'blob',
    })

    const file = new File([res.data], 'img', { type: res.data.type })
    return file
  } catch (error) {
    throw new error()
  }
}
