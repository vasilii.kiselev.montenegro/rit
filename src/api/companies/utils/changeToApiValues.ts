import { FullCompany } from '@/types/company'

export const changeCompanyToApiValues = (company: FullCompany) => {
  return {
    id: company.id,
    name: company.name,
    inn: company.inn,
    logo: company.logo,
    description: company.description,
    site: company.domainAddress,
    place: company.location,
    employee_count: company.employeeCount,
    verificated: company.approvalStatus,
    founded_at: company.foundedAt,
  }
}
