import {
  ApiEvent,
  ApiTimeSlotWithEvents,
  CalendarSlotFormSchedule,
  CalendarSlotFormScheduleApi,
  CreateFreeTimeSlot,
  CreateFreeTimeSlotApi,
  ErrorEventsApi,
  FreeSLot,
  FreeSLotApi,
  GetFreeTimeSlot,
  GetFreeTimeSlotApi,
  RescheduledEventApi,
  RescheduledEventsApi,
  TimeEvent,
  TimeSlotWithEvents,
} from '@/types/calendar'
import dayjs from 'dayjs'
import { TIME_ADAPTER } from './utils'

export const changeTimeslotsEventsApiValues = (data: ApiTimeSlotWithEvents[]) => {
  return data.map((el): TimeSlotWithEvents => {
    return {
      timeSlotId: el.id,
      startDate: dayjs(`${el.day}${el.start_time}`).format('YYYY-MM-DDTHH:mm'),
      endDate: dayjs(`${el.day}${el.end_time}`).format('YYYY-MM-DDTHH:mm'),
      color: el?.color?.hex_code,
      events: el?.events?.map((eventEl) => {
        const checkColor = typeof eventEl?.color?.hex_code === 'string'
        return {
          day: eventEl.day,
          startDate: dayjs(`${el.day}${eventEl.start_time}`).format('HH:mm'),
          endDate: dayjs(`${el.day}${eventEl.end_time}`).format('HH:mm'),
          color: checkColor ? eventEl.color?.hex_code : '#' + 'ff00ff',
          vacancyId: eventEl.vacancy_id,
          name: eventEl.vacancy_name,
        }
      }),
      lock: el.holidays,
      vacancies: el.vacancies,
      timeslotSchedule: el.timeslot_schedule,
      freeSlot: false,
    }
  })
}
export const changeEventsApiValues = (data: ApiEvent[]): TimeEvent[] => {
  return data.map((el) => {
    return {
      day: el.day,
      startDate: dayjs(`${el.day}${el?.start_time}`).format('YYYY-MM-DDTHH:mm'),
      endDate: dayjs(`${el.day}${el?.end_time}`).format('YYYY-MM-DDTHH:mm'),
      color: el?.color?.hex_code,
      vacancyId: el.vacancy_id,
      name: el.vacancy_name,
    }
  })
}

export const changeSheduleSlotApiValues = (slot: CalendarSlotFormScheduleApi): CalendarSlotFormSchedule => {
  return {
    day: slot.day,
    endTime: dayjs(`${TIME_ADAPTER}${slot.end_time}`),
    startTime: dayjs(`${TIME_ADAPTER}${slot.start_time}`),
    recurrenceDays: slot.recurrence_days,
    recurrenceType: slot.recurrence_type,
    vacancies: slot.vacancies,
  }
}

export const changeSheduleSlotValuesToApi = (slot: CalendarSlotFormSchedule): CalendarSlotFormScheduleApi => {
  return {
    day: slot.day,
    end_time: slot.endTime.format('HH:mm:ss'),
    start_time: slot.startTime.format('HH:mm:ss'),
    recurrence_days: [],
    recurrence_type: slot.recurrenceType,
    // recurrence_type: slot.recurrenceType,
    //@ts-expect-error
    vacancies: slot.vacancies.map((el) => el.id),
  }
}

export const convertFreeTimeSlotsToApi = (freeSlot: FreeSLot): FreeSLotApi => {
  return {
    day: freeSlot.date,
    end_time: freeSlot.endTime.format('HH:mm:ss'),
    start_time: freeSlot.startTime.format('HH:mm:ss'),
  }
}

export const convertErrorEventsToRescheduledEventsApi = (eventsArr: ErrorEventsApi[]): RescheduledEventsApi => {
  return {
    events: eventsArr.map((el: ErrorEventsApi): RescheduledEventApi => {
      return { day: el.new_day, id: el.id, end_time: el.new_end_time, start_time: el.new_start_time, vacancy: el.vacancy_id }
    }),
  }
}

export const convertCreateFreeTimeSlotToApiValues = (slot: CreateFreeTimeSlot): CreateFreeTimeSlotApi => {
  return {
    day: slot.date,
    end_time: slot.endTime.format('HH:mm:ss'),
    start_time: slot.startTime.format('HH:mm:ss'),
  }
}
export const convertFreeSlotsApiValues = (slots: GetFreeTimeSlotApi[]): GetFreeTimeSlot[] => {
  return slots.map((el) => ({
    day: el.day,
    endDate: dayjs(`${el.day}${el.end_time}`).format('YYYY-MM-DDTHH:mm'),
    id: el.id,
    startDate: dayjs(`${el.day}${el.start_time}`).format('YYYY-MM-DDTHH:mm'),
    freeSlot: true,
  }))
}
