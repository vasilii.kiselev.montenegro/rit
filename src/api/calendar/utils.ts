import { months } from '@/pages/NewCalendarPage/consts'
import dayjs from 'dayjs'

export const getCurrentWeek = (startDate: string, endDate: string) => {
  const thisWeekStartArr = dayjs(startDate).startOf('week').format('DD-MM-YYYY').split('-')

  thisWeekStartArr[0] = String(Number(thisWeekStartArr[0]) + 1)
  thisWeekStartArr[0] = thisWeekStartArr[0].length === 1 ? `0${thisWeekStartArr[0]}` : thisWeekStartArr[0]

  // console.log(thisWeekStartArr)
  const thisWeekEndArr = dayjs(endDate).endOf('week').format('DD-MM-YYYY').split('-')

  thisWeekEndArr[0] = String(Number(thisWeekEndArr[0]) + 1)
  thisWeekEndArr[0] = thisWeekEndArr[0].length === 1 ? `0${thisWeekEndArr[0]}` : thisWeekEndArr[0]

  return [thisWeekStartArr, thisWeekEndArr]
}

// export const convertSlotValuesToApi = (data: Slot, vacancies: Vacancy[]): SendSlot => {
//   // console.log(vacancies)
//   console.log(data)

//   const vacanciesId = data.vacancies.map((el) => {
//     const vacancyId = vacancies.find((elem) => {
//       console.log(elem.name, el)

//       return elem.id === el
//     })?.id
//     console.log(vacancyId)

//     return vacancyId
//   })

//   return {
//     day: data.date,
//     start_time: dayjs(data.startDate).format('HH:mm'),
//     end_time: dayjs(data.endDate).format('HH:mm'),
//     // vacancies: [...vacanciesId],
//     vacancies: data.vacancies,
//   }
// }
export const convertObjInNumberIdArray = (arr: { id: number; name: string }[]): number[] => {
  return arr.map((el) => el.id)
}

export const convertStringToDate = (date: string): string => {
  const day = date.split(' ')[2]
  const checkMonth = date.split(',')[1].trim()
  const month = Number(months.indexOf(checkMonth)) + 1
  const stringMonth = month < 10 ? `0${month}` : month
  const stringDay = day.length === 1 ? `0${day}` : day
  const year = dayjs().year()
  return `${year}-${stringMonth}-${stringDay}T00:00`
}

export const TIME_ADAPTER = '2000-01-01T'
