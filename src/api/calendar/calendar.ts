import { api } from '@/lib/axiois'
import {
  changeEventsApiValues,
  changeSheduleSlotApiValues,
  changeSheduleSlotValuesToApi,
  changeTimeslotsEventsApiValues,
  convertCreateFreeTimeSlotToApiValues,
  convertErrorEventsToRescheduledEventsApi,
  convertFreeSlotsApiValues,
  convertFreeTimeSlotsToApi,
} from './changeCalendarApiValues'
import { convertObjInNumberIdArray, convertStringToDate, getCurrentWeek, TIME_ADAPTER } from './utils'
import {
  CalendarChildrenSlotForm,
  CalendarChildrenSlotFormApi,
  CalendarFormSlot,
  CalendarSlotFormSchedule,
  CalendarSlotFormScheduleApi,
  CreateFreeTimeSlot,
  ErrorEventsApi,
  FreeSLot,
  GetSlotsAndFreeSlots,
  GetSlotsAndFreeSlotsApi,
} from '@/types/calendar'
import dayjs from 'dayjs'
import { AxiosResponse } from 'axios'

export const getSlots = async (): Promise<GetSlotsAndFreeSlots> => {
  try {
    const res: AxiosResponse<GetSlotsAndFreeSlotsApi> = await api.get('timeslots-events/?from=2024-01-01&to=2026-12-30')
    const slots = await changeTimeslotsEventsApiValues(res.data.timeslots)
    const freeSlots = await convertFreeSlotsApiValues(res.data.free_timeslots)

    return { freeTimeslots: freeSlots, timeslots: slots }
  } catch (err) {
    console.log(err)
    throw new err()
  }
}

export const getWeekEvents = async (startDate: string, endDate: string) => {
  const [[thisWeekStartDay, thisWeekStartWeek, thisWeekStartYear], [thisWeekEndDay, thisWeekEndWeek, thisWeekEndYear]] = getCurrentWeek(
    startDate,
    endDate
  )

  try {
    const res = await api.get(
      `events/?day__gte=${thisWeekStartYear}-${thisWeekStartWeek}-${thisWeekStartDay}&day__lte=${thisWeekEndYear}-${thisWeekEndWeek}-${thisWeekEndDay}`
      // `https://rity.app/api/v2/events/?day__gte=${thisWeekStartYear}-${thisWeekStartWeek}-${thisWeekStartDay}&day__lte=${thisWeekEndYear}-${thisWeekEndWeek}-${thisWeekEndDay}`
    )
    const data = await changeEventsApiValues(res.data)

    // console.log(res)

    return data
  } catch (err) {
    // console.log(err)
    return []
  }
}
interface ErrorObject {
  [key: string]: string[]
}
export const addSlot = async (data: CalendarFormSlot): Promise<{ errors: boolean; res?: ErrorObject }> => {
  const date = convertStringToDate(data.date)

  const sendData = {
    day: dayjs(date).format('YYYY-MM-DD'),
    start_time: data.startDate.format('HH:mm'),
    end_time: data.endDate.format('HH:mm'),
    vacancies: convertObjInNumberIdArray(data.vacancies),
  }

  try {
    await api.post(`timeslot/`, sendData)

    return { errors: false }
  } catch (err) {
    return { errors: true, res: err.response.data }
  }
}
export const editSlot = async (data: CalendarFormSlot, id: number): Promise<{ errors: boolean; res?: ErrorObject }> => {
  const date = convertStringToDate(data.date)
  console.log(data)

  const sendData = {
    day: dayjs(date).format('YYYY-MM-DD'),
    start_time: data.startDate.format('HH:mm'),
    end_time: data.endDate.format('HH:mm'),
    vacancies: convertObjInNumberIdArray(data.vacancies),
  }

  try {
    await api.put(`timeslot/${id}`, sendData)

    return { errors: false }
  } catch (err) {
    return { errors: true, res: err.response.data }
  }
}
export const getSlotsForToolbar = async (startDate: string, endDate: string) => {
  try {
    const res = await api.get(`timeslots-events/?from=${startDate}&to=${endDate}`)
    const data = await changeTimeslotsEventsApiValues(res.data.timeslots)

    // console.log(data)

    return data
  } catch (err) {
    console.log(err)
    return []
  }
}

export const getScheduleSlot = async (sheduleTimeslot: number): Promise<CalendarSlotFormSchedule> => {
  try {
    const res: AxiosResponse<CalendarSlotFormScheduleApi> = await api.get(`timeslot-schedule/${sheduleTimeslot}`)

    return changeSheduleSlotApiValues(res.data)
  } catch (error) {
    throw new error()
  }
}
export const getChildrenSlot = async (timeslot: number): Promise<CalendarChildrenSlotForm> => {
  try {
    const res: AxiosResponse<CalendarChildrenSlotFormApi> = await api.get(`timeslot/${timeslot}`)

    return { startTime: dayjs(`${TIME_ADAPTER}${res.data.start_time}`), endTime: dayjs(`${TIME_ADAPTER}${res.data.end_time}`) }
  } catch (error) {
    throw new error()
  }
}
export const changeScheduleSlot = async (slot: CalendarSlotFormSchedule, id: number) => {
  const sendData = changeSheduleSlotValuesToApi(slot)
  try {
    const res = await api.put(`timeslot-schedule/${id}/`, sendData)
    return res.request
  } catch (error) {
    throw error.response.data.errors[0]
  }
}
export const changeChildrenSlot = async (slot: CalendarChildrenSlotForm, id: number) => {
  const sendData: CalendarChildrenSlotFormApi = {
    end_time: slot.endTime.format('HH:mm:ss'),
    start_time: slot.startTime.format('HH:mm:ss'),
  }
  try {
    await api.put(`timeslot/${id}/`, sendData)
    return true
  } catch (error) {
    throw error.response.data.errors[0]
  }
}
export const createcheduleSlot = async (slot: CalendarSlotFormSchedule) => {
  const sendData = changeSheduleSlotValuesToApi(slot)
  try {
    await api.post(`timeslot-schedule/`, sendData)
    return true
  } catch (error) {
    throw error.response.data.errors[0]
  }
}
export const deleteScheduleSlot = async (id: number) => {
  try {
    await api.delete(`timeslot-schedule/${id}/`)
    return true
  } catch {
    throw new Error()
  }
}
export const deleteChildrenSlot = async (id: number) => {
  try {
    await api.delete(`timeslot/${id}/`)
    return true
  } catch {
    throw new Error()
  }
}

export const checkFreeUpTime = async (data: FreeSLot) => {
  const sendData = convertFreeTimeSlotsToApi(data)
  const [sendStartTimeHour, sendStartTimeMinute] = sendData.start_time.split(':')
  const [sendEndTimeHour, sendEndTimeMinute] = sendData.end_time.split(':')
  console.log(data, sendData)

  try {
    const check = await api.get(
      `/free_timeslot/events/?day=${sendData.day}&start_time=${sendStartTimeHour}%3A${sendStartTimeMinute}%3A00&end_time=${sendEndTimeHour}%3A${sendEndTimeMinute}%3A00`
    )
    console.log(check)

    // if (!check.data.length) {
    //   const res = await api.post('', sendData)
    //   return res.data
    // } else {
    //   return check.data
    // }
    return check.data
  } catch (error) {
    throw new error()
  }
}
export const rescheduleEvents = async (eventsArray: ErrorEventsApi[]) => {
  try {
    const sendData = convertErrorEventsToRescheduledEventsApi(eventsArray)
    await api.post('/free_timeslot/events/create/', sendData)
    return true
  } catch (error) {
    throw new error()
  }
}
export const createFreeTimeSlot = async (slot: CreateFreeTimeSlot) => {
  try {
    const sendData = convertCreateFreeTimeSlotToApiValues(slot)
    await api.post('free_timeslot/', sendData)
    return true
  } catch (error) {
    throw new error()
  }
}
