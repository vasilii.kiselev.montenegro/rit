import { api } from '@/lib/axiois'
import { TokensType } from '@/store/slices/userSlice/utils'
import { RegisterForm } from '@/types/user'

export const apiSignIn = async (data: RegisterForm) => {
  try {
    const res = await api.post('register/', {
      username: data.login,
      password: data.password,
      email: data.email,
      job_title: data.job_title,
      first_name: data.firstName,
      last_name: data.lastName,
    })
    console.log(res.data)
    return { error: false, data: res.data }
  } catch (err) {
    throw err.response.data
  }
}
export const apiLogIn = async (data: { loginEmail: string; password: string }): Promise<TokensType> => {
  try {
    const res = await api.post('login/', {
      username: data.loginEmail,
      password: data.password,
    })
    console.log(res.data)
    return res.data
  } catch (err) {
    console.log('error')
    throw err.response.data
  }
}

