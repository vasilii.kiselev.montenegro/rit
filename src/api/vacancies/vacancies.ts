import { api } from '@/lib/axiois'
import { changeApiValues } from '@/store/slices/vacanciesSlice/utils/changeApiValues'
import { FullVacancy } from '@/types/vacancy'

export const getVacancies = async () => {
  try {
    const res = await api.get(`${import.meta.env.VITE_RITY_API_V1}vacancies/`)

    return res.data
  } catch (err) {
    console.log(err)
    return []
  }
}
export const getVacany = async (id: number | string): Promise<FullVacancy> => {
  try {
    const res = await api.get(`${import.meta.env.VITE_RITY_API_V1}vacancies/${id}`)
    const responseData = await changeApiValues(res.data)
    return responseData[0]
  } catch (err) {
    console.log(err)
    return null
  }
}
