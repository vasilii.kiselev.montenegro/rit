import { getVacanciesThunk } from '@/store/slices/vacanciesSlice'
import { useAppDispatch } from '../useAppDispatch'
import { getCompaniesThunk } from '@/store/slices/companiesSlice'
import { getSlotsEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getSlotsEventsThunk'
// import { getWeekEventsThunk } from '@/store/slices/timeSlotsSlice/asyncThunks/getWeekEvents'

export const useAsyncThunks = () => {
  const dispatch = useAppDispatch()

  dispatch(getVacanciesThunk())
  dispatch(getCompaniesThunk())
  dispatch(getSlotsEventsThunk())
  // dispatch(getWeekEventsThunk())
}
