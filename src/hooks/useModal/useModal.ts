import { useState } from 'react'

export const useModal = () => {
  const [modal, setModal] = useState(false)
  const [modalContent, setModalContent] = useState(null)

  const handleModal = (content = false) => {
    setModal(!modal)
    if (!content === false) {
      setModalContent(content)
    }
  }

  return { modal, handleModal, modalContent }
}
