import {
  logIn,
  setStatusLogIn,
  logOut as userSliceLogOut,
} from '@/store/slices/userSlice/userSlice'
import { useAppDispatch } from '../useAppDispatch'
import { Tokens } from '@/types/user'

export const useAuth = () => {
  const dispatch = useAppDispatch()

  const thisLogIn = (tokens) => {
    console.log('login', tokens)

    localStorage.setItem(Tokens.REFRESH_TOKEN, tokens.refresh)
    localStorage.setItem(Tokens.ACCESS_TOKEN, tokens.access)
    dispatch(logIn())
  }
  const thisLogInSession = (tokens) => {
    localStorage.setItem(Tokens.REFRESH_TOKEN, tokens.refresh)
    localStorage.setItem(Tokens.ACCESS_TOKEN, tokens.access)
    dispatch(setStatusLogIn()) //методы будут различаться, добавлением параметров юзера в первом
  }
  const thisLogOut = () => {
    localStorage.removeItem(Tokens.REFRESH_TOKEN)
    localStorage.removeItem(Tokens.ACCESS_TOKEN)
    dispatch(userSliceLogOut())
  }
  return {
    logOut: thisLogOut,
    logIn: thisLogIn,
    logInSession: thisLogInSession,
  }
}
// const logIn = (tokens) => {
//   console.log('login', tokens)

//   localStorage.setItem(Tokens.REFRESH_TOKEN, tokens.refresh)
//   localStorage.setItem(Tokens.ACCESS_TOKEN, tokens.access)
//   dispatch(logIn())
// }
export const logInSession = (tokens) => {
  console.log('login session', tokens)

  localStorage.setItem(Tokens.REFRESH_TOKEN, tokens.newRefreshToken)
  localStorage.setItem(Tokens.ACCESS_TOKEN, tokens.accessToken)
  // dispatch(logIn()) //методы будут различаться, добавлением параметров юзера в первом
}
export const logOut = () => {
  console.log('logout session')

  localStorage.removeItem(Tokens.REFRESH_TOKEN)
  localStorage.removeItem(Tokens.ACCESS_TOKEN)
}
