import { notify } from '@/UI/Toast'
import { useState } from 'react'

type CopiedValue = string | null
type CopyFn = (text: string) => Promise<void>

export function useCopyToClipboard(): { CopiedValue: CopiedValue; copy: CopyFn } {
  const [copiedText, setCopiedText] = useState<CopiedValue>()

  const copy: CopyFn = async (text) => {
    try {
      await navigator.clipboard.writeText(text)
      setCopiedText(text)
      notify('Ссылка скопирована')
    } catch (error) {
      console.warn(error)
      setCopiedText(null)
    }
  }
  return { CopiedValue: copiedText, copy }
}
