import { useState } from 'react'

export const useDrawer = () => {
  const [drawer, setDrawer] = useState(false)
  const [drawerContent, setDrawerContent] = useState(null)
  const [drawerHeader, setDrawerHeader] = useState(null)

  const handleDrawer = (content = false, header = false) => {
    if (!content === false) {
      setDrawerContent(content)
      setDrawerHeader(header)
    }
    setDrawer(!drawer)
  }

  return { drawer, handleDrawer, drawerContent, drawerHeader }
}
