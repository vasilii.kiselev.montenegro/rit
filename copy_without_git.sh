#!/bin/bash

# Проверяем, что все аргументы переданы
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 source_directory destination_directory"
  exit 1
fi

SOURCE_DIR="$1"
DEST_DIR="$2"

# Проверяем, существует ли исходная директория
if [ ! -d "$SOURCE_DIR" ]; then
  echo "Source directory does not exist: $SOURCE_DIR"
  exit 1
fi

# Создаем целевую директорию, если она не существует
mkdir -p "$DEST_DIR"

# Копируем содержимое из исходной директории в целевую, исключая .git
rsync -av --exclude='.git' "$SOURCE_DIR/" "$DEST_DIR/"

echo "Files copied from $SOURCE_DIR to $DEST_DIR excluding .git"
