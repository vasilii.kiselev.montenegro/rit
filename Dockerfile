FROM node:current-alpine
WORKDIR /app
COPY package.json /app/package.json
RUN npm install --force
COPY . /app

CMD ["npm", "start"]
