#!/bin/bash

# Переменные
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
SRC_DIR=$(basename "$PWD")
DEST_DIR="../${SRC_DIR}_${TIMESTAMP}"
BASE_DIR2="../frontend"
# Получение списка измененных файлов
CHANGED_FILES=$(git status --porcelain | awk '{print $2}')
echo "!!!!!!!!!!!!!!!!!!!!!!"
echo "$CHANGED_FILES"

# Создание директории назначения
mkdir -p "$DEST_DIR"

# Копирование файлов и директорий, кроме .git
rsync -av --exclude='.git' ./ "$DEST_DIR"

# Проверка наличия файла tmp-deploy.info
if [ ! -f "tmp-deploy.info" ]; then
    echo "Файл tmp-deploy.info не найден!"
    exit 1
fi

# Извлечение переменных из файла tmp-deploy.info
DEPLOY_URL=$(grep '^deploy_url=' tmp-deploy.info | cut -d '=' -f 2-)
login=$(grep '^login=' tmp-deploy.info | cut -d '=' -f 2-)
pass=$(grep '^pass=' tmp-deploy.info | cut -d '=' -f 2-)

# Проверка наличия переменной deploy_url
if [ -z "$DEPLOY_URL" ]; then
    echo "Переменная deploy_url не найдена в файле tmp-deploy.info!"
    exit 1
fi

# Переход в директорию назначения
cd "$DEST_DIR" || exit
# Инициализация нового git репозитория
git init
git remote add origin "$DEPLOY_URL"

echo "tmp-deploy.info" >> .gitignore
# Добавление всех файлов и коммит
git add .
CHANGED_FILES2=$(git status --porcelain | awk '{print $2}')
echo "2222222222222222222222222222"
echo "$CHANGED_FILES2"
echo "2222222222222222222222222222"
git commit -m "Deployment on $TIMESTAMP"

# Добавление измененных файлов и коммит
for file in $CHANGED_FILES; do
    git add "$file"
    echo "Удалена старая директория: $file"
done

# Коммит только измененных файлов
git commit -m "Push only changed files on $TIMESTAMP"

# Создание временного файла для GIT_ASKPASS
GIT_ASKPASS=$(mktemp)
chmod +x "$GIT_ASKPASS"
cat << EOF > "$GIT_ASKPASS"
#!/bin/bash
case "\$1" in
    Username*) echo "$login" ;;
    Password*) echo "$pass" ;;
esac
EOF

# Пуш в удаленный репозиторий с использованием логина и пароля
GIT_ASKPASS="$GIT_ASKPASS" git push -u origin master
PUSH_RESULT=$?

if [ $PUSH_RESULT -ne 0 ]; then
    echo "Пуш не удался, пытаемся выполнить git pull --rebase"

    # Выполнение git pull с ребейзом с указанием стратегии ours
    GIT_ASKPASS="$GIT_ASKPASS" git pull --rebase -X ours origin master
    if [ $? -ne 0 ]; then
        echo "Не удалось выполнить git pull --rebase! Устраняем конфликты в нашу пользу."

        # # Вручную разрешаем все конфликты в пользу наших изменений
        # git rebase --abort
        # GIT_ASKPASS="$GIT_ASKPASS" git pull -s recursive -X ours origin master
        # if [ $? -ne 0 ]; then
        #     echo "Не удалось устранить конфликты!"
        #     exit 1
        # fi
    fi


    cd "$BASE_DIR2" || exit
    for file in $CHANGED_FILES2; do
        cp "$file" "$DEST_DIR/$file"
        echo "!!!!!!!!!!!!!Скопирован файл: $file в $DEST_DIR"
    done
    cd "$DEST_DIR" || exit
    for file in $CHANGED_FILES2; do
        git add "$file"
        echo "!!!!!!!!!add файл: $file"
    done
    git commit -m '123'
    # Повторная попытка пуша
    GIT_ASKPASS="$GIT_ASKPASS" git push -u origin master
    if [ $? -ne 0 ]; then
        echo "Пуш после git pull --rebase не удался!"
        exit 1
    fi
fi

echo "Deployment completed successfully!"

# Удаление временного файла GIT_ASKPASS
rm -f "$GIT_ASKPASS"

# Удаление старых директорий frontend_
cd ..
FRONTEND_DIRS=$(ls -dt frontend_* | tail -n +3)

for dir in $FRONTEND_DIRS; do
    rm -rf "$dir"
    echo "Удалена старая директория: $dir"
done
